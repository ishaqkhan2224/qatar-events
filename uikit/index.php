<?php
$filename = 'index.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex,follow">
    <title>CalChoice UI Framework</title>
    <meta name="description" content="CalChoice UI Framework">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato|Open+Sans|Roboto+Mono&display=swap">
    <link rel="stylesheet" id="fontawesome-css" href="../global/css/fontawesome.5.8.2.css?ver=4.7.0" media="all">
    <link rel="stylesheet" id="bootstrap-css" href="../global/css/bootstrap.4.3.1.min.css?ver=4.0.0" media="all">
    <link rel="stylesheet" id="prismStyle-css" href="../global/css/prism.css?ver=1.0.0" media="all">
    <link rel="stylesheet" id="mainStyle-css" href="./scss/uikit-styles.css?ver=1.0" media="all">
</head>

<body class="home">

    <!-- wrapper -->
    <div class="wrapper">

        <!-- header -->
        <header class="main-header">
            <div class="container">
                <div class="header-inner">
                    <a href="/" class="brand">
                        <img class="img-fluid" src="../global/img/calchoicelogo.png">
                    </a>
                    <div class="version-info text-right">
                        <h6>Version 1.6.1</h6>
                        <p><?php echo "Last Updated: " . date ("M d, Y", filemtime($filename)); ?></p>
                    </div>
                </div>
            </div>
        </header>
        <!-- /header -->
        <section class="top-section">
            <div class="container">
                <h1>CalChoice UI Framework</h1>
                <p>Living document created to maintain a modular front-end code and visual consistency across multiple platforms.</p>
            </div>
        </section>

        <section class="tabs-section">
            <div class="container">
              <div class="row">
                  <div class="col">
                      <ul class="nav nav-tabs" id="componentTab" role="tablist">
                        <!-- 
                        <li class="nav-item"><a class="nav-link" id="brand-design-tab" data-toggle="tab" href="#brand-design" role="tab">Brand Design</a></li>
                        -->
                        <li class="nav-item"><a class="nav-link active show" id="colors-tab" data-toggle="tab" href="#colors" role="tab">Colors</a></li>
                        <li class="nav-item"><a class="nav-link" id="navigation-tab" data-toggle="tab" href="#navigation" role="tab">Navigation</a></li>
                        <li class="nav-item"><a class="nav-link" id="cards-tab" data-toggle="tab" href="#cards" role="tab">Cards</a></li>
                        <li class="nav-item"><a class="nav-link" id="forms-tab" data-toggle="tab" href="#forms" role="tab">Forms</a></li>
                        <li class="nav-item"><a class="nav-link" id="buttons-tab" data-toggle="tab" href="#buttons" role="tab">Buttons</a></li>
                        <li class="nav-item"><a class="nav-link" id="tables-tab" data-toggle="tab" href="#tables" role="tab">Tables</a></li>
                        <li class="nav-item"><a class="nav-link" id="confirmations-tab" data-toggle="tab" href="#confirmations" role="tab">Confirmations</a></li>
                        <li class="nav-item"><a class="nav-link" id="utilities-tab" data-toggle="tab" href="#utilities" role="tab">Utilities</a></li>
                        <li class="nav-item"><a class="nav-link" id="public-tab" data-toggle="tab" href="#public" role="tab">Public (Kentico CMS)</a></li>
                      </ul>
                  </div>
              </div>
                
              <div class="tab-content" id="componentTabContent">
                    <!-- <div class="tab-pane fade" id="brand-design" role="tabpanel" aria-labelledby="brand-design-tab">
                        <?php include "inc/brand-design.php"; ?>
                    </div> -->
                    <div class="tab-pane fade active show" id="colors" role="tabpanel" aria-labelledby="colors-tab">
                        <?php include "inc/colors.php"; ?>
                    </div>
                    <div class="tab-pane fade" id="navigation" role="tabpanel" aria-labelledby="navigation-tab">
                        <?php include "inc/navigation.php"; ?>
                    </div>
                    <div class="tab-pane fade" id="cards" role="tabpanel" aria-labelledby="cards-tab">
                        <?php include "inc/cards.php"; ?>
                    </div>
                    <div class="tab-pane fade" id="forms" role="tabpanel" aria-labelledby="forms-tab">
                        <?php include "inc/forms.php"; ?>
                    </div>
                    <div class="tab-pane fade" id="buttons" role="tabpanel" aria-labelledby="buttons-tab">
                        <?php include "inc/buttons.php"; ?>
                    </div>
                    <div class="tab-pane" id="tables" role="tabpanel" aria-labelledby="tables-tab">
                        <?php include "inc/tables.php"; ?>
                    </div>
                    <div class="tab-pane fade" id="confirmations" role="tabpanel" aria-labelledby="confirmations-tab">
                        <?php include "inc/confirmations.php"; ?>
                    </div>
                    <div class="tab-pane fade" id="utilities" role="tabpanel" aria-labelledby="utilities-tab">
                        <?php include "inc/utilities.php"; ?>
                    </div>
                    <div class="tab-pane fade" id="public" role="tabpanel" aria-labelledby="public-tab">
                        <?php include "inc/public.php"; ?>
                    </div>
                </div>
            </div>
        </section>


       
    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="./js/lib/jquery.2.1.4.min.js?ver=4.0.0"></script>
    <script type="text/javascript" src="./js/lib/bootstrap.bundle.min.js?ver=4.0.0"></script>
    <script type="text/javascript" src="./js/lib/prism.js?ver=1.0.0"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
    <script type="text/javascript" src="./js/scripts.js?ver=1.0.0"></script>
 
</body>

</html>