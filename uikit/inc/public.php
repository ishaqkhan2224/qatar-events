<h1>HERO Section</h1>
<div class="component-thumbnail">
    <img src="./img/components/public/hero.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;section class=&#x22;cc-content-template-hero cc-hero-{page-name}&#x22;&#x3E;&#x3C;/section&#x3E;</code></pre>
    <p>Attach the class name <span class="code">cc-hero-{page-name}</span> to the <span class="code">cc-content-template-hero</span> class to customize the banner image</p>
</div>
<hr class="break">
<h1>Page Title</h1>
<div class="component-thumbnail">
    <img src="./img/components/public/page-title.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;section class=&#x22;cc-content-template-outer-wrapper bg-white&#x22;&#x3E;
    &#x3C;div class=&#x22;container&#x22;&#x3E;
        &#x3C;h1&#x22;&#x3E;{Page Title}&#x3C;/h1&#x3E;
        &#x3C;p class=&#x22;text-center&#x22;&#x3E;Aqui volorum, occabo. Ut eiciae laboritatur am eum quatemp erectis erum et eum solestibus.&#x3C;/p&#x3E;
    &#x3C;/div&#x3E;
&#x3C;/section&#x3E;</code></pre>
</div>
<hr class="break">
<h1>Quote Pull</h1>
<div class="component-thumbnail">
    <img src="./img/components/public/quote-pull.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;cc-quote-bg&#x22;&#x3E;
    &#x3C;p&#x3E;{Quote Goes Here}&#x3C;/p&#x3E;
&#x3C;/div&#x3E;</code></pre>
</div>
<hr class="break">
<h1>White Section w/ Grey Outline Cards</h1>
<div class="component-thumbnail">
    <img src="./img/components/public/white-section.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;section class=&#x22;cc-content-template-outer-wrapper bg-white&#x22;&#x3E;
    &#x3C;div class=&#x22;container&#x22;&#x3E;
        &#x3C;h2&#x3E;Section Title&#x3C;/h2&#x3E;
        &#x3C;p class=&#x22;text-center&#x22;&#x3E;Section paragraph goes here&#x3C;/p&#x3E;
        &#x3C;div class=&#x22;row mt-5&#x22;&#x3E;
            &#x3C;div class=&#x22;col-md-6&#x22;&#x3E;
                &#x3C;div class=&#x22;cc-bordered-box&#x22;&#x3E;
                    &#x3C;div class=&#x22;media&#x22;&#x3E;
                      &#x3C;img src=&#x22;{-- image source --}&#x22; class=&#x22;align-self-center mr-3&#x22;&#x3E;
                      &#x3C;div class=&#x22;media-body&#x22;&#x3E;
                        &#x3C;h5 class=&#x22;mt-0&#x22;&#x3E;Card Title&#x3C;/h5&#x3E;
                        &#x3C;p class=&#x22;mb-0&#x22;&#x3E;Card copy. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti, temporibus, odio.&#x3C;/p&#x3E;
                      &#x3C;/div&#x3E;
                    &#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/div&#x3E;
            &#x3C;div class=&#x22;col-md-6&#x22;&#x3E;
                &#x3C;div class=&#x22;cc-bordered-box&#x22;&#x3E;
                    &#x3C;div class=&#x22;media&#x22;&#x3E;
                      &#x3C;img src=&#x22;{-- image source --}&#x22; class=&#x22;align-self-center mr-3&#x22;&#x3E;
                      &#x3C;div class=&#x22;media-body&#x22;&#x3E;
                        &#x3C;h5 class=&#x22;mt-0&#x22;&#x3E;Card Title&#x3C;/h5&#x3E;
                        &#x3C;p class=&#x22;mb-0&#x22;&#x3E;Card Copy. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti, temporibus, odio.&#x3C;/p&#x3E;
                      &#x3C;/div&#x3E;
                    &#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
    &#x3C;/div&#x3E;
&#x3C;/section&#x3E;</code></pre>
</div>

<hr class="break">
<h1>Grey Section w/ White Cards</h1>
<div class="component-thumbnail">
    <img src="./img/components/public/grey-section.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;section class=&#x22;cc-content-template-outer-wrapper&#x22;&#x3E;
    &#x3C;div class=&#x22;container&#x22;&#x3E;
        &#x3C;h2&#x22;&#x3E;Section Title&#x3C;/h2&#x3E;
        &#x3C;div class=&#x22;row&#x22;&#x3E;
            &#x3C;div class=&#x22;col-md-6&#x22;&#x3E;
                &#x3C;div class=&#x22;cc-white-box&#x22;&#x3E;
                    &#x3C;h5&#x3E;Card Title&#x3C;/h5&#x3E;
                    &#x3C;p&#x3E;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti, temporibus, odio.&#x3C;/p&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/div&#x3E;
            &#x3C;div class=&#x22;col-md-6&#x22;&#x3E;
                &#x3C;div class=&#x22;cc-white-box&#x22;&#x3E;
                    &#x3C;h5&#x3E;Card Title&#x3C;/h5&#x3E;
                    &#x3C;p&#x3E;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti, temporibus, odio.&#x3C;/p&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
    &#x3C;/div&#x3E;
&#x3C;/section&#x3E;</code></pre>
</div>
