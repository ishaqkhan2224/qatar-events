<h1>Table Cell Alignments</h1>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col" width="33%" style="background-color: #99999985;">Left Aligned</th>
      <th scope="col" width="34%" class="text-center" style="background-color: #9c9c9c;">Center Aligned</th>
      <th scope="col" width="33%" class="text-right" style="background-color: #99999985;">Right Aligned</th>
    </tr>
  </thead>
  <tbody class="small-body">
    <tr>
      <td>
          <ul>
              <li>Default alignment including all table header (TH) columns</li>
              <li>Any and all data types in the first column will always be left-aligned.</li>
              <li>All left-aligned data must have a corresponding left-aligned table header (TH).</li>
              <li>Any numerical data entry containing counting or incremental numbers must be left-aligned. This includes:
                <ul>
                    <li>ID number</li>
                    <li>Number of employees</li>
                    <li>Percentage</li>
                </ul>
              </li>
          </ul>
      </td>
      <td>
          <ul>
              <li>Data types that have consistent width/character counts must always be center-aligned. This includes:
                  <ul>
                    <li>Effective Date (Character count will always be 10 characters)</li>
                    <li>Age (Character count will almost always be 2 characters)</li>
                    <li>Gender (Character count will always be 1 character)</li>
                </ul>
              </li>
              <li>All dates entered into the table must follow the following formats:
                  <ul>
                    <li>mm/dd/yyyy</li>
                    <li>Apr. 2019</li>
                    <li>mm/yyyy</li>
                </ul>
              </li>
              <li>Any remaining numerical data that is entered must be center aligned. This includes:
                  <ul>
                    <li>Age</li>
                    <li>Phone Number</li>
                </ul>
              </li>
              <li>Any center-aligned table data that is entered must have a corresponding center-aligned table header (TH) label.</li>
          </ul>
      </td>
      <td>
          <ul>
              <li>Any data entered to record currency must be right-aligned.</li>
              <li>Any right-aligned table data that is entered must have a corresponding right-aligned table header (TH) label.</li>
          </ul>
      </td>
    </tr>
  </tbody>
</table>
<h1>Empty State</h1>
<div class="component-thumbnail">
    <img src="./img/components/tables/empty.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;table class=&#x22;table&#x22; id=&#x22;cc-table-data&#x22;&#x3E;
    &#x3C;thead&#x3E;
        &#x3C;tr&#x3E;
            &#x3C;th scope=&#x22;col&#x22;&#x3E;Column 01&#x3C;/th&#x3E;
            &#x3C;th scope=&#x22;col&#x22; class=&#x22;text-center&#x22;&#x3E;Column 02&#x3C;/th&#x3E;
            &#x3C;th scope=&#x22;col&#x22; class=&#x22;text-center&#x22;&#x3E;Column 03&#x3C;/th&#x3E;
            &#x3C;th scope=&#x22;col&#x22; class=&#x22;text-center&#x22;&#x3E;Column 04&#x3C;/th&#x3E;
        &#x3C;/tr&#x3E;
    &#x3C;/thead&#x3E;
    &#x3C;tbody&#x3E;
        &#x3C;tr&#x3E;
            &#x3C;td colspan=&#x22;4&#x22;&#x3E;
                &#x3C;div class=&#x22;empty-state&#x22;&#x3E;
                    &#x3C;h3&#x3E;There are no records&#x3C;/h3&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/td&#x3E;
        &#x3C;/tr&#x3E;
    &#x3C;/tbody&#x3E;
&#x3C;/table&#x3E;
</code></pre>
</div>
<hr class="break">
<h1>Standard Table w/ Filters &amp; Actions Dropdown</h1>
<p>Always use the name <span class="code">cc-table-data__</span> as a prefix for the table <span class="code">&#x3C;ID&#x3E;</span> (e.g. cc-table-data__employees)</p>
<div class="component-thumbnail devnote-wrapper">
    <img src="./img/components/tables/basic-row-search.png">
    <div class="devnote" style="left: 360px; top: 8px;">
      <p>Apply <span>.cc-form-inline-form</span> class to the <span>&#x3C;form&#x3E;</span> element</p>
    </div>
    <div class="devnote large" style="left: 24px; top: 100px;">
      <p>Use <span>data-href</span> element for the URI</p>
      <p>Apply <span>.cc-table__clickable</span> class to the <span>&#x3C;tbody&#x3E;</span> element to implement row hightlight for clickable row</p>
      <p>Apply <span>.cc-table__clickable-row</span> class to the <span>&#x3C;tr&#x3E;</span> element</p>
      <p>If necessary, use <span>.js-clickable-row</span> as a jQuery selector</p>
    </div>
    <div class="devnote" style="left: 760px; top: 124px;">
      <p>Apply <span>.cc-table__td-action</span> class to the <span>&#x3C;td&#x3E;</span> element to enable dropdown menu</p>
      <p>If necessary, use <span>.js-td-action</span> as a jQuery selector</p>
    </div>
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;form id=&#x22;cc-form__manual-search&#x22; method=&#x22;post&#x22; action=&#x22;&#x22; class=&#x22;cc-form-inline-form&#x22;&#x3E;
    &#x3C;div class=&#x22;form-row&#x22;&#x3E;
        &#x3C;div class=&#x22;col-3&#x22;&#x3E;
            &#x3C;div class=&#x22;form-group&#x22;&#x3E;
                &#x3C;input type=&#x22;search&#x22; class=&#x22;form-control&#x22; id=&#x22;business_name&#x22; name=&#x22;business_name&#x22; placeholder=&#x22;First Name, Last Name or SSN&#x22;&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
        &#x3C;div class=&#x22;col-2 text-right align-self-center&#x22;&#x3E;
            &#x3C;div class=&#x22;form-group&#x22;&#x3E;
                &#x3C;label for=&#x22;daterange&#x22;&#x3E;Date Range: &#x3C;i class=&#x22;fa fa-calendar-alt&#x22; id=&#x22;daterange&#x22;&#x3E;&#x3C;/i&#x3E;&#x3C;/label&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
        &#x3C;div class=&#x22;col&#x22;&#x3E;
            &#x3C;div class=&#x22;form-group&#x22;&#x3E;
                &#x3C;input type=&#x22;text&#x22; class=&#x22;form-control date-field&#x22; placeholder=&#x22;MM/DD/YYYY&#x22; id=&#x22;startdate&#x22;&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
        -
        &#x3C;div class=&#x22;col&#x22;&#x3E;
            &#x3C;div class=&#x22;form-group&#x22;&#x3E;
                &#x3C;input type=&#x22;text&#x22; class=&#x22;form-control date-field&#x22; placeholder=&#x22;MM/DD/YYYY&#x22; id=&#x22;enddate&#x22;&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
        &#x3C;div class=&#x22;col&#x22;&#x3E;
            &#x3C;div class=&#x22;form-group&#x22;&#x3E;
                &#x3C;select name=&#x22;&#x22; id=&#x22;&#x22; class=&#x22;form-control&#x22;&#x3E;
                    &#x3C;option value=&#x22;&#x22;&#x3E;&#x3C;/option&#x3E;
                &#x3C;/select&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
        &#x3C;div class=&#x22;col&#x22;&#x3E;
            &#x3C;div class=&#x22;form-group&#x22;&#x3E;
                &#x3C;select name=&#x22;&#x22; id=&#x22;&#x22; class=&#x22;form-control&#x22;&#x3E;
                    &#x3C;option value=&#x22;&#x22;&#x3E;All Status&#x3C;/option&#x3E;
                &#x3C;/select&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
        &#x3C;div class=&#x22;col-1&#x22;&#x3E;
            &#x3C;button class=&#x22;btn btn-blue w-100&#x22;&#x3E;Search&#x3C;/button&#x3E;
        &#x3C;/div&#x3E;
    &#x3C;/div&#x3E;
&#x3C;/form&#x3E;
&#x3C;table class=&#x22;table&#x22; id=&#x22;cc-table-data&#x22;&#x3E;
    &#x3C;thead&#x3E;
        &#x3C;tr&#x3E;
            &#x3C;th scope=&#x22;col&#x22;&#x3E;Column 01&#x3C;/th&#x3E;
            &#x3C;th scope=&#x22;col&#x22; class=&#x22;text-center&#x22;&#x3E;Column 02&#x3C;/th&#x3E;
            &#x3C;th scope=&#x22;col&#x22;&#x3E;Column 03&#x3C;/th&#x3E;
            &#x3C;th scope=&#x22;col&#x22;&#x3E;Column 04&#x3C;/th&#x3E;
            &#x3C;th scope=&#x22;col&#x22; class=&#x22;text-right&#x22;&#x3E;Column 05&#x3C;/th&#x3E;
            &#x3C;th scope=&#x22;col&#x22;&#x3E;&#x3C;/th&#x3E;
        &#x3C;/tr&#x3E;
    &#x3C;/thead&#x3E;
    &#x3C;tbody class=&#x22;cc-table__clickable&#x22;&#x3E;
        &#x3C;tr class=&#x27;cc-table__clickable-row js-clickable-row&#x27; data-href=&#x27;https://www.google.com/&#x27;&#x3E;
            &#x3C;td&#x3E;Widget Inc.&#x3C;/td&#x3E;
            &#x3C;td class=&#x22;text-center&#x22;&#x3E;07/23/2019&#x3C;/td&#x3E;
            &#x3C;td&#x3E;Initial&#x3C;/td&#x3E;
            &#x3C;td&#x3E;2938 W. Mustang Street, Suite #287&#x3C;/td&#x3E;
            &#x3C;td class=&#x22;text-right&#x22;&#x3E;$25.01&#x3C;/td&#x3E;
            &#x3C;td class=&#x22;cc-table__td-action js-td-action&#x22;&#x3E;
                &#x3C;div class=&#x22;dropdown cc-table__td-dropdown&#x22;&#x3E;
                    &#x3C;button class=&#x22;btn dropdown-toggle&#x22; type=&#x22;button&#x22; id=&#x22;dropdownMenuButton&#x22;
                            data-toggle=&#x22;dropdown&#x22; aria-haspopup=&#x22;true&#x22; aria-expanded=&#x22;false&#x22;&#x3E;
                        Actions
                    &#x3C;/button&#x3E;
                    &#x3C;div class=&#x22;dropdown-menu dropdown-menu-right&#x22; aria-labelledby=&#x22;dropdownMenuButton&#x22;&#x3E;
                        &#x3C;a class=&#x22;dropdown-item&#x22; href=&#x22;#&#x22;&#x3E;Menu Option #1&#x3C;/a&#x3E;
                        &#x3C;a class=&#x22;dropdown-item&#x22; href=&#x22;#&#x22;&#x3E;Menu Option #2&#x3C;/a&#x3E;
                        &#x3C;a class=&#x22;dropdown-item&#x22; href=&#x22;#&#x22;&#x3E;Menu Option #3&#x3C;/a&#x3E;
                    &#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/td&#x3E;
        &#x3C;/tr&#x3E;
    &#x3C;/tbody&#x3E;
&#x3C;/table&#x3E;</code></pre>
</div>
<hr class="break">
<h1>Accordion</h1>
<p>Always use the name <span class="code">cc-table-data__</span> as a prefix for the table <span class="code">&#x3C;ID&#x3E;</span> (e.g. cc-table-data__employees)</p>
<div class="component-thumbnail">
    <img src="./img/components/tables/accordion.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;table class=&#x22;table&#x22; id=&#x22;cc-table-data&#x22;&#x3E;
    &#x3C;thead&#x3E;
    &#x3C;tr&#x3E;
        &#x3C;th scope=&#x22;col&#x22; width=&#x22;20&#x22;&#x3E;&#x3C;/th&#x3E;
        &#x3C;th scope=&#x22;col&#x22; width=&#x22;35%&#x22;&#x3E;Company Name&#x3C;/th&#x3E;
        &#x3C;th scope=&#x22;col&#x22; width=&#x22;15%&#x22;&#x3E;Last Modified&#x3C;/th&#x3E;
        &#x3C;th scope=&#x22;col&#x22; class=&#x22;text-center&#x22; width=&#x22;15%&#x22;&#x3E;Quote Number&#x3C;/th&#x3E;
        &#x3C;th scope=&#x22;col&#x22; class=&#x22;text-center&#x22; width=&#x22;25%&#x22;&#x3E;Effective Date&#x3C;/th&#x3E;
        &#x3C;th scope=&#x22;col&#x22; width=&#x22;10%&#x22;&#x3E;Type&#x3C;/th&#x3E;
    &#x3C;/tr&#x3E;
    &#x3C;/thead&#x3E;
    &#x3C;tbody&#x3E;
    &#x3C;tr&#x3E;
        &#x3C;td&#x3E;&#x3C;a href=&#x22;#&#x22; data-toggle=&#x22;collapse&#x22; class=&#x22;collapsed&#x22; data-target=&#x22;#demo1&#x22;&#x3E;&#x3C;i class=&#x22;far&#x22;&#x3E;&#x3C;/i&#x3E;&#x3C;/a&#x3E;&#x3C;/td&#x3E;
        &#x3C;td&#x3E;Dan&#x27;s Auto&#x3C;/td&#x3E;
        &#x3C;td&#x3E;09/25/2019&#x3C;/td&#x3E;
        &#x3C;td class=&#x22;text-center&#x22;&#x3E;123456&#x3C;/td&#x3E;
        &#x3C;td class=&#x22;text-center&#x22;&#x3E;08/01/2019&#x3C;/td&#x3E;
        &#x3C;td&#x3E;Initial&#x3C;/td&#x3E;
    &#x3C;/tr&#x3E;
    &#x3C;tr class=&#x22;cc-table__gradient-row&#x22;&#x3E;
        &#x3C;td colspan=&#x22;6&#x22; class=&#x22;p-0&#x22;&#x3E;
            &#x3C;div id=&#x22;demo1&#x22; class=&#x22;collapse&#x22;&#x3E;
                &#x3C;ul class=&#x22;table-files-list&#x22;&#x3E;
                    &#x3C;li class=&#x22;first&#x22;&#x3E;
                        &#x3C;p&#x3E;PDF created on:&#x3C;/p&#x3E;
                        &#x3C;p&#x3E;&#x3C;b&#x3E;10/02/2019&#x3C;/b&#x3E; @ 12:45 PM&#x3C;/p&#x3E;
                    &#x3C;/li&#x3E;
                    &#x3C;li&#x3E;&#x3C;a href=&#x22;#&#x22;&#x3E;&#x3C;i class=&#x22;fas fa-file-pdf&#x22;&#x3E;&#x3C;/i&#x3E; Initial Quote&#x3C;/a&#x3E;&#x3C;/li&#x3E;
                    &#x3C;li&#x3E;&#x3C;a href=&#x22;#&#x22;&#x3E;&#x3C;i class=&#x22;fas fa-file-pdf&#x22;&#x3E;&#x3C;/i&#x3E; Medical Benefits&#x3C;/a&#x3E;&#x3C;/li&#x3E;
                    &#x3C;li&#x3E;&#x3C;a href=&#x22;#&#x22;&#x3E;&#x3C;i class=&#x22;fas fa-file-pdf&#x22;&#x3E;&#x3C;/i&#x3E; Ancillary Benefits&#x3C;/a&#x3E;&#x3C;/li&#x3E;
                    &#x3C;li&#x3E;&#x3C;a href=&#x22;#&#x22;&#x3E;&#x3C;i class=&#x22;fas fa-file-pdf&#x22;&#x3E;&#x3C;/i&#x3E; Dependent Rates&#x3C;/a&#x3E;&#x3C;/li&#x3E;
                    &#x3C;li&#x3E;&#x3C;a href=&#x22;#&#x22;&#x3E;&#x3C;i class=&#x22;fas fa-download&#x22;&#x3E;&#x3C;/i&#x3E; Download All&#x3C;/a&#x3E;&#x3C;/li&#x3E;
                &#x3C;/ul&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/td&#x3E;
    &#x3C;/tr&#x3E;
    &#x3C;/tbody&#x3E;
&#x3C;/table&#x3E;</code></pre>
</div>
<hr class="break">
<h1>DataTables</h1>
<p>Always use the name <span class="code">cc-table-data__</span> as a prefix for the table <span class="code">&#x3C;ID&#x3E;</span> (e.g. cc-table-data__employees)</p>
<div class="component-thumbnail">
    <img src="./img/components/tables/datatable.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;form action=&#x22;&#x22; class=&#x22;datatable-search-form&#x22;&#x3E;
    &#x3C;label for=&#x22;daterange&#x22;&#x3E;Date Range: &#x3C;i class=&#x22;fa fa-calendar-alt&#x22; id=&#x22;daterange&#x22;&#x3E;&#x3C;/i&#x3E;&#x3C;/label&#x3E;
    &#x3C;input type=&#x22;text&#x22; class=&#x22;form-control date-field&#x22; placeholder=&#x22;MM/DD/YYYY&#x22; id=&#x22;startdate&#x22;&#x3E; -
    &#x3C;input type=&#x22;text&#x22; class=&#x22;form-control date-field&#x22; placeholder=&#x22;MM/DD/YYYY&#x22; id=&#x22;enddate&#x22;&#x3E;
    &#x3C;select name=&#x22;&#x22; id=&#x22;&#x22; class=&#x22;form-control&#x22; style=&#x22;width: 160px&#x22;&#x3E;
        &#x3C;option value=&#x22;&#x22;&#x3E;&#x3C;/option&#x3E;
    &#x3C;/select&#x3E;
    &#x3C;select name=&#x22;&#x22; id=&#x22;&#x22; class=&#x22;form-control&#x22;&#x3E;
        &#x3C;option value=&#x22;&#x22;&#x3E;All Status&#x3C;/option&#x3E;
    &#x3C;/select&#x3E;
    &#x3C;button class=&#x22;btn btn-blue&#x22;&#x3E;Apply&#x3C;/button&#x3E;
&#x3C;/form&#x3E;
&#x3C;table class=&#x22;table js-sortable-table&#x22; id=&#x22;cc-table-data&#x22;&#x3E;
    &#x3C;thead&#x3E;
        &#x3C;tr&#x3E;
            &#x3C;th scope=&#x22;col&#x22;&#x3E;Group Name&#x3C;/th&#x3E;
            &#x3C;th scope=&#x22;col&#x22; class=&#x22;no-sort&#x22;&#x3E;Quote ID&#x3C;/th&#x3E;
            &#x3C;th scope=&#x22;col&#x22; class=&#x22;text-center no-sort&#x22;&#x3E;Last Modified&#x3C;/th&#x3E;
            &#x3C;th scope=&#x22;col&#x22; class=&#x22;no-sort&#x22;&#x3E;Status&#x3C;/th&#x3E;
            &#x3C;th scope=&#x22;col&#x22; class=&#x22;text-center&#x22;&#x3E;Effective Date&#x3C;/th&#x3E;
        &#x3C;/tr&#x3E;
    &#x3C;/thead&#x3E;
    &#x3C;tbody&#x3E;
        &#x3C;tr&#x3E;
            &#x3C;td&#x3E;Applied Business Dynamics&#x3C;/td&#x3E;
            &#x3C;td&#x3E;In Progress&#x3C;/td&#x3E;
            &#x3C;td class=&#x22;text-center&#x22;&#x3E;07/23/2019 10:24 PM&#x3C;/td&#x3E;
            &#x3C;td&#x3E;Unfinished&#x3C;/td&#x3E;
            &#x3C;td class=&#x22;text-center&#x22;&#x3E;08/01/2019&#x3C;/td&#x3E;
        &#x3C;/tr&#x3E;
    &#x3C;/tbody&#x3E;
&#x3C;/table&#x3E;</code></pre>
</div>
