                        <h1>Work List</h1>
                        <p></p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel nostrum facere dolore
                            molestias.</p>
                        <p></p>

                        <div class="component-thumbnail">
                            <img src="./img/components/layouts/workList.png"
                                class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt=""
                                srcset="https://p3dstyleguide.staging.wpengine.com/wp-content/uploads/2018/07/workList.png 489w, https://p3dstyleguide.staging.wpengine.com/wp-content/uploads/2018/07/workList-250x128.png 250w, https://p3dstyleguide.staging.wpengine.com/wp-content/uploads/2018/07/workList-120x61.png 120w"
                                sizes="(max-width: 489px) 100vw, 489px"> </div>


                        <div class="codeArea">
                            <br>
                            <h6>HTML</h6>
                            <pre class="line-numbers language-markup"><code>&lt;ul class=&quot;notification-list&quot;&gt;
    &lt;li class=&quot;notification-item success-noty&quot;&gt;
        &lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;
        &lt;div class=&quot;left&quot;&gt;
            &lt;h5&gt;Annual Ce Due&lt;/h5&gt;
            &lt;p&gt;Last annual completed on..&lt;/p&gt;
        &lt;/div&gt;
        &lt;div class=&quot;right&quot;&gt;
            &lt;a class=&quot;btn view-noty-btn&quot; href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;&lt;/a&gt;
        &lt;/div&gt;
    &lt;/li&gt;
    &lt;li class=&quot;notification-item info-noty&quot;&gt;
        &lt;i class=&quot;fa fa-info&quot;&gt;&lt;/i&gt;
        &lt;div class=&quot;left&quot;&gt;
            &lt;h5&gt;ED Release&lt;/h5&gt;
            &lt;p&gt;Visit 07/05/2017 near hos..&lt;/p&gt;
        &lt;/div&gt;
        &lt;div class=&quot;right&quot;&gt;
            &lt;a class=&quot;btn view-noty-btn&quot; href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;&lt;/a&gt;
        &lt;/div&gt;
    &lt;/li&gt;
    &lt;li class=&quot;notification-item danger-noty&quot;&gt;
        &lt;i class=&quot;fa fa-info&quot;&gt;&lt;/i&gt;
        &lt;div class=&quot;left&quot;&gt;
            &lt;h5&gt;HRA Due&lt;/h5&gt;
            &lt;p&gt;Last HRA was declined by..&lt;/p&gt;
        &lt;/div&gt;
        &lt;div class=&quot;right&quot;&gt;
            &lt;a class=&quot;btn view-noty-btn&quot; href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;&lt;/a&gt;
        &lt;/div&gt;
    &lt;/li&gt;
&lt;/ul&gt;</code></pre>
                        </div>
                        <br><br>