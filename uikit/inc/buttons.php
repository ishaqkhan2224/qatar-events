                        <h1>Standard Buttons</h1>
                        <div class="component-thumbnail">
                            <img src="./img/buttons.png">
                        </div>
                        <div class="codeArea">
                            <h6>HTML</h6>
                            <pre class="line-numbers language-markup"><code>&#x3C;button class=&#x22;btn btn-white&#x22;&#x3E;White Button&#x3C;/button&#x3E;
&#x3C;button class=&#x22;btn btn-white-outline&#x22;&#x3E;White Outline Button&#x3C;/button&#x3E;
&#x3C;button class=&#x22;btn btn-grey-outline&#x22;&#x3E;Grey Outline Button&#x3C;/button&#x3E;
&#x3C;button class=&#x22;btn btn-blue&#x22;&#x3E;Blue Button&#x3C;/button&#x3E;
&#x3C;button class=&#x22;btn btn-orange&#x22;&#x3E;Orange Button&#x3C;/button&#x3E;</code></pre>
                        </div>

                        <hr class="break">
    
                        <h1>Standard Buttons (Disabled)</h1>
                        <p>Styling for disabled state are consistent, regardless of applied styling.</p>
                        <div class="component-thumbnail">
                            <img src="./img/buttons-disabled.png">
                        </div>
                        <div class="codeArea">
                            <h6>HTML</h6>
                            <pre class="line-numbers language-markup"><code>&#x3C;button class=&#x22;btn btn-white&#x22; disabled=&#x22;disabled&#x22;&#x3E;White Button&#x3C;/button&#x3E;
&#x3C;button class=&#x22;btn btn-white-outline&#x22; disabled=&#x22;disabled&#x22;&#x3E;White Outline Button&#x3C;/button&#x3E;
&#x3C;button class=&#x22;btn btn-grey-outline&#x22; disabled=&#x22;disabled&#x22;&#x3E;Grey Outline Button&#x3C;/button&#x3E;
&#x3C;button class=&#x22;btn btn-blue&#x22; disabled=&#x22;disabled&#x22;&#x3E;Blue Button&#x3C;/button&#x3E;
&#x3C;button class=&#x22;btn btn-orange&#x22; disabled=&#x22;disabled&#x22;&#x3E;Orange Button&#x3C;/button&#x3E;</code></pre>
                        </div>

                        <hr class="break">

                        <h1>Anchor Buttons</h1>
                        <p>The <strong>.btn</strong> classes are designed to be used with the <strong>&#x3C;button&#x3E;</strong> element. However, you can also use these classes on <strong>&#x3C;a&#x3E;</strong> elements.</p>
                        <div class="component-thumbnail">
                            <img src="./img/anchor-buttons.png">
                        </div>
                        <div class="codeArea">
                            <h6>HTML</h6>
                            <pre class="line-numbers language-markup"><code>&#x3C;a href=&#x22;#&#x22; class=&#x22;btn btn-white&#x22;&#x3E;White Button&#x3C;/a&#x3E;
&#x3C;a href=&#x22;#&#x22; class=&#x22;btn btn-white-outline&#x22;&#x3E;White Outline Button&#x3C;/a&#x3E;
&#x3C;a href=&#x22;#&#x22; class=&#x22;btn btn-grey-outline&#x22;&#x3E;Grey Outline Button&#x3C;/a&#x3E;
&#x3C;a href=&#x22;#&#x22; class=&#x22;btn btn-blue&#x22;&#x3E;Blue Button&#x3C;/a&#x3E;
&#x3C;a href=&#x22;#&#x22; class=&#x22;btn btn-orange&#x22;&#x3E;Orange Button&#x3C;/a&#x3E;</code></pre>
                        </div>