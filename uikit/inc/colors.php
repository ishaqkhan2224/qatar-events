<h1>Primary Color Palette</h1>
<div class="row">
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #00529C"></div>
            <div class="colorDesc">
                <!-- <h3>Green</h3> -->
                <aside>HEX</aside>#00529C<br>
                <aside>RGB</aside>0/82/156<br>
                <aside>SCSS</aside>$primary-blue
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #444444"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#444444<br>
                <aside>RGB</aside>68/68/68<br>
                <aside>SCSS</aside>$text-color
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #0098D6"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#0098D6<br>
                <aside>RGB</aside>0/152/214<br>
                <aside>SCSS</aside>$broker-blue
            </div>
        </div>
    </div>
</div>
<hr class="break">
<h1>Secondary Color Palette</h1>

<div class="row">
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #f47421"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#f47421<br>
                <aside>RGB</aside>244/116/33<br>
                <aside>SCSS</aside>$employers
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #d86010"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#d86010<br>
                <aside>RGB</aside>216/96/16<br>
                <aside>SCSS</aside>$dark-orange
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #62bc74"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#62bc74<br>
                <aside>RGB</aside>9/96/13<br>
                <aside>SCSS</aside>$employees
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #09600d"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#09600d<br>
                <aside>RGB</aside>98/188/116<br>
                <aside>SCSS</aside>$dark-green
            </div>
        </div>
    </div>
</div>
<hr class="break">
<h1>Shades of gray</h1>

<div class="row">
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #cccccc"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#cccccc<br>
                <aside>RGB</aside>204/204/204<br>
                <aside>SCSS</aside>$grey
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #535353"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#535353<br>
                <aside>RGB</aside>83/83/83<br>
                <aside>SCSS</aside>$dark-grey
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #eff2f3"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#eff2f3<br>
                <aside>RGB</aside>239/242/243<br>
                <aside>SCSS</aside>$light-grey
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #dee2e6"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#dee2e6<br>
                <aside>RGB</aside>222/226/230<br>
                <aside>SCSS</aside>$outline-grey
            </div>
        </div>
    </div>
</div>
<hr class="break">
<h1>Status Colors</h1>
<div class="row">
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #007000"></div>
            <div class="colorDesc">
                <!-- <h3>Green</h3> -->
                <aside>HEX</aside>#007000<br>
                <aside>RGB</aside>0/112/0<br>
                <aside>SCSS</aside>$color-success-bg<br>
                <aside>CLASS</aside>.cc-success
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #B90909"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#B90909<br>
                <aside>RGB</aside>185/9/9<br>
                <aside>SCSS</aside>$color-error-bg<br>
                <aside>CLASS</aside>.cc-erro
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #F5DB1F"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#F5DB1F<br>
                <aside>RGB</aside>245/219/31<br>
                <aside>SCSS</aside>$color-warning-bg<br>
                <aside>CLASS</aside>.cc-warning
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="color-pallete">
            <div class="colorBlock" style="background-color: #00529c"></div>
            <div class="colorDesc">
                <aside>HEX</aside>#00529c<br>
                <aside>RGB</aside>0/82/156<br>
                <aside>SCSS</aside>$color-info-bg<br>
                <aside>CLASS</aside>.cc-info
            </div>
        </div>
    </div>
</div>
