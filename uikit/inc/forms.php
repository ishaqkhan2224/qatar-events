<h1>Form Wrapper</h1>
<div class="codeArea mt-4">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;form id=&#x22;cc-form__new-quote&#x22; method=&#x22;post&#x22; action=&#x22;&#x22;&#x3E;
  &#x3C;section id=&#x22;cc-body&#x22;&#x3E;
    &#x3C;div class=&#x22;container&#x22;&#x3E;
      &#x3C;div class=&#x22;cc-form__box&#x22;&#x3E;
        &#x3C;div class=&#x22;row&#x22;&#x3E;
          &#x3C;div class=&#x22;col-12&#x22;&#x3E;
              &#x3C;!-- form input goes here --&#x3E;
          &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
      &#x3C;/div&#x3E;&#x3C;!-- cc-form__box --&#x3E;
    &#x3C;/div&#x3E;&#x3C;!-- container --&#x3E;
  &#x3C;/section&#x3E;
&#x3C;/form&#x3E;</code></pre>
<p>Always use the name <span class="code">cc-form__</span> as a prefix for the form <span class="code">&#x3C;ID&#x3E;</span> (e.g. cc-form__new-quote)</p>
</div>
<hr class="break">
<h1>Standard Text Input w/ Helper Text</h1>
<div class="component-thumbnail">
    <img src="./img/components/forms/standard-input.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;form-group&#x22;&#x3E;
  &#x3C;label for=&#x22;basic_input_field&#x22;&#x3E;Standard Text&#x3C;/label&#x3E;
  &#x3C;input type=&#x22;text&#x22; class=&#x22;form-control&#x22; id=&#x22;basic_input_field&#x22; name=&#x22;basic_input_field&#x22; required&#x3E;
  &#x3C;small id=&#x22;passwordHelpBlock&#x22; class=&#x22;form-text text-muted&#x22;&#x3E;
    This is an example of a helper text, located nicely below the input field.
  &#x3C;/small&#x3E;
&#x3C;/div&#x3E;</code></pre>
</div>
<hr class="break">
<h1>Password Field w/ Show Hide Tool</h1>
<div class="component-thumbnail">
    <img src="./img/components/forms/standard-password.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;form-group&#x22;&#x3E;
  &#x3C;div class=&#x22;cc-showhidepwd&#x22;&#x3E;
    &#x3C;label for=&#x22;field-CurrentPassword&#x22;&#x3E;Password&#x3C;/label&#x3E;
    &#x3C;span class=&#x22;js-password-display fas fa-eye-slash&#x22; data-toggle=&#x22;tooltip&#x22; data-placement=&#x22;left&#x22; title=&#x22;Click to show password&#x22;&#x3E;
    &#x3C;/span&#x3E;
  &#x3C;/div&#x3E;
  &#x3C;input type=&#x22;password&#x22; class=&#x22;form-control&#x22; id=&#x22;field-CurrentPassword&#x22; name=&#x22;CurrentPassword&#x22; autocomplete=&#x22;off&#x22; required /&#x3E;
&#x3C;/div&#x3E;</code></pre>
<p>Apply the div class <span class="code">.cc-showhidepwd</span> as a wrapper for the label & span icon</p>
</div>
<div class="codeArea mt-4">
    <h6>Javascript</h6>
    <pre class="line-numbers language-javascript"><code>$(&#x27;.js-password-display&#x27;).on(&#x27;click&#x27;, function () {
  var passwordInputField = $(this).closest(&#x27;.form-group&#x27;).find(&#x27;input&#x27;);
  if (passwordInputField.attr(&#x27;type&#x27;) === &#x27;password&#x27;) {
    passwordInputField.attr(&#x27;type&#x27;, &#x27;text&#x27;);
    $(this).removeClass(&#x27;fa-eye-slash&#x27;).addClass(&#x27;fa-eye&#x27;).attr(&#x27;title&#x27;, &#x27;Click to hide password&#x27;).tooltip(&#x27;dispose&#x27;).tooltip(&#x27;show&#x27;);
  } else {
    passwordInputField.attr(&#x27;type&#x27;, &#x27;password&#x27;);
    $(this).removeClass(&#x27;fa-eye&#x27;).addClass(&#x27;fa-eye-slash&#x27;).attr(&#x27;title&#x27;, &#x27;Click to show password&#x27;).tooltip(&#x27;dispose&#x27;).tooltip(&#x27;show&#x27;);
  }
});</code></pre>
</div>
<hr class="break">
<h1>Grouped Input Fields w/ Dropdowns</h1>
<div class="component-thumbnail">
    <img src="./img/components/forms/grouped-fields.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;form-group&#x22;&#x3E;
  &#x3C;div class=&#x22;form-row&#x22;&#x3E;
    &#x3C;div class=&#x22;col-7&#x22;&#x3E;
      &#x3C;input type=&#x22;text&#x22; class=&#x22;form-control&#x22; name=&#x22;city&#x22; placeholder=&#x22;City&#x22; required&#x3E;
    &#x3C;/div&#x3E;
    &#x3C;div class=&#x22;col-2&#x22;&#x3E;
      &#x3C;select id=&#x22;&#x22; class=&#x22;form-control&#x22; required&#x3E;
        &#x3C;option value=&#x22;&#x22;&#x3E;Select&#x3C;/option&#x3E;
        &#x3C;option value=&#x22;CA&#x22;&#x3E;CA&#x3C;/option&#x3E;
        &#x3C;option&#x3E;...&#x3C;/option&#x3E;
      &#x3C;/select&#x3E;
    &#x3C;/div&#x3E;
    &#x3C;div class=&#x22;col-3&#x22;&#x3E;
      &#x3C;input type=&#x22;text&#x22; class=&#x22;form-control&#x22; name=&#x22;zip_code&#x22; placeholder=&#x22;Zip Code&#x22; required&#x3E;
    &#x3C;/div&#x3E;
  &#x3C;/div&#x3E;
&#x3C;/div&#x3E;</code></pre>
</div>
<hr class="break">
<h1>Checkboxes</h1>
<div class="component-thumbnail">
    <img src="./img/components/forms/checkboxes.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;form-group&#x22;&#x3E;
  &#x3C;div class=&#x22;custom-control custom-checkbox&#x22;&#x3E;
    &#x3C;input type=&#x22;checkbox&#x22; class=&#x22;custom-control-input&#x22; id=&#x22;same-address&#x22; name=&#x22;checkbox01&#x22;&#x3E;
    &#x3C;label class=&#x22;custom-control-label&#x22; for=&#x22;same-address&#x22;&#x3E;Checkbox 01&#x3C;/label&#x3E;
  &#x3C;/div&#x3E;
  &#x3C;div class=&#x22;custom-control custom-checkbox&#x22;&#x3E;
    &#x3C;input type=&#x22;checkbox&#x22; class=&#x22;custom-control-input&#x22; id=&#x22;checkbox02&#x22; name=&#x22;checkbox02&#x22;&#x3E;
    &#x3C;label class=&#x22;custom-control-label&#x22; for=&#x22;checkbox02&#x22;&#x3E;Checkbox 02&#x3C;/label&#x3E;
  &#x3C;/div&#x3E;
&#x3C;/div&#x3E;</code></pre>
</div>
<hr class="break">
<h1>Radio Options</h1>
<div class="component-thumbnail">
    <img src="./img/components/forms/radio.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;form-group&#x22;&#x3E;
  &#x3C;div class=&#x22;custom-control custom-radio custom-control-inline&#x22;&#x3E;
    &#x3C;input type=&#x22;radio&#x22; id=&#x22;AgreeDisagree1-a&#x22; name=&#x22;AgreeDisagree1&#x22; class=&#x22;custom-control-input&#x22; value=&#x22;agree&#x22;&#x3E;
    &#x3C;label class=&#x22;custom-control-label&#x22; for=&#x22;AgreeDisagree1-a&#x22;&#x3E;Option A&#x3C;/label&#x3E;
  &#x3C;/div&#x3E;
  &#x3C;div class=&#x22;custom-control custom-radio custom-control-inline&#x22;&#x3E;
    &#x3C;input type=&#x22;radio&#x22; id=&#x22;AgreeDisagree1-b&#x22; name=&#x22;AgreeDisagree1&#x22; class=&#x22;custom-control-input&#x22; value=&#x22;disagree&#x22;&#x3E;
    &#x3C;label class=&#x22;custom-control-label&#x22; for=&#x22;AgreeDisagree1-b&#x22;&#x3E;Option B&#x3C;/label&#x3E;
  &#x3C;/div&#x3E;
&#x3C;/div&#x3E;</code></pre>
</div>
<hr class="break">
<h1>Textarea</h1>
<div class="component-thumbnail">
    <img src="./img/components/forms/textarea.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;form-group&#x22;&#x3E;
&#x9;&#x3C;label for=&#x22;special_instruction&#x22;&#x3E;Textarea&#x3C;/label&#x3E;
&#x9;&#x3C;textarea class=&#x22;form-control&#x22; id=&#x22;special_instruction&#x22; name=&#x22;special_instruction&#x22; rows=&#x22;6&#x22; required&#x3E;&#x3C;/textarea&#x3E;
&#x3C;/div&#x3E;</code></pre>
</div>
<hr class="break">
<h1>Sticky Action Footer</h1>
<div class="component-thumbnail">
    <img src="./img/components/forms/action-footer.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;section class=&#x22;cc-controls sticky&#x22;&#x3E;
  &#x3C;div class=&#x22;container&#x22;&#x3E;
    &#x3C;div class=&#x22;d-flex align-items-center&#x22;&#x3E;
      &#x3C;div class=&#x22;left&#x22;&#x3E;
        &#x3C;a href=&#x22;#&#x22; class=&#x22;btn-link&#x22;&#x3E;Cancel&#x3C;/a&#x3E;
      &#x3C;/div&#x3E;
      &#x3C;div class=&#x22;right&#x22;&#x3E;
        &#x3C;a href=&#x22;#&#x22; class=&#x22;btn-link&#x22;&#x3E;Save &#x26; Exit&#x3C;/a&#x3E;
        &#x3C;button class=&#x22;btn&#x22;&#x3E;Next&#x3C;/button&#x3E;
      &#x3C;/div&#x3E;
    &#x3C;/div&#x3E;
  &#x3C;/div&#x3E;
  &#x3C;div class=&#x22;cc-controls-footer-links&#x22;&#x3E;
    &#x3C;div class=&#x22;container&#x22;&#x3E;
      &#x3C;div class=&#x22;d-flex justify-content-between&#x22;&#x3E;
        &#x3C;div&#x3E;&#x26;copy; 2019 CaliforniaChoice | A CHOICE Administrators Program&#x3C;/div&#x3E;
        &#x3C;div&#x3E;
          &#x3C;a href=&#x22;&#x22;&#x3E;Privacy Policy&#x3C;/a&#x3E; | &#x3C;a href=&#x22;&#x22;&#x3E;Terms of Use&#x3C;/a&#x3E;
        &#x3C;/div&#x3E;
      &#x3C;/div&#x3E;
    &#x3C;/div&#x3E;
  &#x3C;/div&#x3E;
&#x3C;/section&#x3E;</code></pre>
</div>
<hr class="break">
<h1>Form Layout</h1>
<div class="component-thumbnail">
    <img src="./img/components/forms/standard.png">
</div>
<hr class="break">
<h1>Form Layout w/ Error Validation</h1>
<div class="component-thumbnail">
    <img src="./img/components/forms/standard-errors.png">
</div>