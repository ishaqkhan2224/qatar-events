<h1>Standard Card w/ Ribbons <span class="float-right demo-link"><a href="" target="_blank"><i class="far fa-external-link-alt"></i> Demo</a></span></h1>
<div class="component-thumbnail mt-3">
    <img src="./img/components/cards/ribbon.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;cc-card-wrapper&#x22;&#x3E;
  &#x3C;div class=&#x22;cc-card&#x22;&#x3E;
    &#x3C;div class=&#x22;card-ribbon&#x22;&#x3E;
      &#x3C;span&#x3E;Latest&#x3C;/span&#x3E;
    &#x3C;/div&#x3E;
    &#x3C;h4&#x3E;Sub-Title Here&#x3C;/h4&#x3E;
    &#x3C;p&#x3E;Lorem ipsum dolor sit amet.&#x3C;/p&#x3E;
    &#x3C;p&#x3E;Lorem ipsum dolor sit amet.&#x3C;/p&#x3E;
  &#x3C;/div&#x3E;
&#x3C;/div&#x3E;</code></pre>
</div>

<hr class="break">
<h1>Cards w/ Tables</h1>
<div class="component-thumbnail mt-3 devnote-wrapper">
    <img src="./img/components/cards/table.png">
    <div class="devnote" style="left: 60px; top: 90px;">
      <p>For larger cards, simply add <span>.large</span> class to <span>.cc-card</span> class to increase padding</p>
    </div>
    <div class="devnote" style="left: 680px; top: 90px;">
      <p>Apply both <span>.table</span> and <span>.card-table</span> classes to the <span>&#x3C;table&#x3E;</span> element inside the card</p>
    </div>
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;row mt-5&#x22;&#x3E;
  &#x3C;div class=&#x22;col-12&#x22;&#x3E;
    &#x3C;div class=&#x22;cc-card-wrapper&#x22;&#x3E;
      &#x3C;h3&#x3E;Card Title&#x3C;/h3&#x3E;
      &#x3C;p&#x3E;Long description for cards goes here...&#x3C;/p&#x3E;
      &#x3C;div class=&#x22;cc-card large&#x22;&#x3E;
         &#x3C;div class=&#x22;row&#x22;&#x3E;
             &#x3C;div class=&#x22;col-12&#x22;&#x3E;
              &#x3C;h4&#x3E;Card Sub-Title&#x3C;/h4&#x3E;
              &#x3C;table class=&#x22;table card-table&#x22;&#x3E;
                  &#x3C;thead&#x3E;
                      &#x3C;tr&#x3E;
                          &#x3C;th width=&#x22;25%&#x22;&#x3E;Column 01&#x3C;/th&#x3E;
                          &#x3C;th width=&#x22;25%&#x22;&#x3E;Column 02&#x3C;/th&#x3E;
                          &#x3C;th width=&#x22;50%&#x22; class=&#x22;card-table-highlight pl-4&#x22;&#x3E;Highlighted Column 03&#x3C;/th&#x3E;
                      &#x3C;/tr&#x3E;
                  &#x3C;/thead&#x3E;
                  &#x3C;tbody&#x3E;
                  &#x3C;tr&#x3E;
                      &#x3C;td&#x3E;Hello There&#x3C;/td&#x3E;
                      &#x3C;td&#x3E;Widget INC.&#x3C;/td&#x3E;
                      &#x3C;td class=&#x22;card-table-highlight pl-4&#x22;&#x3E;Complete&#x3C;/td&#x3E;
                  &#x3C;/tr&#x3E;
                  &#x3C;tr&#x3E;
                      &#x3C;td&#x3E;Hello There&#x3C;/td&#x3E;
                      &#x3C;td&#x3E;Widget INC.&#x3C;/td&#x3E;
                      &#x3C;td class=&#x22;card-table-highlight pl-4&#x22;&#x3E;Complete&#x3C;/td&#x3E;
                  &#x3C;/tr&#x3E;
                  &#x3C;tr&#x3E;
                      &#x3C;td&#x3E;Hello There&#x3C;/td&#x3E;
                      &#x3C;td&#x3E;Widget INC.&#x3C;/td&#x3E;
                      &#x3C;td class=&#x22;card-table-highlight pl-4&#x22;&#x3E;Complete&#x3C;/td&#x3E;
                  &#x3C;/tr&#x3E;
                  &#x3C;/tbody&#x3E;
              &#x3C;/table&#x3E;
             &#x3C;/div&#x3E;
         &#x3C;/div&#x3E;
      &#x3C;/div&#x3E;&#x3C;!-- cc-card large --&#x3E;
    &#x3C;/div&#x3E;&#x3C;!-- cc-card-wrapper --&#x3E;
  &#x3C;/div&#x3E;&#x3C;!-- col-12 --&#x3E;
&#x3C;/div&#x3E;&#x3C;!-- row --&#x3E;</code></pre>
</div>
<hr class="break">

<h1>Equal Height Cards</h1>
<div class="component-thumbnail mt-3 devnote-wrapper">
    <img src="./img/components/cards/equal.png">
    <div class="devnote" style="left: 70px; top: 90px;">
      <p>Apply <span>.d-flex</span> class to each <span>.col</span> grid class to force equal column heights</p>
    </div>
    <div class="devnote" style="left: 780px; top: 50px;">
      <p>Apply <span>.card-table-no-headers</span> class to <span>&#x3C;tbody&#x3E;</span> element for card tables without table headers</p>
    </div>
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;row&#x22;&#x3E;

  &#x3C;div class=&#x22;col-3 d-flex&#x22;&#x3E;
    &#x3C;div class=&#x22;cc-card large&#x22;&#x3E;
      &#x3C;div class=&#x22;cc-card-wrapper&#x22;&#x3E;
        &#x3C;h4&#x3E;Sub-Title Here&#x3C;/h4&#x3E;
        &#x3C;h5&#x3E;23,499&#x3C;/h5&#x3E;
        &#x3C;hr&#x3E;
        &#x3C;p&#x3E;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis cum culpa, vero mollitia exercitationem libero et possimus assumenda aut, deleniti sapiente rem delectus architecto doloribus beatae, dolorum amet? At, delectus!&#x3C;/p&#x3E;
        &#x3C;p&#x3E;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis cum culpa, vero mollitia exercitationem libero et possimus assumenda aut, deleniti sapiente rem delectus architecto doloribus beatae, dolorum amet? At, delectus!&#x3C;/p&#x3E;
      &#x3C;/div&#x3E;
    &#x3C;/div&#x3E;&#x3C;!-- cc-card --&#x3E;
  &#x3C;/div&#x3E;&#x3C;!-- col-3 --&#x3E;

  &#x3C;div class=&#x22;col-3 d-flex&#x22;&#x3E;
    &#x3C;div class=&#x22;cc-card large&#x22;&#x3E;
      &#x3C;p&#x3E;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis cum culpa, vero mollitia exercitationem libero et possimus assumenda aut, deleniti sapiente rem delectus architecto doloribus beatae, dolorum amet? At, delectus!&#x3C;/p&#x3E;
      &#x3C;table class=&#x22;table card-table&#x22;&#x3E;
        &#x3C;thead&#x3E;
          &#x3C;tr&#x3E;
            &#x3C;th width=&#x22;25%&#x22;&#x3E;Column 01&#x3C;/th&#x3E;
            &#x3C;th width=&#x22;25%&#x22;&#x3E;Column 02&#x3C;/th&#x3E;
          &#x3C;/tr&#x3E;
        &#x3C;/thead&#x3E;
        &#x3C;tbody&#x3E;
          &#x3C;tr&#x3E;
            &#x3C;td&#x3E;Hello There&#x3C;/td&#x3E;
            &#x3C;td&#x3E;Widget INC.&#x3C;/td&#x3E;
          &#x3C;/tr&#x3E;
          &#x3C;tr&#x3E;
            &#x3C;td&#x3E;Hello There&#x3C;/td&#x3E;
            &#x3C;td&#x3E;Widget INC.&#x3C;/td&#x3E;
          &#x3C;/tr&#x3E;
          &#x3C;tr&#x3E;
            &#x3C;td&#x3E;Hello There&#x3C;/td&#x3E;
            &#x3C;td&#x3E;Widget INC.&#x3C;/td&#x3E;
          &#x3C;/tr&#x3E;
        &#x3C;/tbody&#x3E;
      &#x3C;/table&#x3E;
    &#x3C;/div&#x3E;&#x3C;!-- cc-card --&#x3E;
  &#x3C;/div&#x3E;&#x3C;!-- col-3 --&#x3E;

  &#x3C;div class=&#x22;col-3 d-flex&#x22;&#x3E;
    &#x3C;div class=&#x22;cc-card large&#x22;&#x3E;
      &#x3C;div class=&#x22;cc-card-wrapper&#x22;&#x3E;
        &#x3C;p&#x3E;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis cum culpa, vero mollitia exercitationem libero et possimus assumenda aut.&#x3C;/p&#x3E;
      &#x3C;/div&#x3E;
    &#x3C;/div&#x3E;&#x3C;!-- cc-card --&#x3E;
  &#x3C;/div&#x3E;&#x3C;!-- col-3 --&#x3E;

  &#x3C;div class=&#x22;col-3 d-flex&#x22;&#x3E;
    &#x3C;div class=&#x22;cc-card large&#x22;&#x3E;
      &#x3C;div class=&#x22;cc-card-wrapper&#x22;&#x3E;
        &#x3C;table class=&#x22;table card-table&#x22;&#x3E;
          &#x3C;tbody class=&#x22;card-table-no-headers&#x22;&#x3E;
            &#x3C;tr&#x3E;
              &#x3C;td&#x3E;Employees&#x3C;/td&#x3E;
              &#x3C;td class=&#x22;text-right&#x22;&#x3E;&#x3C;h3 class=&#x22;mb-0&#x22;&#x3E;23&#x3C;/h3&#x3E;&#x3C;/td&#x3E;
            &#x3C;/tr&#x3E;
            &#x3C;tr&#x3E;
              &#x3C;td&#x3E;Dependents&#x3C;/td&#x3E;
              &#x3C;td class=&#x22;text-right&#x22;&#x3E;&#x3C;h3 class=&#x22;mb-0&#x22;&#x3E;57&#x3C;/h3&#x3E;&#x3C;/td&#x3E;
            &#x3C;/tr&#x3E;
            &#x3C;tr&#x3E;
              &#x3C;td&#x3E;Out-of-State&#x3C;/td&#x3E;
              &#x3C;td class=&#x22;text-right&#x22;&#x3E;&#x3C;h3 class=&#x22;mb-0&#x22;&#x3E;3&#x3C;/h3&#x3E;&#x3C;/td&#x3E;
            &#x3C;/tr&#x3E;
          &#x3C;/tbody&#x3E;
        &#x3C;/table&#x3E;
        &#x3C;hr&#x3E;
        &#x3C;table class=&#x22;table card-table&#x22;&#x3E;
          &#x3C;tbody class=&#x22;card-table-no-headers&#x22;&#x3E;
            &#x3C;tr&#x3E;
              &#x3C;td&#x3E;Arizona&#x3C;/td&#x3E;
              &#x3C;td class=&#x22;text-right&#x22;&#x3E;&#x3C;h3 class=&#x22;mb-0&#x22;&#x3E;&#x3C;span class=&#x22;badge cc-success ml-3&#x22;&#x3E;NEW&#x3C;/span&#x3E;&#x3C;/h3&#x3E;&#x3C;/td&#x3E;
            &#x3C;/tr&#x3E;
            &#x3C;tr&#x3E;
              &#x3C;td&#x3E;California&#x3C;/td&#x3E;
              &#x3C;td class=&#x22;text-right&#x22;&#x3E;&#x3C;h3 class=&#x22;mb-0&#x22;&#x3E;&#x3C;span class=&#x22;badge cc-error ml-3&#x22;&#x3E;OLD&#x3C;/span&#x3E;&#x3C;/h3&#x3E;&#x3C;/td&#x3E;
            &#x3C;/tr&#x3E;
            &#x3C;tr&#x3E;
              &#x3C;td&#x3E;Texas&#x3C;/td&#x3E;
              &#x3C;td class=&#x22;text-right&#x22;&#x3E;&#x3C;h3 class=&#x22;mb-0&#x22;&#x3E;&#x3C;span class=&#x22;badge cc-success ml-3&#x22;&#x3E;NEW&#x3C;/span&#x3E;&#x3C;/h3&#x3E;&#x3C;/td&#x3E;
            &#x3C;/tr&#x3E;
          &#x3C;/tbody&#x3E;
        &#x3C;/table&#x3E;
      &#x3C;/div&#x3E;
    &#x3C;/div&#x3E;&#x3C;!-- cc-card --&#x3E;
  &#x3C;/div&#x3E;&#x3C;!-- col-3 --&#x3E;
  
&#x3C;/div&#x3E;&#x3C;!-- row --&#x3E;  </code></pre>
</div>