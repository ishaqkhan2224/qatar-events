<h1>Success</h1>
<div class="component-thumbnail">
    <img src="./img/components/confirmation/succcess.png">
</div>
<div class="codeArea">
<h6>View</h6>
<pre class="line-numbers language-markup"><code>@using CalChoice.WebMVC.ViewModels
@model REFRESHConfirmationViewModel
&#x200B;
    &#x3C;section id=&#x22;cc-body&#x22;&#x3E;
        &#x3C;div class=&#x22;container&#x22;&#x3E;
            &#x3C;div class=&#x22;cc-confirmation&#x22;&#x3E;
                &#x3C;div class=&#x22;cc-confirmation-header cc-@Model.MessageType&#x22;&#x3E;
                    @Model.ConfirmationAlertText
                &#x3C;/div&#x3E;
                &#x3C;div class=&#x22;cc-confirmation-body&#x22;&#x3E;
                    &#x3C;img src=&#x22;../../images/icons/cc-confirmation-@{@Model.MessageType}.png&#x22; alt=&#x22;@Model.ImageAltText&#x22;&#x3E;
                    &#x3C;h5&#x3E;@Model.ConfirmationTitleText&#x3C;/h5&#x3E;
                    @if (Model.LineOneLabelText != null || Model.LineOneContentText != null ||
                         Model.LineTwoLabelText != null || Model.LineTwoContentText != null)
                    {
                        &#x3C;div class=&#x22;cc-confirmation-body-info&#x22;&#x3E;
                            &#x3C;p&#x3E;
                                @Model.LineOneLabelText &#x3C;strong&#x3E;@Model.LineOneContentText&#x3C;/strong&#x3E;
                                @if (Model.LineTwoContentText != null || Model.LineTwoLabelText != null)
                                {
                                    &#x3C;br&#x3E;
                                    @Model.LineTwoLabelText &#x3C;strong&#x3E;@Model.LineTwoContentText&#x3C;/strong&#x3E;
                                }
                            &#x3C;/p&#x3E;
                        &#x3C;/div&#x3E;
                    }
                    @if (Model.NoticeTextHtml != null)
                    {
                        &#x3C;div class=&#x22;cc-confirmation-body-message&#x22;&#x3E;
                            @Html.Raw(Model.NoticeTextHtml)
                        &#x3C;/div&#x3E;
                    }
                &#x3C;/div&#x3E;&#x3C;!-- cc-confirmation-body --&#x3E;
                &#x3C;div class=&#x22;cc-confirmation-footer&#x22;&#x3E;
                    @if (Model.FirstButtonText != null)
                    {
                        &#x3C;a href=&#x22;@Url.Action(Model.FirstButtonAction, Model.FirstButtonController)&#x22; class=&#x22;btn btn-blue&#x22;&#x3E;@Model.FirstButtonText&#x3C;/a&#x3E;
                    }
                    @if (Model.SecondButtonText != null)
                    {
                        &#x3C;a href=&#x22;@Url.Action(Model.SecondButtonAction, Model.SecondButtonController)&#x22; class=&#x22;btn btn-grey-outline&#x22;&#x3E;@Model.SecondButtonText&#x3C;/a&#x3E;
                    }
                    @if (Model.ThirdButtonText != null)
                    {
                        &#x3C;a href=&#x22;@Url.Action(Model.ThirdButtonAction, Model.ThirdButtonController)&#x22; class=&#x22;btn btn-grey-outline&#x22;&#x3E;@Model.ThirdButtonText&#x3C;/a&#x3E;
                    }
                &#x3C;/div&#x3E;&#x3C;!-- cc-confirmation-footer --&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
    &#x3C;/section&#x3E;</code></pre>
</div>
<div class="codeArea">
<h6 class="mt-4">ViewModel</h6>
<pre class="line-numbers language-csharp"><code>namespace CalChoice.WebMVC.ViewModels
{
&#x9;public class REFRESHConfirmationViewModel
&#x9;{
&#x9;&#x9;// Banner text alert
&#x9;&#x9;// to determine background color of the banner and the appropriate image source
&#x9;&#x9;// options: &#x27;warning&#x27;, &#x27;success&#x27;, &#x27;error&#x27;
&#x9;&#x9;public string MessageType { get; set; }
&#x9;&#x9;public string ConfirmationAlertText { get; set; }
&#x200B;
&#x9;&#x9;// Title Text
&#x9;&#x9;public string ConfirmationTitleText { get; set; }
&#x9;&#x9;// alternative text for image
&#x9;&#x9;public string ImageAltText { get; set; }
&#x200B;
&#x9;&#x9;// Two Subtitles Text
&#x9;&#x9;public string LineOneLabelText { get; set; }
&#x9;&#x9;public string LineOneContentText { get; set; }
&#x9;&#x9;public string LineTwoLabelText { get; set; }
&#x9;&#x9;public string LineTwoContentText { get; set; }
&#x200B;
&#x9;&#x9;// Main text body (notice, information,...)
&#x9;&#x9;public string NoticeText { get; set; }
&#x9;&#x9;// Allows user to pass HTML to _Confirmation using &#x27;ViewBag.NoticeTextHtml&#x27; (will be displayed using &#x27;Html.Raw&#x27;)
&#x9;&#x9;public string NoticeTextHtml { get; set; }
&#x200B;
&#x9;&#x9;// First Button
&#x9;&#x9;public string FirstButtonText { get; set; }
&#x9;&#x9;public string FirstButtonAction { get; set; }
&#x9;&#x9;public string FirstButtonController { get; set; }
&#x200B;
&#x9;&#x9;// Second Button
&#x9;&#x9;public string SecondButtonText { get; set; }
&#x9;&#x9;public string SecondButtonAction { get; set; }
&#x9;&#x9;public string SecondButtonController { get; set; }
&#x200B;
&#x9;&#x9;//  Third Button
&#x9;&#x9;public string ThirdButtonText { get; set; }
&#x9;&#x9;public string ThirdButtonAction { get; set; }
&#x9;&#x9;public string ThirdButtonController { get; set; }
&#x9;}
}</code></pre>
</div>  
<div class="codeArea">  
<h6 class="mt-4">Building The ViewModel</h6>
<pre class="line-numbers language-csharp"><code>// if the username was successfully changed, display confirmation page
if (usernameChanged)
{
&#x9;var cvm = new REFRESHConfirmationViewModel()
&#x9;{
&#x9;&#x9;MessageType = &#x22;success&#x22;,
&#x9;&#x9;ConfirmationAlertText = &#x22;Your username has been updated&#x22;,
&#x9;&#x9;ImageAltText = &#x22;Confirmation&#x22;,
&#x9;&#x9;LineOneLabelText = &#x22;Date:&#x22;,
&#x9;&#x9;LineOneContentText = DateTime.Today.ToString(&#x22;MM/dd/yyyy&#x22;),
&#x9;&#x9;NoticeTextHtml =
&#x9;&#x9;&#x9;HttpUtility.HtmlEncode(
&#x9;&#x9;&#x9;&#x9;&#x22;&#x3C;p class=\&#x22;text-center\&#x22;&#x3E;Your new username is now effectively immediately.&#x3C;/p&#x3E;&#x22;),
&#x9;&#x9;FirstButtonText = &#x22;Go to Dashboard&#x22;,
&#x9;&#x9;FirstButtonAction = &#x22;Index&#x22;,
&#x9;&#x9;FirstButtonController = &#x22;Employer&#x22;,
&#x9;&#x9;SecondButtonText = &#x22;Back to Update My Profile&#x22;,
&#x9;&#x9;SecondButtonAction = &#x22;UpdateMyInformation&#x22;,
&#x9;&#x9;SecondButtonController = &#x22;Employer/MyProfile&#x22;
&#x9;};
&#x200B;
&#x9;return RedirectToAction(&#x22;REFRESHConfirmation&#x22;, &#x22;Common&#x22;, cvm);
}</code></pre>
</div>
<div class="codeArea" hidden>
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;cc-confirmation&#x22;&#x3E;
    &#x3C;div class=&#x22;cc-confirmation-header cc-success&#x22;&#x3E;
        {Confirmation Alert}
    &#x3C;/div&#x3E;
    &#x3C;div class=&#x22;cc-confirmation-body&#x22;&#x3E;
        &#x3C;img src=&#x22;images/icons/cc-confirmation-success.png&#x22; alt=&#x22;Confirmation&#x22;&#x3E;
        &#x3C;h5&#x3E;{Confirmation Title}&#x3C;/h5&#x3E;
        &#x3C;div class=&#x22;cc-confirmation-body-info&#x22;&#x3E;
            &#x3C;p&#x3E;
                {Name: &#x3C;strong&#x3E;Group/Employee&#x3C;/strong&#x3E;}&#x3C;br&#x3E;
                {Date: &#x3C;strong&#x3E;xx/xx/xxxx&#x3C;/strong&#x3E;}
            &#x3C;/p&#x3E;
        &#x3C;/div&#x3E;
        &#x3C;div class=&#x22;cc-confirmation-body-message&#x22;&#x3E;
            &#x3C;p&#x3E;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit repudiandae, ipsum, quae facilis qui minima consectetur animi iure ipsa dolorem explicabo beatae ducimus temporibus velit corporis doloribus natus quaerat quidem. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit repudiandae.&#x3C;/p&#x3E;
        &#x3C;/div&#x3E;
    &#x3C;/div&#x3E;&#x3C;!-- cc-confirmation-body --&#x3E;
    &#x3C;div class=&#x22;cc-confirmation-footer&#x22;&#x3E;
        &#x3C;a href=&#x22;#&#x22; class=&#x22;btn btn-blue&#x22;&#x3E;Primary CTA&#x3C;/a&#x3E;
        &#x3C;a href=&#x22;#&#x22; class=&#x22;btn btn-grey-outline&#x22;&#x3E;Button&#x3C;/a&#x3E;
        &#x3C;a href=&#x22;#&#x22; class=&#x22;btn btn-grey-outline&#x22;&#x3E;Button&#x3C;/a&#x3E;
    &#x3C;/div&#x3E;&#x3C;!-- cc-confirmation-footer --&#x3E;
&#x3C;/div&#x3E;</code></pre>
</div>
<hr class="break">
<h2>Error</h2>
<div class="component-thumbnail">
    <img src="./img/components/confirmation/error.png">
</div>
<div class="codeArea" hidden>
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;cc-confirmation-header cc-error&#x22;&#x3E;
    {Confirmation Alert}
&#x3C;/div&#x3E;
&#x3C;div class=&#x22;cc-confirmation-body&#x22;&#x3E;
    &#x3C;img src=&#x22;images/icons/cc-confirmation-error.png&#x22; alt=&#x22;Confirmation&#x22;&#x3E;
&#x3C;/div&#x3E;&#x3C;!-- cc-confirmation-body --&#x3E;</code></pre>
</div>
<hr class="break">


<h2>Warning</h2>
<div class="component-thumbnail">
    <img src="./img/components/confirmation/warning.png">
</div>
<div class="codeArea" hidden>
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;cc-confirmation-header cc-warning&#x22;&#x3E;
    {Confirmation Alert}
&#x3C;/div&#x3E;
&#x3C;div class=&#x22;cc-confirmation-body&#x22;&#x3E;
    &#x3C;img src=&#x22;images/icons/cc-confirmation-warning.png&#x22; alt=&#x22;Confirmation&#x22;&#x3E;
&#x3C;/div&#x3E;&#x3C;!-- cc-confirmation-body --&#x3E;</code></pre>
</div>

<hr class="break">

<h3>Confirmation Cards Guidelines</h3>
<ul class="mt-4">
	<li>If the body text is 1 line or less, center (add class="text-center" to element passed in as cvm.NoticeTextHtml); otherwise, left-align</li>
	<li>Apply .cc-error or .cc-warning class to .cc-confirmation-header element for error or warning cards.</li>
</ul>

<hr class="break">