<h1>Sub-Header (Page Banner)</h1>
<div class="component-thumbnail">
    <img src="./img/components/navigation/header-banner.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;section id=&#x22;cc-subheader&#x22;&#x3E;
    &#x3C;div class=&#x22;container&#x22;&#x3E;
        &#x3C;div class=&#x22;row align-items-center&#x22;&#x3E;
            &#x3C;div class=&#x22;col-md-8&#x22;&#x3E;
                &#x3C;div class=&#x22;left&#x22;&#x3E;
                    &#x3C;h2&#x3E;Page Title&#x3C;/h2&#x3E;
                    &#x3C;p&#x3E;Lorem ipsum dolor sit amet.&#x3C;/p&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/div&#x3E;
            &#x3C;div class=&#x22;col-md-4&#x22;&#x3E;
                &#x3C;div class=&#x22;cc-subheader__menu&#x22;&#x3E;
                    &#x3C;a href=&#x22;&#x22; class=&#x22;btn primary mr-3&#x22;&#x3E;Button&#x3C;/a&#x3E;
                    &#x3C;div class=&#x22;dropdown dd-transparent&#x22;&#x3E;
                        &#x3C;button class=&#x22;btn secondary dropdown-toggle&#x22; type=&#x22;button&#x22; id=&#x22;dropdownMenuButton&#x22; data-toggle=&#x22;dropdown&#x22; aria-haspopup=&#x22;true&#x22; aria-expanded=&#x22;false&#x22;&#x3E;More&#x3C;/button&#x3E;
                        &#x3C;div class=&#x22;dropdown-menu dropdown-menu-right&#x22; aria-labelledby=&#x22;dropdownMenuButton&#x22;&#x3E;
                            &#x3C;a class=&#x22;dropdown-item&#x22; href=&#x22;#&#x22;&#x3E;Link&#x3C;/a&#x3E;
                        &#x3C;/div&#x3E;
                    &#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/div&#x3E;
        &#x3C;/div&#x3E;
        &#x3C;nav class=&#x22;cc-subheader__tabs&#x22;&#x3E;
            &#x3C;a class=&#x22;nav-link&#x22; href=&#x22;#&#x22;&#x3E;Normal Link&#x3C;/a&#x3E;
            &#x3C;a class=&#x22;nav-link active&#x22; href=&#x22;#&#x22;&#x3E;Active Link&#x3C;/a&#x3E;
            &#x3C;a class=&#x22;nav-link&#x22; href=&#x22;#&#x22;&#x3E;Normal Link&#x3C;/a&#x3E;
            &#x3C;a class=&#x22;nav-link disabled&#x22; href=&#x22;#&#x22;&#x3E;Disabled Link&#x3C;/a&#x3E;
            &#x3C;a class=&#x22;nav-link&#x22; href=&#x22;#&#x22;&#x3E;Normal Link&#x3C;/a&#x3E;
        &#x3C;/nav&#x3E;
    &#x3C;/div&#x3E;
&#x3C;/section&#x3E;</code></pre>
</div>
<hr class="break">
<div class="codeArea">
<h6>C#</h6>
<pre class="line-numbers language-markup"><code>    @* SUBHEADER *@
    &#x3C;section id=&#x22;cc-subheader&#x22;&#x3E;
        &#x3C;div class=&#x22;container&#x22;&#x3E;
            &#x3C;div class=&#x22;row align-items-center&#x22;&#x3E;
                &#x3C;div class=&#x22;col-md-8&#x22;&#x3E;
                    &#x3C;div class=&#x22;left&#x22;&#x3E;
                        &#x3C;h2&#x3E;Groups&#x3C;/h2&#x3E;
                        &#x3C;p&#x3E;@ResHelper.GetString(&#x22;Broker.Groups&#x22;)&#x3C;/p&#x3E;
                    &#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
                &#x3C;div class=&#x22;col-md-4&#x22;&#x3E;
                    &#x3C;div class=&#x22;cc-subheader__menu&#x22;&#x3E;
                        &#x3C;a href=&#x27;@Url.Action(&#x22;QuoteEntry&#x22;, &#x22;Broker/Quoting&#x22;)&#x27; class=&#x22;btn primary mr-3&#x22;&#x3E;Create a New Quote&#x3C;/a&#x3E;
                        &#x3C;div class=&#x22;dropdown dd-transparent&#x22;&#x3E;
                            &#x3C;button class=&#x22;btn secondary dropdown-toggle&#x22; type=&#x22;button&#x22; id=&#x22;dropdownMenuButton&#x22; data-toggle=&#x22;dropdown&#x22; aria-haspopup=&#x22;true&#x22; aria-expanded=&#x22;false&#x22;&#x3E;More&#x3C;/button&#x3E;
                            &#x3C;div class=&#x22;dropdown-menu dropdown-menu-right&#x22; aria-labelledby=&#x22;dropdownMenuButton&#x22;&#x3E;
                                &#x3C;a class=&#x22;dropdown-item&#x22; href=&#x22;~/Broker/Quoting/NewHireQuote&#x22;&#x3E;Create a New Hire Quote&#x3C;/a&#x3E;
                                &#x3C;a class=&#x22;dropdown-item&#x22; href=&#x22;~/Broker/Commissions&#x22;&#x3E;Commissions&#x3C;/a&#x3E;
                            &#x3C;/div&#x3E;
                        &#x3C;/div&#x3E;
                    &#x3C;/div&#x3E;
                &#x3C;/div&#x3E;
            &#x3C;/div&#x3E;
            &#x3C;nav class=&#x22;cc-subheader__tabs&#x22;&#x3E;
                &#x3C;a class=&#x22;nav-link @Html.REFRESH__ActivePage(&#x22;Groups&#x22;, &#x22;InforceGroups&#x22;)&#x22; href=&#x22;@Url.Action(&#x22;InforceGroups&#x22;, &#x22;Groups&#x22;)&#x22;&#x3E;Inforce&#x3C;/a&#x3E;
                &#x3C;a class=&#x22;nav-link @Html.REFRESH__ActivePage(&#x22;Groups&#x22;, &#x22;LatePays&#x22;)&#x22; href=&#x22;@Url.Action(&#x22;LatePays&#x22;, &#x22;Groups&#x22;)&#x22;&#x3E;Late Pays&#x3C;/a&#x3E;
                &#x3C;a class=&#x22;nav-link @Html.REFRESH__ActivePage(&#x22;Groups&#x22;, &#x22;Renewals&#x22;)&#x22; href=&#x22;@Url.Action(&#x22;Renewals&#x22;, &#x22;Groups&#x22;)&#x22;&#x3E;Renewals&#x3C;/a&#x3E;
                &#x3C;a class=&#x22;nav-link @Html.REFRESH__ActivePage(&#x22;Groups&#x22;, &#x22;TerminatedGroups&#x22;)&#x22; href=&#x22;@Url.Action(&#x22;TerminatedGroups&#x22;, &#x22;Groups&#x22;)&#x22;&#x3E;Terminated&#x3C;/a&#x3E;
            &#x3C;/nav&#x3E;
        &#x3C;/div&#x3E;
    &#x3C;/section&#x3E;</code></pre>
</div>