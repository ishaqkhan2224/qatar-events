<h1>Alerts</h1>
<div class="component-thumbnail mt-3">
    <img src="./img/components/utilities/alerts.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;div class=&#x22;alert cc-success&#x22; role=&#x22;alert&#x22;&#x3E;.cc-success&#x3C;/div&#x3E;
&#x3C;div class=&#x22;alert cc-warning&#x22; role=&#x22;alert&#x22;&#x3E;.cc-warning&#x3C;/div&#x3E;
&#x3C;div class=&#x22;alert cc-error&#x22; role=&#x22;alert&#x22;&#x3E;.cc-error&#x3C;/div&#x3E;
&#x3C;div class=&#x22;alert cc-info&#x22; role=&#x22;alert&#x22;&#x3E;.cc-info&#x3C;/div&#x3E;
&#x3C;div class=&#x22;alert alert-secondary&#x22; role=&#x22;alert&#x22;&#x3E;.alert-secondary&#x3C;/div&#x3E;
&#x3C;div class=&#x22;alert alert-light&#x22; role=&#x22;alert&#x22;&#x3E;.alert-light&#x3C;/div&#x3E;
&#x3C;div class=&#x22;alert alert-dark&#x22; role=&#x22;alert&#x22;&#x3E;.alert-dark&#x3C;/div&#x3E;</code></pre>
</div>
<hr class="break">
<h1>Badges</h1>
<div class="component-thumbnail mt-3">
    <img src="./img/components/utilities/badges.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;h1&#x3E;H1 Headline &#x3C;span class=&#x22;badge cc-success&#x22;&#x3E;New&#x3C;/span&#x3E;&#x3C;/h1&#x3E;
&#x3C;h2&#x3E;H2 Headline &#x3C;span class=&#x22;badge cc-error&#x22;&#x3E;New&#x3C;/span&#x3E;&#x3C;/h2&#x3E;
&#x3C;h3&#x3E;H3 Headline &#x3C;span class=&#x22;badge cc-warning&#x22;&#x3E;New&#x3C;/span&#x3E;&#x3C;/h3&#x3E;
&#x3C;h4&#x3E;H4 Headline &#x3C;span class=&#x22;badge cc-info&#x22;&#x3E;New&#x3C;/span&#x3E;&#x3C;/h4&#x3E;
&#x3C;p&#x3E;Paragraph&#x3C;span class=&#x22;badge cc-success&#x22;&#x3E;&#x3C;i class=&#x22;far fa-check&#x22; aria-hidden=&#x22;true&#x22;&#x3E;&#x3C;/i&#x3E; Approved&#x3C;/span&#x3E;&#x3C;/p&#x3E;
&#x3C;p&#x3E;Paragraph&#x3C;span class=&#x22;badge cc-error&#x22;&#x3E;&#x3C;i class=&#x22;far fa-times&#x22;&#x3E;&#x3C;/i&#x3E;&#x3C;/i&#x3E; Denied&#x3C;/span&#x3E;&#x3C;/p&#x3E;
&#x3C;p&#x3E;Paragraph&#x3C;span class=&#x22;badge cc-warning&#x22;&#x3E;&#x3C;i class=&#x22;far fa-clock&#x22;&#x3E;&#x3C;/i&#x3E;&#x3C;/i&#x3E;&#x3C;/i&#x3E; In Process&#x3C;/span&#x3E;&#x3C;/p&#x3E;
&#x3C;p&#x3E;Paragraph&#x3C;span class=&#x22;badge cc-info&#x22;&#x3E;&#x3C;i class=&#x22;far fa-info-circle&#x22;&#x3E;&#x3C;/i&#x3E;&#x3C;/i&#x3E; New&#x3C;/span&#x3E;&#x3C;/p&#x3E;</code></pre>
</div>
<hr class="break">
<h1>List (Ordered)</h1>
<div class="component-thumbnail mt-3">
    <img src="./img/components/utilities/ol.png">
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;ol class=&#x22;cc-ol&#x22;&#x3E;
  &#x3C;li&#x3E;Lorem ipsum dolor sit amet&#x3C;/li&#x3E;
  &#x3C;li&#x3E;Lorem ipsum dolor sit amet&#x3C;/li&#x3E;
  &#x3C;li&#x3E;Lorem ipsum dolor sit amet&#x3C;/li&#x3E;
  &#x3C;li&#x3E;Lorem ipsum dolor sit amet&#x3C;/li&#x3E;
&#x3C;/ol&#x3E;</code></pre>
</div>
<hr class="break">
<h1>List (Unordered)</h1>
<div class="component-thumbnail mt-3">
    <img src="./img/components/utilities/ul.png">
    <p class="my-3"></p>
    <img src="./img/components/utilities/ul-plain.png">
</div>
<div class="component-thumbnail mt-3">
    
</div>
<div class="codeArea">
    <h6>HTML</h6>
    <pre class="line-numbers language-markup"><code>&#x3C;ul class=&#x22;cc-ul&#x22;&#x3E;
  &#x3C;li&#x3E;Lorem ipsum dolor sit amet&#x3C;/li&#x3E;
  &#x3C;li&#x3E;Lorem ipsum dolor sit amet&#x3C;/li&#x3E;
  &#x3C;li&#x3E;Lorem ipsum dolor sit amet&#x3C;/li&#x3E;
  &#x3C;li&#x3E;Lorem ipsum dolor sit amet&#x3C;/li&#x3E;
&#x3C;/ul&#x3E;

&#x3C;ul class=&#x22;cc-ul&#x22;&#x3E;
  &#x3C;li class=&#x22;plain&#x22;&#x3E;Lorem ipsum dolor sit amet&#x3C;/li&#x3E;
  &#x3C;li class=&#x22;plain&#x22;&#x3E;Lorem ipsum dolor sit amet&#x3C;/li&#x3E;
  &#x3C;li class=&#x22;plain&#x22;&#x3E;Lorem ipsum dolor sit amet&#x3C;/li&#x3E;
  &#x3C;li class=&#x22;plain&#x22;&#x3E;Lorem ipsum dolor sit amet&#x3C;/li&#x3E;
&#x3C;/ul&#x3E;</code></pre>
</div>
<hr class="break">