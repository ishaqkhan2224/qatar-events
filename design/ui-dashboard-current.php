<?php include("inc/_config.php");?>
<?php include("inc/head.php");?>

<body id="comps">
<ul id="toggle">
    <span>Toggle</span>
    <li><a href="ui-dashboard">Theme #1</a></li>
    <li><a href="ui-dashboard-light">Theme #2</a></li>
    <li><a href="ui-dashboard-current">Dev</a></li>
</ul>
<section id="mockup">
    <div class="container-fluid text-center">
        <img src="images/screenshots/dashboard.png" class="img-fluid">
    </div>
</section>

</body>
</html>