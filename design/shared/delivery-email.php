    <div class="container">
        <div class="d-flex">
            <label>You have multiple agencies listed under your license number. Please select which agency you would like to use for this quote:</label>
            <select name="" class="form-control">
                <option value="asdf">Widget Inc.</option>
                <option value="asdf">Widget Inc.</option>
                <option value="asdf">Widget Inc.</option>
            </select>
        </div>
        <p>Please review your information below. If you need to make changes, contact our Finance Customer Service team at <a href="#">commissions@calchoice.com</a> or (714) 567-4390.
        </p>
        <div class="row info-section">
            <div class="col-md-3">
                <div class="info-box d-flex">
                    <div class="_icon"><i class="fal fa-user"></i></div>
                    <div class="_disc">
                        <h4>Broker Information</h4>
                        <p>Broker No. 80366 <br>
                            Aaron Watts</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 border-left-md">
                <div class="info-box d-flex">
                    <div class="_icon"><i class="fal fa-map-marker-alt"></i></div>
                    <div class="_disc">
                        <h4>Contact Information</h4>
                        <p>Address: 721 South Parker <br>
                            City: Orange <br>
                            State: CA ZIP: 92868 <br>
                            Phone: (714) 567-4505 <br>
                            Email: <a href="#">johnny@broker.com</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 border-left-md">
                <div class="info-box d-flex">
                    <div class="_icon"><i class="fal fa-user"></i></div>
                    <div class="_disc">
                        <h4>Inside Sales Rep</h4>
                        <p>Don Hutchinson <br>
                            Phone: (800) 542-4218 <br>
                            Ext: 4431 <br>
                            Email: <a href="#">insales@calchoice.com</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 border-left-md">
                <div class="info-box d-flex">
                    <div class="_icon"><i class="fal fa-user"></i></div>
                    <div class="_disc">
                        <h4>Outside Sales Rep</h4>
                        <p>Johnny Appleseed <br>
                            Phone: (800) 542-4218 <br>
                            Ext: 4431 <br>
                            Email: <a href="#">outsales@calchoice.com</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="delivery-email">
            <label>Delivery Email:</label>
            <input type="text" value="john.doe@gmail.com; jane.doe@hotmail.com" class="form-control">
            <p>If this quote is being sent to multiple email addresses, separate each address with a semicolon(;)</p>
        </div>
    </div>