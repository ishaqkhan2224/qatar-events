<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=1280, initial-scale=1, shrink-to-fit=no">
    <title>CC Concept: Quotes</title>
    <link rel="stylesheet" id="css-bootstrap" href="./css/lib/bootstrap.min.css" media="all">
    <link rel="stylesheet" id="css-cores" href="./css/styles-optional.css" media="all">
    <script src="https://kit.fontawesome.com/d760a10022.js"></script>
</head>
<body id="cc-broker">
<ul id="toggle">
    <span>Toggle</span>
    <li><a href="ui-quotes-tab">Theme #1</a></li>
    <li><a href="ui-quotes">Theme #1 (tab)</a></li>
    <li><a href="ui-quotes-light">Theme #2</a></li>
    <li><a href="ui-quotes-current">Dev</a></li>
</ul>
<?php include "inc/nav.php"; ?>
<section id="banner">
    <div class="container">
      <div class="banner__wrapper">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Quotes</h2>
                    <p>Create new quotes and manage your existing quotes.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="banner-menu">
                    <a href="" class="btn primary mr-3">Create a New Quote</a>
                    <div class="dropdown dd-transparent">
                        <button class="btn secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Order ID Cards</a>
                            <a class="dropdown-item" href="#">View Invoices</a>
                            <a class="dropdown-item" href="#">Commissions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</section>

<section class="cc-body">
    <div class="container">
        <nav class="nav">
            <a class="nav-link" href="#">All Quotes</a>
            <a class="nav-link" href="#">Initial</a>
            <a class="nav-link active" href="#">Enrollment</a>
            <a class="nav-link" href="#">Unfinished</a>
        </nav>
        <table class="table cc-table">
            <thead>
            <tr>
                <th scope="col">
                    GROUP NAME
                    <div class="th-sort">
                        <a href=""><i class="far fa-angle-up"></i></a>
                        <a href=""><i class="far fa-angle-down"></i></a>
                    </div>
                </th>

                <th scope="col" class="text-center">
                    QUOTE ID
                    <div class="th-sort">
                        <a href=""><i class="far fa-angle-up"></i></a>
                        <a href=""><i class="far fa-angle-down"></i></a>
                    </div>
                </th>
                <th scope="col" class="text-center">
                    LAST MODIFIED
                    <div class="th-sort">
                        <a href=""><i class="far fa-angle-up"></i></a>
                        <a href=""><i class="far fa-angle-down"></i></a>
                    </div>
                </th>
                <th scope="col" class="text-center">
                    STATUS
                </th>
                <th scope="col" class="text-center">
                    EFFECTIVE DATE
                    <div class="th-sort">
                        <a href=""><i class="far fa-angle-up"></i></a>
                        <a href=""><i class="far fa-angle-down"></i></a>
                    </div>
                </th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Applied Business Dynamics</td>
                <td class="text-center"><a href="">In Progress</a></td>
                <td class="text-center">07/23/2019 00:00 AM</td>
                <td class="text-center">Initial</td>
                <td class="text-center">08/01/2019</td>
                <td class="td-action">
                    <div class="dropdown td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Edit Quote</a>
                            <a class="dropdown-item" href="#">Convert to Enrollment</a>
                            <a class="dropdown-item" href="#">Start Online Enrollment</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>California Cider Company</td>
                <td class="text-center"><a href="">In Progress</a></td>
                <td class="text-center">07/23/2019 00:00 AM</td>
                <td class="text-center">Initial</td>
                <td class="text-center">08/01/2019</td>
                <td class="td-action">
                    <div class="dropdown td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Edit Quote</a>
                            <a class="dropdown-item" href="#">Convert to Enrollment</a>
                            <a class="dropdown-item" href="#">Start Online Enrollment</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Chase Bank Global</td>
                <td class="text-center"><a href="">In Progress</a></td>
                <td class="text-center">07/23/2019 00:00 AM</td>
                <td class="text-center">Initial</td>
                <td class="text-center">08/01/2019</td>
                <td class="td-action">
                    <div class="dropdown td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Edit Quote</a>
                            <a class="dropdown-item" href="#">Convert to Enrollment</a>
                            <a class="dropdown-item" href="#">Start Online Enrolment</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>KP America Factory</td>
                <td class="text-center"><a href="">In Progress</a></td>
                <td class="text-center">07/23/2019 00:00 AM</td>
                <td class="text-center">Initial</td>
                <td class="text-center">08/01/2019</td>
                <td class="td-action">
                    <div class="dropdown td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Edit Quote</a>
                            <a class="dropdown-item" href="#">Convert to Enrollment</a>
                            <a class="dropdown-item" href="#">Start Online Enrolment</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>JP Morgan Bank</td>
                <td class="text-center"><a href="">In Progress</a></td>
                <td class="text-center">07/23/2019 00:00 AM</td>
                <td class="text-center">Initial</td>
                <td class="text-center">08/01/2019</td>
                <td class="td-action">
                    <div class="dropdown td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Edit Quote</a>
                            <a class="dropdown-item" href="#">Convert to Enrollment</a>
                            <a class="dropdown-item" href="#">Start Online Enrolment</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>George Washington University</td>
                <td class="text-center"><a href="">In Progress</a></td>
                <td class="text-center">07/23/2019 00:00 AM</td>
                <td class="text-center">Initial</td>
                <td class="text-center">08/01/2019</td>
                <td class="td-action">
                    <div class="dropdown td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Edit Quote</a>
                            <a class="dropdown-item" href="#">Convert to Enrollment</a>
                            <a class="dropdown-item" href="#">Start Online Enrolment</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>UCLA Health</td>
                <td class="text-center"><a href="">In Progress</a></td>
                <td class="text-center">07/23/2019 00:00 AM</td>
                <td class="text-center">Initial</td>
                <td class="text-center">08/01/2019</td>
                <td class="td-action">
                    <div class="dropdown td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Edit Quote</a>
                            <a class="dropdown-item" href="#">Convert to Enrollment</a>
                            <a class="dropdown-item" href="#">Start Online Enrolment</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Zebra Inc.</td>
                <td class="text-center"><a href="">In Progress</a></td>
                <td class="text-center">07/23/2019 00:00 AM</td>
                <td class="text-center">Initial</td>
                <td class="text-center">08/01/2019</td>
                <td class="td-action">
                    <div class="dropdown td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Edit Quote</a>
                            <a class="dropdown-item" href="#">Convert to Enrollment</a>
                            <a class="dropdown-item" href="#">Start Online Enrolment</a>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>


        <div class="d-flex justify-content-between cc-pagination">
            <div class="left">
            <p>Showing 1-10 of 100 Results &nbsp;|&nbsp; Results to Show:</p>
                <ul class="row-numbers">
                    <li><a class="active" href="">10</a></li>
                    <li><a href="">20</a></li>
                    <li><a href="">30</a></li>
                    <li><a href="">All</a></li>
                </ul>
            </div>
            <div class="right">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link disabled" href="#">Previous</a></li>
                        <li class="page-item"><a class="page-link active" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">...</a></li>
                        <li class="page-item"><a class="page-link" href="#">9</a></li>
                        <li class="page-item"><a class="page-link" href="#">10</a></li>
                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                    </ul>
                </nav>
            </div>
        </div>

    </div>
</section>

<?php include "inc/footer-lean.php"; ?>