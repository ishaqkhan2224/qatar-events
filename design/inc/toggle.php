<style>
	ul#toggle-css {
		z-index:5; margin:0; padding:0; position: fixed; top: 40%; left: 0px; 
		background-color: rgba(88,88,88,0.9); padding: 2px 8px 8px;
	}
	ul#toggle-css span {padding: 12px 5px 2px; font-size: 11pt; font-weight: 400; display: block; color: rgba(255,255,255,0.6);}
	ul#toggle-css li {list-style: none; display: block;}
	ul#toggle-css li, ul#toggle-css li a {font-size: 9pt; text-transform: uppercase; padding: 0 3px; color: #fff;}
	ul#toggle-css li a.active {font-weight: 600; color: yellow;}
</style>
<ul id="toggle-css" class="parent">
	<span>Theme #1</span>
	<li><a href="javascript:;" data-theme="standard-blue-theme" class="active">Broker</a></li>
	<li><a href="javascript:;" data-theme="standard-orange-theme">Employer</a></li>
	<li><a href="javascript:;" data-theme="standard-green-theme">Employee</a></li>
	<li><a href="javascript:;" data-theme="standard-theme">~Neutral~</a></li>
	<span>Theme #2</span>
	<li><a href="javascript:;" data-theme="blue-theme">Broker</a></li>
	<li><a href="javascript:;" data-theme="orange-theme">Employer</a></li>
	<li><a href="javascript:;" data-theme="green-theme">Employee</a></li>
	<li><a href="javascript:;" data-theme="neutral-theme tabs">~Neutral~</a></li>
	<!-- <li><a href="javascript:;" data-theme="neutral-theme">~Neutral~</a></li> -->
	<span>Theme #3</span>
	<li><a href="javascript:;" data-theme="banner-blue-theme">Broker</a></li>
	<li><a href="javascript:;" data-theme="banner-orange-theme">Employer</a></li>
	<li><a href="javascript:;" data-theme="banner-green-theme">Employee</a></li>
	<li><a href="javascript:;" data-theme="banner-theme">~Neutral~</a></li>
	<!-- <span>Misc</span> -->
	<!-- <li><a href="javascript:;" data-theme="banner-theme option">Hugo</a></li> -->
	<!-- <li><a href="javascript:;" data-theme="neutral-theme option">Kalup</a></li> -->
	<!-- <li>|</li>
	<span id="toggle-banner">
	<li><a href="javascript:;">Toggle Banner</a></li>
	</span> -->
</ul>