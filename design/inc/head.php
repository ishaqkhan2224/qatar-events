<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=1280, initial-scale=1, shrink-to-fit=no">
    <title><?php print $PAGE_TITLE;?></title>
    <link rel="stylesheet" id="css-bootstrap" href="/cc/design/css/lib/bootstrap.min.css" media="all">
    <link rel="stylesheet" id="css-cores" href="/cc/design/css/styles.css" media="all">
    <script src="https://kit.fontawesome.com/d760a10022.js"></script>
</head>