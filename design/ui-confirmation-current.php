<?php include("inc/_config.php");?>
<?php include("inc/head.php");?>

<body id="comps">
<ul id="toggle">
    <span>Toggle</span>
    <li><a href="ui-confirmation">Theme #1</a></li>
    <li><a href="ui-confirmation-light">Theme #2</a></li>
    <li><a href="ui-confirmation-current">Dev</a></li>
</ul>
<section id="mockup">
    <div class="container-fluid text-center">
        <img src="images/screenshots/confirmation.png" class="img-fluid">
    </div>
</section>

</body>
</html>