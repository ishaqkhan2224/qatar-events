<?php include "inc/_config.php"; ?>
<?php include "inc/toggle.php"; ?>
<?php include "inc/head.php"; ?>

    <body id="cc-broker">
<?php include "inc/nav.php"; ?>
    <section id="banner" class="top-banner">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="left">
                    <h2>Page Title</h2>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                            commodo.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="right">
                    <a href="" class="btn btn-white mr-3">
                            <i class="fal fa-plus-circle mr-2"></i> Create a New Quote
                        </a>
                        <div class="dropdown dd-transparent">
                    <button class="btn btn-white-outline dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        More
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Edit Quote</a>
                        <a class="dropdown-item" href="#">Convert to Enrolment</a>
                        <a class="dropdown-item" href="#">Start Online Enrolment</a>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="nav-steps">
            <div class="container">
                <a class="prev" href="#"><span>1</span> First Step</a>
                <a class="prev" href="#"><span>2</span> Second Step</a>
                <a class="active" href="#"><span>3</span> Third Step</a>
                <a href="#"><span>4</span> Fourth Step</a>
                <a href="#"><span>5</span> Fifth Step</a>
            </div>
        </nav>
    </section>

    <section class="cc-body">
        <div class="container">
            <table class="table custom-table">
                <thead>
                <tr>
                    <th scope="col">
                        GROUP NAME
                        <div class="th-sort">
                            <a href=""><i class="far fa-angle-up"></i></a>
                            <a href=""><i class="far fa-angle-down"></i></a>
                        </div>
                    </th>

                    <th scope="col">
                        QUOTE ID
                        <div class="th-sort">
                            <a href=""><i class="far fa-angle-up"></i></a>
                            <a href=""><i class="far fa-angle-down"></i></a>
                        </div>
                    </th>
                    <th scope="col">
                        LAST MODIFIED
                        <div class="th-sort">
                            <a href=""><i class="far fa-angle-up"></i></a>
                            <a href=""><i class="far fa-angle-down"></i></a>
                        </div>
                    </th>
                    <th scope="col">
                        STATUS
                    </th>
                    <th scope="col">
                        EFFECTIVE DATE
                        <div class="th-sort">
                            <a href=""><i class="far fa-angle-up"></i></a>
                            <a href=""><i class="far fa-angle-down"></i></a>
                        </div>
                    </th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>AA</td>
                    <td><a href="">96820</a></td>
                    <td>07/23/2019 00:00 AM</td>
                    <td>Initial</td>
                    <td>08/01/2019</td>
                    <td class="td-action">
                        <div class="dropdown td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrolment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>SS</td>
                    <td><a href="">96820</a></td>
                    <td>07/23/2019 00:00 AM</td>
                    <td>Initial</td>
                    <td>08/01/2019</td>
                    <td class="td-action">
                        <div class="dropdown td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrolment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>GGC</td>
                    <td><a href="">96820</a></td>
                    <td>07/23/2019 00:00 AM</td>
                    <td>Initial</td>
                    <td>08/01/2019</td>
                    <td class="td-action">
                        <div class="dropdown td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrolment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>KK</td>
                    <td><a href="">96820</a></td>
                    <td>07/23/2019 00:00 AM</td>
                    <td>Initial</td>
                    <td>08/01/2019</td>
                    <td class="td-action">
                        <div class="dropdown td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrolment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>AA</td>
                    <td><a href="">96820</a></td>
                    <td>07/23/2019 00:00 AM</td>
                    <td>Initial</td>
                    <td>08/01/2019</td>
                    <td class="td-action">
                        <div class="dropdown td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrolment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>SS</td>
                    <td><a href="">96820</a></td>
                    <td>07/23/2019 00:00 AM</td>
                    <td>Initial</td>
                    <td>08/01/2019</td>
                    <td class="td-action">
                        <div class="dropdown td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrolment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>GGC</td>
                    <td><a href="">96820</a></td>
                    <td>07/23/2019 00:00 AM</td>
                    <td>Initial</td>
                    <td>08/01/2019</td>
                    <td class="td-action">
                        <div class="dropdown td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrolment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>KK</td>
                    <td><a href="">96820</a></td>
                    <td>07/23/2019 00:00 AM</td>
                    <td>Initial</td>
                    <td>08/01/2019</td>
                    <td class="td-action">
                        <div class="dropdown td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrolment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>GGC</td>
                    <td><a href="">96820</a></td>
                    <td>07/23/2019 00:00 AM</td>
                    <td>Initial</td>
                    <td>08/01/2019</td>
                    <td class="td-action">
                        <div class="dropdown td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrolment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>KK</td>
                    <td><a href="">96820</a></td>
                    <td>07/23/2019 00:00 AM</td>
                    <td>Initial</td>
                    <td>08/01/2019</td>
                    <td class="td-action">
                        <div class="dropdown td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrolment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>


            <div class="d-flex justify-content-between custom-pagination">
                <div class="left">
                <p>Showing 1-10 of 100 Results &nbsp;|&nbsp; Results to Show:</p>
                    <ul class="row-numbers">
                        <li><a class="active" href="">10</a></li>
                        <li><a href="">20</a></li>
                        <li><a href="">30</a></li>
                        <li><a href="">All</a></li>
                    </ul>
                </div>
                <div class="right">
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link disabled" href="#">Previous</a></li>
                            <li class="page-item"><a class="page-link active" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">...</a></li>
                            <li class="page-item"><a class="page-link" href="#">9</a></li>
                            <li class="page-item"><a class="page-link" href="#">10</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
    </section>

    <section class="cc-controls">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Cancel</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Print & Preview</a>
                    <a href="#" class="btn-link">Save & Exit</a>
                    <a href="#" class="btn">NEXT</a>
                </div>
            </div>
        </div>
    </section>
<?php include "inc/footer.php"; ?>