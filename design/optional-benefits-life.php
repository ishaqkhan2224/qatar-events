<?php include "inc/_config.php"; ?>
<?php include "inc/head.php"; ?>
<body id="cc-broker">


<section class="cc-body">
    <div class="container">
        <h3>Please select from one of the following life insurance options:</h3>

        <div class="row">
            <div class="col d-flex">
                <div class="option-box">
                    <a class="toggleLifeSelection" href="javascript:;" data-show="lifeSchedule">
                        <div class="top">
                            <h4>Flat Amount</h4>
                            <p>With a Flat Amount, all Employees receive the same Life benefit amount.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col d-flex">
                <div class="option-box">
                    <a class="toggleLifeSelection" href="javascript:;" data-show="lifeFlat">
                        <div class="top">
                            <h4>Life Schedule</h4>
                            <p>A Group can offer different Life benefit amounts for various employee classifications (up to 4 classifications with the highest being no more than 2.5 times the lowest amount).</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="contribution-description">
                    <div class="row">
                        <div class="col text-center">
                            <h3>Life Insurance Requirements (Guaranteed Issued Amounts)</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th scope="col">Eligible Employees</th>
                                  <th scope="col">Minimum</th>
                                  <th scope="col">Maximum</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>1-10</th>
                                  <td>$10,000</td>
                                  <td>$25,000</td>
                                </tr>
                                <tr>
                                  <td>11-25</th>
                                  <td>$10,000</td>
                                  <td>$50,000</td>
                                </tr>
                                <tr>
                                  <td>26-50</th>
                                  <td>$10,000</td>
                                  <td>$75,000</td>
                                </tr>
                                <tr>
                                  <td>51+</th>
                                  <td>$10,000</td>
                                  <td>$100,000</td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                        <div class="col-12">
                            <p>Employers may offer the same amount of life insurance to each employee or establish up to 4 employee classifications with different life insurance amounts for each classification. Insurance amounts are offered in increments of $5,000. When classifications are used, the highest amount of insurance can be no greater than 2.5 times the lowest amount of insurance.</p>
                            <p>When life insurance is elected, the employer is required to pay 100% of the premium and 100% participation is required. ALL employees eligible for medical coverage must enroll in life insurance (even those who have decided to waive medical coverage).</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="contribution-selection" id="lifeFlat">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Life Schedule</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <select class="form-control">
                                <option>Please make a selection...</option>
                                <option value="">$10,000</option>
                                <option value="" selected="selected">$15,000</option>
                                <option value="">$20,000</option>
                                <option value="">$25,000</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control">
                                <option>Please make a selection...</option>
                                <option value="" selected="selected">Hourly</option>
                                <option value="">Salary</option>
                                <option value="">Management</option>
                                <option value="">Executive</option>
                                <option value="">Owner</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <select class="form-control">
                                <option>Please make a selection...</option>
                                <option value="">$10,000</option>
                                <option value="">$15,000</option>
                                <option value="">$20,000</option>
                                <option value="">$25,000</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control">
                                <option>Please make a selection...</option>
                                <option value="">Hourly</option>
                                <option value="" selected="selected">Salary</option>
                                <option value="">Management</option>
                                <option value="">Executive</option>
                                <option value="">Owner</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <select class="form-control">
                                <option>Please make a selection...</option>
                                <option value="">$10,000</option>
                                <option value="">$15,000</option>
                                <option value="">$20,000</option>
                                <option value="">$25,000</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control">
                                <option>Please make a selection...</option>
                                <option value="">Hourly</option>
                                <option value="">Salary</option>
                                <option value="" selected="selected">Management</option>
                                <option value="">Executive</option>
                                <option value="">Owner</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <select class="form-control">
                                <option>Please make a selection...</option>
                                <option value="">$10,000</option>
                                <option value="">$15,000</option>
                                <option value="">$20,000</option>
                                <option value="">$25,000</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control">
                                <option>Please make a selection...</option>
                                <option value="">Hourly</option>
                                <option value="">Salary</option>
                                <option value="">Management</option>
                                <option value="">Executive</option>
                                <option value="" selected="selected">Owner</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="contribution-selection" id="lifeSchedule">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Flat Amount</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <select class="form-control">
                                <option>Please make a selection...</option>
                                <option value="">$15,000</option>
                                <option value="">$20,000</option>
                                <option value="">$25,000</option>
                                <option value="">$30,000</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

    </div>
</section>

<!-- Javascript -->
<script src="js/lib/jquery-3.3.1.min.js"></script>
<script src="js/lib/bootstrap.bundle.min.js"></script>
<script src="js/main.js"></script>
<style>
    .contribution-selection, .contribution-description {display: none;}
    .contribution-selection.active {display: block;}
</style>
<script>
    jQuery(document).ready(function ($) {

        $('.toggleLifeSelection').on('click', function (e) {
            e.preventDefault();

            $('.contribution-description').show();
            $('.option-box,.contribution-selection').removeClass('active');

            $(this).parent('div').addClass('active');
            var show = $(this).attr('data-show');
            $('#' + show).addClass('active');
        });

    });
</script>
</body>
</html>

