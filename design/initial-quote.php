<?php include("inc/_config.php");?>
<?php include("inc/head.v1.php");?>
<body>
<header class="demo-header">Header</header>
<section class="banner">
    <div class="container">
        <h2>New Quote</h2>
        <p>Below is a summary of what’s included in your initial quote. Please select any optional benefits and confirm your information before you request delivery of your quote.</p>
        <nav class="nav">
            <a class="nav-link" href="#">Group Info</a>
            <a class="nav-link" href="#">Census</a>
            <a class="nav-link active" href="#">Delivery</a>
        </nav>
    </div>
</section>

<section class="cc-body">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>Initial Quote Includes the Following Sections:</h3>
                <div class="quote-includes">
                    <div class="row">
                        <div class="col">
                            <i class="fal fa-list-alt"></i>
                            <h4>CaliforniaChoice<br>Overview </h4></div>
                        <div class="col">
                            <i class="fab fa-wpforms"></i>
                            <h4>Census</h4>
                        </div>
                        <div class="col">
                            <i class="fal fa-money-check-edit-alt"></i>
                            <h4>Medical Budget<br>Defined Contribution</h4>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col">
                            <i class="fal fa-files-medical"></i>
                            <h4>Ancillary Rate<br>Summaries</h4>
                        </div>
                        <div class="col">
                            <i class="fal fa-clipboard-user"></i>
                            <h4>Sample Employee<br>Worksheet</h4>
                        </div>
                        <div class="col">
                            <i class="fal fa-file-invoice-dollar"></i>
                            <h4>Sample Invoice</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-5">
                
                <div class="quote-optional pl-0">
                    <div class="d-flex">
                        <div class="_left"><h4>Optional Selections for Your Quote</h4></div>
                        <div class="_right">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Select All Optional Benefits</label>
                            </div>
                        </div>
                    </div>
                    <ul>
                        <li>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck2">
                                <label class="custom-control-label" for="customCheck2">Medical Benefit Summaries</label>
                            </div>
                        </li>
                        <li class="active">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck3" checked="checked">
                                <label class="custom-control-label" for="customCheck3">Ancillary Benefit Summaries (Dental, Vision, Chiro & Acupuncture)</label>
                            </div>
                        </li>
                        <li>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck4">
                                <label class="custom-control-label" for="customCheck4">Employee & Dependent Rates</label>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php include("shared/delivery-email.php");?>
</section>
<section class="cc-controls">
    <div class="container">
        <div class="d-flex align-items-center">
            <div class="left">
                <a href="#" class="btn-link">Cancel</a>
            </div>
            <div class="right">
                <a href="#" class="btn-link">Print & Preview</a>
                <a href="#" class="btn-link">Save & Exit</a>
                <a href="#" class="btn">Deliver Now</a>
            </div>
        </div>
    </div>
</section>
<?php include("inc/footer.php");?>
