<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=1280, initial-scale=1, shrink-to-fit=no">
    <title>CC Concept: Quote Entry</title>
    <link rel="stylesheet" id="css-bootstrap" href="./css/lib/bootstrap.min.css" media="all">
    <link rel="stylesheet" id="css-cores" href="./css/styles.css" media="all">
    <script src="https://kit.fontawesome.com/d760a10022.js"></script>
</head>
<body id="cc-broker">
<ul id="toggle">
    <span>Toggle</span>
    <li><a href="ui-quote-entry">Theme #1</a></li>
    <li><a href="ui-quote-entry-light">Theme #2</a></li>
    <li><a href="ui-quote-entry-current">Dev</a></li>
</ul>
<?php include "inc/nav.php"; ?>
<section id="banner">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>New Quote</h2>
                </div>
            </div>
        </div>
    </div>
    <nav class="nav-steps">
        <div class="container">
            <a class="active" href="#"><span>1</span> Group Info</a>
            <a href="#"><span>2</span> Census</a>
            <a href="#"><span>3</span> Delivery</a>
        </div>
    </nav>
</section>

<section class="cc-body">
    <div class="container">
        <div class="box bg-white">
            <div class="row">
                <div class="col-md-8">
                    <h6 class="mb-3 text-uppercase">Group Information</h6>
                    <form action="" class="quote-entry-form">
                        <div class="form-group">
                            <label>Group Name</label>
                            <input type="text" class="form-control" value="Widget Inc.">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Zip Code</label>
                                    <input type="text" class="form-control" value="92879">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Country</label>
                                    <input type="text" class="form-control" value="Riverside">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>SIC Code</label>
                                    <input type="text" class="form-control" value="0010">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nature of Business</label>
                                    <select class="form-control">
                                        <option value="No SIC Exclusion">No SIC Exclusion</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Requested Effective Date</label>
                                    <input type="text" class="form-control" value="8/1/2019">
                                </div>
                            </div>
                        </div>

                        <h6 class="mb-3 text-uppercase">Number of Employees</h6>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Eligible Employees</label>
                                    <input type="text" class="form-control" value="3">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Part-Time</label>
                                    <input type="text" class="form-control" value="0">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Out-of-State</label>
                                    <input type="text" class="form-control" value="0">
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cc-controls sticky">
    <div class="container">
        <div class="d-flex align-items-center">
            <div class="left">
                <a href="#" class="btn-link">Cancel</a>
            </div>
            <div class="right">
                <a href="#" class="btn-link">Save & Exit</a>
                <a href="#" class="btn">Next</a>
            </div>
        </div>
    </div>
</section>
<?php include "inc/footer-lean.php"; ?>
