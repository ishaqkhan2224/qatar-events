jQuery(document).ready(function ($) {

    $('#toggle-css').on('click', 'a', function (e) {
        e.preventDefault();
        $('#toggle-css a').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('href');
        $('body').attr('id', id);
        var theme = $(this).attr('data-theme');
        $('body').removeClass().addClass(theme);
    });

    $('#toggle-banner').on('click', 'a', function (e) {
        e.preventDefault();
        $('#banner').toggleClass('flat');
    });

});