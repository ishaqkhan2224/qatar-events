<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=1280, initial-scale=1, shrink-to-fit=no">
    <title>CC Concept: Confirmation</title>
    <link rel="stylesheet" id="css-bootstrap" href="./css/lib/bootstrap.min.css" media="all">
    <link rel="stylesheet" id="css-cores" href="./css/styles-light.css" media="all">
    <script src="https://kit.fontawesome.com/d760a10022.js"></script>
</head>
<body id="cc-broker">
<ul id="toggle">
    <span>Toggle</span>
    <li><a href="ui-confirmation">Theme #1</a></li>
    <li><a href="ui-confirmation-light">Theme #2</a></li>
    <li><a href="ui-confirmation-current">Dev</a></li>
</ul>
<?php include "inc/nav.php"; ?>
<section class="cc-body">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center mt-5">
                <img src="images/confirmation.png" alt="">
            </div>
        </div>
    </div>
</section>

<?php include "inc/footer-lean.php"; ?>
