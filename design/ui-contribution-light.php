<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=1280, initial-scale=1, shrink-to-fit=no">
    <title>CC Concept: Quote Contribution</title>
    <link rel="stylesheet" id="css-bootstrap" href="./css/lib/bootstrap.min.css" media="all">
    <link rel="stylesheet" id="css-cores" href="./css/styles-light.css" media="all">
    <script src="https://kit.fontawesome.com/d760a10022.js"></script>
</head>
<body id="cc-broker">
<ul id="toggle">
    <span>Toggle</span>
    <li><a href="ui-contribution">Theme #1</a></li>
    <li><a href="ui-contribution-light">Theme #2</a></li>
    <li><a href="ui-contribution-current">DEV</a></li>
</ul>
<?php include "inc/nav.php"; ?>
<section id="banner">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Enrollment Quote</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet nulla sequi minima corrupti, laboriosam ipsa.</p>
                </div>
            </div>
        </div>
    </div>
    <nav class="nav-steps">
        <div class="container">
            <a class="prev" href="#"><span>1</span> Group Info</a>
            <a class="prev" href="#"><span>2</span> Census</a>
            <a class="prev" href="#"><span>3</span> Metal Tier</a>
            <a class="active" href="#"><span>4</span> Contribution</a>
            <a href="#"><span>5</span> Optional Benefits</a>
            <a href="#"><span>6</span> Quote Delivery</a>
        </div>
    </nav>
</section>

<section class="cc-body">
    <div class="container">
        <h3>Select a Medical Contribution From The Options Below:</h3>

        <div class="row">
            <div class="col d-flex">
                <div class="option-box">
                    <div class="top">
                        <h4>Lowest Cost HMO</h4>
                        <p>This option is based on
                            the employer contributing
                            towards the lowest cost HMO
                            benefit level.</p>
                    </div>
                    <div class="bottom">
                        <a href="#" class="btn">Select</a>
                    </div>
                </div>
            </div>
            <div class="col d-flex">
                <div class="option-box">
                    <div class="top">
                        <h4>Lowest Cost PPO</h4>
                        <p>This option is based on
                            the employer contributing
                            towards the lowest cost PPO
                            benefit level.</p>
                    </div>
                    <div class="bottom">
                        <a href="#" class="btn">Select</a>
                    </div>
                </div>
            </div>
            <div class="col d-flex">
                <div class="option-box">
                    <div class="top">
                        <h4>Employer Fixed</h4>
                        <p>This popular Fixed Dollar
                            approach allows Employers
                            to set a specific per-Employee
                            budget.</p>
                    </div>
                    <div class="bottom">
                        <a href="#" class="btn">Select</a>
                    </div>
                </div>
            </div>
            <div class="col d-flex">
                <div class="option-box">
                    <div class="top">
                        <h4>Employee Fixed</h4>
                        <p>The employee pays a
                            set amount for the base plan
                            regardless of age and the
                            employer pays the difference.</p>
                    </div>
                    <div class="bottom">
                        <a href="#" class="btn">Select</a>
                    </div>
                </div>
            </div>
            <div class="col d-flex">
                <div class="option-box active">
                    <div class="top">
                        <h4>Percentage of Cost</h4>
                        <p>Employer picks from
                            50% to 100% to contribute
                            toward the Employee
                            cost of a specific plan and/or
                            benefit level.</p>
                    </div>
                    <div class="bottom">
                        <a href="#" class="btn"><i class="fad fa-check"></i> Selected</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="contribution-selection">
            <div class="row">
                <div class="col-md-4 left">
                    <h3>Enter the percentage the employer will contribute to their
                        employee's premium</h3>
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group custom-form-group">
                                <input type="text" class="form-control" id="employee1" value="60%" required="required">
                                <label for="employee1">Employee % (min 50%)</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group custom-form-group">
                                <input type="text" class="form-control" id="dependent" required="required">
                                <label for="dependent">Dependent % (optional)</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 offset-1">
                    <h3>Apply Contribution</h3>
                    <div class="d-flex flex-row mb-2">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="HMO-EPO">
                            <label class="custom-control-label" for="HMO-EPO">HMO/EPO</label>
                        </div>
                        <div class="ml-5 custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="PPO">
                            <label class="custom-control-label" for="PPO">PPO</label>
                        </div>
                    </div>
                    <div>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="Any-health-plan" checked="checked">
                            <label class="custom-control-label" for="Any-health-plan">Any health plan (HMO/EPO or PPO) selected by employee</label>
                        </div>
                    </div>

                    <div class="row pt-4">
                        <div class="col-md-4">
                            <h3>Level</h3>
                            <select class="form-control">
                                <option>Gold PPO C</option>
                            </select>
                        </div>
                        <div class="col-md-7 offset-1">
                            <div class="d-flex flex-row justify-content-between">
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="Specific-health-plan" checked="checked">
                                    <label class="custom-control-label" for="Specific-health-plan">Specific health plan</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="Lowest-cost-health-plan">
                                    <label class="custom-control-label" for="Lowest-cost-health-plan">Lowest cost health plan</label>
                                </div>
                            </div>
                            <select class="form-control mt-2">
                                <option>Select Health Plan</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="cc-controls sticky">
    <div class="container">
        <div class="d-flex align-items-center">
            <div class="left">
                <a href="#" class="btn-link">Cancel</a>
            </div>
            <div class="right">
                <a href="#" class="btn-link">Save & Exit</a>
                <a href="#" class="btn">Next</a>
            </div>
        </div>
    </div>
</section>
<?php include "inc/footer-lean.php"; ?>
