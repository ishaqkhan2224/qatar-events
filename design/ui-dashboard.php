<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=1280, initial-scale=1, shrink-to-fit=no">
    <title>CC Concept: Dashboard</title>
    <link rel="stylesheet" id="css-bootstrap" href="./css/lib/bootstrap.min.css" media="all">
    <link rel="stylesheet" href="./js/lib/owl-carousel-2-3-4/assets/owl.carousel.min.css" media="all">
    <link rel="stylesheet" href="./js/lib/owl-carousel-2-3-4/assets/owl.theme.default.min.css" media="all">
    <link rel="stylesheet" id="css-cores" href="./css/styles.css" media="all">
    <script src="https://kit.fontawesome.com/d760a10022.js"></script>
</head>
<body id="cc-broker">
<ul id="toggle">
    <span>Toggle</span>
    <li><a href="ui-dashboard">Theme #1</a></li>
    <li><a href="ui-dashboard-light">Theme #2</a></li>
    <li><a href="ui-dashboard-current">Dev</a></li>
</ul>
<?php include "inc/nav.php"; ?>
<section class="cc-body">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="box bg-white owl-carousel media-carousel">
                    <div class="media">
                        <img src="./images/ui-dashboard/avatar-f-1.jpg" class="img" alt="">
                        <div class="media-body">
                            <h5 class="mt-2">Hi, I’m Amy from CaliforniaChoice</h5>
                            <p>Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident..</p>
                        </div>
                    </div>
                    <div class="media">
                        <img src="./images/ui-dashboard/avatar-m-1.jpg" class="img" alt="">
                        <div class="media-body">
                            <h5 class="mt-2">Hi, I’m Amy from CaliforniaChoice</h5>
                            <p>Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident..</p>
                        </div>
                    </div>
                    <div class="media">
                        <img src="./images/ui-dashboard/avatar-f-2.jpg" class="img" alt="">
                        <div class="media-body">
                            <h5 class="mt-2">Hi, I’m Amy from CaliforniaChoice</h5>
                            <p>Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident..</p>
                        </div>
                    </div>
                    <div class="media">
                        <img src="./images/ui-dashboard/avatar-m-2.jpg" class="img" alt="">
                        <div class="media-body">
                            <h5 class="mt-2">Hi, I’m Amy from CaliforniaChoice</h5>
                            <p>Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident..</p>
                        </div>
                    </div>
                </div>

                <div class="stat-items">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="stat-item">
                                <h5>Current Groups</h5>
                                <p>5</p>
                                <img src="./images/ui-dashboard/icon-1.png">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="stat-item">
                                <h5>No Premiums</h5>
                                <p>$0</p>
                                <img src="./images/ui-dashboard/icon-2.png">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="stat-item">
                                <h5>No Commissions</h5>
                                <p>$0</p>
                                <img src="./images/ui-dashboard/icon-3.png">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="stat-item">
                                <h5>Upcoming Renewals</h5>
                                <p>1</p>
                                <img src="./images/ui-dashboard/icon-4.png">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="box bg-white">
                            <h4 class="box-title">Your Recent Activity</h4>
                            <ul class="timeline-list">
                                <li>
                                    <div class="left">
                                        <p>Enrollment Quote Updated</p>
                                        <span class="time">1 DAY AGO</span>
                                    </div>
                                    <div class="right">
                                        <a href="#" class="btn btn-grey-outline">View</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="left">
                                        <p>Enrollment Quote Created</p>
                                        <span class="time">1 DAY AGO</span>
                                    </div>
                                    <div class="right">
                                        <a href="#" class="btn btn-grey-outline">View</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="left">
                                        <p>Initial Quote Created</p>
                                        <span class="time">1 DAY AGO</span>
                                    </div>
                                    <div class="right">
                                        <a href="#" class="btn btn-grey-outline">View</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="left">
                                        <p>Commissions added to Quote #85656</p>
                                        <span class="time">7 DAY AGO</span>
                                    </div>
                                    <div class="right">
                                        <a href="#" class="btn btn-grey-outline">View</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="left">
                                        <p>Profile Viewed</p>
                                        <span class="time">7 DAY AGO</span>
                                    </div>
                                    <div class="right"></div>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box bg-white">
                            <h4 class="box-title">Recommended For You</h4>
                            <div class="recommended-list">
                                <div class="media">
                                    <img src="./images/ui-dashboard/icon-5.png" alt="">
                                    <div class="media-body">
                                        <h5 class="mt-0">New Initial Quote</h5>
                                        <p>Run a quote for your clients in an easier-to-present format.</p>
                                    </div>
                                </div>
                                <div class="media">
                                    <img src="./images/ui-dashboard/icon-6.png" alt="">
                                    <div class="media-body">
                                        <h5 class="mt-0">Online Enrollment</h5>
                                        <p>Eliminate paper while improving accuracy and speed when enrolling clients.</p>
                                    </div>
                                </div>
                                <div class="media">
                                    <img src="./images/ui-dashboard/icon-7.png" alt="">
                                    <div class="media-body">
                                        <h5 class="mt-0">Borker Tools</h5>
                                        <p>Download program highlights, employer videos and other tools to help you sell.</p>
                                    </div>
                                </div>
                                <div class="media">
                                    <img src="./images/ui-dashboard/icon-8.png" alt="">
                                    <div class="media-body">
                                        <h5 class="mt-0">Find a Doctor</h5>
                                        <p>Look up doctors and specialists for all health plans..</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box bg-white h-100">
                    <h4 class="box-title">My Sales Team</h4>
                    <div class="media team-member">
                        <img src="./images/ui-dashboard/avatar-m-1.jpg" class="mr-3" alt="">
                        <div class="media-body">
                            <h5 class="mt-0">Bob Quaid</h5>
                            <p>Inside Sales Representative</p>
                            <a href="#">bquaid@calchoice.com</a>
                            <a href="#">Work: (800)542-4218,</a>
                            <a href="#">Ext.4431</a>
                        </div>
                    </div>
                    <div class="media team-member">
                        <img src="./images/ui-dashboard/avatar-f-2.jpg" class="mr-3" alt="">
                        <div class="media-body">
                            <h5 class="mt-0">Sonia Hutton</h5>
                            <p>Outside Sales Rep</p>
                            <a href="#">shutton@calchoice.com</a>
                            <a href="#">Work: (800)542-4218,</a>
                            <a href="#">Ext.4431</a>
                        </div>
                    </div>
                    <div class="media team-member">
                        <img src="./images/ui-dashboard/avatar-m-2.jpg" class="mr-3" alt="">
                        <div class="media-body">
                            <h5 class="mt-0">Bob Quaid</h5>
                            <p>Inside Sales Representative</p>
                            <a href="#">bquaid@calchoice.com</a>
                            <a href="#">Work: (800)542-4218,</a>
                            <a href="#">Ext.4431</a>
                        </div>
                    </div>
                    <div class="media team-member">
                        <img src="./images/ui-dashboard/avatar-f-3.jpg" class="mr-3" alt="">
                        <div class="media-body">
                            <h5 class="mt-0">Sonia Hutton</h5>
                            <p>Outside Sales Rep</p>
                            <a href="#">shutton@calchoice.com</a>
                            <a href="#">Work: (800)542-4218,</a>
                            <a href="#">Ext.4431</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box bg-white broker-section">
            <h4 class="box-title">Broker Bites</h4>
            <p class="box-desc">Topics to sink your teeth into.</p>
            <div class="row">
                <div class="col-md-3">
                    <div class="card bite-card">
                        <img src="./images/ui-dashboard/bites-1.jpg" class="card-img-top" alt="Choose Healthy">
                        <div class="card-body">
                            <h5 class="card-title">Choose Healthy</h5>
                            <p class="card-text">Encourage your clients to take
                                advantage of our new partnership
                                with Choose Healthy, which allows
                                them to save on a variety of health
                                and wellness products.</p>
                            <a href="#" class="btn btn-grey-outline">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card bite-card">
                        <img src="./images/ui-dashboard/bites-2.jpg" class="card-img-top" alt="Choose Healthy">
                        <div class="card-body">
                            <h5 class="card-title">Business Solutions Suite</h5>
                            <p class="card-text">Saves your clients money and
                                delivers valuable benefits including
                                access to a Premium Only Plan,
                                online HR Support, and a Flexible
                                Spending Account.</p>
                            <a href="#" class="btn btn-grey-outline">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card bite-card">
                        <img src="./images/ui-dashboard/bites-3.jpg" class="card-img-top" alt="Choose Healthy">
                        <div class="card-body">
                            <h5 class="card-title">Triple Tier</h5>
                            <p class="card-text">Your clients now have a third
                                option when choosing their small
                                group benefits program. Triple tier
                                is available for your new and
                                renewing CaliforniaChoice groups.</p>
                            <a href="#" class="btn btn-grey-outline">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card bite-card">
                        <img src="./images/ui-dashboard/bites-4.jpg" class="card-img-top" alt="Choose Healthy">
                        <div class="card-body">
                            <h5 class="card-title">Marketing for Brokers</h5>
                            <p class="card-text">Arm yourself with creative tools to
                                help grow your business. From
                                business cards to postcards, flyers
                                and more, we'll take care of your
                                marketing needs.</p>
                            <a href="#" class="btn btn-grey-outline">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "inc/footer-lean.php"; ?>

<script>
    jQuery(document).ready(function($){
        $(".media-carousel").owlCarousel({
            items: 1,
            nav: true,
            loop: true,
            autoplay: true
        });
    });
</script>
