<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=1280, initial-scale=1, shrink-to-fit=no">
    <title>CC Concept: Confirmation</title>
    <link rel="stylesheet" id="css-bootstrap" href="./css/lib/bootstrap.min.css" media="all">
    <link rel="stylesheet" id="css-cores" href="./css/styles.css" media="all">
    <script src="https://kit.fontawesome.com/d760a10022.js"></script>
</head>
<body id="cc-broker">
<ul id="toggle">
    <span>Toggle</span>
    <li><a href="ui-confirmation">Theme #1</a></li>
    <li><a href="ui-confirmation-light">Theme #2</a></li>
    <li><a href="ui-confirmation-current">Dev</a></li>
</ul>
<?php include "inc/nav.php"; ?>
<section class="cc-body">
    <div class="container">
        <div class="box bg-white confirmation-box">
            <div class="box-header">
                <i class="fa fa-check-circle"></i>
                Thank you for your submission!
            </div>
            <div class="box-body">
                <img src="./images/icon-confirm.png" alt="icon">
                <h5>Initial Quote #1262724</h5>

                <p>
                    Created for: <b>SADF</b><br>
                    Created on: <b>8/13/2019</b>
                </p>
                <p>Your quote will be processed and sent to your Quotes page and any specified emails.
                    if you do not receive your quote, contact our Help Desk at (877) 440-9469 or email
                    <a href="mailto:helpdesk@calchoice.com">helpdesk@calchoice.com</a>
                </p>
                <a href="#" class="btn btn-blue btn-block mb-3">Create another quote</a>
                <div class="row">
                    <div class="col-md-6">
                        <a href="#" class="btn btn-grey-outline text-uppercase btn-block">Go to Quotes</a>
                    </div>
                    <div class="col-md-6">
                        <a href="#" class="btn btn-grey-outline text-uppercase btn-block">Go to Dashboard</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "inc/footer-lean.php"; ?>
