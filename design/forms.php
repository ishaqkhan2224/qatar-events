<?php include "inc/_config.php"; ?>
<?php include "inc/head.php"; ?>
<body>
    <style>

        input.currency:focus, input.currency:valid {
          padding-left: 16px;
          position: relative;
          background-image: url('images/dollar-sign-light.svg');
          background-repeat: no-repeat;
          background-size: 8px;
          background-position-y: 12px;
        }
        input.currencyR:focus, input.currencyR:valid {
          padding-right: 16px;
          position: relative;
          background-image: url('images/dollar-sign-light.svg');
          background-repeat: no-repeat;
          background-size: 8px;
          background-position-y: 12px;
          text-align: right;
        }
        input.percentage:focus, input.percentage:valid {
          padding-right: 16px;
          position: relative;
          background-image: url('images/percentage-light.svg');
          background-repeat: no-repeat;
          background-size: 12px;
          background-position-y: 12px;
          background-position-x: 100%;
        }
    </style>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
        
                  <h2 class="mt-3 mb-4">My Profile</h2>

                  <form>

                    <div class="row">
                      <div class="col-12">

                          <div class="form-group custom-form-group mb-4">
                              <input type="text" class="form-control" id="firstname" required="">
                              <label for="firstname">First Name</label>
                          </div>

                          <div class="form-group custom-form-group mb-4">
                              <input type="text" class="form-control" id="lastname" required="">
                              <label for="lastname">Last Name</label>
                          </div>

                          <div class="form-group custom-form-group mb-4">
                              <div style="width: 100px;">
                                <input type="text" class="form-control currency" id="annIncome2" required="">
                                <label for="annIncome2">Monthly Bonus</label>
                              </div>
                          </div>

                          <div class="form-group custom-form-group mb-4">
                              <div style="width: 100px;">
                                <input type="text" class="form-control percentage" id="annPercent" required="">
                                <label for="annPercent">Percentage</label>
                              </div>
                          </div>

                          <div class="form-group custom-form-group mb-4">
                              <input type="text" class="form-control" id="college" required="">
                              <label for="college">Company Name</label>
                          </div>

                      </div>
                    </div>

                  </form>
            </div>
            <div class="col-md-4">
            
                  <h2 class="mt-3 mb-4">Invoice</h2>

                  <form>

                    <div class="row">
                      <div class="col-12">

                          <div class="form-group custom-form-group mb-4">
                              <input type="text" class="form-control currencyR" id="asdfasdf" required="">
                              <label for="asdfasdf">First Item</label>
                          </div>

                          <div class="form-group custom-form-group mb-4">
                              <input type="text" class="form-control currencyR" id="3232332" required="">
                              <label for="3232332">Second Item</label>
                          </div>

                      </div>
                    </div>

                  </form>
            </div>
        </div>
    </div>
</section>
<!-- Javascript -->
<script src="js/lib/jquery-3.3.1.min.js"></script>
<script src="js/lib/bootstrap.bundle.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>
