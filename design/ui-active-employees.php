<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=1280, initial-scale=1, shrink-to-fit=no">
    <title>CC Concept: Active Employees</title>
    <link rel="stylesheet" id="css-bootstrap" href="./css/lib/bootstrap.min.css" media="all">
    <link rel="stylesheet" id="css-cores" href="./css/styles.css" media="all">
    <script src="https://kit.fontawesome.com/d760a10022.js"></script>
</head>
<body id="cc-broker">
<ul id="toggle">
    <span>Toggle</span>
    <li><a href="ui-active-employees">New</a></li>
    <li><a href="ui-active-employees-current">Dev</a></li>
</ul>
<?php include "inc/nav.php"; ?>
<section id="banner">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Orange County Land Management Services</h2>
                </div>
            </div>
            <div class="col-md-4">
                <div class="banner-menu">
                    <a href="" class="btn primary mr-3">Create a New Quote</a>
                    <div class="dropdown dd-transparent">
                        <button class="btn secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Order ID Cards</a>
                            <a class="dropdown-item" href="#">View Invoices</a>
                            <a class="dropdown-item" href="#">Commissions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="nav">
            <a class="nav-link" href="#">Overview</a>
            <a class="nav-link active" href="#">Active Employees</a>
            <a class="nav-link" href="#">Employee Add-ons</a>
            <a class="nav-link" href="#">COBRA</a>
            <a class="nav-link" href="#">Terminated Employees</a>
            <a class="nav-link" href="#">Invoices</a>
            <a class="nav-link" href="#">Quotes</a>
        </nav>
    </div>
</section>

<section class="cc-body">
    <div class="container">

        <table class="table cc-table">
            <thead>
            <tr>
                <th scope="col">Employee Name</th>
                <th scope="col" class="text-center">Age</th>
                <th scope="col" class="text-center">Gender</th>
                <th scope="col" class="product-header" colspan="5">Coverage</th>
                <th scope="col" class="text-center">Medical Coverage Type</th>
                <th scope="col" class="text-center">Enrolled Since</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="3"></td>
                <td class="product-icon"><i class="fal fa-briefcase-medical"></i><br>Medical</td>
                <td class="product-icon"><i class="fal fa-tooth"></i><br>Dental</td>
                <td class="product-icon"><i class="fal fa-glasses"></i><br>Vision</td>
                <td class="product-icon"><i class="fal fa-heartbeat"></i><br>Life</td>
                <td class="product-icon"><i class="fal fa-bed"></i><br>Chiro</td>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td>Lisa Leslie</td>
                <td class="text-center">23</td>
                <td class="text-center">F</td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data"><i class="fal fa-check-square"></td>
                <td class="product-data"><i class="fal fa-check-square"></td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="text-center">Employee</td>
                <td class="text-center">07/23/2019</td>
                <td class="text-center"><a href="#">Order ID Card</td>
            </tr>
            <tr>
                <td>Berlynda Armenta</td>
                <td class="text-center">60</td>
                <td class="text-center">M</td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data">--</td>
                <td class="product-data">--</td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="text-center">Employee & Spouse</td>
                <td class="text-center">03/01/2015</td>
                <td class="text-center"><a href="#">Order ID Card</td>
            </tr>
            <tr>
                <td>Hansi Salvidar</td>
                <td class="text-center">23</td>
                <td class="text-center">F</td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data"><i class="fal fa-check-square"></td>
                <td class="product-data"><i class="fal fa-check-square"></td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="text-center">Employee</td>
                <td class="text-center">07/23/2019</td>
                <td class="text-center"><a href="#">Order ID Card</td>
            </tr>
            <tr>
                <td>Maqqaux Lopez</td>
                <td class="text-center">23</td>
                <td class="text-center">F</td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data"><i class="fal fa-check-square"></td>
                <td class="product-data"><i class="fal fa-check-square"></td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="text-center">Employee</td>
                <td class="text-center">07/23/2019</td>
                <td class="text-center"><a href="#">Order ID Card</td>
            </tr>
            <tr>
                <td>Patricia Pena</td>
                <td class="text-center">23</td>
                <td class="text-center">F</td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data"><i class="fal fa-check-square"></td>
                <td class="product-data"><i class="fal fa-check-square"></td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="text-center">Employee & Spouse</td>
                <td class="text-center">07/23/2019</td>
                <td class="text-center"><a href="#">Order ID Card</td>
            </tr>
            <tr>
                <td>Anthony Peeler</td>
                <td class="text-center">23</td>
                <td class="text-center">F</td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data"><i class="fal fa-check-square"></td>
                <td class="product-data">--</td>
                <td class="product-data">--</i></td>
                <td class="text-center">Employee</td>
                <td class="text-center">07/23/2019</td>
                <td class="text-center"><a href="#">Order ID Card</td>
            </tr>
            <tr>
                <td>Linda Nguyen</td>
                <td class="text-center">23</td>
                <td class="text-center">F</td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="product-data">--</i></td>
                <td class="product-data"><i class="fal fa-check-square"></td>
                <td class="product-data"><i class="fal fa-check-square"></td>
                <td class="product-data"><i class="fal fa-check-square"></i></td>
                <td class="text-center">Employee</td>
                <td class="text-center">07/23/2019</td>
                <td class="text-center"><a href="#">Order ID Card</td>
            </tr>
            </tbody>
        </table>

        <div class="d-flex justify-content-between cc-pagination">
            <div class="left">
            <p>Showing 1-10 of 100 Results &nbsp;|&nbsp; Results to Show:</p>
                <ul class="row-numbers">
                    <li><a class="active" href="">10</a></li>
                    <li><a href="">20</a></li>
                    <li><a href="">30</a></li>
                    <li><a href="">All</a></li>
                </ul>
            </div>
            <div class="right">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link disabled" href="#">Previous</a></li>
                        <li class="page-item"><a class="page-link active" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">...</a></li>
                        <li class="page-item"><a class="page-link" href="#">9</a></li>
                        <li class="page-item"><a class="page-link" href="#">10</a></li>
                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                    </ul>
                </nav>
            </div>
        </div>

    </div>
</section>

<?php include "inc/footer-lean.php"; ?>