<?php include("inc/_config.php");?>
<?php include("inc/head.v1.php");?>
<body>
<header class="demo-header">Header</header>
<section class="banner">
    <div class="container">
        <h2>Enrollment Quote</h2>
        <p>Below is a summary of your enrollment quote. Please review and confirm the details before requesting delivery of your quote.</p>
        <nav class="nav">
            <a class="nav-link" href="#">Group Info</a>
            <a class="nav-link" href="#">Census</a>
            <a class="nav-link" href="#">Metal Tier</a>
            <a class="nav-link" href="#">Contribution</a>
            <a class="nav-link" href="#">Optional Benefit</a>
            <a class="nav-link active" href="#">Quote Delivery</a>
        </nav>
    </div>
</section>

<section class="cc-body" id="broker-enrollment-quote">
    <div class="container">
        <h3>Summary</h3>
        <div class="summary-quote">
            <div class="row summary-content-inner">
                <div class="col-md-4">
                    <div class="_summary-left">
                        <h3>Quote for:</h3>
                        <p>ACME Inc.<br>92620 Orange Country</p>
                        <br>
                        <h3>Census</h3>
                        <div class="row">
                            <div class="col-sm-4">
                                <h5>Employees</h5>
                                <div class="counter-number">23</div>
                            </div>
                            <div class="col-sm-4 border-left-md">
                                <h5>Dependents</h5>
                                <div class="counter-number">57</div>
                            </div>
                            <div class="col-sm-4 border-left-md">
                                <h5>Out-of-State</h5>
                                <div class="counter-number">3</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="_summary-right">
                        <h3>Products Included</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="media">
                                    <i class="fal fa-briefcase-medical major mr-3"></i>
                                    <div class="media-body">
                                        <h5>Medical</h5>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <h6>Employees</h6>
                                                        <div class="counter-number">80%</div>
                                                    </div>
                                                    <div class="col-sm-6 border-left-md">
                                                        <h6>Dependents</h6>
                                                        <div class="counter-number">20%</div>
                                                    </div>
                                                </div>
                                                <p>
                                                    <strong>Triple Tier:</strong> Platinum, Gold and Silver
                                                    <br>
                                                    Based on Kaiser HMO Gold
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="medical-options">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="medical-option">
                                                <span class="badge">Not Selected</span>
                                                <i class="fal fa-heartbeat excluded mr-2"></i>
                                                <p>Life</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="medical-option active">
                                                <span class="badge">Not Selected</span>
                                                <i class="fal fa-glasses mr-2"></i>
                                                <p>Vision</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                            <div class="col-md-6">
                                <div class="media">
                                    <i class="fal fa-tooth major mr-3 mt-1"></i>
                                    <div class="media-body">
                                        <h5>Dental</h5>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <h6>Employees</h6>
                                                        <div class="counter-number">50%</div>
                                                    </div>
                                                    <div class="col-sm-6 border-left-md">
                                                        <h6>Dependents</h6>
                                                        <div class="counter-number">50%</div>
                                                    </div>
                                                </div>
                                                <p>Lowest Cost HMO</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="dental-options">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="dental-option">
                                                <span class="badge">Not Selected</span>
                                                <i class="fal fa-bed excluded mr-2"></i>
                                                <p>CHIRO</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="dental-option active">
                                                <span class="badge">Not Selected</span>
                                                <i class="fal fa-file-invoice-dollar mr-2"></i>
                                                <p>Section 125</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php include("shared/delivery-email.php");?>

</section>
<section class="cc-controls">
    <div class="container">
        <div class="d-flex align-items-center">
            <div class="left">
                <a href="#" class="btn-link">Back</a>
            </div>
            <div class="right">
                <a href="#" class="btn-link">Save & Exit</a>
                <a href="#" class="btn">Deliver Now</a>
            </div>
        </div>
    </div>
</section>
<?php include("inc/footer.php");?>
