<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Online Enrollment Management</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Online Enrollment Management</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec quam et lectus imperdiet pulvinar.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="#" class="btn btn-blue mr-3">Start an online Enrollment</a>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link active" href="#">Active</a>
            <a class="nav-link" href="#">Expired</a>
            <a class="nav-link" href="#">Cancel</a>
        </nav>
    </div>
</section>

<section id="cc-body" class="management-detail-page">
    <div class="container">
        <a href="#" class="back-page"><i class="fas fa-long-arrow-alt-left"></i> Back</a>
        <div class="section-overview-title">
            <h2>Paid Piper - Quote #6789012</h2>
        </div>
        <div class="row">
            <div class="col-md-8">
                <!-- <h6>Group Information</h6> -->
                <div class="d-flex justify-content-between mb-3 align-items-end">
                    <h6>Group Information</h6>
                    <a href="br-ole-management-resend" class="btn btn-grey-outline" <?php if (isset($_GET['hidebutton'])): ?>style="visibility: hidden;"<?php endif; ?>>Resend Employer Registration emails</a>
                </div>
                <div class="cc-card mb-4">
                    <div class="row">
                        <div class="col border-right">
                            <div class="media">
                                <span class="mr-1"><i class="fal fa-user"></i></span>
                                <div class="media-body">
                                    <p class="m-0"><strong>Contact Name</strong></p>
                                    <p class="m-0">Dinesh Chughtai</p>
                                </div>
                            </div>
                        </div>
                        <div class="col border-right">
                            <div class="media">
                                <span class="mr-1"><i class="fal fa-phone-alt"></i></span>
                                <div class="media-body">
                                    <p class="m-0"><strong>Phone</strong></p>
                                    <p class="m-0">(213)-555-1145</p>
                                </div>
                            </div>
                        </div>
                        <div class="col border-right">
                            <div class="media">
                                <span class="mr-1"><i class="fal fa-envelope"></i></span>
                                <div class="media-body">
                                    <p class="m-0"><strong>Email</strong></p>
                                    <p class="m-0">DChughtai@piedpiper.com</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="media border-none">
                                <span class="mr-1"><i class="fal fa-map-marker-alt"></i></span>
                                <div class="media-body">
                                    <p class="m-0"><strong>Zip Code</strong></p>
                                    <p class="m-0">92612</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="mb-0 mt-3"><strong>Tax ID:</strong> 20-5889409</p>
                </div>
                <div class="d-flex justify-content-between mt-5 mb-3 align-items-end">
                    <h6>Employee Enrollment Information</h6>
                    <a href="br-ole-management-resend" class="btn btn-grey-outline">Resend Employee Registration emails</a>
                </div>
                <div class="cc-card">
                    <div class="information-statuses">
                        <div class="status-item">
                            <p class="m-0">Not Started</p>
                            <div class="number">2</div>
                        </div>
                        <div class="status-item">
                            <p class="m-0">Incomplete</p>
                            <div class="number">2</div>
                        </div>
                        <div class="status-item">
                            <p class="m-0">Completed</p>
                            <div class="number">5</div>
                        </div>
                        <div class="status-item">
                            <p class="m-0">Total Online</p>
                            <div class="number">9</div>
                        </div>
                        <div class="status-item">
                            <p class="m-0">Total Online</p>
                            <div class="number">9</div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between mt-3 px-2">
                        <p class="m-0"><strong>Effective Date:</strong> 01/01/2020</p>
                        <p class="m-0"><strong>Deadline Date:</strong> 01/08/2020</p>
                    </div>
                </div>
                <div class="data-tables-top mt-5">
                    <div class="right">
                        <div class="dropdown d-inline-block">
                            <button class="btn btn-grey-outline dropdown-toggle" type="button" id="ddAllStatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-filter"></i> All Status
                            </button>
                            <div class="dropdown-menu" aria-labelledby="ddAllStatus">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                        <button class="btn btn-grey-outline d-inline-block js-cc-slideout-show" data-cc-modal="#ccAddEmployeeModal"><i class="fas fa-plus mr-2"></i>New Employees</button>
                    </div>
                </div>
                <div class="mt-5">
                    <table class="table js-sortable-table" id="cc-table-data">
                        <thead>
                        <tr>
                            <th scope="col">Employee Name</th>
                            <th scope="col" class="no-sort">SSN</th>
                            <th scope="col" class="no-sort">Employment Status</th>
                            <th scope="col" class="no-sort">Employment type</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Applied Business Dynamics</td>
                            <td>XXX-XX-1234</td>
                            <td class="">Completed</td>
                            <td class="cc-table__td-action">
                                <div class="dropdown cc-table__td-dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dd-one"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Online
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-one">
                                        <a class="dropdown-item" href="#">Order ID Card</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Xerox Corporation</td>
                            <td>XXX-XX-1234</td>
                            <td class="">Paper</td>
                            <td class="cc-table__td-action">
                                <div class="dropdown cc-table__td-dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dd-two"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Paper
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-two">
                                        <a class="dropdown-item" href="#">Order ID Card</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Widget Inc.</td>
                            <td>XXX-XX-1234</td>
                            <td class="">Incompletd</td>
                            <td class="cc-table__td-action">
                                <div class="dropdown cc-table__td-dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dd-three"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Online
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-three">
                                        <a class="dropdown-item" href="#">Order ID Card</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Boolean Corporation</td>
                            <td>XXX-XX-1234</td>
                            <td class="">Not Started</td>
                            <td class="cc-table__td-action">
                                <div class="dropdown cc-table__td-dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dd-4"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Online
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-4">
                                        <a class="dropdown-item" href="#">Order ID Card</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Bob's Barbershop</td>
                            <td>XXX-XX-1234</td>
                            <td class="">Completed</td>
                            <td class="cc-table__td-action">
                                <div class="dropdown cc-table__td-dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dd-5"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Paper
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-5">
                                        <a class="dropdown-item" href="#">Order ID Card</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Wingstop</td>
                            <td>XXX-XX-1234</td>
                            <td class="">Not Started</td>
                            <td class="cc-table__td-action">
                                <div class="dropdown cc-table__td-dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dd-6"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Online
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-6">
                                        <a class="dropdown-item" href="#">Order ID Card</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-4 ole-management-right-widgets">
                <div class="d-flex justify-content-between mb-3 align-items-end">
                    <h6>Application</h6>
                    <div class="dropdown d-inline-block">
                        <button class="btn btn-grey-outline dropdown-toggle" type="button" id="ddAllStatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="ddAllStatus">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <div class="cc-card mt-2">
                    <div class="d-flex justify-content-between">
                        <?php
                            $progressStatus = "";
                            if (isset($_GET['uw'])):
                            $progressStatus = " active";
                        ?>
                        <div class="date">
                            <p><strong>Effective Date:</strong> 01/01/2020</p>
                            <p><strong>Deadline Date:</strong> 01/08/2020</p>
                            <p><strong>Submitted By:</strong> Broker</p>
                        </div>
                        <div class="completed">
                            <span><i class="fas fa-check"></i></span>
                        </div>
                        <?php elseif (isset($_GET['remaining'])): ?>
                        <div class="date">
                            <p><strong>Effective Date:</strong> 01/01/2020</p>
                            <p><strong>Deadline Date:</strong> 01/08/2020</p>
                        </div>
                        <div class="remaining">
                            <span>21</span>
                            <p>Days left</p>
                        </div>
                        <?php else: ?>
                        <div class="date">
                            <p><strong>Effective Date:</strong> 01/01/2020</p>
                            <p><strong>Deadline Date:</strong> 01/08/2020</p>
                        </div>
                        <div class="days-left">
                            <span>14</span>
                            <p>Days left</p>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="progress-widget mt-3">
                        <div class="progress ole-progress vertical">
                            <div class="progress-bar" style="height:75%"></div>
                            <span class="progress-step first active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                            <span class="progress-step second active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                            <span class="progress-step third" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                        </div>
                        <ul>
                            <li>
                                <div class="media">
                                    <div class="media-body">
                                        <div class="d-flex justify-content-between">
                                           <p class="m-0"><strong>BR Status</strong></p>
                                           <p class="m-0">10/28/2019</p>
                                        </div>
                                        <p class="m-0">Borker Application complete.Employeer Application in process</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <div class="media-body">
                                        <div class="d-flex justify-content-between">
                                           <p class="m-0"><strong>ER Status</strong></p>
                                           <p class="m-0">10/28/2019</p>
                                        </div>
                                        <p class="m-0">Employeer Application complete.Employees now enrolling</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <div class="media-body border-bottom-0">
                                        <div class="d-flex justify-content-between">
                                           <p class="m-0"><strong>UW Status</strong></p>
                                           <?php if ($progressStatus): ?>
                                           <p class="m-0">10/28/2019</p>
                                           <?php endif; ?>
                                        </div>
                                        <p class="m-0">Employer Application and Employee Applications final.</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <a href="#" class="btn btn-blue btn-block">Finalize Enrollment</a>
                </div>
                <h6 class="mt-5">Documents & Summaries</h6>
                <div class="cc-card">
                    <table class="table summary-table">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Document Name</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><i class="fas fa-file-pdf"></i> 10/28/2019</td>
                            <td><a href="#">Employer Master Application</a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-file-pdf"></i> 10/28/2019</td>
                            <td><a href="#">Employee Enrollment Summaries</a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-file-pdf"></i> 01/01/2008</td>
                            <td><a href="#">Lorem Ipsum</a></td>
                            <td><a href="#"><i class="fas fa-trash-alt"></i></a></td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="br-ole-management-upload" class="btn btn-grey-outline btn-block">Upload Additional Documents</a>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="" id="cc-form__modal">
    <div id="ccAddEmployeeModal" class="cc-modal cc-add-employee-modal">
        <div class="cc-modal-header">
            <span class="cc-modal-close js-cc-slideout-close"><i class="fa fa-times"></i></span>
            <h5>Add Employee</h5>
        </div>
        <div class="cc-modal-inner">
            <div class="cc-modal-body">
                <div class="alert cc-success mb-4" role="alert" style="display: none;"><strong>Parth Shah has been added!</strong> Add another employee?</div>
                <div class="cc-modal-inner-content">
                <h5>Employee Information</h5>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="text-uppercase">Last Name</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                    <div class="form-group col-md-6">
                        <label class="text-uppercase">First Name</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label class="text-uppercase">SSN</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                    <div class="form-group col-md-3">
                        <label class="text-uppercase">Age or DOB</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                    <div class="form-group col-md-3">
                        <label class="text-uppercase">Date of Hire</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                    <div class="form-group col-md-3">
                        <label class="text-uppercase">COBRA</label>
                        <select class="form-control form-control-sm">
                            <option value="">Please Select</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label class="text-uppercase">Zip Code</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                    <div class="form-group col-md-3">
                        <label class="text-uppercase">County</label>
                        <select class="form-control form-control-sm">
                            <option value="">Please Select</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="text-uppercase">Enrollment Type</label>
                        <select class="form-control form-control-sm">
                            <option value="">Please Select</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="text-uppercase">Email</label>
                        <input type="email" class="form-control form-control-sm">
                        <p class="text-muted"><small>Required for online enrollment</small></p>
                    </div>
                </div>

                <h5>Spouse & Dependents</h5>
                <div class="form-group">
                  <div class="form-row">
                      <div class="col-6">
                        <label class="col-form-label">
                            Spouse's age or date of birth
                        </label>
                      </div>
                      <div class="col-2">
                        <input type="text" class="form-control form-control-sm">
                      </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="form-row">
                      <div class="col-6">
                        <label class="col-form-label">
                            Number of dependents (Ages 0-14):
                        </label>
                      </div>
                      <div class="col-2">
                        <input type="text" class="form-control form-control-sm">
                      </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="form-row">
                      <div class="col-12">
                        <label class="col-form-label">
                            Number of dependents (Ages 15+)<br>
                            <small>Dependents 26 and older must be disabled to quantify for coverage.</small>
                        </label>
                      </div>
                   </div>
                </div>

                <div class="row">
                    <div class="col-8">
                        <table id="dependentsTable" class="table card-table">
                          <thead>
                              <tr>
                                <th class="pl-0" width="60%">Dependent age or DOB</th>
                                <th width="20%">Disability</th>
                                <th></th>
                            </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td class="pl-0"><input type="text" class="form-control form-control-sm"></td>
                                  <td>
                                      <select class="form-control form-control-sm">
                                          <option value="">Not Disabled</option>
                                          <option value="">Disabled</option>
                                      </select>
                                  </td>
                                  <td>&nbsp;</td>
                              </tr>
                          </tbody>
                        </table>
                        <button type="button" class="btn btn-sm btn-grey-outline js-add-dependent mb-5"><i class="fas fa-plus ml-1"></i> Add Another Dependent...</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="cc-modal-footer fixed">
            <div class="d-flex justify-content-between">
               <div class="col-4">
                <a href="#" class="btn-link js-cc-slideout-close">Close</a>
               </div>
               <div class="col-6">
                    <div class="d-flex justify-content-end">
                        <a href="#" class="btn btn-blue js-show-confirmation-alert">Save</a>
                    </div>

               </div>
            </div>
        </div>
    </div><!-- ccAddEmployeeModal -->
</form>

<?php include "common/footer.php"; ?>
</body>
</html>