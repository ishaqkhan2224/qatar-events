<!DOCTYPE html>
<html>
<head>
  <?php include "inc/header.meta.php"; ?>
  <title>CC UI KIT: Forms</title>
  <?php include "inc/header.link.php"; ?>
</head>
<body>

  <form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body">
      <div class="container">
        <div class="cc-form__box">
          <div class="row">
            <div class="col-7">
              <div class="form-group">
                <label for="basic_input_field">Standard Text</label>
                <input type="text" class="form-control" id="basic_input_field" name="basic_input_field" required>
                <small id="passwordHelpBlock" class="form-text text-muted">
                  This is an example of a helper text, located nicely below the input field.
                </small>
              </div>
              <div class="form-group">
                <div class="cc-showhidepwd">
                  <label for="field-CurrentPassword">Password</label>
                  <span class="js-password-display fas fa-eye-slash" data-toggle="tooltip" data-placement="left" title="Click to show password"></span>
                </div>
                <input type="password" class="form-control" id="field-CurrentPassword" name="CurrentPassword" autocomplete="off" required />
              </div>
              <div class="form-group mt-4">
                <div class="form-row">
                  <div class="col-9">
                    <label for="address">Group Fields</label>
                    <input type="text" class="form-control" id="address" name="address" placeholder="Street Address" required>
                  </div>
                  <div class="col-3">
                    <label for="suite_number">&nbsp;</label>
                    <input type="text" class="form-control" id="suite_number" name="suite_number" placeholder="Suite #" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-row">
                  <div class="col-7">
                    <input type="text" class="form-control" name="city" placeholder="City" required>
                  </div>
                  <div class="col-2">
                    <select id="" class="form-control" required>
                      <option value="">Select</option>
                      <option value="CA">CA</option>
                      <option>...</option>
                    </select>
                  </div>
                  <div class="col-3">
                    <input type="text" class="form-control" name="zip_code" placeholder="Zip Code" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-row">
                  <div class="col">
                    <label for="textarea-field">Textarea (optional)</label>
                    <textarea class="form-control" id="textarea-field" name="textarea-field" rows="6" required></textarea>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="same-address" name="checkbox01">
                  <label class="custom-control-label" for="same-address">Checkbox 01</label>
                </div>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="checkbox02" name="checkbox02">
                  <label class="custom-control-label" for="checkbox02">Checkbox 02</label>
                </div>
              </div>
            </div>
          </div>
        </div><!-- cc-form__box -->

        <hr>

        <div class="cc-form__box mt-5">
          <div class="row">
            <div class="col-8 question-p">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In labore accusantium earum. Sint veritatis, obcaecati quod illo tenetur reprehenderit necessitatibus labore velit sed totam! Quisquam, est, voluptate.</p>
            </div>
            <div class="col-3 offset-1">
              <div class="form-group">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="AgreeDisagree1-a" name="AgreeDisagree1" class="custom-control-input" value="agree">
                  <label class="custom-control-label" for="AgreeDisagree1-a">Option A</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="AgreeDisagree1-b" name="AgreeDisagree1" class="custom-control-input" value="disagree">
                  <label class="custom-control-label" for="AgreeDisagree1-b">Option B</label>
                </div>
              </div>
            </div>
          </div>

          <div class="row mt-4">
            <div class="col-8 question-p">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In labore accusantium earum.</p>
            </div>
            <div class="col-3 offset-1">
              <div class="form-group">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="AgreeDisagree2-a" name="AgreeDisagree2" class="custom-control-input" value="agree">
                  <label class="custom-control-label" for="AgreeDisagree2-a">Option A</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="AgreeDisagree2-b" name="AgreeDisagree2" class="custom-control-input" value="disagree">
                  <label class="custom-control-label" for="AgreeDisagree2-b">Option B</label>
                </div>
              </div>
            </div>
          </div>

          <div class="row mt-4">
            <div class="col-8 question-p">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In labore accusantium earum. Sint veritatis, obcaecati quod illo tenetur reprehenderit necessitatibus labore velit sed totam! Quisquam, est, voluptate. Eaque, laboriosam, molestias. Lorem ipsum dolor sit amet, consectetur adipisicing elit. In labore accusantium earum.</p>
            </div>
            <div class="col-3 offset-1">
              <div class="form-group">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="AgreeDisagree3-a" name="AgreeDisagree3" class="custom-control-input" value="agree">
                  <label class="custom-control-label" for="AgreeDisagree3-a">Option A</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="AgreeDisagree3-b" name="AgreeDisagree3" class="custom-control-input" value="disagree">
                  <label class="custom-control-label" for="AgreeDisagree3-b">Option B</label>
                </div>
              </div>
            </div>
          </div>

          <div class="row mt-4">
            <div class="col-8 question-p">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In labore accusantium earum. Sint veritatis, obcaecati quod illo tenetur reprehenderit necessitatibus labore velit sed totam! Quisquam, est, voluptate.</p>
            </div>
            <div class="col-3 offset-1">
              <div class="form-group">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="AgreeDisagree4-a" name="AgreeDisagree4" class="custom-control-input" value="agree">
                  <label class="custom-control-label" for="AgreeDisagree4-a">Option A</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="AgreeDisagree4-b" name="AgreeDisagree4" class="custom-control-input" value="disagree">
                  <label class="custom-control-label" for="AgreeDisagree4-b">Option B</label>
                </div>
              </div>
            </div>
          </div>
        </div><!-- cc-form__box -->
      </div><!-- container -->
    </section>

    <section class="cc-controls sticky">
      <div class="container">
        <div class="d-flex align-items-center">
          <div class="left">
            <a href="#" class="btn-link">Cancel</a>
          </div>
          <div class="right">
            <a href="#" class="btn-link">Save & Exit</a>
            <button class="btn">Next</button>
          </div>
        </div>
      </div>
      <div class="cc-controls-footer-links">
        <div class="container">
          <div class="d-flex justify-content-between">
            <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
            <div>
              <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </form>
  <?php include "common/footer.php"; ?>
  <script>
    $('#cc-form__new-quote').validate({
      errorClass: "is-invalid",
//validClass: "is-valid",
rules: {
  checkbox01: "required",
  checkbox02: "required",
  AgreeDisagree1: "required",
  AgreeDisagree2: "required",
  AgreeDisagree3: "required",
  AgreeDisagree4: "required"
},
messages: {
  basic_input_field: {
    required: "Custom Message For Error Field."
  }
}
});

    $('.js-password-display').on('click', function () {
      var passwordInputField = $(this).closest('.form-group').find('input');
      if (passwordInputField.attr('type') === 'password') {
        passwordInputField.attr('type', 'text');
        $(this).removeClass('fa-eye-slash').addClass('fa-eye').attr('title', 'Click to hide password').tooltip('dispose').tooltip('show');
      } else {
        passwordInputField.attr('type', 'password');
        $(this).removeClass('fa-eye').addClass('fa-eye-slash').attr('title', 'Click to show password').tooltip('dispose').tooltip('show');
      }
    });
  </script>
</body>
</html>