<header id="cc-header">
	<div class="container">
        <div class="cc-header__wrapper">
            <nav class="navbar navbar-expand-lg cc-header__main-nav">
                <a class="navbar-brand" href="#"><img src="../global/img/calchoicelogo.png" alt="logo"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon fas fa-bars"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Quoting</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Menu One</a>
                                <a class="dropdown-item" href="#">Menu Two</a>
                                <a class="dropdown-item" href="#">Menu Three</a>
                                <a class="dropdown-item" href="#">Menu Four</a>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="#">My Business</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Resources</a></li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item"><a class="nav-link" href="#">Forms and Documents</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Our Program</a></li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">My Profile</a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Update My Profile</a>
                                <a class="dropdown-item" href="#">Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
	</div>
</header>
