<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>New Quote</h2>
                    <p>Description of page goes here. This can be a long sentence.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="" class="btn primary mr-3">Create a New Quote</a>
                    <div class="dropdown dd-transparent">
                        <button class="btn secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Order ID Cards</a>
                            <a class="dropdown-item" href="#">View Invoices</a>
                            <a class="dropdown-item" href="#">Commissions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="cc-subheader__progressive-tabs">
        <div class="container">
            <a class="active" href="#"><span>1</span> Group Info</a>
            <a href="#"><span>2</span> Census</a>
            <a href="#"><span>3</span> Delivery</a>
        </div>
    </nav>
</section>