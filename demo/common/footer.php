<!-- Javascript -->
<script src="js/lib/jquery-1.11.3.min.js"></script>
<script src="js/lib/jquery.dataTables.min.js"></script>
<script src="js/lib/popper.min.js"></script>
<script src="js/lib/bootstrap.bundle.min.js"></script>
<script src="js/lib/owl-carousel-2-3-4/owl.carousel.min.js"></script>
<script src="js/main.js"></script>
<script src="js/uxdemo.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
