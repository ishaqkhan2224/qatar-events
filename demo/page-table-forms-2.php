<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Page Layout > Table</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<div class="cc-site-alert cc-success">
    Site Level Alert Message Here...
</div>
<div class="cc-site-alert cc-error">
    Site Level Alert Message Here...
</div>
<div class="cc-site-alert cc-warning">
    Site Level Alert Message Here...
</div>
<div class="cc-site-alert cc-info">
    Site Level Alert Message Here...
</div>
<?php include "common/subheader-tabs.php"; ?>
<form id="cc-form-data-sample1" action="">
    <section id="cc-body">
        <div class="container">
                    <table class="table mb-5" id="cc-table-data-adsf" hidden>
            <thead>
                <tr>
                    <th scope="col" class="text-center">Primary</th>
                    <th scope="col" width="25%">Contact Name</th>
                    <th scope="col" width="10%">Title</th>
                    <th scope="col" width="5%">Salutation</th>
                    <th scope="col" width="15%">Phone</th>
                    <th scope="col" width="8%">Ext</th>
                    <th scope="col" width="17%">Email</th>
                    <th scope="col" colspan="2">Billing<br>Contact</th>
                </tr>
            </thead>
            <tbody>
                <tr class="cc-table-row-highlight">
                    <td class="text-center">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="AgreeDisagree1-a" name="AgreeDisagree" class="js-row-highlight custom-control-input" value="agree" checked="checked">
                          <label class="custom-control-label" for="AgreeDisagree1-a"></label>
                        </div>
                    </td>
                    <td><input type="text" class="form-control" id="name01" name="name01" value="Daniel Hernandez" required></td>
                    <td><input type="text" class="form-control" id="title01" name="title01" value="CEO" required></td>
                    <td><input type="text" class="form-control" id="salute01" name="salute01" value="6x9" required></td>
                    <td><input type="text" class="form-control" id="phone01" name="phone01" value="555-555-1212" required></td>
                    <td><input type="text" class="form-control" id="ext01" name="ext01" value="21" required></td>
                    <td><input type="text" class="form-control" id="email01" name="email01" value="test@test.com" required></td>
                    <td class="text-center">
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td><a href=""><i class="fad fa-trash-alt"></i></a></td>
                </tr>
                <tr>
                    <td class="text-center">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="AgreeDisagree2-a" name="AgreeDisagree" class="js-row-highlight custom-control-input" value="agree">
                          <label class="custom-control-label" for="AgreeDisagree2-a"></label>
                        </div>
                    </td>
                    <td><input type="text" class="form-control" id="name02" name="name02" required></td>
                    <td><input type="text" class="form-control" id="title02" name="title02" required></td>
                    <td><input type="text" class="form-control" id="salute02" name="salute02" required></td>
                    <td><input type="text" class="form-control" id="phone02" name="phone02" required></td>
                    <td><input type="text" class="form-control" id="ext02" name="ext02" required></td>
                    <td><input type="text" class="form-control" id="email02" name="email02" required></td>
                    <td class="text-center">
                        <label class="switch">
                            <input type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td><a href=""><i class="fad fa-trash-alt"></i></a></td>
                </tr>
                <tr>
                    <td class="text-center">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="AgreeDisagree3-a" name="AgreeDisagree" class="js-row-highlight custom-control-input" value="agree">
                          <label class="custom-control-label" for="AgreeDisagree3-a"></label>
                        </div>
                    </td>
                    <td><input type="text" class="form-control" id="name03" name="name03" required></td>
                    <td><input type="text" class="form-control" id="title03" name="title03" required></td>
                    <td><input type="text" class="form-control" id="salute03" name="salute03" required></td>
                    <td><input type="text" class="form-control" id="phone03" name="phone03" required></td>
                    <td><input type="text" class="form-control" id="ext03" name="ext03" required></td>
                    <td><input type="text" class="form-control" id="email03" name="email03" required></td>
                    <td class="text-center">
                        <label class="switch">
                            <input type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td><a href=""><i class="fad fa-trash-alt"></i></a></td>
                </tr>
                <tr>
                    <td class="text-center">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="AgreeDisagree4-a" name="AgreeDisagree" class="js-row-highlight custom-control-input" value="agree">
                          <label class="custom-control-label" for="AgreeDisagree4-a"></label>
                        </div>
                    </td>
                    <td><input type="text" class="form-control" id="name04" name="name04" required></td>
                    <td><input type="text" class="form-control" id="title04" name="title04" required></td>
                    <td><input type="text" class="form-control" id="salute04" name="salute04" required></td>
                    <td><input type="text" class="form-control" id="phone04" name="phone04" required></td>
                    <td><input type="text" class="form-control" id="ext04" name="ext04" required></td>
                    <td><input type="text" class="form-control" id="email04" name="email04" required></td>
                    <td class="text-center">
                        <label class="switch">
                            <input type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td><a href=""><i class="fad fa-trash-alt"></i></a></td>
                </tr>
                <tr>
                    <td class="text-center">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="AgreeDisagree5-a" name="AgreeDisagree" class="js-row-highlight custom-control-input" value="agree">
                          <label class="custom-control-label" for="AgreeDisagree5-a"></label>
                        </div>
                    </td>
                    <td><input type="text" class="form-control" id="name05" name="name05" required></td>
                    <td><input type="text" class="form-control" id="title05" name="title05" required></td>
                    <td><input type="text" class="form-control" id="salute05" name="salute05" required></td>
                    <td><input type="text" class="form-control" id="phone05" name="phone05" required></td>
                    <td><input type="text" class="form-control" id="ext05" name="ext05" required></td>
                    <td><input type="text" class="form-control" id="email05" name="email05" required></td>
                    <td class="text-center">
                        <label class="switch">
                            <input type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td><a href=""><i class="fad fa-trash-alt"></i></a></td>
                </tr>
                <tr>
                    <td class="text-center">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="AgreeDisagree6-a" name="AgreeDisagree" class="js-row-highlight custom-control-input" value="agree">
                          <label class="custom-control-label" for="AgreeDisagree6-a"></label>
                        </div>
                    </td>
                    <td><input type="text" class="form-control" id="name06" name="name06" required></td>
                    <td><input type="text" class="form-control" id="title06" name="title06" required></td>
                    <td><input type="text" class="form-control" id="salute06" name="salute06" required></td>
                    <td><input type="text" class="form-control" id="phone06" name="phone06" required></td>
                    <td><input type="text" class="form-control" id="ext06" name="ext06" required></td>
                    <td><input type="text" class="form-control" id="email06" name="email06" required></td>
                    <td class="text-center">
                        <label class="switch">
                            <input type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td><a href=""><i class="fad fa-trash-alt"></i></a></td>
                </tr>
            </tbody>
        </table>
        <!-- <a href="" class="btn primary"><i class="fal fa-plus-square"></i> Add Contact</a> -->
        <br>
        <table class="table" id="cc-table-data-sample1">
            <thead>
            <tr>
                <th scope="col" class="text-center">Primary</th>
                <th scope="col" width="25%">Contact Name</th>
                <th scope="col" width="10%">Title</th>
                <th scope="col" width="5%">Salutation</th>
                <th scope="col" width="15%">Phone</th>
                <th scope="col" width="10%">Ext</th>
                <th scope="col" width="15%">Email</th>
                <th scope="col" colspan="2">Billing<br>Contact</th>
            </tr>
            </thead>
            <tbody>
            <tr class="cc-table-row-highlight">
                <td class="text-center">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="testing1-a" name="testing" class="js-row-highlight custom-control-input" value="agree" checked="checked">
                        <label class="custom-control-label" for="testing1-a"></label>
                    </div>
                </td>
                <td><input type="text" class="form-control" id="name01" name="name01" data-msg-required="Contact Name is required." style="width: 80%" value="Daniel Hernandez" required></td>
                <td><input type="text" class="form-control" id="title01" name="title01" data-msg-required="Title is required." style="width: 70%" value="CEO" required></td>
                <td><input type="text" class="form-control" id="salute01" name="salute01" data-msg-required="Salutation is required." style="width: 70%" value="6x9" required></td>
                <td><input type="text" class="form-control" id="phone01" name="phone01" data-msg-required="Phone is required." style="width: 80%" value="555-555-1212" required></td>
                <td><input type="text" class="form-control" id="ext01" name="ext01" data-msg-required="Ext is required." style="width: 50%" value="21" required></td>
                <td><input type="text" class="form-control" id="email01" name="email01" data-msg-required="Email is required." style="width: 80%" value="test@test.com" required></td>
                <td class="text-center">
                    <label class="switch">
                        <input type="checkbox" checked>
                        <span class="slider round"></span>
                    </label>
                </td>
                <td><a href="#" class="delete-tr"><i class="fad fa-trash-alt"></i></a></td>
            </tr>
            </tbody>
        </table>

        <div class="d-flex">
            <a href="#" id="addContact" class="btn btn-grey-outline"><i class="fas fa-plus-circle"></i> Add Contact</a>
        </div>
        </div><!-- container -->
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Cancel</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>

<script>
    $('#cc-form-data-sample1').validate({
        errorClass: "is-invalid",
        //validClass: "is-valid",
        invalidHandler: function(event, validator) {
            // 'this' refers to the form
            $("#alert").remove();
            var errors = validator.numberOfInvalids();
            if (errors) {
                $('<div id="alert" class="alert alert-danger alert-dismissible fade show" role="alert" style="background-color: #FEF5EF;">\n' +
                    '<strong>Uh oh, you got issues!!!</strong> Please click on the <i class="far fa-info-circle"></i> icon next to the error fields for further information. {Marketing Help}\n' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                    '<span aria-hidden="true">&times;</span>\n' +
                    '</button>\n' +
                    '</div>').insertBefore('#cc-table-data-sample1');
            }
        },
        errorPlacement: function(error, element) {
            element.next('i').remove();
            var msg = $('<i class="far fa-info-circle" data-toggle="tooltip" title="' + error.text() + '"></i>');
            msg.insertAfter(element);
            $('[data-toggle="tooltip"]').tooltip();
        },
        unhighlight: function(element, errorClass, validClass){
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).next('i').remove();
        },
        messages: {

        }
    });
</script>

</body>
</html>