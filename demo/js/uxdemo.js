$(document).ready(function() {

    //$('#exampleModal04').modal('show');
    
    // Sprint 11: IVP PIN Code Interaction
    $('form#cc-form__ivr').submit(function(e) {
        e.preventDefault();
        $("form#cc-form__ivr button").append(' <i class=" ml-2 fa fa-spinner fa-pulse spiner-icon"></i>');
        setTimeout(function() { 
            $("form#cc-form__ivr button").find("i").remove();
            $('#cc-form__ivr .form-group,button').fadeOut(500);
            $('form#cc-form__ivr p').delay(500).fadeIn(500);
            return false;
        }, 1000);
    });

    // Sprint 11: Unfinished Enrollment Delete Process and Email
    $('#js-DEMO-confirm-delete-unfinished-enrollment').click(function(e){
    	$('#deleteUnfinishedEnrollment').modal('hide');
    	$("#js-demo-alert-unfinished-enrollment").addClass('show');
    	$('#js-demo-alert-unfinished-enrollment').show();
    	$('#js-DEMO-delete-unfinished-enrollment-row').hide();
    });

    // Sprint 11: Countdown Script

    var seconds = 5; // seconds for HTML
    var foo; // variable for clearInterval() function

    function redirect() {
        document.location.href = 'http://calchoice.com';
    }

    function updateSecs() {
        document.getElementById("seconds").innerHTML = seconds;
        seconds--;
        if (seconds == -1) {
            clearInterval(foo);
            redirect();
        }
    }

    function countdownTimer() {
        foo = setInterval(function () {
            updateSecs()
        }, 1000);
    }
    
    $('#js-DEMO-trigger-modal').click(function(e){
        e.preventDefault();
        $('#exampleModal02').modal();
        countdownTimer();
    });

});