$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

    $(".js-clickable-row td:not('.js-td-action')").click(function () {
        window.location = $(this).closest('tr').data("href");
    });

    $('.js-sortable-table').DataTable({
        "paging": true,
        "ordering": true,
        "info": true,
        "dom": '<f<t>ilp>',
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false,
        }]
    });

    /*Datepicker pop-up*/
    $('#dateOfHirePicker').daterangepicker({
            opens: 'left',
            singleDatePicker: true,
            showDropdowns: true
        },
        function(start,end,label){
            $('#date_of_hire').val(start.format('MM/DD/YYYY'));
        });

    /*Datepicker pop-up*/
    $('#daterange').daterangepicker({
        opens: 'right',
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, function (start, end, label) {
        $('#startdate').val(start.format('MM/DD/YYYY'));
        $('#enddate').val(end.format('MM/DD/YYYY'));
    });


    /* Validate startdate and enddate input fields format*/
    $('#startdate, #enddate').mask('AB/CD/0000', {
        translation: {
            A: {pattern: /[0-1]/},
            B: {pattern: /[0-9]/},
            C: {pattern: /[0-3]/},
            D: {pattern: /[0-9]/}
        },
        onKeyPress: function (a, b, c, d) {
            if (!a) return;
            var m = a.match(/(\d{1})/g);
            if (!m) return;
            if (parseInt(m[2]) === 3) {
                d.translation.D.pattern = /[0-1]/;
            } else if (parseInt(m[2]) === 0) {
                d.translation.D.pattern = /[1-9]/;
            } else {
                d.translation.D.pattern = /[0-9]/;
            }
            if (parseInt(m[0]) == 1) {
                d.translation.B.pattern = /[0-2]/;
            } else if (parseInt(m[0]) === 0) {
                d.translation.B.pattern = /[1-9]/;
            } else {
                d.translation.B.pattern = /[0-9]/;
            }
            var temp_value = c.val();
            c.val('');
            c.unmask().mask('AB/CD/0000', d);
            c.val(temp_value);
        },
        placeholder: "MM/DD/YYYY"
    }).keyup();

    $('table').on('click', '.js-row-highlight', function () {
        $(this).parents('table').find('tr').removeClass('cc-table-row-highlight');
        $(this).parents('tr').toggleClass('cc-table-row-highlight');
    });

    $('#addContact').on('click', function (e) {
        e.preventDefault();
        var tBody = $(this).parents('form').find('#cc-table-data-form-sample tbody');
        var trCount = tBody.find('tr').length;
        var tr = '               <tr>\n' +
            '                    <td class="text-center">\n' +
            '                        <div class="custom-control custom-radio custom-control-inline">\n' +
            '                            <input type="radio" id="testing' + (trCount + 1) + '-a" name="testing" class="js-row-highlight custom-control-input" value="agree">\n' +
            '                            <label class="custom-control-label" for="testing' + (trCount + 1) + '-a"></label>\n' +
            '                        </div>\n' +
            '                    </td>\n' +
            '                    <td><input type="text" class="form-control" id="name0' + (trCount + 1) + '" name="name0' + (trCount + 1) + '" data-msg-required="Contact Name is required." required></td>\n' +
            '                    <td><input type="text" class="form-control cc-w70" id="title0' + (trCount + 1) + '" name="title0' + (trCount + 1) + '" data-msg-required="Title is required." required></td>\n' +
            '                    <td class="text-center">\n' +
            '                        <div class="custom-control custom-checkbox">\n' +
            '                            <input type="checkbox" class="custom-control-input" id="same-address0' + (trCount + 1) + '" name="same_address0' + (trCount + 1) + '" required><label class="custom-control-label" for="same-address"></label>\n' +
            '                        </div>\n' +
            '                    </td>\n' +
            '                    <td><input type="text" class="form-control cc-w60" id="salute0' + (trCount + 1) + '" name="salute0' + (trCount + 1) + '" data-msg-required="Long message goes here..." required></td>\n' +
            '                    <td class="text-center">\n' +
            '                        <select id="email0' + (trCount + 1) + '" name="email0' + (trCount + 1) + '" class="form-control" required>\n' +
            '                            <option value="">Select</option>\n' +
            '                            <option value="CA">CA</option>\n' +
            '                            <option value="TX">TX</option>\n' +
            '                        </select>\n' +
            '                    </td>\n' +
            '                    <td class="text-center">\n' +
            '                        <label class="switch">\n' +
            '                            <input type="checkbox" checked>\n' +
            '                            <span class="slider round"></span>\n' +
            '                        </label>\n' +
            '                    </td>\n' +
            '                    <td><a href="#" class="delete-tr"><i class="fad fa-trash-alt"></i></a></td>\n' +
            '                </tr>';
        tBody.append(tr);
    });

    $('table').on('click', '.delete-tr', function (e) {
        e.preventDefault();
        $(this).parents('tr').remove();
    });

    // Adding and removing Classes in metaltier page
    $('.radio-row').click(function (e) {
        e.preventDefault();
        var parent = $(this).parents('.js-radio-row-container');
        parent.find('a.radio-row').removeClass('selected');
        $(this).addClass('selected');
        parent.find('.cc-card').removeClass('selected').addClass("muted");
        $(this).parents('.cc-card').addClass('selected').removeClass('muted');
    });

    $("#optionalBenefitsSelectAll").click(function () {
        $('.cc-optional-benefits input:checkbox').prop('checked', this.checked);
    });

    $('.btn-select').click(function (e) {
        e.preventDefault();
        var parent = $(this).parents('.js-radio-row-container');
        parent.find('.btn-select').removeClass('btn-blue').addClass('btn-grey-outline').text('Select');
        $(this).removeClass('btn-grey-outline').addClass('btn-blue').text('Selected');
        parent.find('.cc-card').removeClass('selected').addClass("muted");
        $(this).parents('.cc-card').addClass('selected').removeClass('muted');
        var target_id = $(this).data('target');
        $('.cost-forms').hide();
        $('#' + target_id).fadeIn();
        $('html, body').animate({
            scrollTop: $('#' + target_id).offset().top
        }, 500);
    });

    $('.js-cc-slideout-close').on('click', function () {
        $(".cc-modal-overlay").remove();
        $(".cc-modal").removeClass("show");
        $('body').removeClass('cc-modal-showed');
    });

    $(".js-cc-slideout-show").on('click', function () {
        var target = $(this).data('cc-modal');
        $(target).addClass("show");
        $('body').addClass('cc-modal-showed');
        $(".cc-modal").before('<div class="cc-modal-overlay cc-fade"></div>');
    });

    //er-ole-census alert
    $('.js-show-confirmation-alert').on('click',function(e){
        e.preventDefault();
        $('.cc-add-employee-modal .cc-success,.cc-add-employee-modal .cc-modal-inner-content,.cc-add-employee-modal .cc-modal-footer').hide();
        $('.cc-add-employee-modal .cc-success').fadeIn();
        $('.cc-add-employee-modal .cc-modal-inner-content,.cc-add-employee-modal .cc-modal-footer').delay(1000).fadeIn();
    });

    $(".counterUp").on('click', function () {
        var parent = $(this).parents(".form-control-counter");
        var value = parent.find(".form-control").val();
        value++;
        parent.find(".form-control").val(value);
    });
    $(".counterDown").on('click', function () {
        var parent = $(this).parents(".form-control-counter");
        var minVal = parent.find(".form-control").attr('min');
        var value = parent.find(".form-control").val();
        if (value > minVal) {
            value--;
        }
        parent.find(".form-control").val(value);
    });

    // er-ole-census
    $('.js-add-dependent').on('click',function () {
        var tr = '<tr>\n' +
            '                        <td class="pl-0"><input type="text" class="form-control form-control-sm"></td>\n' +
            '                        <td>\n' +
            '                            <select class="form-control form-control-sm">\n' +
            '                                <option value="">Not Disabled</option>\n' +
            '                                <option value="">Disabled</option>\n' +
            '                            </select>\n' +
            '                        </td>\n' +
            '                        <td>\n' +
            '                            <a href="#" class="js-remove-dependent"><i class="fal fa-trash-alt mt-2"></i></a>\n' +
            '                        </td>\n' +
            '                    </tr>';

        $('#dependentsTable').append(tr);
    });

    $('.js-add-employee').on('click',function () {
        var tr = '<tr>\n' +
            '                        <td><input type="text" class="form-control"></td>\n' +
            '                        <td><input type="text" class="form-control"></td>\n' +
            '                        <td><input type="text" class="form-control"></td>\n' +
            '                        <td><input type="text" class="form-control"></td>\n' +
            '                        <td><input type="text" class="form-control"></td>\n' +
            '                        <td><a href="#" class="js-remove-dependent"><i class="fal fa-trash-alt mt-2"></i></a></td>\n' +
            '                    </tr>';

        $(this).siblings('table').find('tbody').append(tr);
    });



    $(document).on('click','.js-remove-dependent',function (e) {
        e.preventDefault();
        $(this).parents('tr').remove();
    });

    $('#expandAllCollapses').on('change',function () {
        if($(this).is(':checked')){
            $('#cc-table-data').find('.collapse').collapse('show');
        }else{
            $('#cc-table-data').find('.collapse').collapse('hide');
        }
    });

    $('.file-upload-custom .btn').on('click',function (e) {
        e.preventDefault();
        $(this).siblings('input').click();
    });

    $('#customerMailingAddress').on('change',function() {
        if( $(this).is(':checked')) {
            $(".jsDifferentMailingAddress").fadeIn();
        }else{
            $(".jsDifferentMailingAddress").fadeOut();
        }
    });

    // ee-ole-medical-assign-pcp
    $("input:checkbox[id^='assignToMe']").on("change", function(){
        $(this).closest("tr").find("input").not(this).prop("disabled", this.checked);
    });

    var data = {
        'network' : "Kaiser Permanente HMsdfsdO A",
        'deductible':'$1,000 / $2,000 (applies to Max OOP)',
        'out_of_pocket':'$7,500 / $15,100',
        'lifetime_max':'Unlimited',
        'office_visit':' $55 Copay (ded waived)' +
    '<ul class="cc-ul">' +
        '<li class="plain">$75 Copay (ded waived)</li>' +
    '<li class="plain">$70 Copay (ded waived)</li>' +
    '<li class="plain">$350 Copay per procedure</li>' +
    '<li class="plain">100% (ded waived</li>' +
    '<li class="plain">Not Covered</li>' +
    '</ul>',
        'hospital_service':'<ul class="cc-ul mt-4">' +
    '<li class="plain">$65</li>' +
        '<li class="plain">$65</li>' +
        '<li class="plain">$65</li>' +
        '<li class="plain">$65%</li>' +
        '</ul>'
    };

    function selectPlan(num,rem){
        if(rem){
            $('#network' + num).html('');
            $('#deductible' + num).html('');
            $('#out_of_pocket' + num).html('');
            $('#lifetime_max' + num).html('');
            $('#office_visit' + num).html('');
            $('#hospital_service' + num).html('');
        }else{
            $('#network' + num).html(data['network']);
            $('#deductible' + num).html(data['deductible']);
            $('#out_of_pocket' + num).html(data['out_of_pocket']);
            $('#lifetime_max' + num).html(data['lifetime_max']);
            $('#office_visit' + num).html(data['office_visit']);
            $('#hospital_service' + num).html(data['hospital_service']);
        }
    }

    $(".select-plan").on('change',function(){
        var td = $(this).parents('.plan-td');
        var index = $(this).parents('tr .plan-td').index();
        if($(this).val()!==""){
            $(this).parents('.plan-table').find('tr').find('.plan-td:nth-child(' + (index + 1) + ')').addClass("active");
            td.find('.plan_radio,.plan_price').fadeIn();
            selectPlan(index,false);
        }else{
            $(this).parents('.plan-table').find('tr').find('.plan-td:nth-child(' + (index + 1) + ')').removeClass("active");
            td.find('.plan_radio,.plan_price').hide();
            selectPlan(index,true);
        }
    });

    $('[name="planOptions"]').on('change',function () {
        var index = $(this).parents('tr .plan-td').index();
        $('.plan-table').find('.plan-td').removeClass("highlight");
        $(this).parents('.plan-table').find('tr').find('.plan-td:nth-child(' + (index + 1) + ')').addClass("highlight");
    })

});
