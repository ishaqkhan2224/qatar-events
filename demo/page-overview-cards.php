<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Forms</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<?php include "common/subheader-tabs.php"; ?>

<section id="cc-body">
    <div class="container">
        <a href="#" class="back-page"><i class="fas fa-long-arrow-alt-left"></i> Back to Processed Requests</a>
        <div class="section-overview-title">
            <h2>Adam West - Manager</h2>
            <?php
                if (isset($_GET['pending'])):
            ?>
            <p><strong>Request Type:</strong> Change Request <span class="badge cc-warning ml-3"><i class="fas fa-clock mr-1"></i> {{status label}} </span></p>
            <?php else: ?>
            <p><strong>Request Type:</strong> Change Request <span class="badge cc-error ml-3"><i class="fas fa-times mr-1"></i> {{status label}} </span></p>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="cc-card-wrapper">
                    <h3>Employee &amp; Dependent Info</h3>
                    <div class="cc-card large">

                        <div class="row mb-3">
                            <div class="col-md-6">
                                <p>
                                    <b>Mailing Address</b><br>
                                    701 N. Baxter St.<br>
                                    Irvine, Orange Country, CA 91301
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p>
                                    <b>Billing Address</b><br>
                                    514 N, Edgley Ave.<br>
                                    Irvine, Orange Country, CA 91301
                                </p>
                            </div>
                        </div>

                       <div class="row">
                           <div class="col-12">
                               <table class="table card-table">
                                   <thead>
                                   <tr>
                                       <th scope="col">Name</th>
                                       <th scope="col" class="text-center">DOB</th>
                                       <th scope="col" class="text-center">Gender</th>
                                       <th scope="col" class="text-center">SSN</th>
                                       <th scope="col">Relationship</th>
                                   </tr>
                                   </thead>
                                   <tbody>
                                   <tr>
                                       <td>Adam West</td>
                                       <td class="text-center">09/19/1928</td>
                                       <td class="text-center">M</td>
                                       <td class="text-center">xxx-xx-1435</td>
                                       <td>Employee</td>
                                   </tr>
                                   <tr>
                                       <td>Adam West</td>
                                       <td class="text-center">09/19/1928</td>
                                       <td class="text-center">M</td>
                                       <td class="text-center">xxx-xx-1435</td>
                                       <td>Spouse</td>
                                   </tr>
                                   <tr>
                                       <td>Adam West</td>
                                       <td class="text-center">09/19/1928</td>
                                       <td class="text-center">M</td>
                                       <td class="text-center">xxx-xx-1435</td>
                                       <td>Son</td>
                                   </tr>
                                   </tbody>
                               </table>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div><!-- row -->

        <div class="row mt-4">
            <div class="col-12">
                <div class="cc-card-wrapper">
                    <h3>Cost Summary</h3>
                    <p>Summary of benefit cost to employer and employee</p>
                    <div class="cc-card large">
                        
                       <div class="row">
                           <div class="col-12">
                                <table class="table card-table">
                                    <thead>
                                    <tr>
                                        <th scope="col" width="5%"></th>
                                        <th scope="col" width="25%">Current Plan</th>
                                        <th scope="col" width="20%" class="text-right pr-4">Current Cost</th>
                                        <th scope="col" width="20%" class="card-table-highlight pl-4">Elected Plan</th>
                                        <th scope="col" width="15%" class="text-right card-table-highlight">Employer Cost</th>
                                        <th scope="col" width="15%" class="text-right card-table-highlight">Employee Cost</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><i class="fal fa-briefcase-medical"></i></td>
                                        <td>UnitedHealthCare Silver HMO</td>
                                        <td class="text-right pr-4">$482.35</td>
                                        <td class="card-table-highlight pl-4">HealthNet Gold HMO  <span class="badge cc-success">New</span></td>
                                        <td class="text-right card-table-highlight">$69.72</td>
                                        <td class="text-right card-table-highlight">$1,569.72</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fal fa-tooth"></i></td>
                                        <td>Dentegra Smile Club</td>
                                        <td class="text-right pr-4">$175.65</td>
                                        <td class="card-table-highlight pl-4">Smile Saver 3000</td>
                                        <td class="text-right card-table-highlight">$50.25</td>
                                        <td class="text-right card-table-highlight">$1,150.25</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fal fa-glasses"></i></td>
                                        <td>EyeMed</td>
                                        <td class="text-right pr-4">$123.42</td>
                                        <td class="card-table-highlight pl-4">EyeMed</td>
                                        <td class="text-right card-table-highlight">$17.28</td>
                                        <td class="text-right card-table-highlight">$1,238.24</td>
                                    </tr>
                                    <tr>
                                        <td><i class="cc-icon-chiro"></i></td>
                                        <td>Landmark Chiro/Accupunture</td>
                                        <td class="text-right pr-4">$0</td>
                                        <td class=" card-table-highlight pl-4">Landmark Chiro/Accupunture</td>
                                        <td class="text-right card-table-highlight">$0</td>
                                        <td class="text-right card-table-highlight">$0</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fal fa-heartbeat"></i></td>
                                        <td>MetLife</td>
                                        <td class="text-right pr-4">$0</td>
                                        <td class="card-table-highlight pl-4">MetLife</td>
                                        <td class="text-right card-table-highlight">$0</td>
                                        <td class="text-right card-table-highlight">$0</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fas fa-usd-square"></i></td>
                                        <td><strong>Total Cost</strong></td>
                                        <td class="text-right pr-4"><strong>$3,737.25</strong></td>
                                        <td class="card-table-highlight pl-4"><strong>Total Cost</strong></td>
                                        <td class="text-right card-table-highlight"><strong>$3,737.25</strong></td>
                                        <td class="text-right card-table-highlight"><strong>$3,737.25</strong></td>
                                    </tr>
                                    </tbody>
                                </table>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div><!-- row -->

        <div class="row mt-4">
            <div class="col-12">
                <div class="cc-card-wrapper">
                    <h3>Coverage Summary</h3>
                    <p>Employee and dependents coverage summary.</p>

                    <div class="cc-card large">
                       <div class="row">
                           <div class="col-12">
                            <h4 class="mb-3">PRODUCT #1</h4>
                            <table class="table card-table">
                                <thead>
                                    <tr>
                                        <th width="25%">Name</th>
                                        <th width="25%">Current</th>
                                        <th width="50%" class="card-table-highlight pl-4">Elected</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Name 01</td>
                                    <td>{Current Plan Name}</td>
                                    <td class="card-table-highlight pl-4">{Elected Plan Name}  <span class="badge cc-success">New</span></td>
                                </tr>
                                <tr>
                                    <td>Name 02</td>
                                    <td><i class="fa fa-check"></i></td>
                                    <td class="card-table-highlight pl-4"><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Name 03</td>
                                    <td><i class="fa fa-check"></i></td>
                                    <td class="card-table-highlight pl-4"><i class="fa fa-check"></i></td>
                                </tr>
                                </tbody>
                            </table>
                           </div>
                       </div>
                    </div><!-- cc-card large -->

                    <div class="cc-card large">
                       <div class="row">
                           <div class="col-12">
                            <h4 class="mb-3">PRODUCT #2</h4>
                            <table class="table card-table">
                                <thead>
                                    <tr>
                                        <th width="25%">Name</th>
                                        <th width="25%">Current</th>
                                        <th width="50%" class="card-table-highlight pl-4">Elected</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Name 01</td>
                                    <td>{Current Plan Name}</td>
                                    <td class="card-table-highlight pl-4">{Elected Plan Name}</td>
                                </tr>
                                <tr>
                                    <td>Name 02</td>
                                    <td><i class="fa fa-check"></i></td>
                                    <td class="card-table-highlight pl-4"><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Name 03</td>
                                    <td><i class="fa fa-check"></i></td>
                                    <td class="card-table-highlight pl-4"><i class="fa fa-check"></i></td>
                                </tr>
                                </tbody>
                            </table>
                           </div>
                       </div>
                    </div><!-- cc-card large -->

                    <div class="cc-card large">
                       <div class="row">
                           <div class="col-12">
                            <h4 class="mb-3">PRODUCT #3</h4>
                            <table class="table card-table">
                                <thead>
                                    <tr>
                                        <th width="25%">Name</th>
                                        <th width="25%">Current</th>
                                        <th width="50%" class="card-table-highlight pl-4">Elected</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Name 01</td>
                                    <td>{Current Plan Name}</td>
                                    <td class="card-table-highlight pl-4">{Elected Plan Name}</td>
                                </tr>
                                <tr>
                                    <td>Name 02</td>
                                    <td><i class="fa fa-check"></i></td>
                                    <td class="card-table-highlight pl-4"><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Name 03</td>
                                    <td><i class="fa fa-check"></i></td>
                                    <td class="card-table-highlight pl-4"><i class="fa fa-check"></i></td>
                                </tr>
                                </tbody>
                            </table>
                           </div>
                       </div>
                    </div><!-- cc-card large -->

                    <div class="cc-card large">
                       <div class="row">
                           <div class="col-12">
                            <h4 class="mb-3">PRODUCT #4</h4>
                            <table class="table card-table">
                                <thead>
                                    <tr>
                                        <th width="25%">Name</th>
                                        <th width="25%">Current</th>
                                        <th width="50%" class="card-table-highlight pl-4">Elected</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Name 01</td>
                                    <td>{Current Plan Name}</td>
                                    <td class="card-table-highlight pl-4">{Elected Plan Name}</td>
                                </tr>
                                <tr>
                                    <td>Name 02</td>
                                    <td><i class="fa fa-check"></i></td>
                                    <td class="card-table-highlight pl-4"><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Name 03</td>
                                    <td><i class="fa fa-check"></i></td>
                                    <td class="card-table-highlight pl-4"><i class="fa fa-check"></i></td>
                                </tr>
                                </tbody>
                            </table>
                           </div>
                       </div>
                    </div><!-- cc-card large -->

                </div>
            </div>
        </div><!-- row -->       

        <div class="row mt-4">
            <div class="col-12">
                <div class="cc-card-wrapper">
                    <h3>Comment</h3>
                    <div class="cc-card large">
                       <div class="row">
                           <div class="col-12">
                               <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti aliquid vero harum rerum voluptates ab ullam explicabo maxime inventore alias, molestias reiciendis voluptate modi sunt placeat in, vel illo, dolorem.</span>
                               <span>In, iure cum? Debitis laborum veritatis, modi eligendi consequuntur ducimus, a laudantium provident, deleniti earum voluptas delectus labore dolor. Beatae excepturi explicabo suscipit deleniti voluptatum quos nostrum iure doloremque, cupiditate.</span>
                               </p>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div><!-- row -->

    </div>
</section>

</body>
</html>