<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer > Management</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Manage Employees</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="" class="btn btn-blue mr-3">Create a New Hire Quote</a>
                    <div class="dropdown dd-transparent">
                        <button class="btn secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Order ID Cards</a>
                            <a class="dropdown-item" href="#">View Invoices</a>
                            <a class="dropdown-item" href="#">Commissions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="javascript:;">Active Employees</a>
            <a class="nav-link" href="javascript:;">Recently Added</a>
            <a class="nav-link" href="javascript:;">COBRA</a>
            <a class="nav-link" href="javascript:;">Terminated</a>
            <a class="nav-link" href="javascript:;">Pending Requests</a>
            <a class="nav-link" href="javascript:;">Processed Requests</a>
            <a class="nav-link active" href="er-manage-division-report-summary">Division Reports</a>
            <a class="nav-link" href="er-manage-renewals">Renewals</a>
        </nav>
    </div>
</section>
<form id="cc-form__manage-employees" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <p class="mb-5">Now assign employees to divisions to created. Any Employees not assigned to a division will be set to unassigned. </p>
                    <div>
                        <div class="data-tables-top">
                            <div class="right">
                                <button class="btn btn btn-grey-outline d-inline-block show-cc-modal" data-cc-modal="#ccAddEmployeeModal"><i class="fal fa-filter mr-2"></i>Filter Division</button>
                                <button class="btn btn btn-grey-outline d-inline-block show-cc-modal" data-cc-modal="#ccAddEmployeeModal"><i class="fal fa-edit mr-2"></i>Edit Division</button>
                            </div>
                        </div>
                        <table class="table js-sortable-table" id="cc-table-data">
                            <thead>
                                <tr>
                                    <th scope="col" class="">Employee Name</th>
                                    <th scope="col" class="no-sort">Status</th>
                                    <th scope="col" class="no-sort">Division</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="">Bertram Gilfoye</td>
                                    <td>Active</td>
                                    <td>
                                        <select id="" class="form-control">
                                            <option value="">Unassigned</option>
                                            <option value="">Midwest</option>
                                            <option value="">Northwest</option>
                                            <option value="">South</option>
                                            <option value="">West</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">Dinesh Chugtai</td>
                                    <td>Active</td>
                                    <td>
                                        <select id="" class="form-control">
                                            <option value="">Unassigned</option>
                                            <option value="">Midwest</option>
                                            <option value="">Northwest</option>
                                            <option value="">South</option>
                                            <option value="">West</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">Donald Dunn</td>
                                    <td>Active</td>
                                    <td>
                                        <select id="" class="form-control">
                                            <option value="">Unassigned</option>
                                            <option value="">Midwest</option>
                                            <option value="">Northwest</option>
                                            <option value="">South</option>
                                            <option value="">West</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">Erlish Bachman</td>
                                    <td>Active</td>
                                    <td>
                                        <select id="" class="form-control">
                                            <option value="">Unassigned</option>
                                            <option value="">Midwest</option>
                                            <option value="">Northwest</option>
                                            <option value="">South</option>
                                            <option value="">West</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">Gavin Belson</td>
                                    <td>Active</td>
                                    <td>
                                        <select id="" class="form-control">
                                            <option value="">Unassigned</option>
                                            <option value="">Midwest</option>
                                            <option value="">Northwest</option>
                                            <option value="">South</option>
                                            <option value="">West</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">Jian Yang</td>
                                    <td>Active</td>
                                    <td>
                                        <select id="" class="form-control">
                                            <option value="">Unassigned</option>
                                            <option value="">Midwest</option>
                                            <option value="">Northwest</option>
                                            <option value="">South</option>
                                            <option value="">West</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">Laurie Bream</td>
                                    <td>Active</td>
                                    <td>
                                        <select id="" class="form-control">
                                            <option value="">Unassigned</option>
                                            <option value="">Midwest</option>
                                            <option value="">Northwest</option>
                                            <option value="">South</option>
                                            <option value="">West</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">Monica Hall</td>
                                    <td>Active</td>
                                    <td>
                                        <select id="" class="form-control">
                                            <option value="">Unassigned</option>
                                            <option value="">Midwest</option>
                                            <option value="">Northwest</option>
                                            <option value="">South</option>
                                            <option value="">West</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">Nelson Bighetti</td>
                                    <td>Active</td>
                                    <td>
                                        <select id="" class="form-control">
                                            <option value="">Unassigned</option>
                                            <option value="">Midwest</option>
                                            <option value="">Northwest</option>
                                            <option value="">South</option>
                                            <option value="">West</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">Rlchard Hendricks</td>
                                    <td>Active</td>
                                    <td>
                                        <select id="" class="form-control">
                                            <option value="">Unassigned</option>
                                            <option value="">Midwest</option>
                                            <option value="">Northwest</option>
                                            <option value="">South</option>
                                            <option value="">West</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Back</a>
                </div>
                <div class="right">
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>