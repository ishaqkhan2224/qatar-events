<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer OLE</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Online Enrollment</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="er-ole-overview">Overview</a>
            <a class="nav-link" href="er-ole-application">Application</a>
            <a class="nav-link active" href="er-ole-census">Census</a>
            <a class="nav-link" href="er-ole-metaltier">Metal Tier</a>
            <a class="nav-link" href="er-ole-contribution">Contribution</a>
            <a class="nav-link" href="er-ole-optional-benefits">Optional Benefits</a>
            <a class="nav-link" href="er-ole-summary">Summary</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h5>Group's census information</h5>
                </div>
                <div class="col-md-6 text-right">

                    <div class="form-row">
                        <div class="col-4 my-auto">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="expandAllCollapses">
                                    <label class="custom-control-label" for="expandAllCollapses">Expand All</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <select class="form-control">
                                    <option value="">Download options</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <button class="btn btn-blue btn-block">Upload Census</button>
                        </div>
                    </div>

                </div>
            </div>
            <table class="table" id="cc-table-data">
                <thead>
                <tr>
                    <th scope="col">Name & SSN</th>
                    <th scope="col">Gender & DOB</th>
                    <th scope="col">County & ZIP</th>
                    <th scope="col">Date of Hire</th>
                    <th scope="col">COBRA</th>
                    <th scope="col">Enrolment Type</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Donal Dunn<br>xxx-xx-0123</td>
                    <td>Male<br>12/24/1988</td>
                    <td>Los Angeles<br>91755</td>
                    <td>04/28/219</td>
                    <td>No</td>
                    <td>Online<br>ddunn@gmail.com</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit</a>
                                <a class="dropdown-item" href="#">Delete</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="border-0">
                        <a href="#" data-toggle="collapse" class="collapsed census-info-dependents" data-target="#demo1">
                            <i class="far"></i> Spouse & 3 Dependents
                        </a>
                    </td>
                </tr>
                <tr class="cc-table__gradient-row">
                    <td colspan="7" class="p-0 border-0">
                        <div id="demo1" class="collapse">
                            <div class="census-info">
                                <p><span>Spouse (Age or DOB):</span> 10/28/1989</p>
                                <p><span># of Dependents (Ages 0-14):</span> 1</p>
                                <p><span># of Dependents (Ages 15+):</span> 3 (16 yrs old, 26 yrs old disabled)</p>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Gavin Belson<br>xxx-xx-0123</td>
                    <td>Male<br>12/24/1988</td>
                    <td>Los Angeles<br>91755</td>
                    <td>04/28/219</td>
                    <td>No</td>
                    <td>Paper<br></td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit</a>
                                <a class="dropdown-item" href="#">Delete</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="cc-table__gradient-row">
                    <td colspan="7" class="p-0">
                        <div id="demo2" class="collapse">
                            <div class="census-info">
                                <p>Spouse</p>
                                <p><span>Spouse (Age or DOB)</span> 10/28/1989</p>
                                <p><span># of Depedents (Ages 0-14):</span> 1</p>
                                <p><span>Dependents (Ages 15+)</span> 16 yrs old, 26 yrs old (Disabled)</p>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Jian Yang<br>xxx-xx-0123</td>
                    <td>Male<br>12/24/1988</td>
                    <td>Los Angeles<br>91755</td>
                    <td>04/28/219</td>
                    <td>No</td>
                    <td>Online<br>jyang@gmail.com</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit</a>
                                <a class="dropdown-item" href="#">Delete</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="border-0">
                        <a href="#" data-toggle="collapse" class="collapsed census-info-dependents" data-target="#demo3">
                            <i class="far"></i> 2 Dependents
                        </a>
                    </td>
                </tr>
                <tr class="cc-table__gradient-row">
                    <td colspan="7" class="p-0 border-0">
                        <div id="demo3" class="collapse">
                            <div class="census-info">
                                <p><span># of Dependents (Ages 0-14):</span> 2</p>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>

            <button type="button" class="btn btn-grey-outline d-inline-block js-cc-slideout-show" data-cc-modal="#ccAddEmployeeModal"><i class="fas fa-plus mr-2"></i>Add Employee</button>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Cancel</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>

<form action="" id="cc-form__modal">
    <div id="ccAddEmployeeModal" class="cc-modal cc-add-employee-modal">
        <div class="cc-modal-header">
            <span class="cc-modal-close js-cc-slideout-close"><i class="fa fa-times"></i></span>
            <h5>Add Employee</h5>
        </div>
        <div class="cc-modal-inner">
            <div class="cc-modal-body">
                <div class="alert cc-success mb-4" role="alert" style="display: none;"><strong>Parth Shah has been added!</strong> Add another employee?</div>
                <div class="cc-modal-inner-content">
                <h5>Employee Information</h5>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="text-uppercase">Last Name</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                    <div class="form-group col-md-6">
                        <label class="text-uppercase">First Name</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label class="text-uppercase">SSN</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                    <div class="form-group col-md-3">
                        <label class="text-uppercase">Age or DOB</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                    <div class="form-group col-md-3">
                        <label class="text-uppercase">Date of Hire</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                    <div class="form-group col-md-3">
                        <label class="text-uppercase">COBRA</label>
                        <select class="form-control form-control-sm">
                            <option value="">Please Select</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label class="text-uppercase">Zip Code</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                    <div class="form-group col-md-3">
                        <label class="text-uppercase">County</label>
                        <select class="form-control form-control-sm">
                            <option value="">Please Select</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="text-uppercase">Enrollment Type</label>
                        <select class="form-control form-control-sm">
                            <option value="">Please Select</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="text-uppercase">Email</label>
                        <input type="email" class="form-control form-control-sm">
                        <p class="text-muted"><small>Required for online enrollment</small></p>
                    </div>
                </div>

                <h5>Spouse & Dependents</h5>
                <div class="form-group">
                  <div class="form-row">
                      <div class="col-6">
                        <label class="col-form-label">
                            Spouse's age or date of birth
                        </label>
                      </div>
                      <div class="col-2">
                        <input type="text" class="form-control form-control-sm">
                      </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="form-row">
                      <div class="col-6">
                        <label class="col-form-label">
                            Number of dependents (Ages 0-14):
                        </label>
                      </div>
                      <div class="col-2">
                        <input type="text" class="form-control form-control-sm">
                      </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="form-row">
                      <div class="col-12">
                        <label class="col-form-label">
                            Number of dependents (Ages 15+)<br>
                            <small>Dependents 26 and older must be disabled to quantify for coverage.</small>
                        </label>
                      </div>
                   </div>
                </div>

                <div class="row">
                    <div class="col-8">
                        <table id="dependentsTable" class="table card-table">
                          <thead>
                              <tr>
                                <th class="pl-0" width="60%">Dependent age or DOB</th>
                                <th width="20%">Disability</th>
                                <th></th>
                            </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td class="pl-0"><input type="text" class="form-control form-control-sm"></td>
                                  <td>
                                      <select class="form-control form-control-sm">
                                          <option value="">Not Disabled</option>
                                          <option value="">Disabled</option>
                                      </select>
                                  </td>
                                  <td>&nbsp;</td>
                              </tr>
                          </tbody>
                        </table>
                        <button type="button" class="btn btn-sm btn-grey-outline js-add-dependent mb-5"><i class="fas fa-plus ml-1"></i> Add Another Dependent</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="cc-modal-footer fixed">
            <div class="d-flex justify-content-between">
               <div class="col-4">
                <a href="#" class="btn-link js-cc-slideout-close">Close</a>
               </div>
               <div class="col-6">
                    <div class="d-flex justify-content-end">
                        <a href="#" class="btn btn-blue js-show-confirmation-alert">Save</a>
                    </div>

               </div>
            </div>
        </div>
    </div><!-- ccAddEmployeeModal -->
</form>
<?php include "common/footer.php"; ?>
</body>
</html>