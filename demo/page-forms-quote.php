<!DOCTYPE html>
<html>
<head>
<?php include "inc/header.meta.php"; ?>
<title>CC UI KIT: Forms</title>
<?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<?php 
//include "common/subheader-progressive-tabs.php";
include "common/subheader-tabs.php";
?>

<form id="cc-form__new-quote" method="post" action="">
<section id="cc-body">
    <div class="container">
        <?php include "inc/forms.standard.php" ?>
    </div><!-- container -->
</section>

<section class="cc-controls sticky">
    <div class="container">
        <div class="d-flex align-items-center">
            <div class="left">
                <a href="#" class="btn-link">Cancel</a>
            </div>
            <div class="right">
                <a href="#" class="btn-link">Save & Exit</a>
                <button class="btn">Next</button>
            </div>
        </div>
    </div>
</section>
</form>
<?php include "common/footer.php"; ?>
<script>
    $('#cc-form__new-quote').validate({
        errorClass: "is-invalid",
        //validClass: "is-valid",
        rules: {
            same_address: "required",
            billing: "required",
            AgreeDisagree1: "required",
            AgreeDisagree2: "required",
            AgreeDisagree3: "required",
            AgreeDisagree4: "required"
        },
        messages: {
            business_name: {
                required: "Business Name is required."
            }
        }
    });
</script>
</body>
</html>