<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Page Layout > Content Template</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body class="cc-public-content-template">
<header class="main-header fixed">
    <div class="container">
        <div class="cc-header__wrapper">
            <nav class="navbar navbar-expand-lg cc-header__main-nav">
                <a class="navbar-brand" href="#"><img src="../global/img/calchoicelogo.png" alt="logo"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon fas fa-bars"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="BrokersDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Brokers</a>
                            <div class="dropdown-menu" aria-labelledby="BrokersDropdown">
                                <a class="dropdown-item" href="#">Menu One</a>
                                <a class="dropdown-item" href="#">Menu Two</a>
                                <a class="dropdown-item" href="#">Menu Three</a>
                                <a class="dropdown-item" href="#">Menu Four</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="EmployersDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Employers</a>
                            <div class="dropdown-menu" aria-labelledby="EmployersDropdown">
                                <a class="dropdown-item" href="#">Menu One</a>
                                <a class="dropdown-item" href="#">Menu Two</a>
                                <a class="dropdown-item" href="#">Menu Three</a>
                                <a class="dropdown-item" href="#">Menu Four</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="EmployeesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Employees</a>
                            <div class="dropdown-menu" aria-labelledby="EmployeesDropdown">
                                <a class="dropdown-item" href="#">Menu One</a>
                                <a class="dropdown-item" href="#">Menu Two</a>
                                <a class="dropdown-item" href="#">Menu Three</a>
                                <a class="dropdown-item" href="#">Menu Four</a>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="#">Who We Are</a></li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item"><a class="nav-link" href="#">Find a Doctor</a></li>
                        <li class="nav-item"><a class="btn btn-light-blue" href="#">Login</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>

<section class="cc-content-template-hero cc-hero-{page-name}"></section>

<section class="cc-content-template-outer-wrapper bg-white">
    <div class="container">
        <h1>Employer Eligibility</h1>
        <p class="text-center">Eligibility requirements for small businesses with 1-100 employees interested in enrolling in CaliforniaChoice&req; are below.<br>For a complete listing of requirements, download our <a href="">Underwriting Guidelines</a></p>
    </div>
</section>
<section class="cc-content-template-outer-wrapper employee-requirements">
    <div class="container">
        <div><img class="img-fluid mx-auto" src="./images/public-layout/title.png" alt=""></div>
        <div><img class="main-avatar" src="./images/public-layout/avatar.png" alt=""></div>
        <div class="row">
            <div class="col-md-3">
                <div class="employee-requirement-box light-blue">
                    <img class="dots" src="./images/public-layout/light-blue-dots.png" alt="">
                    <div class="icon">
                        <img src="./images/public-layout/public-icon-1.png" alt="">
                    </div>
                    <p>Meet the employer's waiting period.</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="employee-requirement-box blue">
                    <img class="dots" src="./images/public-layout/blue-dots.png" alt="">
                    <div class="icon">
                        <img src="./images/public-layout/public-icon-2.png" alt="">
                    </div>
                    <p>Be permanent and actively working an average of 30+ hours per week over the course of a month, at the small employer's
                        regular place of business or 20+ hours per normal work week for at least 50% of the weeks in the previous calendar quarter.</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="employee-requirement-box green">
                    <img class="dots" src="./images/public-layout/green-dots.png" alt="">
                    <div class="icon">
                        <img src="./images/public-layout/public-icon-3.png" alt="">
                    </div>
                    <p>Be a permanent employee who is not eligible for medical healthcase coverage offered by or through a labour union.</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="employee-requirement-box orange">
                    <img class="dots" src="./images/public-layout/orange-dots.png" alt="">
                    <div class="icon">
                        <img src="./images/public-layout/public-icon-4.png" alt="">
                    </div>
                    <p>Be paid on a salary/hourly basis (not 1099, commissioned, or substitute).</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cc-content-template-outer-wrapper bg-white">
    <div class="container">
        <h2>Employee Eligibility at Initial Enrollment</h2>
        <p class="text-center">As an employer, you must meet the following participation requirements:</p>
        <div class="row mt-5">
            <div class="col-md-6">
                <div class="cc-bordered-box">
                    <div class="media">
                      <img src="./images/public-layout/1-2-employees.png" class="align-self-center mr-3">
                      <div class="media-body">
                        <h5 class="mt-0">Participation Requirements for 1 - 2 Employees</h5>
                        <p class="mb-0">100% of all employees. All groups must include at least one medical enrolled employee who is not a business owner or spouse/domestic partner of the business.</p>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="cc-bordered-box">
                    <div class="media">
                      <img src="./images/public-layout/3-100-employees.png" class="align-self-center mr-3">
                      <div class="media-body">
                        <h5 class="mt-0">Participation Requirements for 3 - 100 Employees</h5>
                        <p class="mb-0">100% of all employees. All groups must include at least one medical enrolled employee who is not a business owner or spouse/domestic partner of the business.</p>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cc-content-template-outer-wrapper">
    <div class="container">
        <h2>Other Employee Eligibility Requirements</h2>
        <div class="row">
            <div class="col-md-6 d-flex">
                <div class="cc-white-box">
                    <p>Employees with other group coverage are not counted toward participation unless the employer contribution is 100%.</p>
                </div>
            </div>
            <div class="col-md-6 d-flex">
                <div class="cc-white-box">
                    <p>51% of eligible employees must reside in California.</p>
                </div>
            </div>
            <div class="col-md-6 d-flex">
                <div class="cc-white-box">
                    <p>Home office must be in California (principal executive ofice).</p>
                </div>
            </div>
            <div class="col-md-6 d-flex">
                <div class="cc-white-box">
                    <p>Have active Worker's Compensation coverage.</p>
                </div>
            </div>
            <div class="col-md-6 d-flex">
                <div class="cc-white-box">
                    <p>Have a valid Tax ID Number (not a Social Security Number).</p>
                </div>
            </div>
            <div class="col-md-6 d-flex">
                <div class="cc-white-box">
                    <p>If a business was established after the preceding quarter, payroll may be accepte in lieu of a Quarterly Wage Report, at the discretion of the Underwriter.</p>
                </div>
            </div>
        </div>
        <div class="text-center">
            <a href="#" class="btn btn-light-blue mt-4">Read More</a>
        </div>
    </div>
</section>

<div class="cc-quote-bg">
    <p>Your principal executive home office must be in California to be an eligible employer.</p>
</div>

<section class="cc-content-template-outer-wrapper bg-white">
    <div class="container">
        <h2>Employee Eligibility</h2>
        <p>Eligible employees are permanent and actively working on average of 30+ hours per week over the course of a month, at the employer's regular place of business or, 20+ hours per normal work week for at least 50% of the weeks in the previous calendar quarter.</p>
        <p><strong>Ineligible employees include:</strong></p>
        <div class="row">
            <div class="col-md-6">
                <ul>
                    <li>1099 employees</li>
                    <li>Commissioned employees</li>
                    <li>Permanent employees eligible for medical health care coverage offered by or through a labor union</li>
                    <li>Part-time employees</li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul>
                    <li>Seasonal employees</li>
                    <li>Temporary or subsiture employees</li>
                    <li>Employees on a leave of absence not categorized as FMLA, Workers Compensation, or Military</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="cc-content-template-outer-wrapper">
    <div class="container">
        <h2>Employer Eligibility at Initial Enrollment</h2>
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li>As the employer, you must contribut a minimum of 50% of the lowest cost medical benefit plan available to employees.</li>
                    <li>Dependent participation and contribution, if any, is at your discretion.</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<footer class="main-footer">
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-md-4">
                    <img class="footer-logo" src="./images/logo-white.png" alt="logo">
                    <p class="address-line">
                        721 South Parker, Suite 200<br>
                        Orange, CA 92868
                    </p>
                </div>
                <div class="col-md-8">
                    <ul class="footer-nav">
                        <li><a href="#">Our Story</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                    <p>Department of Insurance License #0B42994 <br>
                        Office Hours:  Monday – Friday 8:00 am to 5:00 pm PST </p>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div>
                <p class="copyright">© 2019 CaliforniaChoice | A CHOICE Administrators Program</p>
            </div>
            <div>
                <div class="social-list">
                    <a href="#"><i class="fab fa-facebook-square"></i></a>
                    <a href="#"><i class="fab fa-twitter-square"></i></a>
                    <a href="#"><i class="fab fa-linkedin"></i></a>
                    <a href="#"><i class="fab fa-google-play"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php include "common/footer.php"; ?>
</body>
</html>