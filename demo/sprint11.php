<!DOCTYPE html>
<html>
<head>
<?php include "inc/header.meta.php"; ?>
<title>CC UI Deliverables: Sprint 11</title>
<?php include "inc/header.link.php"; ?>
</head>
<body>

<!--
<div class="alert alert-warning mb-5" role="alert">
    <strong>xxx</strong> | <a href="xxx" target="_blank">Wireframe</a>
</div>
<div class="row">
    <div class="col-6">
        // goes here
    </div>
</div>
<hr class="break">
-->

<section id="cc-body">
    <div class="container">
        <h1 class="text-center mb-5">Sprint 11 UI Deliverables</h1>

        <div class="alert alert-warning mb-5" role="alert">
            <strong>447183 - OLE Broker: Unfinished Enrollment Delete Process and Email</strong> | <a href="https://nes6iy.axshare.com/#id=zrfq9l&p=ole_br_unfinished_enrollment_effective_date&g=1" target="_blank">Wireframe</a>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="js-demo-alert-unfinished-enrollment" class="alert cc-success alert-dismissible fade" role="alert" style="display:none">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <h5>Enrollment for the following groups are unfinished</h5>
                <table class="table" id="cc-table-data">
                    <thead>
                        <tr>
                            <th scope="col">Company Namae</th>
                            <th scope="col" class="text-center">Quote Number</th>
                            <th scope="col" class="text-center">Effective Date</th>
                            <th scope="col"># of Employees</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="js-DEMO-delete-unfinished-enrollment-row">
                            <td>ABC Company Inc</td>
                            <td class="text-center">123456</td>
                            <td class="text-center">05/01/2019</td>
                            <td>5</td>
                            <td class="cc-table__td-action js-td-action">
                                <div class="dropdown cc-table__td-dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Actions
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" data-toggle="modal" data-target="#continueUnfinishedEnrollment" href="#">Continue</a>
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deleteUnfinishedEnrollment" href="#">Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>ACME Corp</td>
                            <td class="text-center">234567</td>
                            <td class="text-center">09/01/2019</td>
                            <td>12</td>
                            <td class="cc-table__td-action js-td-action">
                                <div class="dropdown cc-table__td-dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Actions
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" data-toggle="modal" data-target="#continueUnfinishedEnrollment" href="#">Continue</a>
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deleteUnfinishedEnrollment" href="#">Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Beyond Tech</td>
                            <td class="text-center">345678</td>
                            <td class="text-center">09/01/2019</td>
                            <td>20</td>
                            <td class="cc-table__td-action js-td-action">
                                <div class="dropdown cc-table__td-dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Actions
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" data-toggle="modal" data-target="#continueUnfinishedEnrollment" href="#">Continue</a>
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deleteUnfinishedEnrollment" href="#">Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Carbon Capital</td>
                            <td class="text-center">456789</td>
                            <td class="text-center">09/01/2019</td>
                            <td>30</td>
                            <td class="cc-table__td-action js-td-action">
                                <div class="dropdown cc-table__td-dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Actions
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" data-toggle="modal" data-target="#continueUnfinishedEnrollment" href="#">Continue</a>
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deleteUnfinishedEnrollment" href="#">Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <div class="modal fade" id="continueUnfinishedEnrollment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">ABC Company Inc (Quote #123456)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <p>The effective date of this quote is no longer available for enrollment. Please select a new Effective Date to continue this enrollment:</p>
                        <section class="px-4 py-3 bg-g">
                          <div class="d-flex justify-content-around">
                            <div>
                              <p><strong>Current Effective Date</strong></p>
                              5/1/2019
                            </div>
                            <span class="border-left"></span>
                            <div>
                              <p><strong>New Effective Date</strong></p>
                              <select id="email01" id="email01" class="form-control" required>
                                  <option value="">Select</option>
                              </select>
                            </div>
                          </div>
                        </section>
                        <div class="row mt-4">
                          <div class="col-12">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="xxx" name="xxx">
                                <label class="custom-control-label" for="xxx">By updating the Effective Date I understand that:</label>
                            </div>
                            <ul class="cc-ul mt-4">
                              <li>The current effective date of the quote will be changed</li>
                              <li>Available Coverages/Plans may change</li>
                              <li>Current enrollment selections will be saved unless no longer available</li>
                            </ul>
                            <p class="mt-4">When you complete your broker signature, a new registration email will be sent to the employer. If the employer has already created an account, they will be able to use the same username and password. Once the employer resubmits, a new registration email will be sent to employees enrolling online. <strong>Note: This change cannot be undone</strong></p>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <a href="#" class="btn-link" data-dismiss="modal">Cancel</a>
                        <button type="button" class="btn btn-blue">Continue</button>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="deleteUnfinishedEnrollment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title text-center" id="exampleModalLabel">Delete Unfinished Enrollment?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <p>Are you sure you want to delete ABC Company Inc's unfinished enrollment?<br><strong>This process can't be undone.</strong></p>
                      </div>
                      <div class="modal-footer">
                        <a href="#" class="btn-link" data-dismiss="modal">Cancel</a>
                        <button type="button" class="btn btn-blue" id="js-DEMO-confirm-delete-unfinished-enrollment">Delete</button>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <hr class="break">

        <div class="alert alert-warning mb-5" role="alert">
            <strong>426888 - Employee/Member: Order ID Cards</strong> | <a href="https://nes6iy.axshare.com/#id=dd20t9&p=employee_-_order_id_card&g=1" target="_blank">Wireframe</a>
        </div>
        <form id="cc-form__unfinished-enrollment" method="post" action="">
        <div class="row">
          <div class="col-12">
            <h5>Select ID Cards</h5>
            <table class="table" id="cc-table-data">
                <thead>
                    <tr>
                        <th scope="col">Employee</th>
                        <th scope="col" class="text-center">SSN</th>
                        <th scope="col" class="text-center">Medical</th>
                        <th scope="col" class="text-center">Dental</th>
                        <th scope="col" class="text-center">Vision</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Brian Appleseed</td>
                        <td class="text-center">xxx-xx-3456</td>
                        <td class="text-center">
                          <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="medical-checkbox" name="medical-checkbox" required><label class="custom-control-label" for="medical-checkbox"></label>
                          </div>
                        </td>
                        <td class="text-center">
                          <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="dental-checkbox" name="dental-checkbox" required><label class="custom-control-label" for="dental-checkbox"></label>
                          </div>
                        </td>
                        <td class="text-center">
                          <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="vision-checkbox" name="vision-checkbox" required><label class="custom-control-label" for="vision-checkbox"></label>
                          </div>
                        </td>
                    </tr>
                </tbody>
            </table>
          </div>
        </div>
        <div class="row mt-4">
            <div class="col-5">
                <h5>Your Info</h5>
                <div class="form-group">
                  <label for="xxx">Your Name</label>
                  <input type="text" class="form-control" id="xxx" name="xxx" value="Brian Appleseed" required>
                </div>
                <div class="form-group">
                  <label for="xxx">Your Email</label>
                  <input type="text" class="form-control" id="xxx" name="xxx" value="bappleseed@gmail.com" required>
                </div>
                <div class="form-group">
                  <div class="form-row">
                      <div class="col-9">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" id="address" name="address" required>
                      </div>
                      <div class="col-3">
                        <label for="suite_number">APT</label>
                        <input type="text" class="form-control" id="suite_number" name="suite_number" required>
                      </div>
                   </div>
                </div>
                <div class="form-group">
                  <label for="xxx">Address (line 2)</label>
                  <input type="text" class="form-control" id="xxx" name="xxx" required>
                </div>
                <div class="form-group">
                  <label for="xxx">Zip Code</label>
                  <input type="text" class="form-control" id="xxx" name="xxx" value="94403" required>
                </div>
                <div class="form-group">
                  <label for="xxx">City</label>
                  <select id="" class="form-control" required>
                    <option value="">San Mateo</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="xxx">State</label>
                  <input type="text" class="form-control" id="xxx" name="xxx" value="California" disabled>
                </div>
            </div>
        </div>
        </form>
        <hr class="break">

        <div class="alert alert-warning mb-5" role="alert">
          <strong>User Story: 426892 - Employer: IVR PIN</strong> | <a href="https://nes6iy.axshare.com/#id=qso725&p=er_resources_-_ivr_pin&g=1" target="_blank">Wireframe</a>
        </div>
        <div class="row">
            <div class="col-6">
                <form id="cc-form__ivr" method="post" action="">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                              <label for="ivr-pin-pwd">For security reasons, please enter your password to get your IVR PIN</label>
                              <input type="password" class="form-control w-50" id="ivr-pin-pwd" name="ivr-pin-pwd" required>
                            </div><!-- form-group -->
                            <button class="btn btn-blue mr-2">Get IVR Pin</button>
                            <p style="display:none">Your IVR PIN: <strong>{pin value}</strong></p>
                        </div><!-- col-12 -->
                    </div><!-- row -->
                </form>
            </div><!-- col -->
            <div class="col-6">
                <div class="cc-card-wrapper">
                  <div class="cc-card">
                    <p>Your Interactive Voice Response (IVR) PIN allows you to access the automated phone system 24 hours a day, 7 days a week. You can access the IVR system by dialing (714) 567 - 4425.</p>
                    <p>You can use the IVR system for the following options:</p>
                    <h4>Account Information</h4>
                    <ul class="cc-ul">
                        <li>Account Balance</li>
                        <li>Last Payment Received</li>
                        <li>Payment Due Date</li>
                    </ul>
                    <h4 class="mt-3">Premium Statements</h4>
                    <ul class="cc-ul">
                        <li>Request a faxed copy</li>
                        <li>Request a mailed copy</li>
                        <li>Request an emailed copy</li>
                    </ul>
                  </div>
                </div>
            </div><!-- col -->
        </div>
        <hr class="break">
        <div class="alert alert-warning mb-5" role="alert">
          <strong>426894 - Employee/Member: My Benefits - Details Page - Medical</strong> | <a href="https://nes6iy.axshare.com/#id=9pnc8o&p=ee_benefit_details_-_medical&g=1" target="_blank">Wireframe
        </div>
        <a href="#" class="back-page"><i class="fas fa-long-arrow-alt-left"></i> Back to Dashboard</a>
        <div class="row mt-4">
            <div class="col-9">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h3 class="mx-0 my-0">Anthem Blue Cross</h3>
                        <p>Silver HMO B Benefit</p>  
                    </div>
                    <div class="text-right">
                        Cost Per Pay Period: <strong>$622.83</strong>
                        <br>
                        Effective Date: <strong>11/1/2019</strong>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-9">
                <div class="cc-card-wrapper">
                   <div class="cc-card large">
                     <div class="row">
                         <div class="col-12">
                          <table class="table card-table">
                              <tbody class="card-table-no-headers">
                              <tr>
                                  <td width="50%">Network</td>
                                  <td width="50%">Anthem Blue Cross Silver HMO</td>
                              </tr>
                              <tr>
                                  <td>Deductible</td>
                                  <td>$1,000 / $2,000 (applies to Max OOP)</td>
                              </tr>
                              <tr>
                                  <td>
                                    Doctor Office Visits
                                    <ul class="cc-ul">
                                        <li>Specialist Visit</li>
                                        <li>Lab and X-Ray</li>
                                        <li>MRI, CT, and PET</li>
                                        <li>Annual Physical Exam</li>
                                        <li>Infertility Evaluation and Treatment</li>
                                    </ul>
                                  </td>
                                  <td>
                                    $55 Copay (ded waived)
                                    <ul class="cc-ul">
                                        <li class="plain">$75 Copay (ded waived)</li>
                                        <li class="plain">$70 Copay (ded waived)</li>
                                        <li class="plain">$350 Copay per procedure</li>
                                        <li class="plain">100% (ded waived</li>
                                        <li class="plain">Not Covered</li>
                                    </ul>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                    <ul class="cc-ul">
                                        <li>Key</li>
                                    </ul>
                                  </td>
                                  <td>
                                    <ul class="cc-ul">
                                        <li class="plain"><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati labore hic earum! Accusantium tempora ad sequi officia ea ullam. Nesciunt odio facilis obcaecati cumque accusamus culpa hic tempore nam ducimus!</div>
                                        <div>Voluptas unde enim, ea delectus repellendus facilis qui, ullam. Cumque nulla recusandae non sed, qui optio totam sequi itaque velit possimus quis eligendi ad odio repellat quae corrupti magnam ab.</div>
                                        <div>Ducimus veniam, officia. Non earum molestias corporis beatae repellat odio iste ducimus, ipsum consequuntur ratione! Facilis aliquid voluptates velit incidunt odio, a dicta cumque architecto minima aperiam, culpa molestiae nulla.</div>
                                        <div>Ut modi, reprehenderit adipisci eveniet harum dicta debitis consectetur, minima incidunt quod, assumenda inventore. Tempora, ipsa esse! Quia possimus ad autem animi, aperiam tempora laboriosam, nemo voluptatem dolores, pariatur, dolorum.</div></li>
                                    </ul>
                                  </td>
                              </tr>
                              <tr>
                                  <td>{key}</td>
                                  <td>{value}</td>
                              </tr>
                              <tr>
                                  <td>{key}</td>
                                  <td>{value}</td>
                              </tr>
                              <tr>
                                  <td>
                                    {key}
                                    <ul class="cc-ul">
                                        <li>{bullet key}</li>
                                        <li>{bullet key}</li>
                                        <li>{bullet key}</li>
                                        <li>{bullet key}</li>
                                    </ul>
                                  </td>
                                  <td>
                                    {value}
                                    <ul class="cc-ul">
                                        <li class="plain">{bullet value}</li>
                                        <li class="plain">{bullet value}</li>
                                        <li class="plain">{bullet value}</li>
                                        <li class="plain">{bullet value}</li>
                                    </ul>
                                  </td>
                              </tr>
                              </tbody>
                          </table>
                         </div>
                     </div>
                  </div><!-- cc-card large -->
                </div><!-- cc-card-wrapper -->
            </div>
        </div>
        <div class="row">
            <div class="col-9">
                <div class="d-flex justify-content-between">
                   <p class="smalltext">&copy;2003-2019 CaliforniaChoice</p>
                   <p class="smalltext">License # 0B42994 – CaliforniaChoice Benefit Administrators, Inc.</p>
                </div>
                <p class="smalltext">11/5/2019 2:01:30 PM UID: [xxxxxx] This matrix is intended to be used to help you compare coverage benefits and is a summary only. The Evidence of Coverage and Summary of Benefits and Coverage should be consulted for a detailed description of coverage benefits and limitations.</p>
            </div>
        </div>
        <hr class="break">
        <div class="alert alert-warning mb-5" role="alert">
          <strong>426894 - Employee/Member: My Benefits - Details Page - Life</strong> | <a href="https://nes6iy.axshare.com/#id=0bp9rg&p=ee_benefit_details_-_life&g=1" target="_blank">Wireframe</a>
        </div>
        <a href="#" class="back-page"><i class="fas fa-long-arrow-alt-left"></i> Back to Dashboard</a>
        <div class="row mt-4">
            <div class="col-9">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h3 class="mx-0">Assurity Life Benefits</h3>
                    </div>
                    <div class="text-right">
                        Benefit Amount: <strong>$10,000.00</strong>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-9">
                <div class="cc-card-wrapper">
                   <div class="cc-card large">
                     <div class="row">
                         <div class="col-12">
                          <table class="table card-table">
                            <thead>
                                <tr>
                                    <th width="35%">Beneficiary Name</th>
                                    <th width="15%">Relationship</th>
                                    <th width="25%" class="text-center">Date of Birth</th>
                                    <th width="25%" class="text-center">% of Benefit</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Minerva Barajas</td>
                                <td>Spouse</td>
                                <td class="text-center">10/18/1964</td>
                                <td class="text-center">50%</td>
                            </tr>
                            <tr>
                                <td>Frank Barajas</td>
                                <td>Son</td>
                                <td class="text-center">10/11/1995</td>
                                <td class="text-center">50%</td>
                            </tr>
                            </tbody>
                          </table>
                         </div>
                     </div>
                  </div><!-- cc-card large -->
                </div><!-- cc-card-wrapper -->
            </div>
        </div>
        <div class="row">
            <div class="col-9">
                <div class="d-flex justify-content-between">
                   <p class="smalltext">&copy;2003-2019 CaliforniaChoice</p>
                   <p class="smalltext">License # 0B42994 – CaliforniaChoice Benefit Administrators, Inc.</p>
                </div>
                <p class="smalltext">11/5/2019 2:01:30 PM UID: [xxxxxx] This matrix is intended to be used to help you compare coverage benefits and is a summary only. The Evidence of Coverage and Summary of Benefits and Coverage should be consulted for a detailed description of coverage benefits and limitations.</p>
            </div>
        </div>
        <hr class="break">

        <div class="alert alert-warning mb-5" role="alert">
            <strong>Interstitial Modals</strong>
        </div>
        <div class="row">
            <div class="col-12">

                <p>426705 - Public: Interstitial - Rx Search (BR/ER/EE) | <a href="https://nes6iy.axshare.com/#id=dc6371&p=interstitial_-_template_1&g=1" target="_blank">Wireframe</a> | <a href="javascript:;" data-toggle="modal" data-target="#exampleModal01">Open Modal</a></p>

                <p>431857 - Public: Interstitial - Medical Acceptance | <a href="https://nes6iy.axshare.com/#id=jyq9os&p=interstitial_-_template_2&g=1" target="_blank">Wireframe</a> | <a href="javascript:;" data-toggle="modal" data-target="#exampleModal03">Open Modal</a> | <a href="sprint11-disclaimer">Full Page</a></p>

                <p>448098 - User Session Inactivity Warning and Logged Out Messages  | <a href="https://nes6iy.axshare.com/#id=er81x9&p=session_timeout&g=1" target="_blank">Wireframe</a> | <a href="javascript:;" data-toggle="modal" data-target="#exampleModal04">Open Modal</a></p>

                <p>431859 - Public: Interstitial - Anthem out-of-state Link & Disclaimer | <a href="https://nes6iy.axshare.com/#id=8vgbdc&p=interstitial_-_template_3&g=1" target="_blank">Wireframe</a> | <a href="javascript:;" id="js-DEMO-trigger-modal">Open Modal</a></p>

                <div class="modal fade" id="exampleModal01" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Redirecting...</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div class="alert alert-dark" role="alert">
                            <strong>You're about to leave CalChoice.com!</strong><br>Here's a few things you should know before you go to:
                        </div>
                        <div class="cc-logo-wrapper">
                            <img src="/cc/demo/images/logo-placeholder.png" alt="">
                        </div>
                        <ol class="cc-ol">
                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                            <li>Proin venenatis non purus ac congue.</li>
                            <li>Sed vitae mi mauris. Cras imperdiet eleifend turpis iaculis molestie.</li>
                            <li>Sed in massa sed eros dignissim varius sit amet nec tortor.</li>
                            <li>Aliquam ut elementum nunc.</li>
                            <li>Aliquam pulvinar dolor eget ornare tempor.</li>
                            <li>Proin vitae luctus mauris.</li>
                        </ol>
                      </div>
                      <div class="modal-footer">
                        <a href="#" class="btn-link" data-dismiss="modal">Cancel</a>
                        <button type="button" class="btn btn-blue">Continue</button>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="exampleModal02" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog small" role="document">
                    <div class="modal-content">
                      <div class="modal-body text-center">
                        <h5 class="mt-3 mb-4">Redirecting...</h5>
                        <p>You're about to leave CalChoice.com and will be automatically redirected to our partner site in:</p>
                        <section class="cc-timer-wrapper">
                          <span class="time" id="seconds">5</span>
                          <div class="time-indicator">seconds</div>
                        </section>
                      </div>
                      <div class="modal-footer justify-content-center">
                        <a href="#" class="btn-link" data-dismiss="modal">Cancel</a>
                        <button type="button" class="btn btn-blue">Continue</button>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="exampleModal03" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">RX Search</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <p>This directory of drug formularies was collected from all plans participating in the CaliforniaChoice program and is accurate to the best of our knowledge at the time the data was posted to the website. However, the drug formularies and policies offered through CaliforniaChoice health plans may change at any time without notice. Keep in mind, this is only a guide and you must verify the information directly with the health plan only before making decisions based on the information provided in this directory. CaliforniaChoice will not be responsible for the information presented in his search.</p>
                        <p><strong>For questions, call Customer Service at (800) 558-8003.</strong></p>
                      </div>
                      <div class="modal-footer">
                        <a href="#" class="btn-link" data-dismiss="modal">Decline</a>
                        <button type="button" class="btn btn-blue">Accept</button>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="exampleModal04" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-body text-center">
                        <h5 class="mt-3 mb-4">Your online session will end in:</h5>
                        <div class="d-flex justify-content-center">
                          <section class="cc-timer-wrapper">
                            <span class="time">1</span>
                            <div class="time-indicator">min</div>
                          </section>
                          <section class="cc-timer-wrapper">
                            <span class="time">:</span>
                          </section>
                          <section class="cc-timer-wrapper">
                            <span class="time">49</span>
                            <div class="time-indicator">sec</div>
                          </section>
                        </div>
                        <p class="mt-4">As a security precaution your online session ends after 20 minutes of inactivity. Please click "Continue" to keep working or Logout to end your session right now.</p>
                      </div>
                      <div class="modal-footer justify-content-center">
                        <a href="#" class="btn-link" data-dismiss="modal">Logout</a>
                        <button type="button" class="btn btn-blue">Continue</button>
                      </div>
                    </div>
                  </div>
                </div>

            </div>
        </div>
    </div><!-- container -->
</section>

<?php include "common/footer.php"; ?>
</body>
</html>