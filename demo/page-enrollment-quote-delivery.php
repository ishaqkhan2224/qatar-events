<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Forms</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Enrollment Quote</h2>
                    <p>Below is a summary of your enrollment quote. Please review and confirm the details before requesting delivery of your quote.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="#">Group Info</a>
            <a class="nav-link" href="#">Census</a>
            <a class="nav-link" href="#">Metal Tier</a>
            <a class="nav-link" href="#">Contribution</a>
            <a class="nav-link" href="#">Optional Benefit</a>
            <a class="nav-link active" href="#">Delivery</a>
        </nav>
    </div>
</section>

<section id="cc-body">
    <div class="container">

        <div class="row">
            <div class="col-3">
                <div class="cc-summary">
                    <h3>Group Info</h3>
                    <div class="cc-card">
                        <h4>ACME Inc.</h4>
                        <p>92620 Orange County</p>
                    </div>
                    <h3 class="mt-4">Census</h3>
                    <div class="cc-card">
                        <table class="table card-table">
                            <tbody class="card-table-no-headers">
                            <tr>
                                <td>Employees</td>
                                <td class="text-right"><h3 class="mb-0">23</h3></td>
                            </tr>
                            <tr>
                                <td>Dependents</td>
                                <td class="text-right"><h3 class="mb-0">57</h3></td>
                            </tr>
                            <tr>
                                <td>Out-of-State</td>
                                <td class="text-right"><h3 class="mb-0">3</h3></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-9">
                <div class="cc-summary">
                    <h3>Products Included</h3>
                    <div class="cc-card large">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="media">
                                    <i class="fal fa-briefcase-medical" aria-hidden="true"></i>
                                    <div class="media-body">
                                        <h5 class="mt-0">Medical</h5>
                                        <p>Triple Tier: Platinum, Gold and Silver Based on Kaiser HMO Gold</p>
                                        <p><strong>Employee:</strong> 80% | <strong>Dependents:</strong> 20%</p>
                                    </div>
                                </div>
                                <div class="media">
                                    <i class="fal fa-glasses" aria-hidden="true"></i>
                                    <div class="media-body">
                                        <h5 class="mt-0">Vision</h5>
                                        <p>Covered</p>
                                    </div>
                                </div>
                                <div class="media m-0">
                                    <i class="fal fa-heartbeat" aria-hidden="true"></i>
                                    <div class="media-body border-0">
                                    <?php if (isset($_GET['flat'])): ?>
                                        <h5 class="mt-0">Life</h5>
                                        <p>MetLife: Flat Amount</p>
                                        <p><strong>Amount:</strong> $20,000</p>
                                    <?php else: ?>
                                        <h5 class="mt-0">Life</h5>
                                        <p>MetLife: Life Schedule</p>
                                        <p><strong>Hourly:</strong> $10,000 | <strong>Salary:</strong> $15,000
                                            <br>
                                        <strong>Mgmt:</strong> $10,000 | <strong>Owner:</strong> $15,000</p>
                                    <?php endif; ?>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="media">
                                    <i class="fal fa-tooth" aria-hidden="true"></i>
                                    <div class="media-body">
                                        <h5 class="mt-0">Dental</h5>
                                        <p>Lowest Cost HMO</p>
                                        <p><strong>Employee:</strong> 80% | <strong>Dependents:</strong> 20%</p>
                                    </div>
                                </div>
                                <div class="media">
                                    <i class="cc-icon-chiro" aria-hidden="true"></i>
                                    <div class="media-body">
                                        <h5 class="mt-0">Chiro & Acupuncture</h5>
                                        <p>Not Covered</p>
                                    </div>
                                </div>
                                <div class="media m-0">
                                    <i class="fal fa-file-invoice-dollar" aria-hidden="true"></i>
                                    <div class="media-body border-0">
                                        <h5 class="mt-0">Section 125</h5>
                                        <p>Covered</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-flex align-items-center mt-5">
            <div>Please review your information below. You have multiple agencies listed, please select which agency you would like to use for this quote:</div>
            <div class="ml-2">
                <select name="cc-select-company" id="cc-select-company" class="form-control">
                    <option value="asdf">Widget Inc.</option>
                    <option value="asdf">Widget Inc.</option>
                    <option value="asdf">Widget Inc.</option>
                </select>    
            </div>
        </div>

        <div class="row mt-2">
          <div class="col-12">
            <div class="cc-card-wrapper">
              <div class="cc-card large">
                 <div class="row info-section">
                     <div class="col-md-3">
                         <div class="info-box d-flex">
                             <div class="_icon"><i class="fal fa-user"></i></div>
                             <div class="_disc">
                                 <h4>Broker Information</h4>
                                 <p>Broker No. 80366 <br>
                                     Aaron Watts</p>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 border-left-md">
                         <div class="info-box d-flex">
                             <div class="_icon"><i class="fal fa-map-marker-alt"></i></div>
                             <div class="_disc">
                                 <h4>Contact Information</h4>
                                 <p>Address: 721 South Parker <br>
                                     City: Orange <br>
                                     State: CA ZIP: 92868 <br>
                                     Phone: (714) 567-4505 <br>
                                     Email: <a href="#">johnny@broker.com</a></p>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 border-left-md">
                         <div class="info-box d-flex">
                             <div class="_icon"><i class="fal fa-user"></i></div>
                             <div class="_disc">
                                 <h4>Inside Sales Rep</h4>
                                 <p>Don Hutchinson <br>
                                     Phone: (800) 542-4218 <br>
                                     Ext: 4431 <br>
                                     Email: <a href="#">insales@calchoice.com</a></p>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 border-left-md">
                         <div class="info-box d-flex">
                             <div class="_icon"><i class="fal fa-user"></i></div>
                             <div class="_disc">
                                 <h4>Outside Sales Rep</h4>
                                 <p>Johnny Appleseed <br>
                                     Phone: (800) 542-4218 <br>
                                     Ext: 4431 <br>
                                     Email: <a href="#">outsales@calchoice.com</a></p>
                             </div>
                         </div>
                     </div>
                 </div>
              </div><!-- cc-card -->
            </div><!-- cc-card-wrapper -->
          </div><!-- col-12 -->
        </div><!-- row -->

        <div class="row">
            <div class="col-12">
                <p>If you need to make changes, contact our Finance Customer Service team at <a href="#">commissions@calchoice.com</a> or (714) 567-4390.</p>        
            </div>
        </div>
        
        <div class="delivery-email mt-4">
            <h4>Delivery Email:</h4>
            <input type="text" value="john.doe@gmail.com; jane.doe@hotmail.com" class="form-control">
            <p>If this quote is being sent to multiple email addresses, separate each address with a semicolon(;)</p>
        </div>
    </div>
</section>

<section class="cc-controls sticky">
    <div class="container">
        <div class="d-flex align-items-center">
            <div class="left">
                <a href="#" class="btn-link">Cancel</a>
            </div>
            <div class="right">
                <a href="#" class="btn-link">Save & Exit</a>
                <button class="btn">Deliver Now</button>
            </div>
        </div>
    </div>
</section>

</body>
</html>