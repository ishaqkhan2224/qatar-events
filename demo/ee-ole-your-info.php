<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employee OLE</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Online Enrollment</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="ee-ole-overview">Overview</a>
            <a class="nav-link active" href="ee-ole-your-info">Your Info</a>
            <a class="nav-link" href="ee-ole-dependents">Dependents</a>
            <a class="nav-link" href="ee-ole-medical">Medical</a>
            <a class="nav-link" href="ee-ole-dental">Dental</a>
            <a class="nav-link" href="ee-ole-chiro">Chiro</a>
            <a class="nav-link" href="ee-ole-vision">Vision</a>
            <a class="nav-link" href="ee-ole-life">Life</a>
            <a class="nav-link" href="ee-ole-section-125">Section 125</a>
            <a class="nav-link" href="ee-ole-summary">Summary</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <div class="cc-form__box cc-form_employ-application">
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-5">
                            <h5 class="mb-3">Your Information</h5>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label>First Name</label>
                                    <input type="text" class="form-control"/>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>MI</label>
                                    <input type="text" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Suffix</label>
                                    <input type="text" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>ssn</label>
                                    <input type="text" class="form-control"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Date of birth</label>
                                    <div class="d-flex align-items-center">
                                        <input id="date_of_birth" type="text" class="form-control date-field w-100" placeholder="MM/DD/YYYY" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Gender</label>
                                    <select class="form-control">
                                        <option selected>Please Select</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Phone Number</label>
                                    <input type="number" class="form-control"/>
                                </div>
                                <div class="form-group col-md-8">
                                    <label>Email</label>
                                    <input type="email" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label>Job Title</label>
                                    <input type="text" class="form-control"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Date of hire</label>
                                    <div class="d-flex align-items-center">
                                        <input id="date_of_hire" type="text" class="form-control date-field w-100 mr-2" placeholder="MM/DD/YYYY" required/>
                                        <i class="fa fa-calendar-alt" id="dateOfHirePicker"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label>Language Preference</label>
                                    <select class="form-control">
                                        <option selected>Please Select</option>
                                        <option>English</option>
                                        <option>2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label>Marital Status</label>
                                    <select class="form-control">
                                        <option selected>Please Select</option>
                                        <option>Married</option>
                                        <option>Single</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="mb-5">
                            <h5 class="mb-3">Home Address</h5>
                            <div class="form-row">
                                <div class="form-group col-md-10">
                                    <label>Address (Line 1)</label>
                                    <input type="text" class="form-control"/>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Apt#</label>
                                    <input type="text" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Address (Line 2)</label>
                                <input type="text" class="form-control"/>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Zip Code</label>
                                    <input type="number" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control"/>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>County</label>
                                    <select class="form-control">
                                        <option selected>Please Select</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>State</label>
                                    <select class="form-control">
                                        <option selected>Please Select</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customerMailingAddress">
                                    <label class="custom-control-label" for="customerMailingAddress">Check this box to add different <b>Mailing Address</b></label>
                                </div>
                            </div>
                            <div class="jsDifferentMailingAddress" style="display: none">
                                <div class="form-row">
                                    <div class="form-group col-md-10">
                                        <label>Address (Line 1)</label>
                                        <input type="text" class="form-control"/>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label>Apt#</label>
                                        <input type="text" class="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Address (Line 2)</label>
                                    <input type="text" class="form-control"/>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label>Zip Code</label>
                                        <input type="number" class="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>City</label>
                                    <input type="text" class="form-control"/>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>County</label>
                                        <select class="form-control">
                                            <option selected>Please Select</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>State</label>
                                        <select class="form-control">
                                            <option selected>Please Select</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Back</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>