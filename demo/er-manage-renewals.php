<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer > Management</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Manage Employees</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="" class="btn btn-blue mr-3">Create a New Hire Quote</a>
                    <div class="dropdown dd-transparent">
                        <button class="btn secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Order ID Cards</a>
                            <a class="dropdown-item" href="#">View Invoices</a>
                            <a class="dropdown-item" href="#">Commissions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="javascript:;">Active Employees</a>
            <a class="nav-link" href="javascript:;">Recently Added</a>
            <a class="nav-link" href="javascript:;">COBRA</a>
            <a class="nav-link" href="javascript:;">Terminated</a>
            <a class="nav-link" href="javascript:;">Pending Requests</a>
            <a class="nav-link" href="javascript:;">Processed Requests</a>
            <a class="nav-link" href="er-manage-division-report-summary">Division Reports</a>
            <a class="nav-link active" href="er-manage-renewals">Renewals</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-division" method="post" action="">
    <section id="cc-body" class="cc-overview-intro">

        <div class="container">
            <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscig elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
            <div class="row">
                <div class="col-12">
                    <div class="cc-card-wrapper">
                        <h3>Renewal - No Employer Level Changes</h3>
                        <div class="cc-card large mb-5">
                           <div class="row">
                               <div class="col-12">
                                  <p>If you are not making any employer level changes please distribute the Employee Renewal Worksheets to your employees so they may review their renewal enrollment options. The renewal includes your Renewal Premium Notification and metal tier(s)</p>
                                  <a href="#" class="btn btn-blue"><i class="fas fa-file-pdf mr-2 text-white"></i> View Renewal - 7/2019</a>
                               </div>
                           </div>
                        </div><!-- cc-card large -->
                        <h3>Need to make changes?</h3>
                        <div class="cc-card large mb-5">
                           <div class="row">
                               <div class="col-12">
                                  <p><strong>For Employer Changes</strong></p>
                                  <p>Create divisions for locations or categories (i.e., San Jose office, Non-exempt employees) and update or delete divisions as needed.</p>
                                  <p>Contact your broker for revised Employee Renewal Enrollment Worksheets</p>
                                  <ul>
                                      <li>To make an employer level change(i.e. add a line of coverage, update your employer contribution,change tiers</li>
                                      <li>If you have employees who are adding dependents</li>
                                      <li>Or to obtain Employee Renewal Worksheets for employee who are not on your renewal census</li>
                                  </ul>
                                  <p>To make your employer level changes effective, complete, sign and submit the Employer Change Request form to CaliforniaChoice</p>
                                  <a href="#" class="btn btn-grey-outline"><i class="fas fa-file-pdf mr-2"></i> Employer Change Request</a>
                                  <hr class="my-4">
                                  <p><strong>For Employee Changes</strong></p>
                                  <p>Currently enrolled members who want to make changes to their coverage or personal information can login to <a href="#">www.calchoice.com</a>  to access the Employee Change request for. The employee then clicks "Open Enrollment" to complete, print, and sign the for,</p>
                                  <a href="#" class="btn btn-grey-outline"><i class="fas fa-file-pdf mr-2"></i> Employee Change Request</a>
                               </div>
                           </div>
                        </div><!-- cc-card large -->
                        <h3>Add Employees Without Coverage?</h3>
                        <div class="cc-card large mb-5">
                           <div class="row">
                               <div class="col-12">
                                  <p><strong>For Paper Access</strong></p>
                                  <p>Contact your broker and request Employee Renewal Worksheets for employees who do nto have any coverage through CaliforniaChoice.Distribute the Employee Renewal Enrollment Worksheets so they can review their renewal enrollment options.</p>
                                  <a href="#" class="btn btn-grey-outline"><i class="fas fa-file-pdf mr-2"></i> Employee Enrollment Application</a>
                                  <hr class="my-4">
                                  <p><strong>For Online Access</strong></p>
                                  <p>Give your employees online access to the for by entering their email address.</p>
                                  <a href="#" class="btn btn-grey-outline"><i class="fas fa-tv-alt mr-2"></i> Online Employee Enrollment Application</a>
                               </div>
                           </div>
                        </div><!-- cc-card large -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>