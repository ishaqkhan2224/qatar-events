<!DOCTYPE html>
<html>
<head>
<?php include "inc/header.meta.php"; ?>
<title>CC UI KIT: Cards</title>
<?php include "inc/header.link.php"; ?>
</head>
<body>
<section id="cc-body">
  <div class="container">

    <h1>Cards</h1>
    <hr class="mb-5">

    <div class="row">
      <div class="col-12">
        <div class="cc-card-wrapper">
          <h3>Basic large card with 2 columns copy</h3>
          <p>Some long description goes here with proper margin</p>
          <div class="cc-card large">
             <div class="row">
                 <div class="col-6">
                    <table class="table card-table">
                        <tbody class="card-table-no-headers">
                        <tr>
                            <td width="20%">Employees</td>
                            <td class="text-center" width="40%">California</td>
                            <td class="text-right" width="40%"><h3 class="mb-0">23</h3></td>
                        </tr>
                        <tr>
                            <td width="20%">Dependents</td>
                            <td class="text-center" width="40%">California</td>
                            <td class="text-right" width="40%"><h3 class="mb-0">57</h3></td>
                        </tr>
                        <tr>
                            <td width="20%">Out-of-State</td>
                            <td class="text-center" width="40%">California</td>
                            <td class="text-right" width="40%"><h3 class="mb-0">3</h3></td>
                        </tr>
                        </tbody>
                    </table>
                 </div>
                 <div class="col-6">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident voluptas recusandae illum iure asperiores labore voluptatibus, porro debitis ex quidem est blanditiis nostrum, voluptates, veritatis explicabo sunt quod animi ipsum.</p>
                 </div>
             </div>
          </div><!-- cc-card large -->
        </div><!-- cc-card-wrapper -->
      </div><!-- col-12 -->
    </div><!-- row -->

    <hr>
    
    <div class="row mt-5">

        <div class="col-12">
          <div class="cc-card-wrapper">
            <h3>Equal height columns</h3>
          </div><!-- cc-card-wrapper -->
        </div><!-- col-12 -->

      <div class="col-6 d-flex">
        <div class="cc-card large">
          <div class="cc-card-wrapper">
           <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis cum culpa, vero mollitia exercitationem libero et possimus assumenda aut, deleniti sapiente rem delectus architecto doloribus beatae, dolorum amet? At, delectus!</p>
          </div>
        </div><!-- cc-card -->
      </div><!-- col-6 -->

      <div class="col-6 d-flex">
        <div class="cc-card large">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis cum culpa, vero mollitia exercitationem libero et possimus assumenda aut, deleniti sapiente rem delectus architecto doloribus beatae, dolorum amet? At, delectus!</p>
           <table class="table card-table">
               <thead>
                   <tr>
                       <th width="25%">Column 01</th>
                       <th width="25%">Column 02</th>
                       <th width="50%" class="card-table-highlight pl-4">Column 03</th>
                   </tr>
               </thead>
               <tbody>
               <tr>
                   <td>Hello There</td>
                   <td>Widget INC.</td>
                   <td class="card-table-highlight pl-4">Complete</td>
               </tr>
               <tr>
                   <td>Hello There</td>
                   <td>Widget INC.</td>
                   <td class="card-table-highlight pl-4">Complete</td>
               </tr>
               <tr>
                   <td>Hello There</td>
                   <td>Widget INC.</td>
                   <td class="card-table-highlight pl-4">Complete</td>
               </tr>
               </tbody>
           </table>
        </div><!-- cc-card -->
      </div><!-- col-6 -->

      <div class="col-6 d-flex">
        <div class="cc-card large">
          <div class="cc-card-wrapper">
           <table class="table card-table">
               <tbody class="card-table-no-headers">
               <tr>
                   <td width="20%">Employees</td>
                   <td class="text-center" width="40%">California</td>
                   <td class="text-right" width="40%"><h3 class="mb-0">23</h3></td>
               </tr>
               <tr>
                   <td width="20%">Dependents</td>
                   <td class="text-center" width="40%">California</td>
                   <td class="text-right" width="40%"><h3 class="mb-0">57</h3></td>
               </tr>
               <tr>
                   <td width="20%">Out-of-State</td>
                   <td class="text-center" width="40%">California</td>
                   <td class="text-right" width="40%"><h3 class="mb-0">3</h3></td>
               </tr>
               </tbody>
           </table>
          </div>
        </div><!-- cc-card -->
      </div><!-- col-6 -->

      <div class="col-6 d-flex">
        <div class="cc-card large">
          <div class="cc-card-wrapper">
           <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis cum culpa, vero mollitia exercitationem libero et possimus assumenda aut, deleniti sapiente rem delectus architecto doloribus beatae, dolorum amet? At, delectus!</p>
           <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis cum culpa, vero mollitia exercitationem libero et possimus assumenda aut, deleniti sapiente rem delectus architecto doloribus beatae, dolorum amet? At, delectus!</p>
          </div>
        </div><!-- cc-card -->
      </div><!-- col-6 -->

    </div><!-- row -->    

    <hr>
    
    <div class="row mt-5 d-flex">

      <div class="col-9">
        <div class="cc-card-wrapper">
          <h3>Stackable Cards</h3>
          <div class="cc-card large">
             <div class="row">
                 <div class="col-6">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis cum culpa, vero mollitia exercitationem libero et possimus assumenda aut, deleniti sapiente rem delectus architecto doloribus beatae, dolorum amet? At, delectus!</p>
                 </div>
                 <div class="col-6">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident voluptas recusandae illum iure asperiores labore voluptatibus, porro debitis ex quidem est blanditiis nostrum, voluptates, veritatis explicabo sunt quod animi ipsum.</p>
                 </div>
             </div>
          </div><!-- cc-card large -->
        </div><!-- cc-card-wrapper -->
      </div><!-- col-9 -->

      <div class="col-3">
        <div class="cc-card-wrapper">
          <h3>Another Title Goes Here</h3>
          <div class="cc-card">
              <h4>Sub-Title Here</h4>
              <h5>23,499</h5>
          </div>
          <div class="cc-card muted">
              <h4>Sub-Title Here</h4>
              <h5>$534</h5>
          </div>
          <div class="cc-card highlight">
              <h4>Sub-Title Here</h4>
              <h5>Hello World</h5>
          </div>
          <div class="cc-card empty pt-1">
              <h4>Sub-Title Here</h4>
              <span class="muted">30 Days</span>
          </div>
        </div><!-- cc-card-wrapper -->
      </div><!-- col-3 -->

    </div><!-- row -->    
    
    <hr>

    <div class="row mt-5">
      <div class="col-4">
        <div class="cc-card-wrapper">
          <div class="cc-card">
            <h4>Sub-Title Here</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis amet ea sit exercitationem tempore quasi libero optio vitae veritatis commodi, perspiciatis minus repellat voluptate nobis labore cumque. Dignissimos, iure aliquid.</p>
          </div>
        </div>
      </div>
      <div class="col-6">
        <div class="cc-card-wrapper">
          <div class="cc-card">
            <div class="card-ribbon">
              <span>Latest</span>
            </div>
            <h4>Sub-Title Here</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis amet ea sit exercitationem tempore quasi libero optio vitae veritatis commodi, perspiciatis minus repellat voluptate nobis labore cumque. Dignissimos, iure aliquid.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis amet ea sit exercitationem tempore quasi libero optio vitae veritatis commodi, perspiciatis.</p>
          </div>
        </div>
      </div>
    </div>

    <hr>

    <div class="row mt-5">
      <div class="col-12">
        <div class="cc-card-wrapper">
          <h3>Card Table</h3>
          <div class="cc-card large">
             <div class="row">
                 <div class="col-12">
                  <h4>PRODUCT #1</h4>
                  <table class="table card-table">
                      <thead>
                          <tr>
                              <th width="25%">Column 01</th>
                              <th width="25%">Column 02</th>
                              <th width="50%" class="card-table-highlight pl-4">Column 03</th>
                          </tr>
                      </thead>
                      <tbody>
                      <tr>
                          <td>Hello There</td>
                          <td>Widget INC.</td>
                          <td class="card-table-highlight pl-4">Complete</td>
                      </tr>
                      <tr>
                          <td>Hello There</td>
                          <td>Widget INC.</td>
                          <td class="card-table-highlight pl-4">Complete</td>
                      </tr>
                      <tr>
                          <td>Hello There</td>
                          <td>Widget INC.</td>
                          <td class="card-table-highlight pl-4">Complete</td>
                      </tr>
                      </tbody>
                  </table>
                 </div>
             </div>
          </div><!-- cc-card large -->
        </div><!-- cc-card-wrapper -->
      </div><!-- col-12 -->
    </div><!-- row -->

    <hr>

    <div class="row mt-5">
      <div class="col-12">
        <div class="cc-card-wrapper">
          <h3>Anchor Buttons</h3>
          <div class="cc-card large empty">
             <div class="d-flex justify-content-around">
                 <a href="#" class="btn btn-white">White Button</a>
                 <a href="#" class="btn btn-white-outline">White Outline Button</a>
                 <a href="#" class="btn btn-grey-outline">Grey Outline Button</a>
                 <a href="#" class="btn btn-blue">Blue Button</a>
                 <a href="#" class="btn btn-orange">Orange Button</a>
             </div>
          </div><!-- cc-card large -->
          <h3>Buttons</h3>
          <div class="cc-card large empty">
             <div class="d-flex justify-content-around">
                 <button class="btn btn-white">White Button</button>
                 <button class="btn btn-white-outline">White Outline Button</button>
                 <button class="btn btn-grey-outline">Grey Outline Button</button>
                 <button class="btn btn-blue">Blue Button</button>
                 <button class="btn btn-orange">Orange Button</button>
             </div>
          </div><!-- cc-card large -->
          <h3>Buttons (Disabled)</h3>
          <div class="cc-card large empty">
             <div class="d-flex justify-content-around">
                 <button class="btn btn-white" disabled="disabled">White Button</button>
                 <button class="btn btn-white-outline" disabled="disabled">White Outline Button</button>
                 <button class="btn btn-grey-outline" disabled="disabled">Grey Outline Button</button>
                 <button class="btn btn-blue" disabled="disabled">Blue Button</button>
                 <button class="btn btn-orange" disabled="disabled">Orange Button</button>
             </div>
          </div><!-- cc-card large -->
        </div><!-- cc-card-wrapper -->
      </div><!-- col-12 -->
    </div><!-- row -->

    
    
  </div><!-- container -->
</section><!-- cc-body -->
<?php include "common/footer.php"; ?>
</body>
</html>