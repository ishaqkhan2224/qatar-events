<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer > Management</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Manage Employees</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="" class="btn btn-blue mr-3">Create a New Hire Quote</a>
                    <div class="dropdown dd-transparent">
                        <button class="btn secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Order ID Cards</a>
                            <a class="dropdown-item" href="#">View Invoices</a>
                            <a class="dropdown-item" href="#">Commissions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="javascript:;">Active Employees</a>
            <a class="nav-link" href="javascript:;">Recently Added</a>
            <a class="nav-link" href="javascript:;">COBRA</a>
            <a class="nav-link" href="javascript:;">Terminated</a>
            <a class="nav-link" href="javascript:;">Pending Requests</a>
            <a class="nav-link" href="javascript:;">Processed Requests</a>
            <a class="nav-link active" href="er-manage-division-report-summary">Division Reports</a>
            <a class="nav-link" href="er-manage-renewals">Renewals</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-division" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="cc-card-wrapper">
                          <p>Create divisions for locations or categories (i.e., San Jose office, Non-exempt employees) and add, update or delete divisions as needed.</p>
                          <div class="cc-card large">
                             <div class="row">
                                 <div class="col-12">

                                  <table class="table card-table">
                                      <tbody class="card-table-no-headers">
                                        <tr>
                                          <td class="border-top-0">
                                              <div class="form-group mb-0">
                                                <input type="text" class="form-control" id="basic_input_field" name="basic_input_field" placeholder="Enter Division Name..." required>
                                              </div>
                                          </td>
                                          <td class="text-right border-top-0"><a href="#" class="delete-tr"><i class="fad fa-trash-alt"></i></a></td>
                                        </tr>
                                        <tr>
                                          <td class="border-top-0">
                                              <div class="form-group mb-0">
                                                <input type="text" class="form-control" id="basic_input_field" name="basic_input_field" placeholder="Enter Division Name..." required>
                                              </div>
                                          </td>
                                          <td class="text-right border-top-0"><a href="#" class="delete-tr"><i class="fad fa-trash-alt"></i></a></td>
                                        </tr>
                                        <tr>
                                          <td class="border-top-0">
                                              <div class="form-group mb-0">
                                                <input type="text" class="form-control" id="basic_input_field" name="basic_input_field" placeholder="Enter Division Name..." required>
                                              </div>
                                          </td>
                                          <td class="text-right border-top-0"><a href="#" class="delete-tr"><i class="fad fa-trash-alt"></i></a></td>
                                        </tr>
                                        <tr>
                                          <td class="border-top-0">
                                              <div class="form-group mb-0">
                                                <input type="text" class="form-control" id="basic_input_field" name="basic_input_field" placeholder="Enter Division Name..." required>
                                              </div>
                                          </td>
                                          <td class="text-right border-top-0"><a href="#" class="delete-tr"><i class="fad fa-trash-alt"></i></a></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                 </div>
                             </div>
                          </div><!-- cc-card large -->
                          <div class="d-flex">
                              <a href="#" id="addDivision" class="btn btn-grey-outline"><i class="fas fa-plus-circle" aria-hidden="true"></i> Add Division</a>
                          </div>
                        </div><!-- cc-card-wrapper -->
                </div>
            </div>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Back</a>
                </div>
                <div class="right">
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>