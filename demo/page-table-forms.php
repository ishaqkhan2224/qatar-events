<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Page Layout > Table</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<div class="cc-site-alert cc-success" hidden>
    Site Level Alert Message Here...
</div>
<div class="cc-site-alert cc-error" hidden>
    Site Level Alert Message Here...
</div>
<div class="cc-site-alert cc-warning">
    Site Level Alert Message Here...
</div>
<div class="cc-site-alert cc-info" hidden>
    Site Level Alert Message Here...
</div>
<?php include "common/subheader-tabs.php"; ?>
<form id="cc-form-data-sample" action="">
    <section id="cc-body">
        <div class="container">
            <table class="table cc-table-data-form" id="cc-table-data-form-sample">
                <thead>
                <tr>
                    <th scope="col" width="5%" class="text-center">Radio</th>
                    <th scope="col" width="30%">Standard Input (80%)</th>
                    <th scope="col" width="30%">Input (70%)</th>
                    <th scope="col" width="5%" class="text-center">Checkbox</th>
                    <th scope="col" width="10%">Small</th>                    
                    <th scope="col" width="25%" class="text-center">Select</th>
                    <th scope="col" class="text-center" width="15%">Toggle</th>
                    <th scope="col" width="15%"></th>
                </tr>
                </thead>
                <tbody>
                <tr class="cc-table-row-highlight">
                    <td class="text-center">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="testing1-a" name="testing" class="js-row-highlight custom-control-input" value="agree" checked="checked">
                            <label class="custom-control-label" for="testing1-a"></label>
                        </div>
                    </td>
                    <td><input type="text" class="form-control" id="name01" name="name01" data-msg-required="Contact Name is required." value="Daniel Hernandez" required></td>
                    <td><input type="text" class="form-control cc-w70" id="title01" name="title01" data-msg-required="Title is required." value="CEO" required></td>
                    <td class="text-center">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="same-address" name="same_address" required><label class="custom-control-label" for="same-address"></label>
                        </div>
                    </td>
                    <td><input type="text" class="form-control cc-w60" id="salute01" name="salute01" data-msg-required="Long message goes here..." value="1" required></td>
                    <td class="text-center">
                        <select id="email01" id="email01" class="form-control" required>
                            <option value="">Select</option>
                            <option value="CA">CA</option>
                            <option value="TX">TX</option>
                        </select>
                    </td>
                    <td class="text-center">
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td><a href="#" class="delete-tr"><i class="fad fa-trash-alt"></i></a></td>
                </tr>
                </tbody>
            </table>

            <div class="d-flex">
                <a href="#" id="addContact" class="btn btn-grey-outline"><i class="fas fa-plus-circle"></i> Add Contact</a>
            </div>
        </div><!-- container -->
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Cancel</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
          <div class="container">
            <div class="d-flex justify-content-between">
                <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                <div>
                  <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                </div>
            </div>
          </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>

<script>
    $('#cc-form-data-sample').validate({
        errorClass: "is-invalid",
        //validClass: "is-valid",
        invalidHandler: function(event, validator) {
            // 'this' refers to the form
            $("#alert").remove();
            var errors = validator.numberOfInvalids();
            if (errors) {
                $('<div id="alert" class="alert cc-error alert-dismissible fade show" role="alert" style="background-color: #FEF5EF;">\n' +
                    '<strong>Uh oh, you got issues!!!</strong> Please click on the <i class="far fa-info-circle"></i> icon next to the error fields for further information. {Marketing Help}\n' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                    '<span aria-hidden="true">&times;</span>\n' +
                    '</button>\n' +
                    '</div>').insertBefore('#cc-table-data-form-sample');
            }
        },
        errorPlacement: function(error, element) {
            element.next('i').remove();
            var msg = $('<i class="fas fa-info-circle" data-toggle="tooltip" title="' + error.text() + '"></i>');
            msg.insertAfter(element);
            $('[data-toggle="tooltip"]').tooltip();
        },
        unhighlight: function(element, errorClass, validClass){
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).next('i').remove();
        },
        messages: {

        }
    });
</script>

</body>
</html>