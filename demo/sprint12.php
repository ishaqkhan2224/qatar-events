<!DOCTYPE html>
<html>
<head>
<?php include "inc/header.meta.php"; ?>
<title>CC UI Deliverables: Sprint 12</title>
<?php include "inc/header.link.php"; ?>
</head>
<body>

<!--
<div class="alert alert-warning mb-5" role="alert">
    <strong>xxx</strong> | <a href="xxx" target="_blank">Wireframe</a>
</div>
<div class="row">
    <div class="col-6">
        // goes here
    </div>
</div>
<hr class="break">
-->

<section id="cc-body">
    <div class="container">
        <h1 class="text-center mb-5">Sprint 12 UI Deliverables</h1>

        <div class="alert alert-warning mb-5" role="alert">
            <strong>426891 - Employer: Order Supplies & 426703 - Broker: Marketing for Brokers - Order Supplies</strong> | <a href="https://nes6iy.axshare.com/#id=2hd9k6&p=er_-_order_supplies&g=1" target="_blank">Wireframe</a>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="js-demo-alert-unfinished-enrollment" class="alert cc-success alert-dismissible fade" role="alert" style="display:none">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt neque alias blanditiis, impedit doloribus id possimus laboriosam est inventore eveniet fugit at ducimus, deserunt assumenda vel et repudiandae aut deleniti.</p>
                <table class="table mt-4" id="cc-table-data">
                    <thead>
                        <tr>
                            <th scope="col" width="70%">Enrollment Forms</th>
                            <th scope="col" class="text-center">English</th>
                            <th scope="col" class="text-center">Spanish</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Employer Master App Kit</td>
                            <td class="text-center">
                              <div class="form-group mb-0">
                                <input type="text" class="form-control text-center" id="xxx" name="xxx" placeholder="" value="0">
                              </div>
                            </td>
                            <td class="text-center"></td>
                        </tr>
                        <tr>
                            <td>Employee App</td>
                            <td class="text-center">
                              <div class="form-group mb-0">
                                <input type="text" class="form-control text-center" id="xxx" name="xxx" placeholder="" value="0">
                              </div>
                            </td>
                            <td class="text-center">
                              <div class="form-group mb-0">
                                <input type="text" class="form-control text-center" id="xxx" name="xxx" placeholder="" value="0">
                              </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table class="table mt-4" id="cc-table-data">
                    <thead>
                        <tr>
                            <th scope="col" width="70%">Brochures and Guides</th>
                            <th scope="col" class="text-center">English</th>
                            <th scope="col" class="text-center">Spanish</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>CaliforniaChoice Fast Facts</td>
                            <td class="text-center">
                              <div class="form-group mb-0">
                                <input type="text" class="form-control text-center" id="xxx" name="xxx" placeholder="" value="0">
                              </div>
                            </td>
                            <td class="text-center"></td>
                        </tr>
                        <tr>
                            <td>Employee Guide</td>
                            <td class="text-center">
                              <div class="form-group mb-0">
                                <input type="text" class="form-control text-center" id="xxx" name="xxx" placeholder="" value="0">
                              </div>
                            </td>
                            <td class="text-center">
                              <div class="form-group mb-0">
                                <input type="text" class="form-control text-center" id="xxx" name="xxx" placeholder="" value="0">
                              </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Rate Guide</td>
                            <td class="text-center">
                              <div class="form-group mb-0">
                                <input type="text" class="form-control text-center" id="xxx" name="xxx" placeholder="" value="0">
                              </div>
                            </td>
                            <td class="text-center"></td>
                        </tr>
                    </tbody>
                </table>

                <form id="cc-form__new-quote" method="post" action="" class="mt-5">
                    <div class="container">
                        <div class="cc-form__box">
                            <div class="row">
                                <div class="col-md-6">
                                  
                                  <div class="d-flex justify-content-between mb-4">
                                     <h5>Mail To</h5>
                                     <a href="#">Clear</a>
                                  </div>

                                  <div class="form-group">
                                      <label for="xxx">Company Name</label>
                                      <input type="text" class="form-control" id="xxx" name="xxx" />
                                  </div>

                                  <div class="form-group">
                                      <label for="xxx">Contact Name</label>
                                      <input type="text" class="form-control" id="xxx" name="xxx" />
                                  </div>

                                  <div class="form-group">
                                      <label for="xxx">Address</label>
                                      <input type="text" class="form-control" id="xxx" name="xxx" />
                                  </div>

                                  <div class="form-group">
                                      <label for="xxx">ZIP Code</label>
                                      <input type="text" class="form-control" id="xxx" name="xxx" />
                                  </div>

                                  <div class="form-group">
                                      <div class="form-row">
                                        <div class="col-8">
                                          <div class="form-group">
                                              <label for="xxx">City</label>
                                              <input type="text" class="form-control" id="xxx" name="xxx" disabled="" value="Orange" />
                                          </div>
                                        </div>
                                        <div class="col-4">
                                          <div class="form-group">
                                              <label for="xxx">State</label>
                                              <input type="text" class="form-control" id="xxx" name="xxx" disabled="" value="CA" />
                                          </div>
                                        </div>

                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <label for="xxx">Phone Number</label>
                                      <input type="text" class="form-control" id="xxx" name="xxx" />
                                  </div>


                                </div>
                            </div>
                        </div>
                    </div><!-- container -->
                </form>
            </div>
        </div>
        
        <hr class="break">
        
        <div class="row">
            <div class="col-12">
                <div id="js-demo-alert-unfinished-enrollment" class="alert cc-success alert-dismissible fade" role="alert" style="display:none">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt neque alias blanditiis, impedit doloribus id possimus laboriosam est inventore eveniet fugit at ducimus, deserunt assumenda vel et repudiandae aut deleniti.</p>
                <table class="table mt-4" id="cc-table-data">
                    <thead>
                        <tr>
                            <th scope="col" width="70%">Enrollment Forms</th>
                            <th scope="col" class="text-center">English</th>
                            <th scope="col" class="text-center">Spanish</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Employer Master App Kit</td>
                            <td class="text-center">4</td>
                            <td class="text-center"></td>
                        </tr>
                        <tr>
                            <td>Employee App</td>
                            <td class="text-center">1</td>
                            <td class="text-center">2</td>
                        </tr>
                    </tbody>
                </table>

                <table class="table mt-4" id="cc-table-data">
                    <thead>
                        <tr>
                            <th scope="col" width="70%">Brochures and Guides</th>
                            <th scope="col" class="text-center">English</th>
                            <th scope="col" class="text-center">Spanish</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>CaliforniaChoice Fast Facts</td>
                            <td class="text-center">4</td>
                            <td class="text-center"></td>
                        </tr>
                        <tr>
                            <td>Employee Guide</td>
                            <td class="text-center">1</td>
                            <td class="text-center">1</td>
                        </tr>
                        <tr>
                            <td>Rate Guide</td>
                            <td class="text-center">2</td>
                            <td class="text-center"></td>
                        </tr>
                    </tbody>
                </table>

                <form id="cc-form__new-quote" method="post" action="" class="mt-5">
                    <div class="container">
                        <div class="cc-form__box">
                            <div class="row">
                                <div class="col-md-6">
                                  <h5 class="mb-4">Your materials will be mailed to:</h5>
                                  <p>
                                    <strong>ABC Inc.</strong>
                                    <br>
                                    John Doe
                                    <br>
                                    123 Main St., Apt 12
                                    <br>
                                    San Mateo, CA 94403
                                  </p>
                                  <div class="form-group">
                                      <label for="xxx">A confirmation of this request will be emailed to:</label>
                                      <input type="text" class="form-control" id="xxx" name="xxx" />
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- container -->
                </form>
            </div>
        </div>

        <hr class="break">
        <div class="alert alert-warning mb-5" role="alert">
          <strong>426894 - Employee/Member: My Benefits - Details Page - Medical</strong> | <a href="https://nes6iy.axshare.com/#id=9pnc8o&p=ee_benefit_details_-_medical&g=1" target="_blank">Wireframe
        </div>
        <a href="#" class="back-page"><i class="fas fa-long-arrow-alt-left"></i> Back to Dashboard</a>
        <div class="row mt-4">
            <div class="col-9">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h3 class="mx-0 my-0">Anthem Blue Cross</h3>
                        <p>Silver HMO B Benefit</p>  
                    </div>
                    <div class="text-right">
                        Cost Per Pay Period: <strong>$622.83</strong>
                        <br>
                        Effective Date: <strong>11/1/2019</strong>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-9">
                <div class="cc-card-wrapper">
                   
                   <div class="cc-card large">
                     <div class="row">
                         <div class="col-12">
                          <table class="table card-table">
                              <tbody class="card-table-no-headers">
                              <tr>
                                  <td width="50%">Network</td>
                                  <td width="50%">Anthem Blue Cross Silver HMO</td>
                              </tr>
                              <tr>
                                  <td>
                                    Doctor Office Visits
                                    <ul class="cc-ul">
                                        <li>Specialist Visit</li>
                                        <li>Lab and X-Ray</li>
                                        <li>MRI, CT, and PET</li>
                                        <li>Annual Physical Exam</li>
                                        <li>Infertility Evaluation and Treatment</li>
                                    </ul>
                                  </td>
                                  <td>
                                    $55 Copay (ded waived)
                                    <ul class="cc-ul">
                                        <li class="plain">$75 Copay (ded waived)</li>
                                        <li class="plain">$70 Copay (ded waived)</li>
                                        <li class="plain">$350 Copay per procedure</li>
                                        <li class="plain">100% (ded waived</li>
                                        <li class="plain">Not Covered</li>
                                    </ul>
                                  </td>
                              </tr>
                              <tr>
                                  <td colspan="2">
                                    <table class="table table-borderless table-lean">
                                      <tbody>
                                        <tr>
                                          <td width="50%">Doctor Office Visit</td>
                                          <td width="50%">$55 Copay (ded waived)</td>
                                        </tr>
                                        <tr>
                                          <td>
                                            <ul class="cc-ul"><li>Specialist Visit</li></ul>
                                          </td>
                                          <td>$75 Copay (ded waived)</td>
                                        </tr>
                                        <tr>
                                          <td>
                                            <ul class="cc-ul"><li>Lab and X-Ray</li></ul>
                                          </td>
                                          <td>$70 Copay (ded waived)</td>
                                        </tr>
                                        <tr>
                                          <td>
                                            <ul class="cc-ul"><li>MRI, CT, and PET</li></ul>
                                          </td>
                                          <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, provident, veritatis aut, officia optio voluptatem nihil, deserunt omnis cum odio accusantium! Corrupti ipsa natus nam alias aspernatur minus sed inventore.</td>
                                        </tr>
                                        <tr>
                                          <td>
                                            <ul class="cc-ul"><li>Annual Physical Exam</li></ul>
                                          </td>
                                          <td>Not Covered</td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                              </tr>
                              <tr>
                                  <td>{key}</td>
                                  <td>{value}</td>
                              </tr>
                              <tr>
                                  <td>
                                    {key}
                                    <ul class="cc-ul">
                                        <li>{bullet key}</li>
                                        <li>{bullet key}</li>
                                        <li>{bullet key}</li>
                                        <li>{bullet key}</li>
                                    </ul>
                                  </td>
                                  <td>
                                    {value}
                                    <ul class="cc-ul">
                                        <li class="plain">{bullet value}</li>
                                        <li class="plain">{bullet value}</li>
                                        <li class="plain">{bullet value}</li>
                                        <li class="plain">{bullet value}</li>
                                    </ul>
                                  </td>
                              </tr>
                              <tr>
                                  <td colspan="2">
                                    <table class="table table-borderless table-lean">
                                      <tbody>
                                        <tr>
                                          <td width="50%">{key}</td>
                                          <td width="50%">{value}</td>
                                        </tr>
                                        <tr>
                                          <td>
                                            <ul class="cc-ul"><li>{bullet key}</li></ul>
                                          </td>
                                          <td>{bullet value}</td>
                                        </tr>
                                        <tr>
                                          <td>
                                            <ul class="cc-ul"><li>{bullet key}</li></ul>
                                          </td>
                                          <td>{Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, provident, veritatis aut, officia optio voluptatem nihil, deserunt omnis cum odio accusantium! Corrupti ipsa natus nam alias aspernatur minus sed inventore.}</td>
                                        </tr>
                                        <tr>
                                          <td>
                                            <ul class="cc-ul"><li>{bullet key}</li></ul>
                                          </td>
                                          <td>{bullet value}</td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                              </tr>
                              </tbody>
                          </table>
                         </div>
                     </div>
                  </div><!-- cc-card large -->

                   <div class="cc-card large">
                     <div class="row">
                         <div class="col-12">
                          <table class="table card-table">
                              <tbody class="card-table-no-headers">
                                <tr>
                                  <th width="40%"></th>
                                  <th width="30%">In Network</th>
                                  <th width="30%">Out of Network</th>
                                </tr>
                                <tr>
                                    <td>Network Name</td>
                                    <td>Select PPO</td>
                                    <td>N/A</td>
                                </tr>
                                <tr>
                                    <td>Deductible</td>
                                    <td>$1,700 / $3,400 (combined medical & dental apply to Max OOP)</td>
                                    <td>$3,400 / $6,800 (combined medical & dental apply to Max OOP)</td>
                                </tr>
                                <tr>
                                    <td>Lifetime Maximum</td>
                                    <td>Unlimited</td>
                                    <td>Unlimited</td>
                                </tr>
                              </tbody>
                          </table>
                         </div>
                     </div>
                  </div><!-- cc-card large -->
                </div><!-- cc-card-wrapper -->
            </div>
        </div>
        <div class="row">
            <div class="col-9">
                <div class="d-flex justify-content-between">
                   <p class="smalltext">&copy;2003-2019 CaliforniaChoice</p>
                   <p class="smalltext">License # 0B42994 – CaliforniaChoice Benefit Administrators, Inc.</p>
                </div>
                <p class="smalltext">11/5/2019 2:01:30 PM UID: [xxxxxx] This matrix is intended to be used to help you compare coverage benefits and is a summary only. The Evidence of Coverage and Summary of Benefits and Coverage should be consulted for a detailed description of coverage benefits and limitations.</p>
            </div>
        </div>
        <hr class="break">

    </div><!-- container -->
</section>

<?php include "common/footer.php"; ?>
</body>
</html>