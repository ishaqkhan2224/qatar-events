<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer OLE</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Online Enrollment</h2>
                    <p>Thank you for going paperless! Enrolling your business online helps eliminate incomplete applications, reduce the number of pending items, and decrease processing time.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link active" href="er-ole-overview">Overview</a>
            <a class="nav-link" href="er-ole-application">Application</a>
            <a class="nav-link" href="er-ole-census">Census</a>
            <a class="nav-link" href="er-ole-metaltier">Metal Tier</a>
            <a class="nav-link" href="er-ole-contribution">Contribution</a>
            <a class="nav-link" href="er-ole-optional-benefits">Optional Benefits</a>
            <a class="nav-link" href="er-ole-summary">Summary</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body" class="cc-overview-intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h5>How It Works</h5>
                    <p>Online enrollment is simple and easy. Below is a quick summary to help you understand the process so you know what to expect every step of the way.</p>
                    <div class="media mt-5">
                        <i class="fal fa-user"></i>
                        <div class="media-body">
                            <h6>Step 1: Broker (Completed)</h6>
                            <p>Your broker initiates the enrollment process by providing information from your quote.</p>
                        </div>
                    </div>
                    <hr>
                    <div class="media">
                        <i class="fal fa-building"></i>
                        <div class="media-body">
                            <h6>Step 2: Employer (Your Step)</h6>
                            <p>You register and create your account. From here, you review the information provided by your broker to ensure accuracy. You can also include any additional information needed to complete online enrollment. Verify and confirm your contribution amount and product offering, then launch online enrollment for your employees. A valid email address is required for online enrollment. If an employee does not have an email address, paper forms can be submitted.</p>
                        </div>
                    </div>
                    <hr>
                    <div class="media">
                        <i class="fal fa-users"></i>
                        <div class="media-body">
                            <h6>Step 3: Employees</h6>
                            <p>After you launch the enrollment, employees will be asked to enter their personal and dependent information, if applicable, view side-by-side plan comparisons, choose the preferred health plan, and electronically sign the Employee Enrollment Application.</p>
                        </div>
                    </div>
                    <hr>
                    <div class="media">
                        <i class="fal fa-building"></i>
                        <div class="media-body">
                            <h6>Step 4: Employer (Your Step)</h6>
                            <p>Monitor the process and submit a completed enrollment to CaliforniaChoice® after your employees make their selections and submit their application.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="right">
                    <button class="btn">Continue</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>