<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Page Layout > Table</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<?php include "common/subheader-tabs.php"; ?>

<section id="cc-body">
    <div class="container">

        <div class="alert alert-warning alert-dismissible fade show mb-5" role="alert">
          <strong>Uh oh, one or more group require re-certification.</strong> Don't worry, we're here to help - please contact your administrator for assistance.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="cc-active-employees mb-4">

            <div class="row">
                <div class="col-10">
                    <div class="cc-summary">
                        <h3>Contribution Summary</h3>
                        <div class="cc-card large">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="media">
                                        <i class="fal fa-briefcase-medical" aria-hidden="true"></i>
                                        <div class="media-body">
                                            <h5 class="mt-0">Medical</h5>
                                            <p>$800.00 toward the total employee rate of any plan. $0 of the dependent rate for the same plan.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <i class="cc-icon-chiro" aria-hidden="true"></i>
                                        <div class="media-body">
                                            <h5 class="mt-0">Chiro</h5>
                                            <p>100% contribution toward employee adn dependents rate.</p>
                                        </div>
                                    </div>
                                    <div class="media m-0">
                                        <i class="fal fa-tooth" aria-hidden="true"></i>
                                        <div class="media-body border-0">
                                            <h5 class="mt-0">Dental</h5>
                                            <p>100% of employee rate for 3000. 100% of the depedent rate for the same plan.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="media">
                                        <i class="fal fa-glasses" aria-hidden="true"></i>
                                        <div class="media-body">
                                            <h5 class="mt-0">Vision</h5>
                                            <p>$800.00 toward the total employee rate of any plan. $0 of the dependent rate for the same plan.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <i class="fal fa-heartbeat" aria-hidden="true"></i>
                                        <div class="media-body">
                                            <h5 class="mt-0">Life</h5>
                                            <p>100% contribution toward employee adn dependents rate.</p>
                                        </div>
                                    </div>
                                    <div class="media m-0">
                                        <i class="fal fa-file-invoice-dollar" aria-hidden="true"></i>
                                        <div class="media-body border-0">
                                            <h5 class="mt-0">Section 125</h5>
                                            <p>$800.00 toward the total employee rate of any plan. $0 of the dependent</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-2">
                    <div class="cc-summary">
                        <h3>Enrollment Summary</h3>
                        <div class="cc-card muted">
                            <h4>Active Employees</h4>
                            <h5>9</h5>
                        </div>
                        <div class="cc-card muted">
                            <h4>COBRA participants</h4>
                            <h5>3</h5>
                        </div>
                        <div class="cc-card highlight">
                            <h4>Total Enrollment</h4>
                            <h5>12</h5>
                        </div>
                        <div class="cc-card empty pt-1">
                            <h4>Group Waiting Period</h4>
                            <span class="muted">30 Days</span>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

        <?php //include "inc/tables.basic.php" ?>
        <?php include "inc/tables.product-highlights.php" ?>
    </div><!-- container -->
</section>

<?php include "common/footer.php"; ?>
</body>
</html>