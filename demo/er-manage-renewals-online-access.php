<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer > Management</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Manage Employees</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="" class="btn btn-blue mr-3">Create a New Hire Quote</a>
                    <div class="dropdown dd-transparent">
                        <button class="btn secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Order ID Cards</a>
                            <a class="dropdown-item" href="#">View Invoices</a>
                            <a class="dropdown-item" href="#">Commissions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="javascript:;">Active Employees</a>
            <a class="nav-link" href="javascript:;">Recently Added</a>
            <a class="nav-link" href="javascript:;">COBRA</a>
            <a class="nav-link" href="javascript:;">Terminated</a>
            <a class="nav-link" href="javascript:;">Pending Requests</a>
            <a class="nav-link" href="javascript:;">Processed Requests</a>
            <a class="nav-link" href="er-manage-division-report-summary">Division Reports</a>
            <a class="nav-link active" href="er-manage-renewals">Renewals</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-division" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <p class="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in.</p>
            <div class="row">
                <div class="col-md-9">
                    <table class="table cc-table__renewals-online" id="cc-table-data">
                        <thead>
                        <tr>
                            <th width="20%" scope="col">Last Name</th>
                            <th width="20%" scope="col">First Name</th>
                            <th width="15%" scope="col" class="">SSN</th>
                            <th width="10%" scope="col" class="">Zip Code</th>
                            <th width="30%" scope="col" class="">Email Address</th>
                            <th width="10%" scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="email" class="form-control"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="text" class="form-control"></td>
                            <td><input type="email" class="form-control"></td>
                            <td><a href="#" class="delete-tr"><i class="fal fa-trash-alt"></i></a></td>
                        </tr>
                        </tbody>
                    </table>
                       <button type="button" class="btn btn-grey-outline js-add-employee"><i class="fas fa-plus mr-2"></i>Add Another Employee</button>
                </div>
            </div>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="right">
                    <button class="btn">Submit</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>