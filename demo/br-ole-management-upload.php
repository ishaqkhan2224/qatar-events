<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Online Enrollment Management</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Online Enrollment Management</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. corporis, sunt distinctio</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="#" class="btn primary">Start an online enrollment</a>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link active" href="#">Active</a>
            <a class="nav-link" href="#">Expired</a>
            <a class="nav-link" href="#">Canceled</a>
        </nav>
    </div>
</section>

<section id="cc-body">
    <div class="container">
        <div class="section-overview-title">
            <h5>Upload Documents for Pied Piper - Quote # 6789012</h5>
            <p>View and upload additional enrollment documents.</p>
        </div>
        <div class="row">
            <div class="col-md-8">
                
                <div class="form-row">
                    <div class="form-group col">
                        <label>CATEGORY</label>
                        <select class="form-control">
                            <option value="">Other</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                        </select>
                    </div>
                    <div class="form-group col">
                        <label>TYPE</label>
                        <select class="form-control">
                            <option value="">Other</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                        </select>
                    </div>
                    <div class="form-group col">
                        <label>DOCUMENT DESCRIPTION</label>
                        <input type="text" class="form-control" value="Lorem Ipsum">
                    </div>
                </div>
                <div class="file-upload-box my-3">
                    <div class="media">
                        <div class="upload-icon">
                            <i class="fa fa-cloud-upload"></i>
                        </div>
                        <div class="media-body">
                            <p>Drag and drop PDF here or</p>
                            <div class="file-upload-custom">
                                <input type="file">
                                <button class="btn btn-blue">BROWSE FILE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <h6>Uploaded Documents</h6>
                <ul class="files-list">
                    <li><i class="fa fa-file-pdf mr-3"></i> <a href="#">Lorem Ipsum</a> <span class="remove-file"><i class="fad fa-trash-alt"></i></span></li>
                    <li><i class="fa fa-file-pdf mr-3"></i> <a href="#">Lorem Ipsum</a> <span class="remove-file"><i class="fad fa-trash-alt"></i></span></li>
                </ul>
            </div>
        </div>
        <hr>
        <h5 class="mt-4">How It Works</h5>
        <p><b>Step 1 -</b> Select document category and type</p>
        <p><b>Step 2 -</b> Upload PDF</p>
        <p><b>Step 3 -</b> Review and submit document</p>
    </div><!-- container -->
</section>

<form method="post" action="">
    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Cancel</a>
                </div>
                <div class="right">
                    <button class="btn">Submit Documents</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>

<?php include "common/footer.php"; ?>
</body>
</html>