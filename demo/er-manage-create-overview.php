<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer > Management</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Manage Employees</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="" class="btn btn-blue mr-3">Create a New Hire Quote</a>
                    <div class="dropdown dd-transparent">
                        <button class="btn secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Order ID Cards</a>
                            <a class="dropdown-item" href="#">View Invoices</a>
                            <a class="dropdown-item" href="#">Commissions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="javascript:;">Active Employees</a>
            <a class="nav-link" href="javascript:;">Recently Added</a>
            <a class="nav-link" href="javascript:;">COBRA</a>
            <a class="nav-link" href="javascript:;">Terminated</a>
            <a class="nav-link" href="javascript:;">Pending Requests</a>
            <a class="nav-link" href="javascript:;">Processed Requests</a>
            <a class="nav-link active" href="er-manage-division-report-summary">Division Reports</a>
            <a class="nav-link" href="er-manage-renewals">Renewals</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-division" method="post" action="">
    <section id="cc-body" class="cc-overview-intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h5>How It Works</h5>
                    <p>Division Reports allows you to view and track your CaliforniaChoice premiums by the divisions or categories that you create. For example, you may want to look at separate billing amounts for hourly employees at your corporate facility or at different office locations. In just a few easy steps, Divisional Reports will eliminate the need to make manual calculations each month.</p>
                    <div class="media mt-5">
                        <i class="fal fa-desktop-alt"></i>
                        <div class="media-body">
                            <h6>Step 1: Create Divisions</h6>
                            <p>Create divisions for locations or categories (i.e., San Jose office, Non-exempt employees) and update or delete divisions as needed.</p>
                        </div>
                    </div>
                    <hr>
                    <div class="media">
                        <i class="fal fa-users"></i>
                        <div class="media-body">
                            <h6>Step 2: Assign Employees</h6>
                            <p>Assign employees to a division. Unassigned employees can be added to a division or remain in this category.</p>
                        </div>
                    </div>
                    <hr>
                    <div class="media">
                        <i class="fal fa-file-alt"></i>
                        <div class="media-body">
                            <h6>Step 3: Generate Reports</h6>
                            <p>Select from three report formats: 1.) Create a summary report that reflects the total monthly billing and employer / employee contributions by division 2.) Create a detailed report that itemizes each employee in each division 3.) Create a detailed report for a specific division and a summary report for all other divisions.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="right">
                    <button class="btn">Continue</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>