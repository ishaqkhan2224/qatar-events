<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer > Management</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Manage Employees</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="#" class="btn btn-blue mr-3">Create a new hire quote</a>
                    <div class="dropdown dd-transparent">
                        <button class="btn btn-grey-outline dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Order ID Cards</a>
                            <a class="dropdown-item" href="#">View Invoices</a>
                            <a class="dropdown-item" href="#">Commissions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="javascript:;">Active Employees</a>
            <a class="nav-link" href="javascript:;">Recently Added</a>
            <a class="nav-link" href="javascript:;">COBRA</a>
            <a class="nav-link" href="javascript:;">Terminated</a>
            <a class="nav-link" href="javascript:;">Pending Requests</a>
            <a class="nav-link" href="javascript:;">Processed Requests</a>
            <a class="nav-link active" href="er-manage-division-report-summary">Division Reports</a>
            <a class="nav-link" href="er-manage-renewals">Renewals</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <p>Select division and premium month to generate a report.</p>

            <div class="form-row align-items-end">
                <div class="form-group col-3">
                    <label class="font-weight-normal">Division</label>
                    <select class="form-control">
                        <option value="">All Divisions</option>
                        <option value="">B</option>
                        <option value="">C</option>
                    </select>
                </div>
                <div class="form-group col-3">
                    <label class="font-weight-normal">Premium Month</label>
                    <select class="form-control">
                        <option value="">September 2019</option>
                        <option value="">B</option>
                        <option value="">C</option>
                    </select>
                </div>
                <div class="col-2 mb-3">
                    <button class="btn btn-blue">Generate Report</button>
                </div>
                <div class="col-4 mb-3 text-right">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-grey-outline"><i class="fal fa-users mr-2"></i>Assign Employees</button>
                        <button type="button" class="btn btn-grey-outline"><i class="fal fa-edit mr-2"></i>Edit Division</button>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row mt-5">
                <div class="col-12">
                    <div class="cc-card-wrapper">
                        <h3>Midwest</h3>
                        <div class="cc-card large">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table card-table mb-0">
                                        <thead>
                                        <tr>
                                            <th width="25%">Employee Name</th>
                                            <th class="text-center th-icon"><i class="fal fa-briefcase-medical"></i><br>Medical</th>
                                            <th class="text-center th-icon"><i class="fal fa-tooth"></i><br>Dental</th>
                                            <th class="text-center th-icon"><i class="fal fa-glasses"></i><br>Vision</th>
                                            <th class="text-center th-icon"><i class="cc-icon-chiro"></i><br>Chiro</th>
                                            <th class="text-center th-icon"><i class="fal fa-heartbeat"></i><br>Life</th>
                                            <th class="text-right">Employer Contribution</th>
                                            <th class="text-right">Employee Contribution</th>
                                            <th class="text-center">Month</th>
                                            <th class="text-center">Change Code</th>
                                            <th class="text-right">Total Premium</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Bertram Gilfoyle</td>
                                            <td class="text-center"><i class="fa fa-check"></i></td>
                                            <td class="text-center"><i class="fa fa-check"></i></td>
                                            <td class="text-center"><i class="fa fa-check"></i></td>
                                            <td class="text-center"><i class="fa fa-check"></i></td>
                                            <td class="text-center"><i class="fa fa-check"></i></td>
                                            <td class="text-right">$855.38</td>
                                            <td class="text-right">$8.64</td>
                                            <td class="text-right">Sep</td>
                                            <td class="text-center"></td>
                                            <td class="text-right">$864.02</td>
                                        </tr>
                                        <tr>
                                            <td>Denish Chugtai</td>
                                            <td class="text-center"><i class="fa fa-check"></i></td>
                                            <td class="text-center"><i class="fa fa-check"></i></td>
                                            <td class="text-center"><i class="fa fa-check"></i></td>
                                            <td class="text-center"><i class="fa fa-check"></i></td>
                                            <td class="text-center"><i class="fa fa-check"></i></td>
                                            <td class="text-right">$855.38</td>
                                            <td class="text-right">$8.64</td>
                                            <td class="text-right">Sep</td>
                                            <td class="text-center"></td>
                                            <td class="text-right">$864.02</td>
                                        </tr>
                                        <tr class="adjustment-total-tr">
                                            <td>Total without Adjustment</td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-right">$2,566.14</td>
                                            <td class="text-right">$25.92</td>
                                            <td class="text-center"></td>
                                            <td class="text-right"></td>
                                            <td class="text-right">$2,592.06</td>
                                        </tr>
                                        <tr class="middle-tr">
                                            <th>ADJUSTMENTS</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <td>Bertram Gilfoyle</td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-right"></td>
                                            <td class="text-right"></td>
                                            <td class="text-right">Aug</td>
                                            <td class="text-center">CA</td>
                                            <td class="text-right">$178.02</td>
                                        </tr>
                                        <tr>
                                            <td>Denish Chugtai</td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-right"></td>
                                            <td class="text-right"></td>
                                            <td class="text-right">Aug</td>
                                            <td class="text-center">CA</td>
                                            <td class="text-right">$121.98</td>
                                        </tr>
                                        <tr class="adjustment-total-tr">
                                            <td>Adjustment Total</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="text-right"></td>
                                            <td class="text-right"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-right">$300.00</td>
                                        </tr>
                                        <tr>
                                            <td><b>Division Total</b></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="text-right"><b></b></td>
                                            <td class="text-right"><b></b></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-right"><b>$2,892.06</b></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- cc-card large -->
                    </div><!-- cc-card-wrapper -->
                </div><!-- col-12 -->
            </div><!-- row -->
            <div class="row mt-5">
                <div class="col-12">
                    <div class="cc-card-wrapper">
                        <h3>Total by Division</h3>
                        <div class="cc-card large">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table card-table mb-0">
                                        <thead>
                                        <tr>
                                            <th width="55%"></th>
                                            <th width="25%"># of Employees</th>
                                            <th width="25%" class="text-right">Total Premium</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Midwest</td>
                                            <td class="">2</td>
                                            <td class="text-right">$1,728.04</td>
                                        </tr>
                                        <tr>
                                            <td>Northeast</td>
                                            <td class="">2</td>
                                            <td class="text-right">$2,592,06</td>
                                        </tr>
                                        <tr>
                                            <td>South</td>
                                            <td class="">2</td>
                                            <td class="text-right">$864.02</td>
                                        </tr>
                                        <tr class="adjustment-tr">
                                            <td>West</td>
                                            <td class="">2</td>
                                            <td class="text-right">$3,456.08</td>
                                        </tr>
                                        <tr>
                                            <td>Unassigned</td>
                                            <td class="">2</td>
                                            <td class="text-right">$0</td>
                                        </tr>
                                        <tr>
                                            <td>Fees</td>
                                            <td class=""></td>
                                            <td class="text-right">$300.00</td>
                                        </tr>
                                        <tr>
                                            <td><b>Company Total</b></td>
                                            <td><b>8</b></td>
                                            <td class="text-right"><b>$12,248.36</b></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- cc-card large -->
                    </div><!-- cc-card-wrapper -->
                </div><!-- col-12 -->
            </div><!-- row -->

            <div class="row mt-5 mb-4">
                <div class="col">
                    <ul class="cc-b-list">
                        <li><b>A:</b> Add</li>
                        <li><b>AC:</b> Add COBRA</li>
                        <li><b>C:</b> Change Plan</li>
                        <li><b>CA:</b> Change Age</li>
                    </ul>
                </div>
                <div class="col">
                    <ul class="cc-b-list">
                        <li><b>CI:</b> Change Information</li>
                        <li><b>CO:</b> Correction</li>
                        <li><b>DA:</b> Dependent Add</li>
                        <li><b>DT:</b> Dependent Termination</li>
                    </ul>
                </div>
                <div class="col">
                    <ul class="cc-b-list">
                        <li><b>ER:</b> Employee Reinstatement</li>
                        <li><b>GR:</b> Group Reinstatement</>
                        <li><b>NT:</b> New Termination</li>
                        <li><b>RA:</b> Retro Add</li>
                    </ul>
                </div>
                <div class="col">
                    <ul class="cc-b-list">
                        <li><b>RC:</b> Retro</li>
                        <li><b>RDA:</b> Retro Dependent Add</li>
                        <li><b>RDT:</b> Retro Dependent Termination</li>
                        <li><b>RT:</b> Retro Termination</li>
                    </ul>
                </div>
                <div class="col">
                    <ul class="cc-b-list">
                        <li><b>VC:</b> Life Volume Change</li>
                    </ul>
                </div>
            </div>

            <p><b>Note:</b> This report is intended to give you a breakdown of your monthly charges by division and does not reflect received. Please refer to your invoice statement to make your monthly premium payments.</p>
            <p>Client#: 41130</p>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Cancel</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>