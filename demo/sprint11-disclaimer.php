<!DOCTYPE html>
<html>
<head>
<?php include "inc/header.meta.php"; ?>
<title>CC UI Deliverables: Sprint 11</title>
<?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header-plain.php"; ?>
<section id="cc-body">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2>RX Search</h2>
          <p>This directory of drug formularies was collected from all plans participating in the CaliforniaChoice program and is accurate to the best of our knowledge at the time the data was posted to the website. However, the drug formularies and policies offered through CaliforniaChoice health plans may change at any time without notice. Keep in mind, this is only a guide and you must verify the information directly with the health plan only before making decisions based on the information provided in this directory. CaliforniaChoice will not be responsible for the information presented in his search.</p>
          <p><strong>For questions, call Customer Service at (800) 558-8003.</strong></p>
        </div>
      </div>
    </div><!-- container -->
</section>

<section class="cc-controls sticky">
    <div class="container">
        <div class="d-flex align-items-center">
            <div class="left">
                <a href="#" class="btn-link">Decline</a>
            </div>
            <div class="right">
                <button class="btn">Accept</button>
            </div>
        </div>
    </div>
    <div class="cc-controls-footer-links">
      <div class="container">
        <div class="d-flex justify-content-between">
            <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
            <div>
              <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
            </div>
        </div>
      </div>
    </div>
</section>
<?php include "common/footer.php"; ?>
</body>
</html>