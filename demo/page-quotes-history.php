<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Forms</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Quotes</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec quam et lectus imperdiet pulvinar.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="" class="btn primary mr-3 disabled">Create a New Quote</a>
                    <div class="dropdown dd-transparent">
                        <button class="btn secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Create a New Hire Quote</a>
                            <a class="dropdown-item" href="#">View Invoices</a>
                            <a class="dropdown-item" href="#">Commissions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link active" href="#">Initial</a>
            <a class="nav-link" href="#">Enrollment</a>
            <a class="nav-link" href="#">Unfinished</a>
            <a class="nav-link" href="#">New Hire</a>
        </nav>
    </div>
</section>

<section id="cc-body">
    <div class="container">
        <a href="#" class="back-page"><i class="fas fa-long-arrow-alt-left"></i> Back to Quotes</a>
        <div class="section-overview-title">
            <h2>Mike's Bike Shop - Quote #567891</h2>
            <p>Below are PDFs generated for this quote. Click on individual files or download all files to view and compare.</p>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="cc-card-wrapper">
                    <div class="cc-card history-card">
                        <div class="card-ribbon">
                            <span>Latest</span>
                        </div>
                        <div class="d-flex align-items-center">
                            <div class="details-right">
                                <p>Created On</p>
                                <p class="date-time"><b>10/21/2019</b> @ 12:45 pm</p>
                            </div>
                        </div>
                        <div class="details">
                            <a href="#"><i class="fas fa-file-pdf"></i>Initial Quote</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Ancillary Benefits</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Medical Benefits</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Dependent Rate</a>
                        </div>
                        <a href="#" class="btn btn-grey-outline d-block"><i class="fas fa-download"></i> Download All</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="cc-card-wrapper">
                    <div class="cc-card history-card">
                        <div class="d-flex align-items-center">
                            <div class="details-right">
                                <p>Created On</p>
                                <p class="date-time"><b>10/21/2019</b> @ 12:45 pm</p>
                            </div>
                        </div>
                        <div class="details">
                            <a href="#"><i class="fas fa-file-pdf"></i>Initial Quote</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Ancillary Benefits</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Medical Benefits</a>
                        </div>
                        <a href="#" class="btn btn-grey-outline d-block"><i class="fas fa-download"></i> Download All</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="cc-card-wrapper">
                    <div class="cc-card history-card">
                        <div class="d-flex align-items-center">
                            <div class="details-right">
                                <p>Created On</p>
                                <p class="date-time"><b>10/21/2019</b> @ 12:45 pm</p>
                            </div>
                        </div>
                        <div class="details">
                            <a href="#"><i class="fas fa-file-pdf"></i>Initial Quote</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Ancillary Benefits</a>
                        </div>
                        <a href="#" class="btn btn-grey-outline d-block"><i class="fas fa-download"></i> Download All</a>
                    </div>
                </div>
            </div>
        </div><!-- row -->
        <hr class="mb-4">
        <div class="row">
            <div class="col-4">
                <div class="cc-card-wrapper">
                    <div class="cc-card history-card">
                        <div class="d-flex align-items-center">
                            <div class="details-right">
                                <p>Created On</p>
                                <p class="date-time"><b>10/21/2019</b> @ 12:45 pm</p>
                            </div>
                        </div>
                        <div class="details">
                            <a href="#"><i class="fas fa-file-pdf"></i>Initial Quote</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Ancillary Benefits</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Medical Benefits</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Dependent Rate</a>
                        </div>
                        <a href="#" class="btn btn-grey-outline d-block"><i class="fas fa-download"></i> Download All</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="cc-card-wrapper">
                    <div class="cc-card history-card">
                        <div class="d-flex align-items-center">
                            <div class="details-right">
                                <p>Created On</p>
                                <p class="date-time"><b>10/21/2019</b> @ 12:45 pm</p>
                            </div>
                        </div>
                        <div class="details">
                            <a href="#"><i class="fas fa-file-pdf"></i>Initial Quote</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Ancillary Benefits</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Medical Benefits</a>
                        </div>
                        <a href="#" class="btn btn-grey-outline d-block"><i class="fas fa-download"></i> Download All</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="cc-card-wrapper">
                    <div class="cc-card history-card">
                        <div class="d-flex align-items-center">
                            <div class="details-right">
                                <p>Created On</p>
                                <p class="date-time"><b>10/21/2019</b> @ 12:45 pm</p>
                            </div>
                        </div>
                        <div class="details">
                            <a href="#"><i class="fas fa-file-pdf"></i>Initial Quote</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Ancillary Benefits</a>
                        </div>
                        <a href="#" class="btn btn-grey-outline d-block"><i class="fas fa-download"></i> Download All</a>
                    </div>
                </div>
            </div>
        </div><!-- row -->
        <hr class="mb-4">
        <div class="row">
            <div class="col-4">
                <div class="cc-card-wrapper">
                    <div class="cc-card history-card">
                        <div class="d-flex align-items-center">
                            <div class="details-right">
                                <p>Created On</p>
                                <p class="date-time"><b>10/21/2019</b> @ 12:45 pm</p>
                            </div>
                        </div>
                        <div class="details">
                            <a href="#"><i class="fas fa-file-pdf"></i>Initial Quote</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Ancillary Benefits</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Medical Benefits</a>
                        </div>
                        <a href="#" class="btn btn-grey-outline d-block"><i class="fas fa-download"></i> Download All</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="cc-card-wrapper">
                    <div class="cc-card history-card">
                        <div class="d-flex align-items-center">
                            <div class="details-right">
                                <p>Created On</p>
                                <p class="date-time"><b>10/21/2019</b> @ 12:45 pm</p>
                            </div>
                        </div>
                        <div class="details">
                            <a href="#"><i class="fas fa-file-pdf"></i>Initial Quote</a>
                            <a href="#"><i class="fas fa-file-pdf"></i>Ancillary Benefits</a>
                        </div>
                        <a href="#" class="btn btn-grey-outline d-block"><i class="fas fa-download"></i> Download All</a>
                    </div>
                </div>
            </div>
        </div><!-- row -->

</section>

</body>
</html>