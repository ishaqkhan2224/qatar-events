<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Online Enrollment Management</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
<<<<<<< HEAD
                    <h2>Online Enrollment Management tesiting changes</h2>
=======
                    <h2>Online Enrollment Management</h2>
>>>>>>> e99f074... Remove change testing
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. corporis, sunt distinctio</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="#" class="btn primary">Start an online enrollment</a>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link active" href="#">Active</a>
            <a class="nav-link" href="#">Expired</a>
            <a class="nav-link" href="#">Canceled</a>
        </nav>
    </div>
</section>

<section id="cc-body">
    <div class="container">
        <table class="table js-sortable-table" id="cc-table-data">
            <thead>
            <tr>
                <th colspan="3" class="cc-table__product-empty" rowspan="1"></th>
                <th class="cc-table__product-description" rowspan="1"><span>Status</span></th>
                <th colspan="2" class="cc-table__product-empty" rowspan="1"></th>
            </tr>
            <tr>
                <th scope="col">Group</th>
                <th scope="col" class="no-sort">Quote Number</th>
                <th scope="col" class="no-sort">Effective Date</th>
                <th scope="col" class="cc-table__product-icon fixed-width no-sort">
                    <div class="th-progress">
                        <span>BR</span>
                        <span>ER</span>
                        <span>UW</span>
                    </div>
                </th>
                <th scope="col">Days left</th>
                <th scope="col" class="no-sort"></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Acme Corporation</td>
                <td>3456789</td>
                <td>12/01/2019</td>
                <td>
                    <div class="progress ole-progress">
                        <div class="progress-bar" style="width:100%"></div>
                        <span class="progress-step first active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                        <span class="progress-step second active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                        <span class="progress-step third active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                    </div>
                </td>
                <td>
                    0
                </td>
                <td class="cc-table__td-action js-td-action">
                    <div class="dropdown cc-table__td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">View Detail</a>
                            <a class="dropdown-item" href="#">Finalize Enrollment</a>
                            <a class="dropdown-item" href="#">Cancel Enrollment</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Cyberdyne Systems</td>
                <td>4567985</td>
                <td>12/01/2019</td>
                <td class="text-center">
                    <div class="progress ole-progress">
                        <div class="progress-bar" style="width:75%"></div>
                        <span class="progress-step first active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                        <span class="progress-step second active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                        <span class="progress-step third"><i class="fas fa-check"></i></span>
                    </div>
                </td>
                <td>
                    0
                </td>
                <td class="cc-table__td-action js-td-action">
                    <div class="dropdown cc-table__td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">View Detail</a>
                            <a class="dropdown-item" href="#">Finalize Enrollment</a>
                            <a class="dropdown-item" href="#">Cancel Enrollment</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Hooli <i class="fas fa-exclamation-square" data-toggle="tooltip" title="" aria-hidden="true" data-original-title="Warning Message"></i></td>
                <td>963214</td>
                <td>12/01/2019</td>
                <td class="text-center">
                    <div class="progress ole-progress">
                        <div class="progress-bar" style="width:50%"></div>
                        <span class="progress-step first active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                        <span class="progress-step second active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                        <span class="progress-step third"><i class="fas fa-check"></i></span>
                    </div>
                </td>
                <td>7</td>
                <td class="cc-table__td-action js-td-action">
                    <div class="dropdown cc-table__td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">View Detail</a>
                            <a class="dropdown-item" href="#">Finalize Enrollment</a>
                            <a class="dropdown-item" href="#">Cancel Enrollment</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Pied Piper</td>
                <td>6548527</td>
                <td>12/01/2019</td>
                <td class="text-center">
                    <div class="progress ole-progress">
                        <div class="progress-bar" style="width:25%"></div>
                        <span class="progress-step first active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                        <span class="progress-step second"><i class="fas fa-check"></i></span>
                        <span class="progress-step third"><i class="fas fa-check"></i></span>
                    </div>
                </td>
                <td>
                    14
                </td>
                <td class="cc-table__td-action js-td-action">
                    <div class="dropdown cc-table__td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">View Detail</a>
                            <a class="dropdown-item" href="#">Finalize Enrollment</a>
                            <a class="dropdown-item" href="#">Cancel Enrollment</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Venement Capital</td>
                <td>2586547</td>
                <td>12/01/2019</td>
                <td class="text-center">
                    <div class="progress ole-progress">
                        <div class="progress-bar" style="width:0"></div>
                        <span class="progress-step first active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                        <span class="progress-step second"><i class="fas fa-check"></i></span>
                        <span class="progress-step third"><i class="fas fa-check"></i></span>
                    </div>
                </td>
                <td>30</td>
                <td class="cc-table__td-action js-td-action">
                    <div class="dropdown cc-table__td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">View Detail</a>
                            <a class="dropdown-item" href="#">Finalize Enrollment</a>
                            <a class="dropdown-item" href="#">Cancel Enrollment</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Initech</td>
                <td>9632584</td>
                <td>12/01/2019</td>
                <td class="text-center">
                    <div class="progress ole-progress">
                        <div class="progress-bar" style="width:50%"></div>
                        <span class="progress-step first active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                        <span class="progress-step second active" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="fas fa-check"></i></span>
                        <span class="progress-step third"><i class="fas fa-check"></i></span>
                    </div>
                </td>
                <td>30</td>
                <td class="cc-table__td-action js-td-action">
                    <div class="dropdown cc-table__td-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">View Detail</a>
                            <a class="dropdown-item" href="#">Finalize Enrollment</a>
                            <a class="dropdown-item" href="#">Cancel Enrollment</a>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div><!-- container -->
</section>

<?php include "common/footer.php"; ?>
</body>
</html>