<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employee OLE</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Online Enrollment</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="ee-ole-overview">Overview</a>
            <a class="nav-link" href="ee-ole-your-info">Your Info</a>
            <a class="nav-link active" href="ee-ole-dependents">Dependents</a>
            <a class="nav-link" href="ee-ole-medical">Medical</a>
            <a class="nav-link" href="ee-ole-dental">Dental</a>
            <a class="nav-link" href="ee-ole-chiro">Chiro</a>
            <a class="nav-link" href="ee-ole-vision">Vision</a>
            <a class="nav-link" href="ee-ole-life">Life</a>
            <a class="nav-link" href="ee-ole-section-125">Section 125</a>
            <a class="nav-link" href="ee-ole-summary">Summary</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <div class="d-flex align-items-end justify-content-between mb-2">
                <h5 class="mb-0">Spouse or Domestic Partner</h5>
                <a class="ml-2" href="#">View eligibility requirements</a>
            </div>
            <?php if (!isset($_GET['completed'])): ?>
            <div class="cc-card d-flex align-items-center mt-2">
                <p class="mb-0">Do they need coverage?</p>
                <button type="button" class="btn btn-grey-outline ml-3 js-cc-slideout-show" data-cc-modal="#ccAddSpouse"><i class="fas fa-plus mr-2"></i>Add Spouse</button>
            </div>
            <?php endif; ?>
            <?php if (isset($_GET['completed'])): ?>
            <div class="cc-card">
                <table class="table card-table" id="cc-table-data">
                    <tbody class="card-table-no-headers">
                    <tr>
                        <th width="25%" scope="col">Name</th>
                        <th width="20%" scope="col">Social Security Number</th>
                        <th width="20%" scope="col">Date of Birth</th>
                        <th width="15%" scope="col">Gender</th>
                        <th width="15%" scope="col">Relationship</th>
                        <th width="10%" scope="col"></th>
                    </tr>
                    <tr>
                        <td>Rechard W. Hendrick Sr.</td>
                        <td>xxx-xx-0123</td>
                        <td>10/28/1979</td>
                        <td>Male</td>
                        <td>Spouse</td>
                        <td class="cc-table__td-action js-td-action">
                            <div class="dropdown cc-table__td-dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Edit</a>
                                    <a class="dropdown-item" href="#">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <button type="button" class="btn btn-grey-outline d-inline-block js-cc-slideout-show" data-cc-modal="#ccAddSpouse"><i class="fas fa-plus mr-2"></i>Add Spouse</button>
            <?php endif; ?>
            <div class="d-flex align-items-end justify-content-between mt-5 mb-2">
                <div>
                    <h5 class="mb-0">Dependent</h5>
                    <p class="mb-0 smalltext">Depending on your section of health plan, you may be asked to supply some additional Information</p>
                </div>
                <a href="#">View eligibility requirements</a>
            </div>
            <?php if (!isset($_GET['completed'])): ?>
            <div class="cc-card d-flex align-items-center mt-2">
                <p class="mb-0">Do they need coverage?</p>
                <button type="button" class="btn btn-grey-outline ml-3 js-cc-slideout-show" data-cc-modal="#ccAddDependents"><i class="fas fa-plus mr-2"></i>Add Dependents</button>
            </div>
            <?php endif; ?>
            <?php if (isset($_GET['completed'])): ?>
            <div class="cc-card">
                <table class="table card-table" id="cc-table-data">
                    <tbody class="card-table-no-headers">
                    <tr>
                        <th width="20%" scope="col">Name</th>
                        <th width="20%" scope="col">Social Security Number</th>
                        <th width="20%" scope="col">Date of Birth</th>
                        <th width="10%" scope="col">Gender</th>
                        <th width="10%" scope="col">Relationship</th>
                        <th width="10%" scope="col">Disabled</th>
                        <th width="10%" scope="col"></th>
                    </tr>
                    <tr>
                        <td>Rechard W. Hendrick Jr.</td>
                        <td>xxx-xx-5466</td>
                        <td>11/26/2009</td>
                        <td>Male</td>
                        <td>Son</td>
                        <td>No</td>
                        <td class="cc-table__td-action js-td-action">
                            <div class="dropdown cc-table__td-dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Edit</a>
                                    <a class="dropdown-item" href="#">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Sarah Hendrick</td>
                        <td>xxx-xx-6546</td>
                        <td>10/16/2012</td>
                        <td>Female</td>
                        <td>Daughter</td>
                        <td>No</td>
                        <td class="cc-table__td-action js-td-action">
                            <div class="dropdown cc-table__td-dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Edit</a>
                                    <a class="dropdown-item" href="#">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <button type="button" class="btn btn-grey-outline d-inline-block mb-5 js-cc-slideout-show" data-cc-modal="#ccAddDependents"><i class="fas fa-plus mr-2"></i>Add Dependents</button>
            <?php endif; ?>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Back</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>

<form action="" id="cc-form__spouse-modal">
    <div id="ccAddSpouse" class="cc-modal cc-add-employee-modal">
        <div class="cc-modal-header">
            <span class="cc-modal-close js-cc-slideout-close"><i class="fa fa-times"></i></span>
            <h5>Spouse or Domestic Partner</h5>
        </div>
        <div class="cc-modal-inner">
            <div class="cc-modal-body">
                <div class="cc-modal-inner-content">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-row">
                                <div class="form-group col-md-10">
                                    <label>First Name</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group col-md-2">
                                    <label>MI</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-10">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Suffix</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>ssn</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Date of birth</label>
                                    <div class="d-flex align-items-center">
                                        <input type="text" class="form-control date-field w-100 mr-2" placeholder="MM/DD/YYYY" required="">
                                        <i class="fa fa-calendar-alt" id="dateOfBirth" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Gender</label>
                                    <select class="form-control">
                                        <option selected="">Please Select</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Relationship</label>
                                    <select class="form-control">
                                        <option selected="">Please Select</option>
                                        <option>Brother</option>
                                        <option>Friend</option>
                                        <option>Son</option>
                                        <option>Daughter</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cc-modal-footer fixed">
            <div class="d-flex justify-content-between">
                <div class="col-4">
                    <a href="#" class="btn-link js-cc-slideout-close">Close</a>
                </div>
                <div class="col-6">
                    <div class="d-flex justify-content-end">
                        <a href="#" class="btn btn-blue js-show-confirmation-alert">Save</a>
                    </div>

                </div>
            </div>
        </div>
    </div><!-- ccAddEmployeeModal -->
</form>

<form action="" id="cc-form__dependents-modal">
    <div id="ccAddDependents" class="cc-modal cc-add-employee-modal">
        <div class="cc-modal-header">
            <span class="cc-modal-close js-cc-slideout-close"><i class="fa fa-times"></i></span>
            <h5>Dependents</h5>
        </div>
        <div class="cc-modal-inner">
            <div class="cc-modal-body">
                <div class="alert cc-success mb-4 text-white" role="alert"><strong><i class="fa fa-check mr-3 text-white"></i></strong>Richard W. Hendrick has been added</div>
                <div class="cc-modal-inner-content">
                    <div class="row">
                        <div class="col-md-7">
                            <h6 class="mb-3">Dependents Information</h6>
                            <div class="form-row">
                                <div class="form-group col-md-10">
                                    <label>First Name</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group col-md-2">
                                    <label>MI</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-10">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Suffix</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>ssn</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Date of birth</label>
                                    <div class="d-flex align-items-center">
                                        <input type="text" class="form-control date-field w-100 mr-2" placeholder="MM/DD/YYYY" required="">
                                        <i class="fa fa-calendar-alt" id="dateOfBirth" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Gender</label>
                                    <select class="form-control">
                                        <option selected="">Please Select</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Relationship</label>
                                    <select class="form-control">
                                        <option selected="">Please Select</option>
                                        <option>Brother</option>
                                        <option>Son</option>
                                        <option>Daughter</option>
                                        <option>Father</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Disabled</label>
                                    <select class="form-control">
                                        <option selected="">Please Select</option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </div>
                                <p class="smalltext ml-1">A Disabled Dependent Certification form must be completed and/or be on file for a disabled dependent. The disability diagnosis must occur before the dependent reached the maximum dependent age allowed by carrier.</p>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="mb-5">
                                <table class="table">
                                    <thead>
                                    <tr colspan="2"><h6 class="mb-3">Dependents Added</h6></tr>
                                    </thead>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td><a href="#">Richard W. Hendrick Jr.</a></td>
                                        <td><a href="#"><i class="fas fa-trash-alt"></i></a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cc-modal-footer fixed">
            <div class="d-flex justify-content-between">
                <div class="col-4">
                    <a href="#" class="btn-link js-cc-slideout-close">Close</a>
                </div>
                <div class="col-6">
                    <div class="d-flex justify-content-end">
                        <a href="#" class="btn btn-blue js-show-confirmation-alert">Save</a>
                    </div>

                </div>
            </div>
        </div>
    </div><!-- ccAddEmployeeModal -->
</form>


<?php include "common/footer.php"; ?>
</body>
</html>