<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer OLE</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Online Enrollment</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="er-ole-overview">Overview</a>
            <a class="nav-link" href="er-ole-application">Application</a>
            <a class="nav-link" href="er-ole-census">Census</a>
            <a class="nav-link active" href="er-ole-metaltier">Metal Tier</a>
            <a class="nav-link" href="er-ole-contribution">Contribution</a>
            <a class="nav-link" href="er-ole-optional-benefits">Optional Benefits</a>
            <a class="nav-link" href="er-ole-summary">Summary</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <h5>Select a Metal Tier from the option below.</h5>
            <div class="row mt-5 js-radio-row-container">
                <div class="col-4 d-flex">
                    <div class="cc-card large flex-grow-1">
                      <div class="cc-card-wrapper text-center">
                        <div class="card-ribbon">
                              <span>Most Popular</span>
                            </div>
                        <div class="metaltier-header">
                            <h5>Triple Tier</h5>
                            <p>Access to plans and benefits in<br>three adjoining metal tiers</p>
                        </div>
                        <div class="metaltier-action">
                            <a href="javascript:;" class="radio-row">Platinum &amp; Gold &amp; Silver</a>
                        </div>
                      </div>
                    </div><!-- cc-card -->
                </div>
                <div class="col-4 d-flex">
                    <div class="cc-card large flex-grow-1">
                      <div class="cc-card-wrapper text-center">
                        <div class="metaltier-header">
                            <h5>Double Tier</h5>
                            <p>Access to plans and benefits in<br>neighboring metal tiers</p>
                        </div>
                        <div class="metaltier-action">
                            <a href="javascript:;" class="radio-row">Platinum &amp; Gold</a>
                            <a href="javascript:;" class="radio-row">Gold & Silver</a>
                            <a href="javascript:;" class="radio-row">Silver & Bronze</a>
                        </div>
                      </div>
                    </div><!-- cc-card -->
                </div>
                <div class="col-4 d-flex">
                    <div class="cc-card large flex-grow-1">
                      <div class="cc-card-wrapper text-center">
                        <div class="metaltier-header">
                            <h5>Single Tier</h5>
                            <p>Access to a single metal tier</p>
                        </div>
                        <div class="metaltier-action">
                            <a href="javascript:;" class="radio-row">Platinum</a>
                            <a href="javascript:;" class="radio-row">Gold</a>
                            <a href="javascript:;" class="radio-row">Silver</a>
                            <a href="javascript:;" class="radio-row">Bronze</a>
                        </div>
                      </div>
                    </div><!-- cc-card -->
                </div>
            </div>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="javascript:;" class="btn-link">Cancel</a>
                </div>
                <div class="right">
                    <a href="javascript:;" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>