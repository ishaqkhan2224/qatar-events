<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Forms</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<?php include "common/subheader-tabs.php"; ?>

<section id="cc-body">
    <div class="container">
        <a href="#" class="back-page"><i class="fas fa-long-arrow-alt-left"></i> Back to Processed Requests</a>
        <div class="section-overview-title">
            <h2>Adam West - Manager</h2>
            <p><strong>Request Type:</strong> New Hire <span class="badge cc-success ml-3"><i class="far fa-check"></i> {{status label}} </span></p>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="cc-card-wrapper">
                    <h3>Employee &amp; Dependent Info</h3>
                    <div class="cc-card large">

                        <div class="row mb-3">
                            <div class="col-md-6">
                                <p>
                                    <b>Mailing Address</b><br>
                                    701 N. Baxter St.<br>
                                    Irvine, Orange Country, CA 91301
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p>
                                    <b>Billing Address</b><br>
                                    514 N, Edgley Ave.<br>
                                    Irvine, Orange Country, CA 91301
                                </p>
                            </div>
                        </div>

                       <div class="row">
                           <div class="col-12">
                               <table class="table card-table">
                                   <thead>
                                   <tr>
                                       <th scope="col">Name</th>
                                       <th scope="col" class="text-center">DOB</th>
                                       <th scope="col" class="text-center">Gender</th>
                                       <th scope="col" class="text-center">SSN</th>
                                       <th scope="col">Relationship</th>
                                   </tr>
                                   </thead>
                                   <tbody>
                                   <tr>
                                       <td>Adam West <span class="badge cc-success">New</span></td>
                                       <td class="text-center">09/19/1928</td>
                                       <td class="text-center">M</td>
                                       <td class="text-center">xxx-xx-1435</td>
                                       <td>Employee</td>
                                   </tr>
                                   <tr>
                                       <td>Adam West</td>
                                       <td class="text-center">09/19/1928</td>
                                       <td class="text-center">M</td>
                                       <td class="text-center">xxx-xx-1435</td>
                                       <td>Spouse</td>
                                   </tr>
                                   <tr>
                                       <td>Adam West</td>
                                       <td class="text-center">09/19/1928</td>
                                       <td class="text-center">M</td>
                                       <td class="text-center">xxx-xx-1435</td>
                                       <td>Son</td>
                                   </tr>
                                   </tbody>
                               </table>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div><!-- row -->

        <div class="row mt-4">
            <div class="col-12">
                <div class="cc-card-wrapper">
                    <h3>Cost Summary</h3>
                    <p>Summary of benefit cost to employer and employee</p>
                    <div class="cc-card large">
                        
                       <div class="row">
                           <div class="col-12">
                                <table class="table card-table">
                                    <thead>
                                    <tr>
                                        <th scope="col" width="5%"></th>
                                        <th scope="col" width="45%">Elected Plan</th>
                                        <th scope="col" width="25%" class="text-right">Employer Cost</th>
                                        <th scope="col" width="25%" class="text-right">Employee Cost</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><i class="fal fa-briefcase-medical"></i></td>
                                        <td>UnitedHealthCare Silver HMO</td>
                                        <td class="text-right">$69.72</td>
                                        <td class="text-right">$1,569.72</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fal fa-tooth"></i></td>
                                        <td>Dentegra Smile Club</td>
                                        <td class="text-right">$50.25</td>
                                        <td class="text-right">$1,150.25</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fal fa-glasses"></i></td>
                                        <td>EyeMed</td>
                                        <td class="text-right">$17.28</td>
                                        <td class="text-right">$1,238.24</td>
                                    </tr>
                                    <tr>
                                        <td><i class="cc-icon-chiro"></i></td>
                                        <td>Landmark Chiro/Accupunture</td>
                                        <td class="text-right">$0</td>
                                        <td class="text-right">$0</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fal fa-heartbeat"></i></td>
                                        <td>MetLife</td>
                                        <td class="text-right">$0</td>
                                        <td class="text-right">$0</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fas fa-usd-square"></i></td>
                                        <td><strong>Total Cost</strong></td>
                                        <td class="text-right"><strong>$3,737.25</strong></td>
                                        <td class="text-right"><strong>$3,737.25</strong></td>
                                    </tr>
                                    </tbody>
                                </table>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div><!-- row -->

        <div class="row mt-4">
            <div class="col-12">
                <div class="cc-card-wrapper">
                    <h3>Coverage Summary</h3>
                    <p>Employee and dependents coverage summary.</p>

                    <div class="cc-card large">
                       <div class="row">
                           <div class="col-12">
                            <h4 class="mb-3">PRODUCT #1</h4>
                            <table class="table card-table">
                                <thead>
                                    <tr>
                                        <th width="50%">Name</th>
                                        <th width="50%">Elected</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Name 01</td>
                                    <td>{Elected Plan Name}</td>
                                </tr>
                                <tr>
                                    <td>Name 02</td>
                                    <td><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Name 03</td>
                                    <td><i class="fa fa-check"></i></td>
                                </tr>
                                </tbody>
                            </table>
                           </div>
                       </div>
                    </div><!-- cc-card large -->

                    <div class="cc-card large">
                       <div class="row">
                           <div class="col-12">
                            <h4 class="mb-3">PRODUCT #2</h4>
                            <table class="table card-table">
                                <thead>
                                    <tr>
                                        <th width="50%">Name</th>
                                        <th width="50%">Elected</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Name 01</td>
                                    <td>{Elected Plan Name}</td>
                                </tr>
                                <tr>
                                    <td>Name 02</td>
                                    <td><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Name 03</td>
                                    <td><i class="fa fa-check"></i></td>
                                </tr>
                                </tbody>
                            </table>
                           </div>
                       </div>
                    </div><!-- cc-card large -->

                    <div class="cc-card large">
                       <div class="row">
                           <div class="col-12">
                            <h4 class="mb-3">PRODUCT #3</h4>
                            <table class="table card-table">
                                <thead>
                                    <tr>
                                        <th width="50%">Name</th>
                                        <th width="50%">Elected</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Name 01</td>
                                    <td>{Elected Plan Name}</td>
                                </tr>
                                <tr>
                                    <td>Name 02</td>
                                    <td><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Name 03</td>
                                    <td><i class="fa fa-check"></i></td>
                                </tr>
                                </tbody>
                            </table>
                           </div>
                       </div>
                    </div><!-- cc-card large -->

                    <div class="cc-card large">
                       <div class="row">
                           <div class="col-12">
                            <h4 class="mb-3">PRODUCT #4</h4>
                            <table class="table card-table">
                                <thead>
                                    <tr>
                                        <th width="50%">Name</th>
                                        <th width="50%">Elected</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Name 01</td>
                                    <td>{Elected Plan Name}</td>
                                </tr>
                                <tr>
                                    <td>Name 02</td>
                                    <td><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Name 03</td>
                                    <td><i class="fa fa-check"></i></td>
                                </tr>
                                </tbody>
                            </table>
                           </div>
                       </div>
                    </div><!-- cc-card large -->

                </div>
            </div>
        </div><!-- row -->       

        <div class="row mt-4">
            <div class="col-12">
                <div class="cc-card-wrapper">
                    <h3>Comment</h3>
                    <div class="cc-card large">
                       <div class="row">
                           <div class="col-12">
                               <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti aliquid vero harum rerum voluptates ab ullam explicabo maxime inventore alias, molestias reiciendis voluptate modi sunt placeat in, vel illo, dolorem.</span>
                               <span>In, iure cum? Debitis laborum veritatis, modi eligendi consequuntur ducimus, a laudantium provident, deleniti earum voluptas delectus labore dolor. Beatae excepturi explicabo suscipit deleniti voluptatum quos nostrum iure doloremque, cupiditate.</span>
                               </p>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div><!-- row -->

    </div>
</section>

</body>
</html>