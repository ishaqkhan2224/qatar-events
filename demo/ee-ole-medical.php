<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employee OLE</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Online Enrollment</h2>
                    <p>Go paperless and enroll in CaliforniaChoice online.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="ee-ole-overview">Overview</a>
            <a class="nav-link" href="ee-ole-your-info">Your Info</a>
            <a class="nav-link" href="ee-ole-dependents">Dependents</a>
            <a class="nav-link active" href="ee-ole-medical">Medical</a>
            <a class="nav-link" href="ee-ole-dental">Dental</a>
            <a class="nav-link" href="ee-ole-chiro">Chiro</a>
            <a class="nav-link" href="ee-ole-vision">Vision</a>
            <a class="nav-link" href="ee-ole-life">Life</a>
            <a class="nav-link" href="ee-ole-section-125">Section 125</a>
            <a class="nav-link" href="ee-ole-summary">Summary</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <div class="cc-card-wrapper">
                <h5>Medical Plans</h5>
                <p>Select your coverage and plan.</p>
                <div class="cc-card large">
                    <div class="row">
                        <div class="col-md-8">
                            <h6>Include in Coverage</h6>
                            <div class="row mt-3">
                                <div class="col-md-4">
                                    <h6 class="mb-3">Spouse</h6>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="spouse">
                                        <label class="custom-control-label" for="spouse">Richard W. Hendrick Sr.</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h6 class="mb-3">Dependents (Ages 0-20)</h6>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="dependents1">
                                        <label class="custom-control-label" for="dependents1">Richard W. Hendrick Jr.</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mt-2">
                                        <input type="checkbox" class="custom-control-input" id="dependents2">
                                        <label class="custom-control-label" for="dependents2">Dan Hendrick</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h6 class="mb-3">Dependents (Ages 21+)</h6>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="dependentsPlus1">
                                        <label class="custom-control-label" for="dependentsPlus1">Sarah Hendrick</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mt-2">
                                        <input type="checkbox" class="custom-control-input" id="dependentsPlus2">
                                        <label class="custom-control-label" for="dependentsPlus2">Kevin Hendrick</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="cc-card bg-light mb-0 mt-3">
                                <h6>Your Employer's Contribution</h6>
                                <p>50% of the Lowest Cost Employee Rate for HMO/EPO 0% of the Dependent Rate for Same Plan as Above</p>
                            </div>
                        </div>
                    </div>
                    <div class="custom-control custom-checkbox mt-5">
                        <input type="checkbox" class="custom-control-input" id="waiveMedicalCoverage">
                        <label class="custom-control-label" for="waiveMedicalCoverage"><b>Waive Medical Coverage</b></label>
                    </div>
                    <p>I understand that if I elect to waive medical coverage for myself, my spouse , and/or my dependents, I will not be eligible to enroll until the next open enrollment perior unless there is a change in my family status. </p>
                    <hr>
                    <table class="table card-table border-bottom-0" id="cc-table-data-form-sample">
                        <tbody class="card-table-no-headers">
                        <tr>
                            <th scope="col" width="5%" class="text-center">Select</th>
                            <th scope="col" width="25%">Plan Name</th>
                            <th scope="col" width="15%">Employee</th>
                            <th scope="col" width="15%">+ Spouse</th>
                            <th scope="col" width="15%" class="">+ Dependents</th>
                            <th scope="col" width="25%">Total Cast</th>
                        </tr>
                        <tr class="text-center bg-light">
                            <th colspan="6"><strong>BRONZE TIER</strong></th>
                        </tr>
                        <tr>
                            <td class="d-flex">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="selectPlan1" name="selectPlan" class="js-row-highlight custom-control-input">
                                    <label class="custom-control-label" for="selectPlan1"></label>
                                </div>
                            </td>
                            <td><a href="#">Kaiser Permanente HMO A</a></td>
                            <td>$202.10</td>
                            <td>$0</td>
                            <td class="text-center">$0</td>
                            <td>$202.10</td>
                        </tr>
                        <tr>
                            <td class="d-flex">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="selectPlan2" name="selectPlan" class="js-row-highlight custom-control-input">
                                    <label class="custom-control-label" for="selectPlan2"></label>
                                </div>
                            </td>
                            <td><a href="#">UnitedHealthcare HMO B</a></td>
                            <td>$227.10</td>
                            <td>$0</td>
                            <td class="text-center">$0</td>
                            <td>$227.10</td>
                        </tr>
                        <tr>
                            <td class="d-flex">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="selectPlan3" name="selectPlan" class="js-row-highlight custom-control-input">
                                    <label class="custom-control-label" for="selectPlan3"></label>
                                </div>
                            </td>
                            <td><a href="#">Kaiser Permanente HMO C</a></td>
                            <td>$295.64</td>
                            <td>$0</td>
                            <td class="text-center">$0</td>
                            <td>$295.64</td>
                        </tr>
                        <tr>
                            <td class="d-flex">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="selectPlan4" name="selectPlan" class="js-row-highlight custom-control-input">
                                    <label class="custom-control-label" for="selectPlan4"></label>
                                </div>
                            </td>
                            <td><a href="#">Health Net HSP A</a></td>
                            <td>$347.50</td>
                            <td>$0</td>
                            <td class="text-center">
                                $0
                            </td>
                            <td>$347.50</td>
                        </tr>
                        <tr>
                            <td class="d-flex">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="selectPlan5" name="selectPlan" class="js-row-highlight custom-control-input">
                                    <label class="custom-control-label" for="selectPlan5"></label>
                                </div>
                            </td>
                            <td><a href="#">Anthem Blue Cross EPO A</a></td>
                            <td>$380.75</td>
                            <td>$0</td>
                            <td class="text-center">
                                $0
                            </td>
                            <td class="cc-tag-td">
                                <span class="tag">Employer contribution basis plan</span>
                                $380.75
                            </td>
                        </tr>
                        <tr>
                            <td class="d-flex">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="selectPlan6" name="selectPlan" class="js-row-highlight custom-control-input">
                                    <label class="custom-control-label" for="selectPlan6"></label>
                                </div>
                            </td>
                            <td><a href="#">Oscar EPO A</a></td>
                            <td>$380.72</td>
                            <td>$0</td>
                            <td class="text-center">
                                $0
                            </td>
                            <td>$380.72</td>
                        </tr>

                        <tr class="text-center bg-light">
                            <th colspan="6"><strong>SILVER TIER</strong></th>
                        </tr>

                        <tr>
                            <td class="d-flex">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="selectPlan7" name="selectPlan" class="js-row-highlight custom-control-input">
                                    <label class="custom-control-label" for="selectPlan7"></label>
                                </div>
                            </td>
                            <td><a href="#">Kaiser Permanente HMO A</a></td>
                            <td>$202.10</td>
                            <td>$0</td>
                            <td class="text-center">
                                $0
                            </td>
                            <td class="cc-tag-td">
                                <span class="tag">Lowest cast plan</span>
                                $202.10
                            </td>
                        </tr>
                        <tr>
                            <td class="d-flex">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="selectPlan8" name="selectPlan" class="js-row-highlight custom-control-input">
                                    <label class="custom-control-label" for="selectPlan8"></label>
                                </div>
                            </td>
                            <td><a href="#">UnitedHealthcare HMO B</a></td>
                            <td>$227.10</td>
                            <td>$0</td>
                            <td class="text-center">
                                $0
                            </td>
                            <td>$227.10</td>
                        </tr>
                        <tr>
                            <td class="d-flex">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="selectPlan9" name="selectPlan" class="js-row-highlight custom-control-input">
                                    <label class="custom-control-label" for="selectPlan9"></label>
                                </div>
                            </td>
                            <td><a href="#">Kaiser Permanente HMO C</a></td>
                            <td>$295.64</td>
                            <td>$0</td>
                            <td class="text-center">
                                $0
                            </td>
                            <td>$295.64</td>
                        </tr>
                        <tr>
                            <td class="d-flex">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="selectPlan10" name="selectPlan" class="js-row-highlight custom-control-input">
                                    <label class="custom-control-label" for="selectPlan10"></label>
                                </div>
                            </td>
                            <td><a href="#">Health Net HSP A</a></td>
                            <td>$347.50</td>
                            <td>$0</td>
                            <td class="text-center">
                                $0
                            </td>
                            <td>$347.50</td>
                        </tr>
                        <tr>
                            <td class="d-flex">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="selectPlan11" name="selectPlan" class="js-row-highlight custom-control-input">
                                    <label class="custom-control-label" for="selectPlan11"></label>
                                </div>
                            </td>
                            <td><a href="#">Anthem Blue Cross EPO A</a></td>
                            <td>$380.75</td>
                            <td>$0</td>
                            <td class="text-center">
                                $0
                            </td>
                            <td>$380.75</td>
                        </tr>
                        <tr>
                            <td class="d-flex">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="selectPlan12" name="selectPlan" class="js-row-highlight custom-control-input">
                                    <label class="custom-control-label" for="selectPlan12"></label>
                                </div>
                            </td>
                            <td><a href="#">Oscar EPO A</a></td>
                            <td>$380.72</td>
                            <td>$0</td>
                            <td class="text-center">
                                $0
                            </td>
                            <td>$380.72</td>
                        </tr>
                        </tbody>
                    </table>
                </div><!-- cc-card large -->
            </div>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Back</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Save & Exit</a>
                    <button class="btn">Compare Plans</button>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>