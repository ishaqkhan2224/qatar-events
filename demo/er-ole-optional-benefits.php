<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer OLE</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Online Enrollment</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="er-ole-overview">Overview</a>
            <a class="nav-link" href="er-ole-application">Application</a>
            <a class="nav-link" href="er-ole-census">Census</a>
            <a class="nav-link" href="er-ole-metaltier">Metal Tier</a>
            <a class="nav-link" href="er-ole-contribution">Contribution</a>
            <a class="nav-link active" href="er-ole-optional-benefits">Optional Benefits</a>
            <a class="nav-link" href="er-ole-summary">Summary</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body" class="optional-benefits-body">
        <div class="container">
            <p>Your quote already includes medical benefits,You can add any additional product lines form our optional Selection to your quote.</p>
            <div class="d-flex justify-content-between">
                <h5>Optional Selections</h5>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="optionalBenefitsSelectAll">
                    <label class="custom-control-label" for="optionalBenefitsSelectAll">Select All</label>
                </div>
            </div>
            <section class="row cc-optional-benefits mt-2">
                <div class="col-12">
                    <div class="cc-card large">
                        <div class="cc-flex-row">
                            <div class="cc-flex-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="dentalBenefitsCheckbox">
                                    <label class="custom-control-label" for="dentalBenefitsCheckbox">Dental Benefits</label>
                                </div>
                                <p>There are two ways to offer employees dental: Employer Sponsored or Voluntary.</p>
                            </div>
                            <div class="cc-flex-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="visionBenefitsCheckbox">
                                    <label class="custom-control-label" for="visionBenefitsCheckbox">Vision Benefits</label>
                                </div>
                                <p>Medical enrolled members and their dependents may enroll in Voluntary Vision plans through EyeMed and VSP provided by Ameritas.</p>
                            </div>
                            <div class="cc-flex-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="chiroBenefitsCheckbox">
                                    <label class="custom-control-label" for="chiroBenefitsCheckbox">Chiro Benefits</label>
                                </div>
                                <p>Landmark Healthplan Chiropractic and Acupuncture benefits are Available to members for a low monthly employer paid premium and affordable copay. </p>
                            </div>
                            <div class="cc-flex-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="lifeBenefitsCheckbox">
                                    <label class="custom-control-label" for="lifeBenefitsCheckbox">Life Benefits</label>
                                </div>
                                <p>Employers may elect to provide optional life Insurance/AD&D coverage. Coverage begins at a $10,000 minimum life insurance amount and increases based on the number of eligible employees
                                    at initial enrollment.</p>
                            </div>
                            <div class="cc-flex-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="section125Checkbox">
                                    <label class="custom-control-label" for="section125Checkbox">Section 125</label>
                                </div>
                                <p>A Section 125 Premium Only Plan allows your employees to pay their share of insurance premiums on a pre-tax basis.</p>
                            </div>
                        </div>
                    </div><!-- cc-card -->
                </div>
            </section>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="javascript:;" class="btn-link">Cancel</a>
                </div>
                <div class="right">
                    <a href="javascript:;" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>