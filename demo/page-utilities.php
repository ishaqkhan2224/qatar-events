<!DOCTYPE html>
<html>
<head>
<?php include "inc/header.meta.php"; ?>
<title>CC UI KIT: Utilities</title>
<?php include "inc/header.link.php"; ?>
</head>
<body>
<section id="cc-body">
  <div class="container">

    <h1>Badges</h1>
    <hr class="mb-5">

    <div class="row">
      <div class="col-5">
        <div class="cc-card-wrapper">
          <div class="cc-card large">
            <h1>H1 Headline <span class="badge cc-success">New</span></h1>
            <h2>H2 Headline <span class="badge cc-error">New</span></h2>
            <h3>H3 Headline <span class="badge cc-warning">New</span></h3>
            <h4>H4 Headline <span class="badge cc-info">New</span></h4>
            <hr>
            <p>Paragraph<span class="badge cc-success"><i class="far fa-check" aria-hidden="true"></i> Approved</span></p>
            <p>Paragraph<span class="badge cc-error"><i class="far fa-times"></i></i> Denied</span></p>
            <p>Paragraph<span class="badge cc-warning"><i class="far fa-clock"></i></i></i> In Process</span></p>
            <p>Paragraph<span class="badge cc-info"><i class="far fa-info-circle"></i></i> New</span></p>
          </div>
        </div>
        
      </div><!-- col-12 -->
    </div><!-- row -->    

    <h1 class="mt-5">Alerts</h1>
    <hr class="mb-5">

    <div class="row">
      <div class="col-12">
        
        <div class="alert cc-success" role="alert">.cc-success</div>
        <div class="alert cc-warning" role="alert">.cc-warning</div>
        <div class="alert cc-error" role="alert">.cc-error</div>
        <div class="alert cc-info" role="alert">.cc-info</div>
        <div class="alert alert-secondary" role="alert">.alert-secondary</div>
        <div class="alert alert-light" role="alert">.alert-light</div>
        <div class="alert alert-dark" role="alert">.alert-dark</div>
        
      </div><!-- col-x -->
    </div><!-- row -->    


    <h1 class="mt-5">Lists</h1>
    <hr class="mb-5">

    <div class="row">
      <div class="col-12">
        
        <ul class="cc-ul">
          <li>Lorem ipsum dolor sit amet</li>
          <li>Lorem ipsum dolor sit amet</li>
          <li>Lorem ipsum dolor sit amet</li>
          <li>Lorem ipsum dolor sit amet</li>
        </ul>
        <hr class="break">
        <ul class="cc-ul">
          <li class="plain">Lorem ipsum dolor sit amet</li>
          <li class="plain">Lorem ipsum dolor sit amet</li>
          <li class="plain">Lorem ipsum dolor sit amet</li>
          <li class="plain">Lorem ipsum dolor sit amet</li>
        </ul>
        <hr class="break">
        <ol class="cc-ol">
          <li>Lorem ipsum dolor sit amet</li>
          <li>Lorem ipsum dolor sit amet</li>
          <li>Lorem ipsum dolor sit amet</li>
          <li>Lorem ipsum dolor sit amet</li>
        </ol>
      </div><!-- col-x -->
    </div><!-- row -->   
    
  </div><!-- container -->
</section><!-- cc-body -->
<?php include "common/footer.php"; ?>
</body>
</html>