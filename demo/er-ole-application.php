<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer OLE</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Online Enrollment</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="er-ole-overview">Overview</a>
            <a class="nav-link active" href="er-ole-application">Application</a>
            <a class="nav-link" href="er-ole-census">Census</a>
            <a class="nav-link" href="er-ole-metaltier">Metal Tier</a>
            <a class="nav-link" href="er-ole-contribution">Contribution</a>
            <a class="nav-link" href="er-ole-optional-benefits">Optional Benefits</a>
            <a class="nav-link" href="er-ole-summary">Summary</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <div class="cc-form__box cc-form_employ-application">
                <div class="row">
                    <div class="col-md-6">
                        <div class="employer-information mb-5">
                            <h5 class="mb-3">Employer Information</h5>
                            <div class="form-group">
                                <label for="company_name">Legal Company Name</label>
                                <input type="text" class="form-control" id="company_name" name="company_name" placeholder="" disabled/>
                            </div>
                            <div class="form-group">
                                <label for="dba_name">DBA (Doing Business As)</label>
                                <input type="text" class="form-control" id="dba_name" name="dba_name" placeholder="" required/>
                            </div>
                            <div class="form-group">
                                <label for="owner_name">Owner/President Name</label>
                                <input type="text" class="form-control" id="owner_name" name="owner_name" placeholder="" required/>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="business_start_date">Date Business Started</label>
                                    <div class="d-flex align-items-center">
                                        <input type="text" class="form-control date-field mr-2" placeholder="MM/DD/YYYY" id="business_start_date" required/>
                                        <i class="fa fa-calendar-alt" id="daterange"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="company_structure">Company Structure</label>
                                <select class="form-control" id="company_structure" name="company_structure">
                                    <option selected>Other</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="other_company_name">Other Company Structure</label>
                                <input type="text" class="form-control" id="other_company_name" name="other_company_name" required/>
                            </div>
                        </div>
                        <div class="employ-location mb-5">
                            <h5 class="mb-3">Location</h5>
                            <div class="form-group row">
                                <div class="col-sm-3 pr-0"><span class="m-0"><strong>Mailing Address</strong></span></div>
                                <div class="col-sm-9 p-0">
                                    <div class="custom-control custom-checkbox">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="" name="">
                                            <label class="custom-control-label" for="">Residence Address</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-9">
                                    <label for="mailing_street_address">Street Address</label>
                                    <input type="text" class="form-control" id="mailing_street_address" name="mailing_street_address" placeholder="1234 Main St" required/>
                                </div>
                                <div class="col-md-3 pl-0">
                                    <label for="mailing_unit_address">SUITE/UNIT</label>
                                    <input type="text" class="form-control" id="mailing_unit_address" name="mailing_unit_address"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mailing_zip_code">Zip Code</label>
                                <input type="text" class="form-control" id="mailing_zip_code" name="mailing_zip_code" placeholder="87698" disabled/>
                            </div>
                            <div class="form-group">
                                <label for="mailing_city_name">City</label>
                                <select class="form-control" id="mailing_city_name" name="mailing_city_name" disabled>
                                    <option selected>Los Anglas</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-9">
                                    <label for="country_name">Country</label>
                                    <select class="form-control" id="country_name" name="country_name">
                                        <option selected>Los Angeles</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div class="col-md-3 pl-0">
                                    <label for="state_name">STATE</label>
                                    <select class="form-control" id="state_name" name="state_name" disabled>
                                        <option selected>CA</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox p-0">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="" name="">
                                        <label class="custom-control-label" for="">Billing Address is different from above</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mt-5">
                                <div class="col-sm-3 pr-0"><span class="m-0 cc-font-normal"><strong>Billing Address</strong></span></div>
                                <div class="col-sm-9 p-0">
                                    <div class="custom-control custom-checkbox">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="" name="">
                                            <label class="custom-control-label" for="">Residence Address</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-9">
                                    <label for="billing_street_address">Street Address</label>
                                    <input type="text" class="form-control" id="billing_street_address" name="billing_street_address" required/>
                                </div>
                                <div class="col-md-3 pl-0">
                                    <label for="billing_unit_address">SUITE/UNIT</label>
                                    <input type="text" class="form-control" id="billing_unit_address" name="billing_unit_address"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="billing_zip_code">Zip Code</label>
                                <input type="text" class="form-control" id="billing_zip_code" name="billing_zip_code" placeholder="87698" required/>
                            </div>
                            <div class="form-group">
                                <label for="billing_city_name">City</label>
                                <select class="form-control" id="billing_city_name" name="billing_city_name">
                                    <option selected>Select</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-8">
                                    <label for="billing_country_name">Country</label>
                                    <select class="form-control" id="billing_country_name" name="billing_country_name">
                                        <option selected>Select</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div class="col-md-4 pl-0">
                                    <label for="billing_state_name">State</label>
                                    <select class="form-control" id="billing_state_name" name="billing_state_name">
                                        <option selected>Select</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="workers-comp mb-5">
                            <h5 class="mb-3">Workers Comp</h5>
                            <div class="form-group">
                                <label for="worker_carrier_name">Workers Comp Carrier Name</label>
                                <input type="text" class="form-control" id="worker_carrier_name" name="worker_carrier_name" required>
                                <small id="passwordHelpBlock" class="form-text text-muted">
                                    not Broker or Agency namae
                                </small>
                            </div>
                            <div class="form-group">
                                <label for="worker_policy_no">Workers Comp Policy No</label>
                                <input type="text" class="form-control" id="worker_policy_no" name="worker_policy_no" required>
                            </div>
                            <div class="form-group">
                                <label for="future_renewal_date">Future Renewal Date</label>
                                <div class="d-flex align-items-center">
                                    <input type="text" class="form-control date-field mr-2" placeholder="MM/DD/YYYY" id="business_start_date" required/>
                                    <i class="fa fa-calendar-alt" id="daterange"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <p><strong>Note:</strong> Worker's Compensation Coverage must be effective on or prior to the effective date requested with CaliforniaChoice. We are not covered by Worker's Compensation coverage due to legal exemption under the following checked condition:</p>
                        <div class="form-group mb-5">
                            <div class="custom-control custom-checkbox p-0">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="" name="">
                                    <label class="custom-control-label" for="">100% family-related running business out of home (does not include domestic partner, family members must be same residence)</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="employ_name">Name</label>
                            <input type="text" class="form-control" id="employ_name">
                        </div>
                        <div class="form-group">
                            <label for="employ_job_title">Job Title</label>
                            <input type="text" class="form-control" id="employ_job_title">
                        </div>
                        <div class="form-group">
                            <label for="employ_email">Email</label>
                            <input type="email" class="form-control" id="employ_email">
                        </div>
                        <div class="form-group">
                            <label for="employ_phone_no">Phone</label>
                            <input type="number" class="form-control" id="employ_phone_no">
                        </div>
                        <div class="form-group">
                            <label for="employ_fax_no">Fax</label>
                            <input type="number" class="form-control" id="employ_fax_no">
                        </div>
                        <div class="form-group mb-5">
                            <div class="custom-control custom-checkbox p-0">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="" name="">
                                    <label class="custom-control-label" for="">Add Broker of Record as an Authorized Group Contact</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <h5 class="mb-3">Enrollment and eligibility Information</h5>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <p>Total number COBRA eligible applying?</p>
                            </div>
                            <div class="col-md-2">
                                <input class="form-control" type="number" id="total_eligible_applying" name="total_eligible_applying">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <p>Have your employed 20 or more employees during at least 50% of the preceding calendar year? <br> For more Information on Cobra please <a href="#">click here</a>.</p>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" id="select_year" name="select_year">
                                    <option selected>Select</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <p>Do you want your future COBRA participants on your bill? <br> Otherwise your COBRA participants will be billed directly by CONEXIS</p>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" id="feature_participants" name="feature_participants">
                                    <option selected>Select</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <p>Click the link to download and print the form. <a href="#">Group COBRA Direct Billing Contract</a> (only applicable for groups subjects to federal COBRA)</p>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <p>Have you employed 20 or more employees for 20 or more weeks during the current or preceding year?<br>(TEFRA)</p>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" id="more_employees" name="more_employees">
                                    <option selected>Select</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <p>Does your group currently have group medical coverage?</p>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" id="medical_coverage" name="medical_coverage">
                                    <option selected>Select</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label for="carrier_name">Carrier Name</label>
                                <input type="text" class="form-control" id="carrier_name" name="carrier_name">
                            </div>
                            <div class="col-md-2 pl-0">
                                <label for="policy_number">Policy Number</label>
                                <input type="text" class="form-control" id="policy_number" name="policy_number">
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-10">
                                        <label for="termination_date">Termination Date</label>
                                        <div class="d-flex">
                                            <input type="text" class="form-control date-field mr-2" placeholder="MM/DD/YYYY" id="termination_date" required/>
                                            <i class="fa fa-calendar-alt" id="daterange"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <p>Eligible employees must work the following number of hours to qualify:</p>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" id="total_hours" name="total_hours">
                                    <option selected>Select</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <p>Waiting period for future employees is first day of the month following:</p>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" id="waiting_period" name="waiting_period">
                                    <option selected>Select</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <p>Waiting period applies to</p>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" id="applies_waiting_period" name="applies_waiting_period">
                                    <option selected>Select</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <p>Number in waiting period:</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="number_in_waiting_period" name="number_in_waiting_period">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <p>Total number of employees on payroll regardless of hours worked (including owners, seasonal....):</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="payroll_employ" name="payroll_employ">
                            </div>
                        </div>
                        <hr>
                        <p>Total number of ineligible employees in each of the following categories</p>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <label for="employ_union_category">Union</label>
                                <input type="text" class="form-control" id="employ_union_category">
                            </div>
                            <div class="col-md-2">
                                <label for="employ_part_time_category">Part-Time</label>
                                <input type="text" class="form-control" id="employ_part_time_category">
                            </div>
                            <div class="col-md-2">
                                <label for="employ_seasonal_category">Seasonal</label>
                                <input type="text" class="form-control" id="employ_seasonal_category">
                            </div>
                            <div class="col-md-2">
                                <label for="employ_temp_category">Temp</label>
                                <input type="text" class="form-control" id="employ_temp_category">
                            </div>
                            <div class="col-md-2">
                                <label for="employ_terminated_category">Terminated</label>
                                <input type="text" class="form-control" id="employ_terminated_category">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <p>How many of the employees (including owners) enrolling are related by blood or marriage?</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="related_employees" name="related_employees">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Cancel</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>