<!DOCTYPE html>
<html>
<head>
<?php include "inc/header.meta.php"; ?>
<title>CC UI KIT: Page Layout > Table</title>
<?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<?php include "common/subheader-tabs.php"; ?>

<section id="cc-body">
    <div class="container">
        <?php 
        //include "inc/tables.basic.php"
        include "inc/tables.datatable-search.php"
        //include "inc/tables.form.profile.php"
        ?>
    </div><!-- container -->
</section>

<?php include "common/footer.php"; ?>
</body>
</html>