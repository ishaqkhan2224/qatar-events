<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Online Enrollment Management</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Online Enrollment Management</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. corporis, sunt distinctio</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="#" class="btn primary">Start an online enrollment</a>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link active" href="#">Active</a>
            <a class="nav-link" href="#">Expired</a>
            <a class="nav-link" href="#">Canceled</a>
        </nav>
    </div>
</section>

<section id="cc-body">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div>
                    <h5 class="mb-4">Pied Piper - Quote # 6789012</h5>
                    <table class="table js-sortable-table-resend" id="cc-table-data">
                        <thead>
                        <tr>
                            <th scope="col" width="5%" class="no-sort">
                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="checkAll">
                                    <label class="custom-control-label" for="checkAll"></label>
                                </div>
                            </th>
                            <th scope="col" class="">Employee Name</th>
                            <th scope="col" class="no-sort">SSN</th>
                            <th scope="col" class="no-sort">Enrollment Status</th>
                            <th scope="col" class="no-sort">Enrollment Type</th>
                            <th scope="col" class="no-sort">Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="check1">
                                    <label class="custom-control-label" for="check1"></label>
                                </div>
                            </td>
                            <td class="">Bertram Gilfoye</td>
                            <td>xxx-x-xx-123</td>
                            <td>Not Started</td>
                            <td>Online</td>
                            <td>
                                <div class="form-group mb-0">
                                  <input type="text" class="form-control" id="basic_input_field" name="basic_input_field">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="check2">
                                    <label class="custom-control-label" for="check2"></label>
                                </div>
                            </td>
                            <td class="">Dinesh Chugtai</td>
                            <td>xxxx-xx-2345</td>
                            <td>Not Started</td>
                            <td>Online</td>
                            <td>
                                <div class="form-group mb-0">
                                  <input type="text" class="form-control" id="basic_input_field" name="basic_input_field">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="check3">
                                    <label class="custom-control-label" for="check3"></label>
                                </div>
                            </td>
                            <td class="">Donald dunn</td>
                            <td>xxx-x-xx-3456</td>
                            <td>Not Started</td>
                            <td>Online</td>
                            <td>
                                <div class="form-group mb-0">
                                  <input type="text" class="form-control" id="basic_input_field" name="basic_input_field">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="check4">
                                    <label class="custom-control-label" for="check4"></label>
                                </div>
                            </td>
                            <td class="">Erlich Bachman</td>
                            <td>xxx-x-xx-5678</td>
                            <td>Not Started</td>
                            <td>Online</td>
                            <td>
                                <div class="form-group mb-0">
                                  <input type="text" class="form-control" id="basic_input_field" name="basic_input_field">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="check5">
                                    <label class="custom-control-label" for="check5"></label>
                                </div>
                            </td>
                            <td class="">Gavin Belson</td>
                            <td>xxx-x-xx-6789</td>
                            <td>Not Started</td>
                            <td>Online</td>
                            <td>
                                <div class="form-group mb-0">
                                  <input type="text" class="form-control" id="basic_input_field" name="basic_input_field">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="check6">
                                    <label class="custom-control-label" for="check6"></label>
                                </div>
                            </td>
                            <td class="">Jian Yang</td>
                            <td>xxx-x-xx-7890</td>
                            <td>Not Started</td>
                            <td>Online</td>
                            <td>
                                <div class="form-group mb-0">
                                  <input type="text" class="form-control" id="basic_input_field" name="basic_input_field">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="check7">
                                    <label class="custom-control-label" for="check7"></label>
                                </div>
                            </td>
                            <td class="">laurie Bream</td>
                            <td>xxx-x-xx-8901</td>
                            <td>Not Started</td>
                            <td>Online</td>
                            <td>
                                <div class="form-group mb-0">
                                  <input type="text" class="form-control" id="basic_input_field" name="basic_input_field">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="check8">
                                    <label class="custom-control-label" for="check8"></label>
                                </div>
                            </td>
                            <td class="">Monica Hall</td>
                            <td>xxx-x-xx-9012</td>
                            <td>Not Started</td>
                            <td>Online</td>
                            <td>
                                <div class="form-group mb-0">
                                  <input type="text" class="form-control" id="basic_input_field" name="basic_input_field">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="check9">
                                    <label class="custom-control-label" for="check9"></label>
                                </div>
                            </td>
                            <td class="">Nelson Bighetti</td>
                            <td>xxx-x-xx-0123</td>
                            <td>Not Started</td>
                            <td>Online</td>
                            <td>
                                <div class="form-group mb-0">
                                  <input type="text" class="form-control" id="basic_input_field" name="basic_input_field">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="check10">
                                    <label class="custom-control-label" for="check10"></label>
                                </div>
                            </td>
                            <td class="">Rlchard Hendricks </td>
                            <td>xxx-x-xx-1234</td>
                            <td>Not Started</td>
                            <td>Online</td>
                            <td>
                                <div class="form-group mb-0">
                                  <input type="text" class="form-control" id="basic_input_field" name="basic_input_field">
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div><!-- container -->
</section>

<?php include "common/footer.php"; ?>
<script>
    $(document).ready(function () {
        $('.js-sortable-table-resend').DataTable({
            "paging": false,
            "ordering": true,
            "info": false,
            "order": [[ 1, "desc" ]],
            "dom": '<f<t>ilp>',
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
            }]
        });
    });

</script>
</body>
</html>