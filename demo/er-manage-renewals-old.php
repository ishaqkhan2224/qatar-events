<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer > Management</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Manage Employees</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="" class="btn btn-blue mr-3">Create a New Hire Quote</a>
                    <div class="dropdown dd-transparent">
                        <button class="btn secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Order ID Cards</a>
                            <a class="dropdown-item" href="#">View Invoices</a>
                            <a class="dropdown-item" href="#">Commissions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="javascript:;">Active Employees</a>
            <a class="nav-link" href="javascript:;">Recently Added</a>
            <a class="nav-link" href="javascript:;">COBRA</a>
            <a class="nav-link" href="javascript:;">Terminated</a>
            <a class="nav-link" href="javascript:;">Pending Requests</a>
            <a class="nav-link" href="javascript:;">Processed Requests</a>
            <a class="nav-link" href="er-manage-division-report-summary">Division Reports</a>
            <a class="nav-link active" href="er-manage-renewals">Renewals</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-division" method="post" action="">
    <section id="cc-body" class="cc-overview-intro">
        <div class="container">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscig elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
            <div class="row mt-5 align-items-center">
                <div class="col-md-9">
                    <div class="media mb-4">
                        <i class="fal fa-robot"></i>
                        <div class="media-body">
                            <h5 class="mb-0">Renewal - No Employer level changes</h5>
                        </div>
                    </div>
                    <p>If you are not making any employer level changes please distribute the Employee Renewal Worksheets to your employees so they may review their renewal enrollment options. The renewal includes your Renewal Premium Notification and metal tier(s)</p>
                </div>
                <div class="col-md-3 text-center">
                    <a href="#" class="btn btn-blue btn-block"><i class="fas fa-file-pdf mr-2 text-white"></i> View Renewal - 7/2019</a>
                        </div>
                    </div>
                    <hr>
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="media mb-4">
                        <i class="fal fa-users"></i>
                        <div class="media-body">
                            <h5 class="mb-0">Need to make changes?</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-9">
                    <h6>For Employer Changes</h6>
                    <p>Create divisions for locations or categories (i.e., San Jose office, Non-exempt employees) and update or delete divisions as needed.</p>
                    <p>Contact your broker for revised Employee Renewal Enrollment Worksheets</p>
                    <ul>
                        <li>To make an employer level change(i.e. add a line of coverage, update your employer contribution,change tiers</li>
                        <li>If you have employees who are adding dependents</li>
                        <li>Or to obtain Employee Renewal Worksheets for employee who are not on your renewal census</li>
                    </ul>
                    <p>To make your employer level changes effective, complete, sign and submit the Employer Change Request form to CaliforniaChoice</p>
                </div>
                <div class="col-md-3 text-center">
                    <a href="#"><i class="fas fa-file-pdf"></i> Employer Changes Request</a>
                </div>
            </div>
            <div class="row mt-4 align-items-center">
                <div class="col-md-9">
                    <h6>For Employee Changes</h6>
                    <p>Currently enrolled members who want to make changes to their coverage or personal information can login to <a href="#">www.calchoice.com</a>  to access the Employee Change request for. The employee then clicks "Open Enrollment" to complete, print, and sign the for,</p>
                </div>
                <div class="col-md-3 text-center">
                    <a href="#"><i class="fas fa-file-pdf"></i> Employee Changes Request</a>
                        </div>
                    </div>
                    <hr>
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="media mb-4">
                        <i class="fal fa-file-alt"></i>
                        <div class="media-body">
                            <h6 class="mb-0">Add Employees Without Coverage?</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-9">
                    <h6>For Paper Access</h6>
                    <p>Contact your broker and request Employee Renewal Worksheets for employees who do nto have any coverage through CaliforniaChoice.Distribute the Employee Renewal Enrollment Worksheets so they can review their renewal enrollment options.</p>
                </div>
                <div class="col-md-3">
                    <a href="#"><i class="fas fa-file-pdf"></i> Employee Enrollment Application</a>
                </div>
            </div>
            <div class="row mt-4 align-items-center">
                <div class="col-md-8">
                    <h6>For Online Access</h6>
                    <p>Give your employees online access to the for by entering their email address.</p>
                </div>
                <div class="col-md-4 text-right">
                    <a href="#">Online Employee Enrollment Application</a>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>