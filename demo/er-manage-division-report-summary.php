<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer > Management</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="left">
                    <h2>Manage Employees</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cc-subheader__menu">
                    <a href="#" class="btn btn-blue mr-3">Create a new hire quote</a>
                    <div class="dropdown dd-transparent">
                        <button class="btn btn-grey-outline dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Order ID Cards</a>
                            <a class="dropdown-item" href="#">View Invoices</a>
                            <a class="dropdown-item" href="#">Commissions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="javascript:;">Active Employees</a>
            <a class="nav-link" href="javascript:;">Recently Added</a>
            <a class="nav-link" href="javascript:;">COBRA</a>
            <a class="nav-link" href="javascript:;">Terminated</a>
            <a class="nav-link" href="javascript:;">Pending Requests</a>
            <a class="nav-link" href="javascript:;">Processed Requests</a>
            <a class="nav-link active" href="er-manage-division-report-summary">Division Reports</a>
            <a class="nav-link" href="er-manage-renewals">Renewals</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <p>Select division and premium month to generate a report. Click Edit Report to change division or employee assignment.</p>

            <div class="form-row align-items-end">
                <div class="form-group col-3">
                    <label class="font-weight-normal">Division</label>
                    <select class="form-control">
                        <option value="">All Divisions</option>
                        <option value="">B</option>
                        <option value="">C</option>
                    </select>
                </div>
                <div class="form-group col-3">
                    <label class="font-weight-normal">Premium Month</label>
                    <select class="form-control">
                        <option value="">September 2019</option>
                        <option value="">B</option>
                        <option value="">C</option>
                    </select>
                </div>
                <div class="col-2 mb-3">
                    <button class="btn btn-blue">Generate Report</button>
                </div>
                <div class="col-4 mb-3 text-right">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-grey-outline"><i class="fal fa-users mr-2"></i>Assign Employees</button>
                        <button type="button" class="btn btn-grey-outline"><i class="fal fa-edit mr-2"></i>Edit Division</button>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row mt-5">
                <div class="col-12">
                    <div class="cc-card-wrapper">
                        <h3>Midwest</h3>
                        <div class="cc-card large">
                            <table class="table card-table mb-0">
                                <thead>
                                    <tr>
                                        <th width="25%"></th>
                                        <th># OF EMPLOYEES</th>
                                        <th class="text-right">EMPLOYER CONTRIBUTION</th>
                                        <th class="text-right">EMPLOYEE CONTRIBUTION</th>
                                        <th class="text-right">TOTAL PREMIUM</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Monthly Total</td>
                                        <td>2</td>
                                        <td class="text-right">$1,710.76</td>
                                        <td class="text-right">%17.28</td>
                                        <td class="text-right">$1,728.04</td>
                                    </tr>
                                    <tr>
                                        <td>Adjustment Total</td>
                                        <td></td>
                                        <td class="text-right">$200.00</td>
                                        <td class="text-right">$2.72</td>
                                        <td class="text-right">$1,930.76</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- cc-card large -->
                    </div>
                </div>
            </div><!-- row end -->
            <div class="row mt-5">
                <div class="col-12">
                    <div class="cc-card-wrapper">
                        <h3>Unassigned</h3>
                        <div class="cc-card large">
                            <table class="table card-table mb-0">
                                <thead>
                                    <tr>
                                        <th width="25%"></th>
                                        <th># OF EMPLOYEES</th>
                                        <th class="text-right">EMPLOYER CONTRIBUTION</th>
                                        <th class="text-right">EMPLOYEE CONTRIBUTION</th>
                                        <th class="text-right">TOTAL PREMIUM</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Monthly Total</td>
                                        <td>0</td>
                                        <td class="text-right">$0</td>
                                        <td class="text-right">%0</td>
                                        <td class="text-right">$0</td>
                                    </tr>
                                    <tr>
                                        <td>Adjustment Total</td>
                                        <td></td>
                                        <td class="text-right">$0</td>
                                        <td class="text-right">$0</td>
                                        <td class="text-right">$0</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- cc-card large -->
                    </div>
                </div>
            </div><!-- row end -->
            <div class="row mt-5">
                <div class="col-12">
                    <div class="cc-card-wrapper">
                        <h3>Total by Division</h3>
                        <div class="cc-card">
                            <table class="table card-table mb-0">
                                <thead>
                                    <tr>
                                        <th width="60%"></th>
                                        <th># OF EMPLOYEES</th>
                                        <th class="text-right">TOTAL PREMIUM</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Midwest</td>
                                        <td>2</td>
                                        <td class="text-right">$1,728.04</td>
                                    </tr>
                                    <tr>
                                        <td>Northwest</td>
                                        <td>3</td>
                                        <td class="text-right">$2,592.06</td>
                                    </tr>
                                    <tr>
                                        <td>South</td>
                                        <td>1</td>
                                        <td class="text-right">$864.02</td>
                                    </tr>
                                    <tr>
                                        <td>West</td>
                                        <td>4</td>
                                        <td class="text-right">$3,456.08</td>
                                    </tr>
                                    <tr>
                                        <td>Unassigned</td>
                                        <td>0</td>
                                        <td class="text-right">$0</td>
                                    </tr>
                                    <tr>
                                        <td>Total Adjustments</td>
                                        <td></td>
                                        <td class="text-right">$304.08</td>
                                    </tr>
                                    <tr>
                                        <td>Division Total</td>
                                        <td>10</td>
                                        <td class="text-right">$9,248.36</td>
                                    </tr>
                                    <tr>
                                        <td>Fees</td>
                                        <td></td>
                                        <td class="text-right">$300.00</td>
                                    </tr>
                                    <tr>
                                        <td><b>Company Total</b></td>
                                        <td></td>
                                        <td class="text-right"><b>$12,248.36</b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- cc-card large -->
                    </div>
                </div>
            </div><!-- row end -->

            <p class="mt-4"><b>Note:</b> This report is intended to give you a breakdown of your monthly charges by division and does not reflect received. Please refer to your invoice statement to make your monthly premium payments.</p>
            <p>Client#: 41130</p>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Cancel</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>