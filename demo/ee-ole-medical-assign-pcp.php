<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employee OLE</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Online Enrollment</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="ee-ole-overview">Overview</a>
            <a class="nav-link" href="ee-ole-your-info">Your Info</a>
            <a class="nav-link" href="ee-ole-dependents">Dependents</a>
            <a class="nav-link active" href="ee-ole-medical">Medical</a>
            <a class="nav-link" href="ee-ole-dental">Dental</a>
            <a class="nav-link" href="ee-ole-chiro">Chiro</a>
            <a class="nav-link" href="ee-ole-vision">Vision</a>
            <a class="nav-link" href="ee-ole-life">Life</a>
            <a class="nav-link" href="ee-ole-section-125">Section 125</a>
            <a class="nav-link" href="ee-ole-summary">Summary</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <h5>Request Primary Care Physician</h5>
            <p>Select the Primary Care Physician (PCP) for yourself and your dependent</p>
            <div class="row">
                <div class="col-md-9">
                    <div class="cc-card">
                        <table class="table card-table">
                            <thead>
                            <tr>
                                <th width="25%" scope="col">Name</th>
                                <th width="25%" scope="col">PCP</th>
                                <th width="15%" scope="col" class="">PCP ID</th>
                                <th width="25%" scope="col" class="">PCP City</th>
                                <th width="10%" scope="col" class="">Current Patient</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <p class="mb-2">Amy Hendrick</p>
                                    <div class="form-group mb-0">
                                        <div class="custom-control custom-checkbox p-0">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="assignToMe1">
                                                <label class="custom-control-label" for="assignToMe1">Assign a PCP</label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td><input type="text" class="form-control"></td>
                                <td><input type="text" class="form-control"></td>
                                <td><input type="text" class="form-control"></td>
                                <td class="text-center">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="xxx" name="xxx" required><label class="custom-control-label" for="xxx"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="mb-2">Richard W. Hendrick Sr.</p>
                                    <div class="form-group mb-0">
                                        <div class="custom-control custom-checkbox p-0">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="assignToMe2">
                                                <label class="custom-control-label" for="assignToMe2">Assign a PCP</label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td><input type="text" class="form-control"></td>
                                <td><input type="text" class="form-control"></td>
                                <td><input type="text" class="form-control"></td>
                                <td class="text-center">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="xxx" name="xxx" required><label class="custom-control-label" for="xxx"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="mb-2">Richard W. Hendrick Sr.</p>
                                    <div class="form-group mb-0">
                                        <div class="custom-control custom-checkbox p-0">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="assignToMe3">
                                                <label class="custom-control-label" for="assignToMe3">Assign a PCP</label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td><input type="text" class="form-control"></td>
                                <td><input type="text" class="form-control"></td>
                                <td><input type="text" class="form-control"></td>
                                <td class="text-center">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="xxx" name="xxx" required><label class="custom-control-label" for="xxx"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="mb-2">Sarah Hendrick</p>
                                    <div class="form-group mb-0">
                                        <div class="custom-control custom-checkbox p-0">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="assignToMe4">
                                                <label class="custom-control-label" for="assignToMe4">Assign a PCP</label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td><input type="text" class="form-control"></td>
                                <td><input type="text" class="form-control"></td>
                                <td><input type="text" class="form-control"></td>
                                <td class="text-center">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="xxx" name="xxx" required><label class="custom-control-label" for="xxx"></label>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-3">
                    <h6>Need Help?</h6>
                    <p class="mt-4">Our <a href="#">Online Provider Search</a> To help locate a Primary Care Physician eligible in your selected plan.</p>
                    <p class="mt-4">Our <a href="#">RX Search tool</a> to find information on prescriptions available in your selected plan.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Back</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>