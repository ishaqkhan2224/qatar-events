<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer OLE</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Online Enrollment</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="er-ole-overview">Overview</a>
            <a class="nav-link" href="er-ole-application">Application</a>
            <a class="nav-link" href="er-ole-census">Census</a>
            <a class="nav-link" href="er-ole-metaltier">Metal Tier</a>
            <a class="nav-link" href="er-ole-contribution">Contribution</a>
            <a class="nav-link" href="er-ole-optional-benefits">Optional Benefits</a>
            <a class="nav-link active" href="er-ole-summary">Summary</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body" class="cc-er-ole-summary">
        <div class="container">
            <div class="cc-card-wrapper">
                <div class="d-flex justify-content-between align-items-end mb-2">
                   <h3 class="mb-0">Application Information</h3>
                   <button class="btn btn-grey-outline btn-sm ml-3"><i class="far fa-edit mr-2"></i>Edit</button>
                </div>
                <div class="cc-card large">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="media">
                                <i class="fal fa-building mr-2"></i>
                                <div class="media-body border-bottom-0">
                                    <h6 class="mb-3">Employee Information</h6>
                                    <p><b>Legal Company Name</b><br>
                                        ACME
                                    </p>
                                    <p><b>Date Business Started</b><br>
                                        07/06/2001
                                    </p>
                                    <p><b>DBA (Doing Business As)</b><br>
                                        ACME
                                    </p>
                                    <p><b>Nature Of Business</b><br>
                                        Construction Sand and Gravel
                                    </p>
                                    <p><b>Owner/President Name</b><br>
                                        Donal Duck
                                    </p>
                                    <p><b>Company Structure</b><br>
                                        Lorem Ipsum
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="media">
                                <i class="fas fa-map-marker-alt mr-2"></i>
                                <div class="media-body border-bottom-0">
                                    <h6 class="mb-3">Location</h6>
                                    <p class="mb-0"><b>Address</b><br>
                                        1028 hillside rd.<br>
                                        New Port Beach, CA 91231
                                    </p>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="residenceAddressCheckbox" checked>
                                        <label class="custom-control-label" for="residenceAddressCheckbox">Residence address</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mt-2">
                                        <input type="checkbox" class="custom-control-input" id="billAddressCheckbox" checked>
                                        <label class="custom-control-label" for="billAddressCheckbox">Bill address is different</label>
                                    </div>
                                    <p><b>Billing Address</b><br>
                                        143919 Beach Blvd. #19 <br>
                                        New Port Beach, CA 91231
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="media">
                                <i class="fas fa-briefcase mr-2"></i>
                                <div class="media-body border-bottom-0">
                                    <h6 class="mb-3">Worker's Comp</h6>
                                    <p><b>Carrier Name</b><br>
                                        Lorem Ipsum
                                    </p>
                                    <p><b>Policy No.</b><br>
                                        987654
                                    </p>
                                    <p><b>Future Renewal Date</b><br>
                                        10/29/2020
                                    </p>
                                    <div class="custom-control custom-checkbox mt-2">
                                        <input type="checkbox" class="custom-control-input" id="familyRelatedCheckbox" checked>
                                        <label class="custom-control-label" for="familyRelatedCheckbox">100% family-related running business out of home (does not include domestic partners, family members must be same
                                            residence)</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="media">
                                <i class="far fa-address-book mr-2"></i>
                                <div class="media-body border-bottom-0">
                                    <h6 class="mb-3">Contact Person</h6>
                                    <p><b>Name</b><br>
                                        Mickey Mouse
                                    </p>
                                    <p><b>Job Title</b><br>
                                        Lorem Ipsum
                                    </p>
                                    <p><b>Email</b><br>
                                        heymickey@acme.com
                                    </p>
                                    <p><b>Phone</b><br>
                                        (626) 555-1234
                                    </p>
                                    <p><b>Fax</b><br>
                                        (626) 555-4546
                                    </p>
                                    <div class="custom-control custom-checkbox mt-2">
                                        <input type="checkbox" class="custom-control-input" id="brokerCheckbox" checked>
                                        <label class="custom-control-label" for="brokerCheckbox">Add Broker of Record as an Authorized Group Contact</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h3 class="mt-4">Enrollment and Eligibility Information</h3>
                    <div class="d-flex justify-content-between align-items-center">
                        <p>Total number COBRA eligibles applying?</p>
                        <p>3</p>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-between align-items-center">
                        <p>Have you employed 20 or more employees during at least 50% of the preceding calendar year?<br>For more information on COBRA please <a href="#">click here</a>.</p>
                        <p>Yes</p>
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <p>Do you want your future COBRA participants on your bill?<br>
                            Otherwise your COBRA participants will be billed directly by CONEXIS
                        </p>
                        <p>Yes</p>
                    </div>
                    <p>Click the link to download and print the form. <a href="#">Group COBRA Direct Billing Contract</a><br>
                        (Only applicable for groups subjects to federal COBRA)</p>
                    <hr>
                    <div class="d-flex justify-content-between align-items-center">
                        <p class="mb-0">Have you employed 20 or more employees for 20 or more weeks during the current or preceding year? (TEFRA)</p>
                        <p class="mb-0">Yes</p>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-between align-items-center">
                        <p class="mb-0">Does your group currently have group medical coverage?</p>
                        <p class="mb-0">No</p>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label>CARRIER NAME</label>
                            <input type="text" class="form-control" placeholder="Aetna" disabled>
                        </div>
                        <div class="form-group col-md-2">
                            <label>POLICY NUMBER</label>
                            <input type="text" class="form-control" placeholder="123123" disabled>
                        </div>
                        <div class="form-group col-md-2">
                            <label>TERMINATION DATE</label>
                            <input type="text" class="form-control" placeholder="10/12/2019" disabled>
                        </div>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-between align-items-center">
                        <p class="mb-0">Eligible employees must work the following number of hours to qualify</p>
                        <p class="mb-0">20+ hr</p>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-between align-items-center">
                        <p>Waiting Period for future employees is first day of the month following:</p>
                        <p>Date of Hire</p>
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <p>Waiting period applies to</p>
                        <p>Current Employees</p>
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <p class="mb-0">Number in waiting period:</p>
                        <p class="mb-0">1</p>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-between align-items-center">
                        <p class="mb-0">Total number of employees on payroll regardless of hours worked (including owners, seasonal....):</p>
                        <p class="mb-0">3</p>
                    </div>
                    <hr>
                    <p>Total number of ineligible employees in each of the following categories</p>
                    <div class="form-row">
                        <div class="form-group col-md-1">
                            <label>UNION</label>
                            <input type="text" class="form-control text-center" placeholder="0" disabled>
                        </div>
                        <div class="form-group col-md-1">
                            <label>PART-TIME</label>
                            <input type="text" class="form-control text-center" placeholder="1" disabled>
                        </div>
                        <div class="form-group col-md-1">
                            <label>SEASONAL</label>
                            <input type="text" class="form-control text-center" placeholder="1" disabled>
                        </div>
                        <div class="form-group col-md-1">
                            <label>TEMP</label>
                            <input type="text" class="form-control text-center" placeholder="0" disabled>
                        </div>
                        <div class="form-group col-md-1">
                            <label>TERMINATED</label>
                            <input type="text" class="form-control text-center" placeholder="0" disabled>
                        </div>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-between align-items-center">
                        <p>How many of th employees (including owners) enrolling are related by blood or marriage?</p>
                        <p>3</p>
                    </div>
                </div><!-- cc-card large -->
            </div>
            <hr class="my-4">
            <div class="cc-card-wrapper">
                <div class="d-flex justify-content-between align-items-end mb-2">
                   <h3 class="mb-0">Census</h3>
                   <button class="btn btn-grey-outline btn-sm ml-3"><i class="far fa-edit mr-2"></i>Edit</button>
                </div>
                <div class="cc-card large">
                    <table class="table card-table mb-0" hidden>
                        <thead>
                        <tr>
                            <th>Last Name</th>
                            <th>First Name</th>
                            <th>SSN</th>
                            <th class="text-center">Age or <br>DOB</th>
                            <th class="text-center">Gender</th>
                            <th class="text-center">Zip Code</th>
                            <th>Country</th>
                            <th>Date of Hire</th>
                            <th class="text-center">COBRA</th>
                            <th class="text-center">Number of Dependents age 0 - 14</th>
                            <th>Delivery Preference</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Anderson</td>
                            <td>Roy</td>
                            <td>555-12-4567</td>
                            <td class="text-center">36</td>
                            <td class="text-center">M</td>
                            <td class="text-center">90210</td>
                            <td>Los Angeles</td>
                            <td>10/28/2019</td>
                            <td class="text-center">No</td>
                            <td class="text-center">0</td>
                            <td>randerson@gmail.com</td>
                        </tr>
                        <tr>
                            <td>Bernard</td>
                            <td>Andy</td>
                            <td>555-54-6466</td>
                            <td class="text-center">39</td>
                            <td class="text-center">M</td>
                            <td class="text-center">90210</td>
                            <td>Los Angeles</td>
                            <td>10/28/2019</td>
                            <td class="text-center">No</td>
                            <td class="text-center">0</td>
                            <td>abernard@gmail.com</td>
                        </tr>
                        <tr class="border-top-0">
                            <td></td>
                            <td>&#10551; Spouse</td>
                            <td>37</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="border-top-0">
                            <td></td>
                            <td>&#10551; Dependent (Disabled)</td>
                            <td>16</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div><!-- cc-card large -->
            </div>
            <hr class="my-4">
            <div class="row">
                <div class="col-md-4">
                    <div class="cc-card-wrapper">
                        <div class="d-flex justify-content-between align-items-end mb-2">
                           <h3 class="mb-0">Metal Tier</h3>
                           <button class="btn btn-grey-outline btn-sm ml-3"><i class="far fa-edit mr-2"></i>Edit</button>
                        </div>
                        <div class="cc-card">
                            <h4>Triple Tier</h4>
                            <p>Platinum & Gold & Silver</p>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="my-4">
            <div class="row">
                <div class="col-md-4">
                    <div class="cc-card-wrapper">
                        <div class="d-flex justify-content-between align-items-end mb-2">
                           <h3 class="mb-0">Contribution</h3>
                           <button class="btn btn-grey-outline btn-sm ml-3"><i class="far fa-edit mr-2"></i>Edit</button>
                        </div>
                        <div class="cc-card">
                            <p><b>Contribution Type</b><br>
                                Lowest Cost HMO</p>
                            <p><b>Contribution Percentage</b><br>
                                Employer: 50% <br>
                                Dependent: 0%
                            </p>
                            <p><b>Worksheet Medical Preferences</b><br>
                                Show rates after employer contribution <br> 12 paychecks per year</p>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="my-4">
            <div class="cc-card-wrapper">
                <div class="d-flex justify-content-between align-items-end mb-2">
                   <h3 class="mb-0">Optional Benefits</h3>
                   <button class="btn btn-grey-outline btn-sm ml-3"><i class="far fa-edit mr-2"></i>Edit</button>
                </div>
                <div class="cc-card large">
                    <div class="media">
                        <i class="fal fa-tooth mr-3"></i>
                        <div class="media-body">
                            <h6 class="mb-3">Dental</h6>
                            <p><b>SmileSaver Plan 3000</b></p>
                            <div class="row">
                                <div class="col">
                                    <p><b>PPO</b><br>
                                        Ameritas Group
                                    </p>
                                </div>
                                <div class="col">
                                    <p><b>Employee</b><br>
                                        80%
                                    </p>
                                </div>
                                <div class="col">
                                    <p><b>Dependents</b><br>
                                        20%
                                    </p>
                                </div>
                                <div class="col">
                                    <p><b>Ortho</b><br>
                                        Yes
                                    </p>
                                </div>
                                <div class="col">
                                    <p><b>Rate Preferences</b><br>
                                        Show rates after employer contribution
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="media">
                        <i class="fal fa-heartbeat mr-3"></i>
                        <div class="media-body">
                            <h6 class="mb-3">Life</h6>
                            <p><b>MetLife</b><br>
                                Life Schedule</p>
                            <div class="row">
                                <div class="col">
                                    <p><b>Hourly</b><br>
                                        $10,000
                                    </p>
                                </div>
                                <div class="col">
                                    <p><b>Salary</b><br>
                                        $15,000
                                    </p>
                                </div>
                                <div class="col">
                                    <p><b>Mgmt</b><br>
                                        $20,000
                                    </p>
                                </div>
                                <div class="col">
                                    <p><b>Owner</b><br>
                                        $25,000
                                    </p>
                                </div>
                                <div class="col">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="media">
                        <i class="fal fa-glasses mr-3"></i>
                        <div class="media-body">
                            <h6 class="mb-3">Vision
                                <br><small>Covered</small></h6>
                        </div>
                    </div>
                    <div class="media">
                        <i class="fal fa-bed mr-3"></i>
                        <div class="media-body">
                            <h6 class="mb-3">Chiro & Acupuncture
                                <br><small>Not Covered</small></h6>
                        </div>
                    </div>
                    <div class="media mb-0">
                        <i class="fal fa-file-check mr-3"></i>
                        <div class="media-body border-bottom-0">
                            <h6 class="mb-3">Section 125
                                <br><small>Elected</small></h6>
                            <div class="row">
                                <div class="col">
                                    <p><b>President, Principal, Partners</b><br>
                                        Jim Jones
                                    </p>
                                </div>
                                <div class="col">
                                    <p><b>Company Structure</b><br>
                                        Corporation
                                    </p>
                                </div>
                                <div class="col">
                                    <p><b>State of Incorp</b><br>
                                        CA
                                    </p>
                                </div>
                                <div class="col">
                                    <p><b>Last Day of Plan Year</b><br>
                                        5/1/2019
                                    </p>
                                </div>
                                <div class="col">
                                    <p><b>Prem Payments Selected</b><br>
                                        Medical
                                    </p>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col">
                                    <p><b>Corporate Secretary</b><br>
                                        Jim Jones
                                    </p>
                                </div>
                                <div class="col">
                                </div>
                                <div class="col">
                                </div>
                                <div class="col">
                                </div>
                                <div class="col">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Cancel</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>