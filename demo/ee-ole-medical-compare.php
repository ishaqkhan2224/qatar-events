<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employee OLE</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Online Enrollment</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="ee-ole-overview">Overview</a>
            <a class="nav-link" href="ee-ole-your-info">Your Info</a>
            <a class="nav-link" href="ee-ole-dependents">Dependents</a>
            <a class="nav-link active" href="ee-ole-medical">Medical</a>
            <a class="nav-link" href="ee-ole-dental">Dental</a>
            <a class="nav-link" href="ee-ole-chiro">Chiro</a>
            <a class="nav-link" href="ee-ole-vision">Vision</a>
            <a class="nav-link" href="ee-ole-life">Life</a>
            <a class="nav-link" href="ee-ole-section-125">Section 125</a>
            <a class="nav-link" href="ee-ole-summary">Summary</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="cc-card-wrapper">
                        <div class="cc-card large bg-light">
                            <div class="row">
                                <div class="col-12">
                                    <h6>Compare Medical Plans</h6>
                                    <p class="mb-0">lorem ipsum</p>
                                    <table class="table card-table plan-table">
                                        <thead>
                                        <tr>
                                            <th width="19%">
                                                <h6 class="mt-4 mb-0">Details & Description</h6>
                                            </th>
                                            <th class="plan-td" width="27%">
                                                <div class="cc-card m-0">
                                                    <div class="d-flex align-items-center">
                                                        <div class="custom-control m-0 custom-radio custom-control-inline plan_radio" style="display: none;">
                                                            <input type="radio" id="checkRadio1" name="planOptions" class="custom-control-input">
                                                            <label class="custom-control-label" for="checkRadio1"></label>
                                                        </div>
                                                        <select class="form-control select-plan">
                                                            <option selected value="">Select Plan</option>
                                                            <option disabled value="">---------------- Bronze Tier ----------------</option>
                                                            <option value="Kaiser Permanente HMO A($602.75">Kaiser Permanente HMO A($602.75</option>
                                                            <option value="UnitedHealthcare HMO B($640.75)">UnitedHealthcare HMO B($640.75</option>
                                                            <option value="Kaiser Permanente HMO C($725.99)">Kaiser Permanente HMO C($725.99)</option>
                                                            <option value="Health Net HSP A($798.15)">Health Net HSP A($798.15)</option>
                                                            <option value="Anthem Blue Cross EPO A($891.10)">Anthem Blue Cross EPO A($891.10)</option>
                                                            <option value="Oscar EPO A($1046.17)">Oscar EPO A($1046.17)</option>
                                                            <option disabled value="">------------------ Sliver Tier -----------------</option>
                                                            <option value="Kaiser Permanente HMO A($602.75)">Kaiser Permanente HMO A($602.75)</option>
                                                            <option value="UnitedHealthcare HMO B($640.75)">UnitedHealthcare HMO B($640.75)</option>
                                                            <option value="Kaiser Permanente HMO C($725.99)">Kaiser Permanente HMO C($725.99)</option>
                                                            <option value="Health Net HSP A($798.15)">Health Net HSP A($798.15)</option>
                                                            <option value="Anthem Blue Cross EPO A($891.10)">Anthem Blue Cross EPO A($891.10)</option>
                                                            <option value="Oscar EPO A($1046.17)">Oscar EPO A($1046.17)</option>
                                                        </select>
                                                    </div>
                                                    <h6 class="mt-2 text-right plan_price" style="display: none;">$602.75</h6>
                                                </div>
                                            </th>
                                            <th class="plan-td" width="27%">
                                                <div class="cc-card m-0">
                                                    <div class="d-flex align-items-center">
                                                        <div class="custom-control m-0 custom-radio custom-control-inline plan_radio" style="display: none;">
                                                            <input type="radio" id="checkRadio2" name="planOptions" class="custom-control-input">
                                                            <label class="custom-control-label" for="checkRadio2"></label>
                                                        </div>
                                                        <select class="form-control select-plan">
                                                            <option selected value="">Select Plan</option>
                                                            <option disabled value="">---------------- Bronze Tier ----------------</option>
                                                            <option value="Kaiser Permanente HMO A($602.75">Kaiser Permanente HMO A($602.75</option>
                                                            <option value="UnitedHealthcare HMO B($640.75)">UnitedHealthcare HMO B($640.75</option>
                                                            <option value="Kaiser Permanente HMO C($725.99)">Kaiser Permanente HMO C($725.99)</option>
                                                            <option value="Health Net HSP A($798.15)">Health Net HSP A($798.15)</option>
                                                            <option value="Anthem Blue Cross EPO A($891.10)">Anthem Blue Cross EPO A($891.10)</option>
                                                            <option value="Oscar EPO A($1046.17)">Oscar EPO A($1046.17)</option>
                                                            <option disabled value="">------------------ Sliver Tier -----------------</option>
                                                            <option value="Kaiser Permanente HMO A($602.75)">Kaiser Permanente HMO A($602.75)</option>
                                                            <option value="UnitedHealthcare HMO B($640.75)">UnitedHealthcare HMO B($640.75)</option>
                                                            <option value="Kaiser Permanente HMO C($725.99)">Kaiser Permanente HMO C($725.99)</option>
                                                            <option value="Health Net HSP A($798.15)">Health Net HSP A($798.15)</option>
                                                            <option value="Anthem Blue Cross EPO A($891.10)">Anthem Blue Cross EPO A($891.10)</option>
                                                            <option value="Oscar EPO A($1046.17)">Oscar EPO A($1046.17)</option>
                                                        </select>
                                                    </div>
                                                    <h6 class="mt-2 text-right plan_price" style="display: none;">$602.75</h6>
                                                </div>
                                            </th>
                                            <th class="plan-td" width="27%">
                                                <div class="cc-card m-0">
                                                    <div class="d-flex align-items-center">
                                                        <div class="custom-control m-0 custom-radio custom-control-inline plan_radio" style="display: none;">
                                                            <input type="radio" id="checkRadio3" name="planOptions" class="custom-control-input">
                                                            <label class="custom-control-label" for="checkRadio3"></label>
                                                        </div>
                                                        <select class="form-control select-plan">
                                                            <option selected value="">Select Plan</option>
                                                            <option disabled value="">---------------- Bronze Tier ----------------</option>
                                                            <option value="Kaiser Permanente HMO A($602.75">Kaiser Permanente HMO A($602.75</option>
                                                            <option value="UnitedHealthcare HMO B($640.75)">UnitedHealthcare HMO B($640.75</option>
                                                            <option value="Kaiser Permanente HMO C($725.99)">Kaiser Permanente HMO C($725.99)</option>
                                                            <option value="Health Net HSP A($798.15)">Health Net HSP A($798.15)</option>
                                                            <option value="Anthem Blue Cross EPO A($891.10)">Anthem Blue Cross EPO A($891.10)</option>
                                                            <option value="Oscar EPO A($1046.17)">Oscar EPO A($1046.17)</option>
                                                            <option disabled value="">------------------ Sliver Tier -----------------</option>
                                                            <option value="Kaiser Permanente HMO A($602.75)">Kaiser Permanente HMO A($602.75)</option>
                                                            <option value="UnitedHealthcare HMO B($640.75)">UnitedHealthcare HMO B($640.75)</option>
                                                            <option value="Kaiser Permanente HMO C($725.99)">Kaiser Permanente HMO C($725.99)</option>
                                                            <option value="Health Net HSP A($798.15)">Health Net HSP A($798.15)</option>
                                                            <option value="Anthem Blue Cross EPO A($891.10)">Anthem Blue Cross EPO A($891.10)</option>
                                                            <option value="Oscar EPO A($1046.17)">Oscar EPO A($1046.17)</option>
                                                        </select>
                                                    </div>
                                                    <h6 class="mt-2 text-right plan_price" style="display: none;">$652.75</h6>
                                                </div>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="card-table-no-headers">
                                        <tr>
                                            <td>Network Name</td>
                                            <td class="plan-td" id="network1"></td>
                                            <td class="plan-td" id="network2"></td>
                                            <td class="plan-td" id="network3"></td>
                                        </tr>
                                        <tr>
                                            <td>Deductible</td>
                                            <td class="plan-td" id="deductible1"></td>
                                            <td class="plan-td" id="deductible2"></td>
                                            <td class="plan-td" id="deductible3"></td>
                                        </tr>
                                        <tr>
                                            <td>Out-of-Pocket Max - Ind / Fam</td>
                                            <td class="plan-td" id="out_of_pocket1"></td>
                                            <td class="plan-td" id="out_of_pocket2"></td>
                                            <td class="plan-td" id="out_of_pocket3"></td>
                                        </tr>
                                        <tr>
                                            <td>Lifetime Maximum</td>
                                            <td class="plan-td" id="lifetime_max1"></td>
                                            <td class="plan-td" id="lifetime_max2"></td>
                                            <td class="plan-td" id="lifetime_max3"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Doctor Office Visits
                                                <ul class="cc-ul">
                                                    <li>Specialist Visit</li>
                                                    <li>Lab and X-Ray</li>
                                                    <li>MRI,CT, and PET</li>
                                                    <li>Annual Physical Exam</li>
                                                    <li>Infertility Evaluation</li>
                                                </ul>
                                            </td>
                                            <td class="plan-td" id="office_visit1"></td>
                                            <td class="plan-td" id="office_visit2"></td>
                                            <td class="plan-td" id="office_visit3"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Hospital Services
                                                <ul class="cc-ul">
                                                    <li>In-Patient Physician Fees</li>
                                                    <li>Emergency Room</li>
                                                    <li>Out-Patient Surgery</li>
                                                    <li>2nd Surgical Opinion</li>
                                                </ul>
                                            </td>
                                            <td class="plan-td" id="hospital_service1"></td>
                                            <td class="plan-td" id="hospital_service2"></td>
                                            <td class="plan-td" id="hospital_service3"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- cc-card large -->
                    </div><!-- cc-card-wrapper -->
                </div>
            </div>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="#" class="btn-link">Back</a>
                </div>
                <div class="right">
                    <a href="#" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>