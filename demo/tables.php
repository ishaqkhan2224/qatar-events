<!DOCTYPE html>
<html>
<head>
<?php include "inc/header.meta.php"; ?>
<title>CC UI FRAMEWORK: Tables</title>
<?php include "inc/header.link.php"; ?>
</head>
<body>

<section id="cc-body">
    <div class="container">
        <h3>Empty State</h3>
        <?php include "inc/tables.empty.php"; ?>
        <hr class="break">
        <h3>Inline Forms</h3>
        <?php include "inc/tables.basic.php"; ?>
        <hr class="break">
        <h3>Accordion</h3>
        <?php include "inc/tables.accordion.php"; ?>
        <hr class="break">
        <h3>Standard DataTable</h3>
        <?php include "inc/tables.datatable-search.php" ?>
        <hr class="break">
        <h3>Product Highlights</h3>
        <?php include "inc/tables.product-highlights.php" ?>
        <hr class="break">
        <h3>Products & Currencies</h3>
        <?php include "inc/tables.commissions.php" ?>
        <hr class="break">
        <h3>Stacked Cell</h3>
        <?php include "inc/tables.stacked.php" ?>
        <hr class="break">
        <h3>Upload > Family Census Review</h3>
        <?php include "inc/tables.census.php" ?>
    </div><!-- container -->
</section>

<?php include "common/footer.php"; ?>
</body>
</html>