<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Forms</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>

<section id="cc-body">
    <div class="container">
        <div class="cc-confirmation">
            <div class="cc-confirmation-header cc-success">
                {Confirmation Alert}
            </div>
            <div class="cc-confirmation-body">
                <img src="images/icons/cc-confirmation-success.png" alt="Confirmation">
                <h5>{Confirmation Title}</h5>
                <div class="cc-confirmation-body-info">
                    <p>
                        {Name: <strong>Group/Employee</strong>}<br>
                        {Date: <strong>xx/xx/xxxx</strong>}
                    </p>
                </div>
                <div class="cc-confirmation-body-message">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit repudiandae, ipsum, quae facilis qui minima consectetur animi iure ipsa dolorem explicabo beatae ducimus temporibus velit corporis doloribus natus quaerat quidem. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit repudiandae.</p>
                </div>
            </div><!-- cc-confirmation-body -->
            <div class="cc-confirmation-footer">
                <a href="#" class="btn btn-blue">Primary CTA</a>
                <a href="#" class="btn btn-grey-outline">Button</a>
                <a href="#" class="btn btn-grey-outline">Button</a>
            </div><!-- cc-confirmation-footer -->
        </div>
        <hr class="my-5">
        <div class="cc-confirmation">
            <div class="cc-confirmation-header cc-error">
                {Confirmation Alert}
            </div>
            <div class="cc-confirmation-body">
                <img src="images/icons/cc-confirmation-error.png" alt="Confirmation">
                <h5>{Confirmation Title}</h5>
                <div class="cc-confirmation-body-info">
                    <p>
                        {Name: <strong>Group/Employee</strong>}<br>
                        {Date: <strong>xx/xx/xxxx</strong>}
                    </p>
                </div>
                <div class="cc-confirmation-body-message">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit repudiandae, ipsum, quae facilis qui minima consectetur animi iure ipsa dolorem explicabo beatae ducimus temporibus velit corporis doloribus natus quaerat quidem. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit repudiandae.</p>
                </div>
            </div><!-- cc-confirmation-body -->
            <div class="cc-confirmation-footer">
                <a href="#" class="btn btn-blue">Primary CTA</a>
                <a href="#" class="btn btn-grey-outline">Button</a>
                <a href="#" class="btn btn-grey-outline">Button</a>
            </div><!-- cc-confirmation-footer -->
        </div>
        <hr class="my-5">
        <div class="cc-confirmation">
            <div class="cc-confirmation-header cc-warning">
                {Confirmation Alert}
            </div>
            <div class="cc-confirmation-body">
                <img src="images/icons/cc-confirmation-warning.png" alt="Confirmation">
                <h5>{Confirmation Title}</h5>
                <div class="cc-confirmation-body-info">
                    <p>
                        {Name: <strong>Group/Employee</strong>}<br>
                        {Date: <strong>xx/xx/xxxx</strong>}
                    </p>
                </div>
                <div class="cc-confirmation-body-message">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit repudiandae, ipsum, quae facilis qui minima consectetur animi iure ipsa dolorem explicabo beatae ducimus temporibus velit corporis doloribus natus quaerat quidem. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit repudiandae.</p>
                </div>
            </div><!-- cc-confirmation-body -->
            <div class="cc-confirmation-footer">
                <a href="#" class="btn btn-blue">Primary CTA</a>
                <a href="#" class="btn btn-grey-outline">Button</a>
                <a href="#" class="btn btn-grey-outline">Button</a>
            </div><!-- cc-confirmation-footer -->
        </div>
        <hr class="my-5">
        <div class="cc-confirmation">
            <div class="cc-confirmation-header cc-info">
                {Information Alert}
            </div>
            <div class="cc-confirmation-body">
                <img src="images/icons/cc-confirmation-warning.png" alt="Confirmation">
                <h5>{Confirmation Title}</h5>
                <div class="cc-confirmation-body-info">
                    <p>
                        {Name: <strong>Group/Employee</strong>}<br>
                        {Date: <strong>xx/xx/xxxx</strong>}
                    </p>
                </div>
                <div class="cc-confirmation-body-message">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit repudiandae, ipsum, quae facilis qui minima consectetur animi iure ipsa dolorem explicabo beatae ducimus temporibus velit corporis doloribus natus quaerat quidem. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit repudiandae.</p>
                </div>
            </div><!-- cc-confirmation-body -->
            <div class="cc-confirmation-footer">
                <a href="#" class="btn btn-blue">Primary CTA</a>
                <a href="#" class="btn btn-grey-outline">Button</a>
                <a href="#" class="btn btn-grey-outline">Button</a>
            </div><!-- cc-confirmation-footer -->
        </div>
    </div>
</section>

</body>
</html>