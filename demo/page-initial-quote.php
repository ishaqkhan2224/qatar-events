<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Forms</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>New Quote</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="#">Group Info</a>
            <a class="nav-link" href="#">Census</a>
            <a class="nav-link active" href="#">Delivery</a>
        </nav>
    </div>
</section>

<section id="cc-body">
    <div class="container">

        <div class="row">
          <div class="col-12">
            <div class="cc-card-wrapper">
              <h3>Initial Quote Includes the Following Sections:</h3>
              <div class="cc-card">
                 <div class="row">
                     <div class="col-6">
                        <div class="quote-includes">
                            <div class="row">
                                <div class="col">
                                    <i class="fal fa-list-alt"></i>
                                    <h4>CaliforniaChoice<br>Overview </h4></div>
                                <div class="col">
                                    <i class="fab fa-wpforms"></i>
                                    <h4>Census</h4>
                                </div>
                                <div class="col">
                                    <i class="fal fa-money-check-edit-alt"></i>
                                    <h4>Medical Budget<br>Defined Contribution</h4>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col">
                                    <i class="fal fa-files-medical"></i>
                                    <h4>Ancillary Rate<br>Summaries</h4>
                                </div>
                                <div class="col">
                                    <i class="fal fa-clipboard-user"></i>
                                    <h4>Sample Employee<br>Worksheet</h4>
                                </div>
                                <div class="col">
                                    <i class="fal fa-file-invoice-dollar"></i>
                                    <h4>Sample Invoice</h4>
                                </div>
                            </div>
                        </div><!-- quote-includes -->
                     </div>
                     <div class="col-6">
                        <div class="quote-optional pl-0">
                            <div class="d-flex justify-content-between">
                                <div class="_left"><h4>Optional Selections for Your Quote</h4></div>
                                <div class="_right">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Select All Optional Benefits</label>
                                    </div>
                                </div>
                            </div>
                            <ul>
                                <li>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2">Medical Benefit Summaries</label>
                                    </div>
                                </li>
                                <li class="active">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck3" checked="checked">
                                        <label class="custom-control-label" for="customCheck3">Ancillary Benefit Summaries (Dental, Vision, Chiro & Acupuncture)</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck4">
                                        <label class="custom-control-label" for="customCheck4">Employee & Dependent Rates</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                     </div>
                 </div>
              </div><!-- cc-card large -->
            </div><!-- cc-card-wrapper -->
          </div><!-- col-12 -->
        </div><!-- row -->

        <div class="d-flex align-items-center mt-4">
            <div>Please review your information below. You have multiple agencies listed, please select which agency you would like to use for this quote:</div>
            <div class="ml-2">
                <select name="cc-select-company" id="cc-select-company" class="form-control">
                    <option value="asdf">Widget Inc.</option>
                    <option value="asdf">Widget Inc.</option>
                    <option value="asdf">Widget Inc.</option>
                </select>    
            </div>
        </div>

        <div class="row mt-2">
          <div class="col-12">
            <div class="cc-card-wrapper">
              <div class="cc-card large">
                 <div class="row info-section">
                     <div class="col-md-3">
                         <div class="info-box d-flex">
                             <div class="_icon"><i class="fal fa-user"></i></div>
                             <div class="_disc">
                                 <h4>Broker Information</h4>
                                 <p>Broker No. 80366 <br>
                                     Aaron Watts</p>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 border-left-md">
                         <div class="info-box d-flex">
                             <div class="_icon"><i class="fal fa-map-marker-alt"></i></div>
                             <div class="_disc">
                                 <h4>Contact Information</h4>
                                 <p>Address: 721 South Parker <br>
                                     City: Orange <br>
                                     State: CA ZIP: 92868 <br>
                                     Phone: (714) 567-4505 <br>
                                     Email: <a href="#">johnny@broker.com</a></p>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 border-left-md">
                         <div class="info-box d-flex">
                             <div class="_icon"><i class="fal fa-user"></i></div>
                             <div class="_disc">
                                 <h4>Inside Sales Rep</h4>
                                 <p>Don Hutchinson <br>
                                     Phone: (800) 542-4218 <br>
                                     Ext: 4431 <br>
                                     Email: <a href="#">insales@calchoice.com</a></p>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 border-left-md">
                         <div class="info-box d-flex">
                             <div class="_icon"><i class="fal fa-user"></i></div>
                             <div class="_disc">
                                 <h4>Outside Sales Rep</h4>
                                 <p>Johnny Appleseed <br>
                                     Phone: (800) 542-4218 <br>
                                     Ext: 4431 <br>
                                     Email: <a href="#">outsales@calchoice.com</a></p>
                             </div>
                         </div>
                     </div>
                 </div>
              </div><!-- cc-card -->
            </div><!-- cc-card-wrapper -->
          </div><!-- col-12 -->
        </div><!-- row -->

        <div class="row">
            <div class="col-12">
                <p>If you need to make changes, contact our Finance Customer Service team at <a href="#">commissions@calchoice.com</a> or (714) 567-4390.</p>        
            </div>
        </div>
        
        <div class="delivery-email mt-4">
            <h4>Delivery Email:</h4>
            <input type="text" value="john.doe@gmail.com; jane.doe@hotmail.com" class="form-control">
            <p>If this quote is being sent to multiple email addresses, separate each address with a semicolon(;)</p>
        </div>
    </div>
</section>

<section class="cc-controls sticky">
    <div class="container">
        <div class="d-flex align-items-center">
            <div class="left">
                <a href="#" class="btn-link">Cancel</a>
            </div>
            <div class="right">
                <a href="#" class="btn-link">Print & Preview</a>
                <a href="#" class="btn-link">Save & Exit</a>
                <button class="btn">Deliver Now</button>
            </div>
        </div>
    </div>
</section>

</body>
</html>