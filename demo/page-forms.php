<!DOCTYPE html>
<html>
<head>
<?php include "inc/header.meta.php"; ?>
<title>CC UI KIT: Forms</title>
<?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<?php 
//include "common/subheader-progressive-tabs.php";
include "common/subheader-tabs.php";
?>

<form id="cc-form__new-quote" method="post" action="">
<section id="cc-body">
    <div class="container">
          <div class="cc-form__box">
            <div class="row">
                <div class="col-7">
                    <h5>Section Title Goes Here</h5>
                    <div class="form-group">
                      <label for="business_name">Business Name *</label>
                      <input type="text" class="form-control" id="business_name" name="business_name" required>
                      <small id="passwordHelpBlock" class="form-text text-muted">
                            This is an example of a helper text, located nicely below the input field.
                      </small>
                    </div>
                    <div class="form-group">
                      <div class="cc-showhidepwd">
                        <label for="field-CurrentPassword">Current Password</label>
                        <span class="js-password-display fas fa-eye-slash" data-toggle="tooltip" data-placement="left" title="Click to show password"></span>
                      </div>
                      <input type="password" class="form-control" id="field-CurrentPassword" name="CurrentPassword" autocomplete="off" required />
                    </div>
                    <h5 class="mt-5">Section Title Goes Here</h5>
                    <div class="form-group">
                      <label for="first_name">First Name</label>
                      <input type="text" class="form-control" id="first_name" name="first_name" required>
                    </div>
                    <div class="form-group">
                      <label for="last_name">Last Name</label>
                      <input type="text" class="form-control" id="last_name" name="last_name" required>
                    </div>
                    <div class="form-group">
                      <div class="form-row">
                          <div class="col-8">
                            <label for="social_security_number">Social Security Number</label>
                            <input type="text" class="form-control" id="social_security_number" name="social_security_number" placeholder="xxx-xx-xxxx" required>
                          </div>
                          <div class="col-4">
                            <label for="hire_date">Hire Date</label>
                            <div class="d-flex align-items-center">
                              <input type="text" class="form-control date-field mr-2" placeholder="MM/DD/YYYY" name="hire_date" id="hire_date" required="">
                              <i class="fa fa-calendar-alt" id="hiredatepicker" aria-hidden="true"></i>
                            </div>
                          </div>
                       </div>
                    </div>
                    <div class="form-group">
                      <div class="form-row">
                          <div class="col-8">
                            <label for="gender_field_name">Gender</label>
                            <select id="" class="form-control" required>
                                <option value="">Select</option>
                                <option value="">Male</option>
                                <option value="">Female</option>
                            </select>
                          </div>
                          <div class="col-4">
                            <label for="date_of_birth_fieldname">Date of Birth</label>
                            <input type="text" class="form-control" id="date_of_birth_fieldname" name="date_of_birth_fieldname" required>
                          </div>
                       </div>
                    </div>
                    <h5 class="mt-5">Section Title Goes Here</h5>
                    <div class="form-group">
                      <div class="form-row">
                          <div class="col-9">
                            <label for="address">Address *</label>
                            <input type="text" class="form-control" id="address" name="address" required>
                          </div>
                          <div class="col-3">
                            <label for="suite_number">&nbsp;</label>
                            <input type="text" class="form-control" id="suite_number" name="suite_number" placeholder="Suite # *" required>
                          </div>
                       </div>
                    </div>
                    <div class="form-group">
                      <div class="form-row">
                          <div class="col-7">
                            <input type="text" class="form-control" name="city" placeholder="City *" required>
                          </div>
                          <div class="col-2">
                            <select id="" class="form-control" required>
                                <option value="">Choose One...</option>
                                <option value="CA">CA</option>
                                <option>...</option>
                            </select>
                          </div>
                          <div class="col-3">
                            <input type="text" class="form-control" name="zip_code" placeholder="Zip Code *" required>
                          </div>
                       </div>
                    </div>
                    <div class="form-group">
                      <label for="special_instruction">Special Instruction *</label>
                      <textarea class="form-control" id="special_instruction" name="special_instruction" rows="6" required></textarea>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="same-address" name="same_address">
                            <label class="custom-control-label" for="same-address">Shipping address is the same as my billing address</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="billing" name="billing">
                            <label class="custom-control-label" for="billing">Use the same address as billing</label>
                        </div>
                    </div>
                </div>
            </div>
          </div><!-- cc-form__box -->

          <hr>

          <div class="cc-form__box mt-5">
            <div class="row">
                <div class="col-8 question-p">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In labore accusantium earum. Sint veritatis, obcaecati quod illo tenetur reprehenderit necessitatibus labore velit sed totam! Quisquam, est, voluptate.</p>
                </div>
                <div class="col-3 offset-1">
                    <div class="form-group">
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="AgreeDisagree1-a" name="AgreeDisagree1" class="custom-control-input" value="agree">
                        <label class="custom-control-label" for="AgreeDisagree1-a">Agree</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="AgreeDisagree1-b" name="AgreeDisagree1" class="custom-control-input" value="disagree">
                        <label class="custom-control-label" for="AgreeDisagree1-b">Disagree</label>
                      </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-8 question-p">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In labore accusantium earum.</p>
                </div>
                <div class="col-3 offset-1">
                    <div class="form-group">
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="AgreeDisagree2-a" name="AgreeDisagree2" class="custom-control-input" value="agree">
                        <label class="custom-control-label" for="AgreeDisagree2-a">Agree</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="AgreeDisagree2-b" name="AgreeDisagree2" class="custom-control-input" value="disagree">
                        <label class="custom-control-label" for="AgreeDisagree2-b">Disagree</label>
                      </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-8 question-p">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In labore accusantium earum. Sint veritatis, obcaecati quod illo tenetur reprehenderit necessitatibus labore velit sed totam! Quisquam, est, voluptate. Eaque, laboriosam, molestias. Lorem ipsum dolor sit amet, consectetur adipisicing elit. In labore accusantium earum.</p>
                </div>
                <div class="col-3 offset-1">
                    <div class="form-group">
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="AgreeDisagree3-a" name="AgreeDisagree3" class="custom-control-input" value="agree">
                        <label class="custom-control-label" for="AgreeDisagree3-a">Agree</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="AgreeDisagree3-b" name="AgreeDisagree3" class="custom-control-input" value="disagree">
                        <label class="custom-control-label" for="AgreeDisagree3-b">Disagree</label>
                      </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-8 question-p">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In labore accusantium earum. Sint veritatis, obcaecati quod illo tenetur reprehenderit necessitatibus labore velit sed totam! Quisquam, est, voluptate.</p>
                </div>
                <div class="col-3 offset-1">
                    <div class="form-group">
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="AgreeDisagree4-a" name="AgreeDisagree4" class="custom-control-input" value="agree">
                        <label class="custom-control-label" for="AgreeDisagree4-a">Agree</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="AgreeDisagree4-b" name="AgreeDisagree4" class="custom-control-input" value="disagree">
                        <label class="custom-control-label" for="AgreeDisagree4-b">Disagree</label>
                      </div>
                    </div>
                </div>
            </div>
          </div><!-- cc-form__box -->
    </div><!-- container -->
</section>

<section class="cc-controls sticky">
    <div class="container">
        <div class="d-flex align-items-center">
            <div class="left">
                <a href="#" class="btn-link">Cancel</a>
            </div>
            <div class="right">
                <a href="#" class="btn-link">Save & Exit</a>
                <button class="btn">Next</button>
            </div>
        </div>
    </div>
    <div class="cc-controls-footer-links">
      <div class="container">
        <div class="d-flex justify-content-between">
            <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
            <div>
              <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
            </div>
        </div>
      </div>
    </div>
</section>
</form>
<?php include "common/footer.php"; ?>
<script>
$(document).ready(function () {
    $('#cc-form__new-quote').validate({
        errorClass: "is-invalid",
        //validClass: "is-valid",
        rules: {
            same_address: "required",
            billing: "required",
            AgreeDisagree1: "required",
            AgreeDisagree2: "required",
            AgreeDisagree3: "required",
            AgreeDisagree4: "required"
        },
        messages: {
            business_name: {
                required: "Business Name is required."
            }
        }
    });

    $('.js-password-display').on('click', function () {
         var passwordInputField = $(this).closest('.form-group').find('input');
         if (passwordInputField.attr('type') === 'password') {
             passwordInputField.attr('type', 'text');
             $(this).removeClass('fa-eye-slash').addClass('fa-eye').attr('title', 'Click to hide password').tooltip('dispose').tooltip('show');
         } else {
             passwordInputField.attr('type', 'password');
             $(this).removeClass('fa-eye').addClass('fa-eye-slash').attr('title', 'Click to show password').tooltip('dispose').tooltip('show');
         }
    });

    /*Datepicker pop-up*/
    $('#hiredatepicker').daterangepicker({
        opens: 'left',
        singleDatePicker: true,
        showDropdowns: true
    },
     function(start,end,label){
      $('#hire_date').val(start.format('MM/DD/YYYY'));
     });

    /* Validate startdate and enddate input fields format*/
    $('#hire_date, #date_of_birth_fieldname').mask('AB/CD/0000', {
        translation: {
            A: {pattern: /[0-1]/},
            B: {pattern: /[0-9]/},
            C: {pattern: /[0-3]/},
            D: {pattern: /[0-9]/}
        },
        onKeyPress: function (a, b, c, d) {
            if (!a) return;
            let m = a.match(/(\d{1})/g);
            if (!m) return;
            if (parseInt(m[2]) === 3) {
                d.translation.D.pattern = /[0-1]/;
            } else if (parseInt(m[2]) === 0) {
                d.translation.D.pattern = /[1-9]/;
            } else {
                d.translation.D.pattern = /[0-9]/;
            }
            if (parseInt(m[0]) == 1) {
                d.translation.B.pattern = /[0-2]/;
            } else if (parseInt(m[0]) === 0) {
                d.translation.B.pattern = /[1-9]/;
            } else {
                d.translation.B.pattern = /[0-9]/;
            }
            let temp_value = c.val();
            c.val('');
            c.unmask().mask('AB/CD/0000', d);
            c.val(temp_value);
        },
        placeholder: "MM/DD/YYYY"
    }).keyup();

});
</script>
</body>
</html>