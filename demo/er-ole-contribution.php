<!DOCTYPE html>
<html>
<head>
    <?php include "inc/header.meta.php"; ?>
    <title>CC UI KIT: Employer OLE</title>
    <?php include "inc/header.link.php"; ?>
</head>
<body>
<?php include "common/header.php"; ?>
<section id="cc-subheader">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="left">
                    <h2>Online Enrollment</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
        <nav class="cc-subheader__tabs">
            <a class="nav-link" href="er-ole-overview">Overview</a>
            <a class="nav-link" href="er-ole-application">Application</a>
            <a class="nav-link" href="er-ole-census">Census</a>
            <a class="nav-link" href="er-ole-metaltier">Metal Tier</a>
            <a class="nav-link active" href="er-ole-contribution">Contribution</a>
            <a class="nav-link" href="er-ole-optional-benefits">Optional Benefits</a>
            <a class="nav-link" href="er-ole-summary">Summary</a>
        </nav>
    </div>
</section>
<form id="cc-form__new-quote" method="post" action="">
    <section id="cc-body">
        <div class="container">
            <h5>Select a medical contribution from the option below.</h5>
            <section class="cc-contribution cc-flex-row mt-5 js-radio-row-container">
                <div class="cc-flex-item">
                    <a href="javascript:;">
                        <div class="cc-card">
                            <div class="cc-card-wrapper text-center">
                                <div class="contribution-header">
                                    <h5>Lowest Cost HMO</h5>
                                    <p>This option is based on the employer contributing towards the lowest cost HMO benefit level.</p>
                                </div>
                                <button class="btn btn-select btn-grey-outline" data-target="lowest_cost_hmo">Select</button>
                            </div>
                        </div><!-- cc-card -->
                    </a>
                </div>
                <div class="cc-flex-item">
                    <a href="javascript:;">
                        <div class="cc-card">
                            <div class="cc-card-wrapper text-center">
                                <div class="contribution-header">
                                    <h5>Lowest Cost PPO</h5>
                                    <p>This option is based on the employer contributing towards the lowest cost HMO benefit level.</p>
                                </div>
                                <button class="btn btn-select btn-grey-outline" data-target="lowest_cost_ppo">Select</button>
                            </div>
                        </div><!-- cc-card -->
                    </a>
                </div>
                <div class="cc-flex-item">
                    <a href="javascript:;">
                        <div class="cc-card">
                            <div class="cc-card-wrapper text-center">
                                <div class="contribution-header">
                                    <h5>Employer Fixed</h5>
                                    <p>This popular Fixed Dollar approach allows Employers to set a specific per-Employee budget.</p>
                                </div>
                                <button class="btn btn-select btn-grey-outline" data-target="employer_fixed">Select</button>
                            </div>
                        </div><!-- cc-card -->
                    </a>
                </div>
                <div class="cc-flex-item">
                    <a href="javascript:;">
                        <div class="cc-card">
                            <div class="cc-card-wrapper text-center">
                                <div class="contribution-header">
                                    <h5>Employee Fixed</h5>
                                    <p>The employee pays a set amount for the base plan regardless of age and the employer pays the difference.</p>
                                </div>
                                <button class="btn btn-select btn-grey-outline" data-target="employee_fixed">Select</button>
                            </div>
                        </div><!-- cc-card -->
                    </a>
                </div>
                <div class="cc-flex-item">
                    <a href="javascript:;">
                        <div class="cc-card">
                            <div class="cc-card-wrapper text-center">
                                <div class="contribution-header">
                                    <h5>Percentage of Cost</h5>
                                    <p>Employer picks from 50% to 100% to contribute toward the Employee cost of a specific plan and/or benefit level.</p>
                                </div>
                                <button class="btn btn-select btn-grey-outline" data-target="percentage_of_cost">Select</button>
                            </div>
                        </div><!-- cc-card -->
                    </a>
                </div>
            </section>
            <section class="row mt-4">
                <div class="col-12">
                    <div id="lowest_cost_hmo" class="cc-card large cost-forms" style="display: none">
                        <div class="cc-card-wrapper">
                            <h5>Lowest Cost HMO</h5>
                            <p>Enter the percentage the employer will contribute to their employees's premium.</p>
                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-2">
                                        <label>Employee</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control mr-1">
                                            <img src="./images/icons/p-icon.png" alt="">
                                        </div>
                                        <small class="form-text text-muted">(50% min)</small>
                                    </div>
                                    <div class="form-group col-md-2 ml-3">
                                        <label>Dependent</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control mr-1">
                                            <img src="./images/icons/p-icon.png" alt="">
                                        </div>
                                        <small class="form-text text-muted">(Optional)</small>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label>Show Rates</label>
                                        <select class="form-control">
                                            <option selected>SELECT</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>NUMBER OF PAYCHECKS PER YEAR</label>
                                        <select class="form-control">
                                            <option selected>SELECT</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- cc-card -->

                    <div id="lowest_cost_ppo" class="cc-card large cost-forms" style="display: none;">
                        <div class="cc-card-wrapper">
                            <h5>Lowest Cost PPO</h5>
                            <p>Enter the percentage the employer will contribute to their employees's premium.</p>
                            <div>
                                <div class="form-row">
                                    <div class="form-group col-md-2">
                                        <label>Employee</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control mr-1">
                                            <img src="./images/icons/p-icon.png" alt="">
                                        </div>
                                        <small class="form-text text-muted">(50% min)</small>
                                    </div>
                                    <div class="form-group col-md-2 ml-3">
                                        <label>Dependent</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control mr-1">
                                            <img src="./images/icons/p-icon.png" alt="">
                                        </div>
                                        <small class="form-text text-muted">(Optional)</small>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label>Show Rates</label>
                                        <select class="form-control">
                                            <option selected>SELECT</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>NUMBER OF PAYCHECKS PER YEAR</label>
                                        <select class="form-control">
                                            <option selected>SELECT</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- cc-card -->

                    <div id="employer_fixed" class="cc-card large cost-forms" style="display: none">
                        <div class="cc-card-wrapper">
                            <h5>Employer Fixed</h5>
                            <p>Enter the dollar amount the employer will contribute to their employees's premium.</p>
                            <div>
                                <div class="form-row">
                                    <div class="form-group col-md-2">
                                        <p class="m-0">Employee</p>
                                        <div class="form-control-wrap">
                                            <span class="currency">$</span>
                                            <input type="text" class="form-control mr-1" placeholder="0">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2 ml-3">
                                        <p class="m-0">Dependent</p>
                                        <div class="form-control-wrap">
                                            <span class="currency">$</span>
                                            <input type="text" class="form-control mr-1" placeholder="0">
                                        </div>
                                        <small id="dependent" class="form-text text-muted">(Optional)</small>
                                    </div>
                                    <div class="form-group col-md-1 d-flex justify-content-between align-items-center">
                                        <div class="or-circle">OR</div>
                                    </div>
                                    <div class="form-group col-md-2 ml-3">
                                        <p class="m-0">Combined</p>
                                        <div class="form-control-wrap">
                                            <span class="currency">$</span>
                                            <input type="text" class="form-control mr-1" placeholder="0">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label>Show Rates</label>
                                        <select class="form-control">
                                            <option selected>SELECT</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>NUMBER OF PAYCHECKS PER YEAR</label>
                                        <select class="form-control">
                                            <option selected>SELECT</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- cc-card -->

                    <div id="employee_fixed" class="cc-card large cost-forms" style="display: none">
                        <div class="cc-card-wrapper">
                            <h5>Employee Fixed</h5>
                            <div class="row">
                                <div class="col-md-6 border-right">
                                    <p>Enter the dollar amount the employee will pay to their own premium.</p>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label class="ml-4">Employee</label>
                                            <div class="form-control-wrap">
                                                <span class="currency">$</span>
                                                <input type="text" class="form-control mr-1 ml-4" id="employee_per" aria-describedby="employeePer">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label class="ml-4">Optional</label>
                                            <div class="form-control-wrap">
                                                <span class="currency">$</span>
                                                <input type="text" class="form-control mr-1 ml-4">
                                            </div>
                                            <div class="custom-control custom-checkbox ml-4 mt-1">
                                                <input type="checkbox" class="custom-control-input" id="family">
                                                <label class="custom-control-label" for="family">Family</label>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="ml-4">Optional</label>
                                            <div class="form-control-wrap">
                                                <span class="currency">$</span>
                                                <input type="text" class="form-control mr-1 ml-4">
                                            </div>
                                            <div class="custom-control custom-checkbox ml-4 mt-1">
                                                <input type="checkbox" class="custom-control-input" id="spouse">
                                                <label class="custom-control-label" for="spouse">Spouse</label>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="ml-4">Optional</label>
                                            <div class="form-control-wrap">
                                                <span class="currency">$</span>
                                                <input type="text" class="form-control mr-1 ml-4">
                                            </div>
                                            <div class="custom-control custom-checkbox ml-4 mt-1">
                                                <input type="checkbox" class="custom-control-input" id="children">
                                                <label class="custom-control-label" for="children">Children</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <p>Apply contribution</p>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radio_hmo_epo" name="hmo_epo_ppo" class="custom-control-input">
                                        <label class="custom-control-label" for="radio_hmo_epo">HMO/EPO</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radio_ppo" name="hmo_epo_ppo" class="custom-control-input">
                                        <label class="custom-control-label" for="radio_ppo">PPO</label>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label>Show Rates</label>
                                    <select class="form-control">
                                        <option selected>SELECT</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>NUMBER OF PAYCHECKS PER YEAR</label>
                                    <select class="form-control">
                                        <option selected>SELECT</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div><!-- cc-card -->

                    <div id="percentage_of_cost" class="cc-card large cost-forms" style="display: none">
                        <div class="cc-card-wrapper">
                            <h5>Percentage of Cost</h5>
                            <div>
                                <div class="row">
                                    <div class="col-md-6 border-right">
                                        <p>Enter the percentage the employer will contribute to their employees's premium.</p>
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                                <label for="employee_per">Employee</label>
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <input type="text" class="form-control mr-1" id="employee_per" aria-describedby="employeePer">
                                                    <img src="./images/icons/p-icon.png" alt="">
                                                </div>
                                                <small id="employeePer" class="form-text text-muted">(50% min)</small>
                                            </div>
                                            <div class="form-group col-md-3 ml-3">
                                                <label for="dependent_per">Dependent</label>
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <input type="text" class="form-control mr-1" id="dependent_per" aria-describedby="dependent">
                                                    <img src="./images/icons/p-icon.png" alt="">
                                                </div>
                                                <small id="dependent" class="form-text text-muted">(Optional)</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Apply contribution</p>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radio_hmo_epo_2" name="hmo_epo_ppo_any" class="custom-control-input">
                                            <label class="custom-control-label" for="radio_hmo_epo_2">HMO/EPO</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radio_ppo_2" name="hmo_epo_ppo_any" class="custom-control-input">
                                            <label class="custom-control-label" for="radio_ppo_2">PPO</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radio_any" name="hmo_epo_ppo_any" class="custom-control-input">
                                            <label class="custom-control-label" for="radio_any">Any health plan (HMO/EPO or PPO) selected by employee</label>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label >Show Rates</label>
                                        <select  class="form-control">
                                            <option selected>SELECT</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>NUMBER OF PAYCHECKS PER YEAR</label>
                                        <select  class="form-control">
                                            <option selected>SELECT</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div><!-- cc-card -->
                </div>
            </section>
        </div>
    </section>

    <section class="cc-controls sticky">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left">
                    <a href="javascript:;" class="btn-link">Cancel</a>
                </div>
                <div class="right">
                    <a href="javascript:;" class="btn-link">Save & Exit</a>
                    <button class="btn">Next</button>
                </div>
            </div>
        </div>
        <div class="cc-controls-footer-links">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div>&copy; 2019 CaliforniaChoice | A CHOICE Administrators Program</div>
                    <div>
                        <a href="">Privacy Policy</a> | <a href="">Terms of Use</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<?php include "common/footer.php"; ?>
</body>
</html>