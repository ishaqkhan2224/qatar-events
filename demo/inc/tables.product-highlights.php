<table class="table js-sortable-table" id="cc-table-data">
    <thead>
        <tr>
            <th colspan="3" class="cc-table__product-empty"></th>
            <th colspan="5" class="cc-table__product-description"><span>Coverage</span></th>
            <th colspan="3" class="cc-table__product-empty"></th>
        </tr>
        <tr>
            <th scope="col" width="18%">Employee Name</th>
            <th scope="col" class="text-center">Age</th>
            <th scope="col" class="text-center">Gender</th>
            <th scope="col" class="cc-table__product-icon fixed-width no-sort"><i class="fal fa-briefcase-medical"></i><br>Medical</th>
            <th scope="col" class="cc-table__product-icon fixed-width no-sort"><i class="fal fa-tooth"></i><br>Dental</th>
            <th scope="col" class="cc-table__product-icon fixed-width no-sort"><i class="fal fa-glasses"></i><br>Vision</th>
            <th scope="col" class="cc-table__product-icon fixed-width not-available no-sort"><i class="fal fa-heartbeat"></i><br>Life</th>
            <th scope="col" class="cc-table__product-icon fixed-width no-sort"><i class="cc-icon-chiro"></i><br>Chiro</th>
            <th scope="col">Medical Coverage Type</th>
            <th scope="col" class="text-center">Enrolled Since</th>
            <th scope="col" class="no-sort"></th>
        </tr>
    </thead>
    <tbody class="cc-table__clickable">
    <tr>
        <td>Lisa Leslie</td>
        <td class="text-center">23</td>
        <td class="text-center">F</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">--</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data"></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td>Employee</td>
        <td class="text-center">07/23/2019</td>
        <td class="cc-table__td-action">
    <div class="dropdown cc-table__td-dropdown">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Actions
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Order ID Card</a>
        </div>
    </div>
</td>
    </tr>
    <tr>
        <td>Berlynda Armenta<i class="fas fa-exclamation-square" data-toggle="tooltip" title="Tooltip Message Goes Here"></i></td>
        <td class="text-center">60</td>
        <td class="text-center">M</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data">--</td>
        <td class="cc-table__product-data"></td>
        <td class="cc-table__product-data">--</td>
        <td>Employee & Spouse</td>
        <td class="text-center">03/01/2015</td>
        <td class="cc-table__td-action">
    <div class="dropdown cc-table__td-dropdown">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Actions
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Order ID Card</a>
        </div>
    </div>
</td>
    </tr>
    <tr>
        <td>Hansi Salvidar</td>
        <td class="text-center">23</td>
        <td class="text-center">F</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data"></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td>Employee</td>
        <td class="text-center">07/23/2019</td>
        <td class="cc-table__td-action">
    <div class="dropdown cc-table__td-dropdown">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Actions
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Order ID Card</a>
        </div>
    </div>
</td>
    </tr>
    <tr>
        <td>Maqqaux Lopez<i class="fas fa-exclamation-square" data-toggle="tooltip" title="Tooltip Message Goes Here"></i></td>
        <td class="text-center">23</td>
        <td class="text-center">F</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data"></td>
        <td class="cc-table__product-data">--</td>
        <td>Employee</td>
        <td class="text-center">07/23/2019</td>
        <td class="cc-table__td-action">
    <div class="dropdown cc-table__td-dropdown">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Actions
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Order ID Card</a>
        </div>
    </div>
</td>
    </tr>
    <tr>
        <td>Patricia Pena</td>
        <td class="text-center">23</td>
        <td class="text-center">F</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data"></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td>Employee & Spouse</td>
        <td class="text-center">07/23/2019</td>
        <td class="cc-table__td-action">
    <div class="dropdown cc-table__td-dropdown">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Actions
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Order ID Card</a>
        </div>
    </div>
</td>
    </tr>
    <tr>
        <td>Anthony Peeler<i class="fas fa-exclamation-square" data-toggle="tooltip" title="Tooltip Message Goes Here"></i></td>
        <td class="text-center">23</td>
        <td class="text-center">F</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data"></td>
        <td class="cc-table__product-data">--</td>
        <td>Employee</td>
        <td class="text-center">07/23/2019</td>
        <td class="cc-table__td-action">
    <div class="dropdown cc-table__td-dropdown">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Actions
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Order ID Card</a>
        </div>
    </div>
</td>
    </tr>
    <tr>
        <td>Linda Nguyen</td>
        <td class="text-center">23</td>
        <td class="text-center">F</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data">--</i></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="cc-table__product-data"></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td>Employee</td>
        <td class="text-center">07/23/2019</td>
        <td class="cc-table__td-action">
    <div class="dropdown cc-table__td-dropdown">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Actions
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Order ID Card</a>
        </div>
    </div>
</td>
    </tr>
    </tbody>
</table>
