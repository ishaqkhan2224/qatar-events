  <div class="cc-form__box">
  	<div class="row">
  		<div class="col-7">
		  	<div class="form-group">
		  	  <label for="business_name">Group Name</label>
		  	  <input type="email" class="form-control" id="business_name" name="business_name" required>
		  	  <small id="passwordHelpBlock" class="form-text text-muted">
		  			This is an example of a helper text, located nicely below the input field.
			  </small>
		  	</div>
		  	<div class="form-group">
		  	  <div class="form-row">
		  	      <div class="col-6">
		  	      	<label for="xxx">Zip Code</label>
		  	        <input type="text" class="form-control" placeholder="ex.">
		  	      </div>
		  	      <div class="col-6">
		  	      	<label for="xxx">County</label>
		  	        <input type="text" class="form-control" placeholder="">
		  	      </div>
		  	   </div>
		  	</div>
		  	<div class="form-group">
		  	  <div class="form-row">
		  	      <div class="col">
		  	      	<label for="xxx">SIC Code</label>
		  	        <input type="text" class="form-control" placeholder="">
		  	        <small id="passwordHelpBlock" class="form-text text-muted"><a href="">SIC Code Lookup</a></small>
		  	      </div>
		  	      <div class="col">
		  	      	<label for="xxx">Requested Effective Date</label>
				  	    <select id="" class="form-control">
				  	        <option value="10/1/2019">10/1/2019</option>
				  	        <option>...</option>
				  	    </select>
		  	      </div>
		  	   </div>
		  	</div>
		  	<div class="form-group">
		  		<div class="form-row">
		  		    <div class="col">
		  		    	<label for="xxx">Nature of Business</label>
		  		    	<select id="" class="form-control">
		  		    	   <option selected>Choose...</option>
		  		    	   <option>...</option>
		  		    	</select>
		  		    </div>
		  		 </div>
		  	</div>
		  	<div class="form-group">
		  	  <div class="form-row">
		  	      <div class="col-4">
		  	      	<label for="xxx">Number Of Employees</label>
		  	        <input type="text" class="form-control" placeholder="">
		  	        <small id="passwordHelpBlock" class="form-text text-muted">Eligible Employees</small>
		  	      </div>
		  	      <div class="col-4">
		  	      	<label>&nbsp;</label>
		  	        <input type="text" class="form-control" placeholder="">
		  	        <small id="passwordHelpBlock" class="form-text text-muted">Part-Time Employees</small>
		  	      </div>
		  	      <div class="col-4">
		  	      	<label>&nbsp;</label>
		  	        <input type="text" class="form-control" placeholder="">
		  	        <small id="passwordHelpBlock" class="form-text text-muted">Out-of-State Employees</small>
		  	      </div>
		  	   </div>
		  	</div>
  		</div>
  	</div>
  </div><!-- cc-form__box -->
