<form action="" class="datatable-search-form">
    <label for="daterange">Date Range: <i class="fa fa-calendar-alt" id="daterange"></i></label>
    <input type="text" class="form-control date-field" placeholder="MM/DD/YYYY" id="startdate"> -
    <input type="text" class="form-control date-field" placeholder="MM/DD/YYYY" id="enddate">
    <select name="" id="" class="form-control" style="width: 160px">
        <option value="">All Quotes</option>
        <option value="">All Quote That Is Very Looooong</option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
    </select>
    <select name="" id="" class="form-control">
        <option value="">All Status</option>
    </select>
    <button class="btn btn-blue">Apply</button>
</form>

<table class="table js-sortable-table" id="cc-table-data">
    <thead>
        <tr>
            <th scope="col">Group Name</th>
            <th scope="col" class="no-sort">Quote ID</th>
            <th scope="col" class="text-center no-sort">Last Modified</th>
            <th scope="col" class="no-sort">Status</th>
            <th scope="col" class="text-center">Effective Date</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Applied Business Dynamics</td>
            <td>In Progress</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Unfinished</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>Xerox Corporation</td>
            <td>In Progress</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Unfinished</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>Widget Inc.</td>
            <td>In Progress</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Enrollment</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>Boolean Corporation</td>
            <td>983627384</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Initial</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>Bob's Barbershop</td>
            <td>283749867</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Enrollment</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>Wingstop</td>
            <td>982736782</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Initial</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>Buffalo Wild Wings</td>
            <td>In Progress</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Unfinished</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>Popeye's Chicken</td>
            <td>374882947</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Enrollment</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>Macaroni Grill</td>
            <td>432344342</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Enrollment</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>McDonald's</td>
            <td>In Progress</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Initial</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>Texas Roadhouse</td>
            <td>983647588</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Initial</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>Morton's Steakhouse</td>
            <td>5433442343</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Enrollment</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>Lazy Dog</td>
            <td>In Progress</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Initial</td>
            <td class="text-center">08/01/2019</td>
        </tr>
        <tr>
            <td>Chick-fil-A</td>
            <td>982738738</td>
            <td class="text-center">07/23/2019 10:24 PM</td>
            <td>Initial</td>
            <td class="text-center">08/01/2019</td>
        </tr>
    </tbody>
</table>