<table class="table" id="cc-table-data">
    <thead>
        <tr>
            <th scope="col">Column 01</th>
            <th scope="col" class="text-center">Column 02</th>
            <th scope="col" class="text-center">Column 03</th>
            <th scope="col" class="text-center">Column 04</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="4">
                <div class="empty-state">
                    <h3>There are no records</h3>
                </div>
            </td>
        </tr>
    </tbody>
</table>
