<link rel="stylesheet" id="css-bootstrap" href="../global/css/bootstrap.4.3.1.min.css" media="all">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" media="all">
<link rel="stylesheet" id="css-core" href="./scss/demo.css" media="all">
<script src="https://kit.fontawesome.com/d760a10022.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
