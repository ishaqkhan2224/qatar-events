        <form id="cc-form__manual-search" method="post" action="" class="cc-form-inline-form">
        <div class="form-row">
            <div class="col-3">
                <div class="form-group">
                    <input type="search" class="form-control" id="business_name" name="business_name" placeholder="First Name, Last Name or SSN">
                </div>
            </div>
            <div class="col">
                <button class="btn btn-blue">Search</button>
            </div>
        </div>
        </form>

        <table class="table" id="cc-table-data">
            <thead>
                <tr>
                    <th scope="col">Column 01</th>
                    <th scope="col" class="text-center">Column 02</th>
                    <th scope="col">Column 03</th>
                    <th scope="col">Column 04</th>
                    <th scope="col" class="text-right">Column 05</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody class="cc-table__clickable">
                <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
                    <td>Widget Inc.</td>
                    <td class="text-center">07/23/2019</td>
                    <td>Initial</td>
                    <td>2938 W. Mustang Street, Suite #287</td>
                    <td class="text-right">$25.01</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Menu Option #1</a>
                                <a class="dropdown-item" href="#">Menu Option #2</a>
                                <a class="dropdown-item" href="#">Menu Option #3</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
                    <td>Widget Inc.</td>
                    <td class="text-center">07/23/2019</td>
                    <td>Completed</td>
                    <td>2938 W. Mustang Street, Suite #287</td>
                    <td class="text-right">$25.01</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Menu Option #1</a>
                                <a class="dropdown-item" href="#">Menu Option #2</a>
                                <a class="dropdown-item" href="#">Menu Option #3</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
                    <td>Widget Inc.</td>
                    <td class="text-center">07/23/2019</td>
                    <td>Initial</td>
                    <td>2938 W. Mustang Street, Suite #287</td>
                    <td class="text-right">$25.01</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Menu Option #1</a>
                                <a class="dropdown-item" href="#">Menu Option #2</a>
                                <a class="dropdown-item" href="#">Menu Option #3</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
                    <td>Widget Inc.</td>
                    <td class="text-center">07/23/2019</td>
                    <td>Completed</td>
                    <td>2938 W. Mustang Street, Suite #287</td>
                    <td class="text-right">$25.01</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Menu Option #1</a>
                                <a class="dropdown-item" href="#">Menu Option #2</a>
                                <a class="dropdown-item" href="#">Menu Option #3</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

        <hr class="break">

        <form id="cc-form__manual-search" method="post" action="" class="cc-form-inline-form">
        <div class="form-row">
            <div class="col-3">
                <div class="form-group">
                    <input type="search" class="form-control" id="business_name" name="business_name" placeholder="First Name, Last Name or SSN">
                </div>
            </div>
            <div class="col-2 text-right align-self-center">
                <div class="form-group">
                    <label for="daterange">Date Range: <i class="fa fa-calendar-alt" id="daterange"></i></label>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <input type="text" class="form-control date-field" placeholder="MM/DD/YYYY" id="startdate">
                </div>
            </div>
            -
            <div class="col">
                <div class="form-group">
                    <input type="text" class="form-control date-field" placeholder="MM/DD/YYYY" id="enddate">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <select name="" id="" class="form-control">
                        <option value="">All Quotes</option>
                        <option value="">All Quote That Is Very Looooong</option>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <select name="" id="" class="form-control">
                        <option value="">All Status</option>
                    </select>
                </div>
            </div>
            <div class="col-1">
                <button class="btn btn-blue w-100">Search</button>
            </div>
        </div>
        </form>
        <table class="table" id="cc-table-data">
            <thead>
                <tr>
                    <th scope="col">Column 01</th>
                    <th scope="col" class="text-center">Column 02</th>
                    <th scope="col">Column 03</th>
                    <th scope="col">Column 04</th>
                    <th scope="col" class="text-right">Column 05</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody class="cc-table__clickable">
                <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
                    <td>Widget Inc.</td>
                    <td class="text-center">07/23/2019</td>
                    <td>Initial</td>
                    <td>2938 W. Mustang Street, Suite #287</td>
                    <td class="text-right">$25.01</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Menu Option #1</a>
                                <a class="dropdown-item" href="#">Menu Option #2</a>
                                <a class="dropdown-item" href="#">Menu Option #3</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
                    <td>Widget Inc.</td>
                    <td class="text-center">07/23/2019</td>
                    <td>Completed</td>
                    <td>2938 W. Mustang Street, Suite #287</td>
                    <td class="text-right">$25.01</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Menu Option #1</a>
                                <a class="dropdown-item" href="#">Menu Option #2</a>
                                <a class="dropdown-item" href="#">Menu Option #3</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
                    <td>Widget Inc.</td>
                    <td class="text-center">07/23/2019</td>
                    <td>Initial</td>
                    <td>2938 W. Mustang Street, Suite #287</td>
                    <td class="text-right">$25.01</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Menu Option #1</a>
                                <a class="dropdown-item" href="#">Menu Option #2</a>
                                <a class="dropdown-item" href="#">Menu Option #3</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
                    <td>Widget Inc.</td>
                    <td class="text-center">07/23/2019</td>
                    <td>Completed</td>
                    <td>2938 W. Mustang Street, Suite #287</td>
                    <td class="text-right">$25.01</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Menu Option #1</a>
                                <a class="dropdown-item" href="#">Menu Option #2</a>
                                <a class="dropdown-item" href="#">Menu Option #3</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

        <hr class="break">

        <form id="cc-form__manual-search" method="post" action="" class="cc-form-inline-form">
        <div class="form-row">
            <div class="col-3">
                <div class="form-group">
                    <input type="search" class="form-control" id="business_name" name="business_name" placeholder="First Name, Last Name or SSN">
                </div>
            </div>
            <div class="col"></div>
            <div class="col-2">
                <div class="form-group">
                    <select name="" id="" class="form-control">
                        <option value="">All Quotes</option>
                        <option value="">All Quote That Is Very Looooong</option>
                    </select>
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <select name="" id="" class="form-control">
                        <option value="">All Status</option>
                    </select>
                </div>
            </div>
            <div class="col-1">
                <button class="btn btn-blue w-100">Search</button>
            </div>
        </div>
        </form>
        <table class="table" id="cc-table-data">
            <thead>
                <tr>
                    <th scope="col">Column 01</th>
                    <th scope="col" class="text-center">Column 02</th>
                    <th scope="col">Column 03</th>
                    <th scope="col">Column 04</th>
                    <th scope="col" class="text-right">Column 05</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody class="cc-table__clickable">
                <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
                    <td>Widget Inc.</td>
                    <td class="text-center">07/23/2019</td>
                    <td>Initial</td>
                    <td>2938 W. Mustang Street, Suite #287</td>
                    <td class="text-right">$25.01</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Menu Option #1</a>
                                <a class="dropdown-item" href="#">Menu Option #2</a>
                                <a class="dropdown-item" href="#">Menu Option #3</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
                    <td>Widget Inc.</td>
                    <td class="text-center">07/23/2019</td>
                    <td>Completed</td>
                    <td>2938 W. Mustang Street, Suite #287</td>
                    <td class="text-right">$25.01</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Menu Option #1</a>
                                <a class="dropdown-item" href="#">Menu Option #2</a>
                                <a class="dropdown-item" href="#">Menu Option #3</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
                    <td>Widget Inc.</td>
                    <td class="text-center">07/23/2019</td>
                    <td>Initial</td>
                    <td>2938 W. Mustang Street, Suite #287</td>
                    <td class="text-right">$25.01</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Menu Option #1</a>
                                <a class="dropdown-item" href="#">Menu Option #2</a>
                                <a class="dropdown-item" href="#">Menu Option #3</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
                    <td>Widget Inc.</td>
                    <td class="text-center">07/23/2019</td>
                    <td>Completed</td>
                    <td>2938 W. Mustang Street, Suite #287</td>
                    <td class="text-right">$25.01</td>
                    <td class="cc-table__td-action js-td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Menu Option #1</a>
                                <a class="dropdown-item" href="#">Menu Option #2</a>
                                <a class="dropdown-item" href="#">Menu Option #3</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>