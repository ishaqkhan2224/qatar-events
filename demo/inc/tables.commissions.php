<table class="table" id="cc-table-data__asdfasdf">
    <thead>
        <tr>
            <th class="cc-table__product-empty"></th>
            <th scope="col" colspan="6" class="cc-table__product-description cc-table__border-left"><span>Premiums</span></th>
            <th scope="col" colspan="6" class="cc-table__product-description cc-table__border-left"><span>Commissions</span></th>
        </tr>
        <tr>
            <th scope="col">Paid Month</th>
            <th class="cc-table__product-icon cc-table__border-left"><i class="fal fa-briefcase-medical"></i><br>Medical</th>
            <th class="cc-table__product-icon"><i class="fal fa-tooth"></i><br>Dental</th>
            <th class="cc-table__product-icon"><i class="fal fa-glasses"></i><br>Vision</th>
            <th class="cc-table__product-icon"><i class="fal fa-heartbeat"></i><br>Life</th>
            <th class="cc-table__product-icon"><i class="cc-icon-chiro"></i><br>Chiro</th>
            <th class="cc-table__product-icon"><i class="fas fa-usd-square"></i><br><strong>Total</strong></th>
            <th class="cc-table__product-icon cc-table__border-left"><i class="fal fa-briefcase-medical"></i><br>Medical</th>
            <th class="cc-table__product-icon"><i class="fal fa-tooth"></i><br>Dental</th>
            <th class="cc-table__product-icon"><i class="fal fa-glasses"></i><br>Vision</th>
            <th class="cc-table__product-icon"><i class="fal fa-heartbeat"></i><br>Life</th>
            <th class="cc-table__product-icon"><i class="cc-icon-chiro"></i><br>Chiro</th>
            <th class="cc-table__product-icon"><i class="fas fa-usd-square"></i><br><strong>Total</strong></th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>Aug 2019</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$343,723.00</strong></td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$874,723.00</strong></td>
    </tr>
    <tr>
        <td>Aug 2019</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
    </tr>
    <tr>
        <td>Aug 2019</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
    </tr>
    <tr>
        <td>Aug 2019</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
    </tr>
    <tr>
        <td>Aug 2019</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$23,724.25</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
    </tr>
    <tr>
        <td>Aug 2019</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
    </tr>
    <tr>
        <td>Aug 2019</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
    </tr>
    <tr>
        <td>Aug 2019</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data--standard">$724.00</td>
        <td class="cc-table__product-data"><strong>$34,723.00</strong></td>
    </tr>
    </tbody>
</table>
