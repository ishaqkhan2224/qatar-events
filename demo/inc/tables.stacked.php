<form action="" class="datatable-search-form">
    <label for="daterange">Date Range: <i class="fa fa-calendar-alt" id="daterange"></i></label>
    <input type="text" class="form-control date-field" placeholder="MM/DD/YYYY" id="startdate"> -
    <input type="text" class="form-control date-field" placeholder="MM/DD/YYYY" id="enddate">
    <select name="" id="" class="form-control" style="width: 160px">
        <option value="">All Quotes</option>
        <option value="">All Quote That Is Very Looooong</option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
    </select>
    <select name="" id="" class="form-control">
        <option value="">All Status</option>
    </select>
    <button class="btn btn-blue">Apply</button>
</form>

<table class="table js-sortable-table" id="cc-table-data">
    <thead>
        <tr>
            <th colspan="4" class="cc-table__product-empty"></th>
            <th colspan="6" class="cc-table__product-description"><span>Number of Employees in Coverage</span></th>
            <th class="cc-table__product-empty"></th>
        </tr>
        <tr>
            <th scope="col">Company</th>
            <th scope="col" class="text-center">Employee</th>
            <th scope="col">Contact</th>
            <th scope="col">Address</th>
            <th class="cc-table__product-icon fixed-width no-sort"><i class="fal fa-briefcase-medical"></i><br>Medical</th>
            <th class="cc-table__product-icon fixed-width no-sort"><i class="fal fa-tooth"></i><br>Dental</th>
            <th class="cc-table__product-icon fixed-width no-sort"><i class="fal fa-glasses"></i><br>Vision</th>
            <th class="cc-table__product-icon fixed-width no-sort"><i class="fal fa-heartbeat"></i><br>Life</th>
            <th class="cc-table__product-icon fixed-width no-sort"><i class="cc-icon-chiro"></i><br>Chiro</th>
            <th class="cc-table__product-icon fixed-width no-sort"><i class="fal fa-file-check"></i></i><br>Section 125</th>
            <th scope="col" class="text-center no-sort">Enrolled Since</th>
        </tr>
    </thead>
    <tbody class="cc-table__clickable">
    <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
        <td>Oakland Raiders Inc.</td>
        <td>Mike Jones</td>
        <td>(310) 483-4399</span></td>
        <td>3827 West Mustang Rd.<span class="cc-table__detail">Orange, CA 92839</span></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">4</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">5</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="text-center">07/23/2019</td>
    </tr>
    <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
        <td>Oakland Raiders Inc.</td>
        <td>Mike Jones</td>
        <td>(310) 483-4399</span></td>
        <td>3827 West Mustang Rd.<span class="cc-table__detail">Orange, CA 92839</span></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">4</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">5</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="text-center">07/23/2019</td>
    </tr>
    <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
        <td>Oakland Raiders Inc.</td>
        <td>Mike Jones</td>
        <td>(310) 483-4399</span></td>
        <td>3827 West Mustang Rd.<span class="cc-table__detail">Orange, CA 92839</span></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">4</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">5</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="text-center">07/23/2019</td>
    </tr>
    <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
        <td>Oakland Raiders Inc.</td>
        <td>Mike Jones</td>
        <td>(310) 483-4399</span></td>
        <td>3827 West Mustang Rd.<span class="cc-table__detail">Orange, CA 92839</span></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">4</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">5</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="text-center">07/23/2019</td>
    </tr>
    <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
        <td>Oakland Raiders Inc.</td>
        <td>Mike Jones</td>
        <td>(310) 483-4399</span></td>
        <td>3827 West Mustang Rd.<span class="cc-table__detail">Orange, CA 92839</span></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">4</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">5</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="text-center">07/23/2019</td>
    </tr>
    <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
        <td>Oakland Raiders Inc.</td>
        <td>Mike Jones</td>
        <td>(310) 483-4399</span></td>
        <td>3827 West Mustang Rd.<span class="cc-table__detail">Orange, CA 92839</span></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">4</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">5</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="text-center">07/23/2019</td>
    </tr>
    <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
        <td>Oakland Raiders Inc.</td>
        <td>Mike Jones</td>
        <td>(310) 483-4399</span></td>
        <td>3827 West Mustang Rd.<span class="cc-table__detail">Orange, CA 92839</span></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">4</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">5</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="text-center">07/23/2019</td>
    </tr>
    <tr class='cc-table__clickable-row js-clickable-row' data-href='https://www.google.com/'>
        <td>Oakland Raiders Inc.</td>
        <td>Mike Jones</td>
        <td>(310) 483-4399</span></td>
        <td>3827 West Mustang Rd.<span class="cc-table__detail">Orange, CA 92839</span></td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">4</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">5</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here">0</td>
        <td class="cc-table__product-data" data-toggle="tooltip" title="Tooltip Message Goes Here"><i class="far fa-check"></i></td>
        <td class="text-center">07/23/2019</td>
    </tr>
    </tbody>
</table>
