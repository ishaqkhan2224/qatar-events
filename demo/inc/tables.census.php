        <h6 class="mt-4">NEW RECORDS: These records from your Excel file will be added to the census. Use the dropdown to confirm your action.</h6>
        <table class="table" id="cc-table-data">
            <thead>
                <tr>
                    <th scope="col">Last Name</th>
                    <th scope="col">First Name</th>
                    <th scope="col" class="text-center">SSN</th>
                    <th scope="col" class="text-center">Gender</th>
                    <th scope="col" class="text-center">Zip Code</th>
                    <th scope="col">Relationship</th>
                    <th scope="col" class="text-center">Age or DOB</th>
                    <th scope="col" class="text-center">Cobra</th>
                    <!-- <th scope="col" class="text-center"># of Dependents&nbsp;&nbsp;<i class="fal fa-info-circle" data-toggle="tooltip" data-placement="top" title="Ages 0-14"></i></th> -->
                    <th scope="col" class="text-center"># of Dependents (Ages 0-14)</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody class="cc-table__census">
                <tr>
                    <td>Lewis</td>
                    <td>Bob</td>
                    <td class="text-center">323-432-9837</td>
                    <td class="text-center">M</td>
                    <td class="text-center">92879</td>
                    <td>Employee</td>
                    <td class="text-center">24</td>
                    <td class="text-center">Y</td>
                    <td class="text-center">0</td>
                    <td class="cc-table__td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrollment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="cc-table__census--family-employee">
                    <td>Johnson</td>
                    <td>Mariah</td>
                    <td class="text-center">293-324-2388</td>
                    <td class="text-center">F</td>
                    <td class="text-center">93655</td>
                    <td>Employee</td>
                    <td class="text-center">56</td>
                    <td class="text-center">N</td>
                    <td class="text-center">3</td>
                    <td class="cc-table__td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrollment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="cc-table__census--family-member">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="family-linked">Spouse</td>
                    <td class="text-center">42</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="cc-table__census--family-member">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="family-linked">Child</td>
                    <td class="text-center">21</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="cc-table__census--family-member">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="family-linked">Child</td>
                    <td class="text-center">14</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Cook</td>
                    <td>DeQuan</td>
                    <td class="text-center">736-435-5422</td>
                    <td class="text-center">M</td>
                    <td class="text-center">90210</td>
                    <td>Employee</td>
                    <td class="text-center">32</td>
                    <td class="text-center">N</td>
                    <td class="text-center">0</td>
                    <td class="cc-table__td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrollment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Johnson</td>
                    <td>Mariah</td>
                    <td class="text-center">293-324-2388</td>
                    <td class="text-center">F</td>
                    <td class="text-center">93655</td>
                    <td>Employee</td>
                    <td class="text-center">56</td>
                    <td class="text-center">N</td>
                    <td class="text-center">3</td>
                    <td class="cc-table__td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrollment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

        <h6 class="mt-4">UPDATE RECORDS: These records from your Excel file match records on the current census. Use the dropdown to confirm your action.</h6>
        <table class="table" id="cc-table-data">
            <thead>
                <tr>
                    <th scope="col">Last Name</th>
                    <th scope="col">First Name</th>
                    <th scope="col" class="text-center">SSN</th>
                    <th scope="col" class="text-center">Gender</th>
                    <th scope="col" class="text-center">Zip Code</th>
                    <th scope="col">Relationship</th>
                    <th scope="col" class="text-center">Age or DOB</th>
                    <th scope="col" class="text-center">Cobra</th>
                    <!-- <th scope="col" class="text-center"># of Dependents&nbsp;&nbsp;<i class="fal fa-info-circle" data-toggle="tooltip" data-placement="top" title="Ages 0-14"></i></th> -->
                    <th scope="col" class="text-center"># of Dependents (Ages 0-14)</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody class="cc-table__census">
                <tr>
                    <td>Lewis</td>
                    <td>Bob</td>
                    <td class="text-center">323-432-9837</td>
                    <td class="text-center">M</td>
                    <td class="text-center">92879</td>
                    <td>Employee</td>
                    <td class="text-center">24</td>
                    <td class="text-center">Y</td>
                    <td class="text-center">0</td>
                    <td class="cc-table__td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrollment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Johnson</td>
                    <td>Mariah</td>
                    <td class="text-center">293-324-2388</td>
                    <td class="text-center">F</td>
                    <td class="text-center">93655</td>
                    <td>Employee</td>
                    <td class="text-center">56</td>
                    <td class="text-center">N</td>
                    <td class="text-center">3</td>
                    <td class="cc-table__td-action">
                        <div class="dropdown cc-table__td-dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit Quote</a>
                                <a class="dropdown-item" href="#">Convert to Enrollment</a>
                                <a class="dropdown-item" href="#">Start Online Enrolment</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>