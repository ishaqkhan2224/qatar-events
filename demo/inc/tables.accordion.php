<table class="table" id="cc-table-data">
    <thead>
    <tr>
        <th scope="col" width="20"></th>
        <th scope="col" width="35%">Company Name</th>
        <th scope="col" width="15%">Last Modified</th>
        <th scope="col" class="text-center" width="15%">Quote Number</th>
        <th scope="col" class="text-center" width="25%">Effective Date</th>
        <th scope="col" width="10%">Type</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><a href="#" data-toggle="collapse" class="collapsed" data-target="#demo1"><i class="far"></i></a></td>
        <td>Dan's Auto</td>
        <td>09/25/2019</td>
        <td class="text-center">123456</td>
        <td class="text-center">08/01/2019</td>
        <td>Initial</td>
    </tr>
    <tr class="cc-table__gradient-row">
        <td colspan="6" class="p-0">
            <div id="demo1" class="collapse">
                <ul class="table-files-list">
                    <li class="first">
                        <p>PDF created on:</p>
                        <p><b>10/02/2019</b> @ 12:45 PM</p>
                    </li>
                    <li><a href="#"><i class="fas fa-file-pdf"></i> Initial Quote</a></li>
                    <li><a href="#"><i class="fas fa-file-pdf"></i> Medical Benefits</a></li>
                    <li><a href="#"><i class="fas fa-file-pdf"></i> Ancillary Benefits</a></li>
                    <li><a href="#"><i class="fas fa-file-pdf"></i> Dependent Rates</a></li>
                    <li><a href="#"><i class="fas fa-download"></i> Download All</a></li>
                </ul>
            </div>
        </td>
    </tr>
    <tr>
        <td><a href="#" data-toggle="collapse" class="collapsed" data-target="#demo2"><i class="far"></i></a></td>
        <td>Dan's Auto</td>
        <td>09/25/2019</td>
        <td class="text-center">123456</td>
        <td class="text-center">08/01/2019</td>
        <td>Initial</td>
    </tr>
    <tr class="cc-table__gradient-row">
        <td colspan="6" class="p-0">
            <div id="demo2" class="collapse">
                <ul class="table-files-list">
                    <li class="first">
                        <p>PDF created on</p>
                        <p><b>10/02/2019</b> @ 12:45 PM</p>
                    </li>
                    <li>
                        <a href="#"><i class="fas fa-file-pdf"></i> Initial Quote</a>
                    </li>
                    <li>
                        <a href="#"><i class="fas fa-file-pdf"></i> Medical Benefits</a>
                    </li>
                    <li>
                        <a href="#"><i class="fas fa-file-pdf"></i> Ancillary Benefits</a>
                    </li>
                    <li>
                        <a href="#"><i class="fas fa-file-pdf"></i> Dependent Rates</a>
                    </li>
                    <li>
                        <a href="#"><i class="fas fa-download"></i> Download All</a>
                    </li>
                </ul>
            </div>
        </td>
    </tr>
    <tr>
        <td><a href="#" data-toggle="collapse" class="collapsed" data-target="#demo3"><i class="far"></i></a></td>
        <td>Dan's Auto</td>
        <td>09/25/2019</td>
        <td class="text-center">123456</td>
        <td class="text-center">08/01/2019</td>
        <td>Initial</td>
    </tr>
    <tr class="cc-table__gradient-row">
        <td colspan="6" class="p-0">
            <div id="demo3" class="collapse">
                <ul class="table-files-list">
                    <li class="first">
                        <p>PDF created on</p>
                        <p><b>10/02/2019</b> @ 12:45 PM</p>
                    </li>
                    <li>
                        <a href="#"><i class="fas fa-file-pdf"></i> Initial Quote</a>
                    </li>
                    <li>
                        <a href="#"><i class="fas fa-file-pdf"></i> Medical Benefits</a>
                    </li>
                    <li>
                        <a href="#"><i class="fas fa-file-pdf"></i> Ancillary Benefits</a>
                    </li>
                    <li>
                        <a href="#"><i class="fas fa-file-pdf"></i> Dependent Rates</a>
                    </li>
                    <li>
                        <a href="#"><i class="fas fa-download"></i> Download All</a>
                    </li>
                </ul>
            </div>
        </td>
    </tr>
    </tbody>
</table>