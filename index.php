

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CC Project: UI Kit &amp; UI Prototypes</title>
    <link rel="stylesheet" id="css-bootstrap" href="/cc/design/css/lib/bootstrap.min.css" media="all">
    <link rel="stylesheet" id="css-cores" href="/cc/demo/scss/demo.css" media="all">
    <script src="https://kit.fontawesome.com/d760a10022.js"></script>
    <style>
      h3 {font-size: 1.4rem;}
      h4 {font-size: 1.2rem !important;}
      .cc-card.large {padding: 14px 24px;}
      .cc-ul {color: #959595;}
      .cc-ul span {color: #444;}
    </style>
</head>

<body>

<section>

    <div class="container">

      <div class="row justify-content-md-center">
        <div class="col-md-4 mt-3">
          <img src="global/img/calchoicelogo.png" class="img-fluid mb-4">
        </div>
      </div>

      <div class="row justify-content-center">
          <div class="col-md-4">
              <div class="cc-card large">
                <div class="row">
                  <div class="col-12">
                      <a class="btn btn-blue mt-2 mb-3 d-block" href="uikit/">Framework Documentation</a>
                      <div class="row">
                        <div class="col-6">
                          <ul class="cc-ul">
                            <li><a href="demo/page-cards">Cards</a></li>
                            <li><a href="demo/page-confirmation">Confirmation</a></li>
                            <li><a href="demo/page-forms">Forms</a></li>
                          </ul>
                        </div>
                        <div class="col-6">
                          <ul class="cc-ul">
                            <li><a href="demo/tables">Tables</a></li>
                            <li><a href="demo/page-table-datatable">Table > Datatable</a></li>
                            <li><a href="demo/page-table-forms">Table > Forms</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- cc-card -->
                <h4 class="mt-4">UI Deliverables</h4>
                <div class="cc-card large">
                  <div class="row">
                    <div class="col-12">
                        <ul class="cc-ul">
                          <li><a href="demo/sprint11">Sprint 11</a></li>
                          <li><a href="demo/sprint12">Sprint 12</a></li>
                          <!-- <li><a href="javascript:;" onClick="alert('Coming Soon')">Sprint 13</a></li> -->
                        </ul>
                      </div>
                    </div>
                </div><!-- cc-card -->

                <h4 class="mt-4">Employee</h4>
                <div class="cc-card large">
                  <div class="row">
                    <div class="col-12">
                        <strong>OLE</strong>
                        <ul class="cc-ul">
                          <li><a href="demo/ee-ole-overview">Overview</a></li>
                          <li><a href="demo/ee-ole-your-info">Your Info</a></li>
                          <li><a href="demo/ee-ole-dependents">Dependents</a> //empty state</li>
                          <li><a href="demo/ee-ole-dependents?completed">Dependents</a> //completed state</li>
                          <li><a href="demo/ee-ole-medical">Medical > Select Plan</a></li>
                          <li><a href="demo/ee-ole-medical-compare">Medical > Plan Comparison</a></li>
                          <li><a href="demo/ee-ole-medical-assign-pcp">Medical > Assign PCP</a></li>
                          <li>Dental</li>
                          <li>Chiro</li>
                          <li>Vision</li>
                          <li>Life</li>
                          <li>Section 125</li>
                          <li>Summary</li>
                        </ul>
                      </div>
                    </div>
                </div><!-- cc-card -->

                <h4 class="mt-4">Employer</h4>
                <div class="cc-card large">
                  <div class="row">
                    <div class="col-12">
                        <strong>OLE</strong>
                        <ul class="cc-ul">
                          <li><a href="demo/er-ole-overview">Overview</a></li>
                          <li><a href="demo/er-ole-application">Application</a></li>
                          <li><a href="demo/er-ole-census">Census</a></li>
                          <li><a href="demo/er-ole-metaltier">Metal Tier</a></li>
                          <li><a href="demo/er-ole-contribution">Contribution</a></li>
                          <li><a href="demo/er-ole-optional-benefits">Optional Benefits</a></li>
                          <li><a href="demo/er-ole-summary">Summary</a> (pending approval)</li>
                          <!-- <li><a href="javascript:;" onClick="alert('Coming Soon')">xxx</a></li> -->
                        </ul>
                        <hr>
                        <strong>Manage Employees</strong>
                        <ul class="cc-ul">
                          <li><span>Active Employees</span>
                            <ul>
                              <li><a href="demo/page-overview-cards-active">New Hire</a>
                              <li><a href="demo/page-overview-cards?pending=1">Pending</a></li>
                              <li><a href="demo/page-overview-cards">Denied</a></li>
                            </ul>
                          </li>
                          <li><span>Division Reports</span>
                            <ul>
                              <li><a href="demo/er-manage-create-overview">Overview</a></li>
                              <li><a href="demo/er-manage-create-divisions">Create Divisions</a></li>
                              <li><a href="demo/er-manage-division-employees">Manage Employees</a></li>
                              <li><a href="demo/er-manage-division-report-detail">Reports (Detail)</a></li>
                              <li><a href="demo/er-manage-division-report-summary">Reports (Summary)</a></li>
                              <!-- <li><a href="javascript:;" onClick="alert('Coming Soon')">xxx</a></li> -->
                            </ul>
                          </li>
                          <li><span>Renewals</span>
                            <ul>
                              <li><a href="demo/er-manage-renewals">Overview</a>
                              <li><a href="demo/er-manage-renewals-online-access">Online Access</a></li>
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div>
                </div><!-- cc-card -->

                <h4 class="mt-4">Broker</h4>
                <div class="cc-card large">
                  <div class="row">
                    <div class="col-12">
                        <strong>OLE</strong>
                        <ul class="cc-ul">
                          <li><a href="demo/br-ole-management">Management</a></li>
                          <li><span>Management > Details</span>
                            <ul>
                              <li><a href="demo/br-ole-management-details?remaining=1">Management > Details (Remaining)</a></li>
                              <li><a href="demo/br-ole-management-details">Management > Details (Days Left)</a></li>
                              <li><a href="demo/br-ole-management-details?uw=1">Management > Details (UW)</a></li>
                            </ul>
                          </li>
                          <li><a href="demo/br-ole-management-resend">Management > Details > Resend Email</a></li>
                          <li><a href="demo/br-ole-management-upload">Management > Details > Upload</a></li>
                          <!-- <li><a href="javascript:;" onClick="alert('Coming Soon')">xxx</a></li> -->
                        </ul>
                      </div>
                    </div>
                </div><!-- cc-card -->

                <h4 class="mt-4">Misc</h4>
                <div class="cc-card large">
                  <div class="row">
                    <div class="col-12">
                        <ul class="cc-ul">
                          <li><a href="demo/page-initial-quote">Quotes > Delivery</a></li>
                          <li><a href="demo/page-quotes-history">Quotes > View History</a></li>
                          <li><span>Enrollment > Delivery</span>
                            <ul>
                              <li><a href="demo/page-enrollment-quote-delivery">Life: Schedule</a></li>
                              <li><a href="demo/page-enrollment-quote-delivery?flat=1">Life: Flat</a></li>
                            </ul>
                          </li>
                          <li><a href="demo/page-enrollment-history">Enrollment > View History</a></li>
                          <li><a href="demo/page-public-layout">Public > Content Template</a></li>
                        </ul>
                      </div>
                    </div>
                </div><!-- cc-card -->

                <div class="cc-card large" style="background-color: rgba(255,255,255,0.5);">
                  <div class="row">
                    <div class="col-12">
                        <h4>Marketing Concepts</h4>
                        <ul class="cc-ul">
                          <li><a href="design/marketing-table">Table Layout</a></li>
                          <li><a href="design/marketing-process">Process Flow</a></li>
                          <li><a href="design/ui-quotes-tab">01 - Quotes</a></li>
                          <li><a href="design/ui-contribution">02 - Quote > Contribution</a></li>
                          <li><a href="design/ui-quote-entry">03 - Quote > Entry</a></li>
                          <li><a href="design/ui-confirmation">04 - Confirmation</a></li>
                          <li><a href="design/ui-dashboard">05 - Dashboard</a></li>
                          <li><a href="design/ui-active-employees">06 - Active Employees</a></li>
                        </ul>
                      </div>
                    </div>
                </div><!-- cc-card -->

                <div class="cc-card large mb-5" style="background-color: rgba(255,255,255,0.5);">
                  <div class="row">
                    <div class="col-12">
                        <h4>Archives</h4>
                        <ul class="cc-ul">
                          <li><a href="design/renewals">Renewals</a></li>
                          <li><a href="design/group-census-upload">Group Census Upload/Download</a></li>
                          <li><a href="design/commissions">Commissions</a></li>
                          <li><a href="design/initial-quote">Initial Quote</a></li>
                          <li><a href="design/enrollment-quote">Enrollment Quote</a></li>
                          <li><a href="design/contribution">Contribution</a></li>
                          <li><a href="design/optional-benefits-life">Optional Benefits (Life)</a></li>
                        </ul>
                      </div>
                    </div>
                </div><!-- cc-card -->

          </div><!-- col-md-4 -->
      </div><!-- row -->

</section>
</body>
</html>